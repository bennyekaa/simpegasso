<html>

<head>
    <title>Daftar Riwayat Hidup</title>
</head>

<body>

   <?php
        set_time_limit(1200);
        $kd_pegawai = $_REQUEST['id'];
        require_once('plugins/tcpdf/config/lang/eng.php');
        require_once('plugins/tcpdf/tcpdf.php');
        include("config.php");
        koneksi();
        
$count = 0;
function right($value, $count)
{
	return substr($value, ($count * -1));
}

function left($string, $count)
{
	return substr($string, 0, $count);
}

function datediff($d1, $d2)
{
	$d1 = (is_string($d1) ? strtotime($d1) : $d1);
	$d2 = (is_string($d2) ? strtotime($d2) : $d2);
	$diff_secs = abs($d1 - $d2);
	$base_year = min(date("Y", $d1), date("Y", $d2));
	$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
	return array('years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);
}

?>
<?php


$querylink = mysql_query("SELECT
	pegawai.kd_pegawai,
	pegawai.NIP,
	pegawai.nama_pegawai,
	pegawai.gelar_depan,
	pegawai.gelar_belakang,
	pegawai.tempat_lahir,
	kabupaten.nama_kabupaten,
	pegawai.tgl_lahir,
	pegawai.jns_kelamin,
	pegawai.kd_agama,
	pegawai.status_asal,
	pegawai.id_jns_pegawai,
	pegawai.status_pegawai,
	pegawai.status_kawin,
	pegawai.status_remun,
	pegawai.alamat,
	pegawai.RT,
	pegawai.RW,
	pegawai.kd_kelurahan,
	pegawai.kode_pos,
	pegawai.no_telp,
	pegawai.gol_darah,
	pegawai.tmt_kp,
	pegawai.tmt_kgb,
	pegawai.jamkerja_bulanan,
	pegawai.kode_unit,
	pegawai.tmt_unit_terakhir,
	pegawai.kode_unit_induk,
	pegawai.id_golpangkat_terakhir,
	pegawai.tmt_golpangkat_terakhir,
	pegawai.id_jabatan_terakhir,
	pegawai.id_jabatan_struktural,
	pegawai.id_jabatan_uj,
	pegawai.tmt_jabatan_terakhir,
	pegawai.tmt_jabatan_s_terakhir,
	pegawai.id_pendidikan_terakhir,
	pegawai.ket_pendidikan,
	pegawai.id_pasangan_pegawai,
	pegawai.no_sk_cpns,
	pegawai.tmt_cpns,
	pegawai.no_sk_pns,
	pegawai.tmt_pns,
	pegawai.no_sk_pensiun,
	pegawai.tgl_sk_pensiun,
	pegawai.npwp,
	pegawai.tgl_npwp,
	pegawai.karpeg,
	pegawai.no_ASKES,
	pegawai.taspen,
	pegawai.foto,
	pegawai.tgl_resign,
	pegawai.nip_lama,
	pegawai.loker,
	pegawai.eselon,
	pegawai.angka_kredit,
	pegawai.mk_tambahan_th,
	pegawai.mk_tambahan_bl,
	pegawai.mk_selisih_th,
	pegawai.mk_selisih_bl,
	pegawai.ket_pensiun,
	pegawai.validasi_data,
	pegawai.NIDN,
	pegawai.no_KTP,
	pegawai.hp,
	pegawai.sandi_dosen,
	pegawai.sertifikat_pendidik,
	pegawai.bidangilmu_serdos,
	pegawai.nira,
	pegawai.nia,
	pegawai.email,
	pegawai.drh_tinggi,
	pegawai.drh_bb,
	pegawai.drh_rambut,
	pegawai.drh_bentuk_muka,
	pegawai.drh_warna_kulit,
	pegawai.drh_ciri_ciri,
	pegawai.drh_cacat,
	pegawai.hobi_kesenian,
	pegawai.hobi_olahraga,
	pegawai.hobi_lain,
	pegawai.drh_nama_kecil,
	pegawai.alamat_asal,
	pegawai.kp4_11a,
	pegawai.kp4_11b,
	pegawai.kp4_11c,
	pegawai.id_prodi,
	pegawai.tahun_tunj_profesi,
	pegawai.tahun_tunj_kehormatan,
	pegawai.kode_pt,
	pegawai.nama_pt,
	pegawai.alamat_pt,
	pegawai.fakultas,
	pegawai.jurusan,
	pegawai.prodi,
	pegawai.bidang_ilmu,
	pegawai.prodi_s2,
	pegawai.prodi_s3,
	pegawai.matkul_pokok,
	pegawai.status_tugas,
	pegawai.sertifikasi_laboran,
	pegawai.pp_nip,
	pegawai.app_nip,
	pegawai.aktif,
	pegawai.email_resmi,
	pegawai.google_scholar,
	pegawai.research_gate,
	pegawai.wkt_update_pegawai,
	pegawai.update_oleh_pegawai,
	pegawai.user_id,
	pegawai.rekening,
	pegawai.status_aktif,
	kelurahan.nama_kelurahan,
	kecamatan.nama_kecamatan,
	kabupaten.nama_kabupaten,
	kabupaten.nama_propinsi,
	agama.agama,
	jenis_pegawai.jenis_pegawai,
	unit_kerja.nama_unit,
	golongan_pangkat.golongan,
	golongan_pangkat.pangkat,
	golongan_pangkat.tingkat
FROM
	pegawai
LEFT JOIN agama ON agama.kd_agama = pegawai.kd_agama
LEFT JOIN jenis_pegawai ON jenis_pegawai.id_jns_pegawai = pegawai.id_jns_pegawai
LEFT JOIN unit_kerja ON unit_kerja.kode_unit = pegawai.kode_unit
LEFT JOIN golongan_pangkat ON golongan_pangkat.id_golpangkat = pegawai.id_golpangkat_terakhir
LEFT JOIN kelurahan ON kelurahan.kd_kelurahan = pegawai.kd_kelurahan
LEFT JOIN kecamatan ON kelurahan.kd_kecamatan = kecamatan.kd_kecamatan
LEFT JOIN kabupaten ON kecamatan.kd_kabupaten = kabupaten.kd_kabupaten
WHERE
	nip NOT LIKE ''
AND nip NOT LIKE '-'
AND kd_pegawai = '" . $kd_pegawai . "'
ORDER BY
	pegawai.id_jns_pegawai ASC,
	pegawai.id_golpangkat_terakhir DESC,
	pegawai.NIP");
//$querylink = mysql_query("select * from pegawai where kd_pegawai='" . $kd_pegawai . "'");
if ($querylink) {
	while ($daftar = mysql_fetch_array($querylink)) {

		$kd_pegawai = $daftar['kd_pegawai'];
		$nama_pegawai_saja = $daftar['nama_pegawai'];
		$nama_pegawai = $daftar['gelar_depan'] . " " . strtolower($daftar['nama_pegawai']) . " " . $daftar['gelar_belakang'];
		$NIP = $daftar['NIP'];
		$no_KTP = $daftar['no_KTP'];
		$nama_peg = $daftar['nama_pegawai'];
		$tempat = $daftar['tempat_lahir'];
		$namalahir = $daftar['nama_kabupaten'];
		$tanggal = $daftar['tgl_lahir'];
		$jns_kelamin = $daftar['jns_kelamin'] != 1 ? "Perempuan" : "Laki-Laki";
		$alamat = $daftar['alamat'];
		$alamat_asal = $daftar['alamat_asal'];
		$kelurahan = $daftar['nama_kelurahan'];
		$kecamatan = $daftar['nama_kecamatan'];
		$kabupaten = $daftar['nama_kabupaten'];
		$provinsi = $daftar['nama_propinsi'];
		$hp = $daftar['hp'];
		$email = $daftar['email'];
		$npwp = $daftar['npwp'];
		$no_ASKES = $daftar['no_ASKES'];
		$taspen = $daftar['taspen'];
		$karpeg = $daftar['karpeg'];
		$loker = $daftar['loker'];
		$kode_pos = $daftar['kode_pos'];
		$kd_agama = $daftar['kd_agama'];
		$agama = $daftar['agama'];
		$kd_kelurahan = $daftar['kd_kelurahan'];
		$pangkat = "";
		$golongan = "";
		$pangkat = !isset($daftar['pangkat']) ?  "-" :  $daftar['pangkat'];
		$golongan = !isset($daftar['golongan']) ?  "-" :  $daftar['golongan'];
		$rt = "";
		$rw = "";
		if ($daftar['RT'] != '')
			$rt = $daftar['RT'];
		if ($daftar['RW'] != '')
			$rw = $daftar['RW'];
		$gol_darah = $daftar['gol_darah'];
		$id_pendidikan_terakhir = $daftar['id_pendidikan_terakhir'];
		$drh_tinggi = ltrim($daftar['drh_tinggi']);
		$drh_bb = ltrim($daftar['drh_bb']);
		$drh_rambut = ltrim($daftar['drh_rambut']);
		$drh_bentuk_muka = ltrim($daftar['drh_bentuk_muka']);
		$drh_warna_kulit = ltrim($daftar['drh_warna_kulit']);
		$drh_ciri_ciri = ltrim($daftar['drh_ciri_ciri']);
		$drh_cacat = $daftar['drh_cacat'];
		$hobi_kesenian = ltrim($daftar['hobi_kesenian']);
		$hobi_olahraga = ltrim($daftar['hobi_olahraga']);
		$hobi_lain = ltrim($daftar['hobi_lain']);
		$drh_nama_kecil = $daftar['drh_nama_kecil'];
		if ($daftar['jns_kelamin'] == 1)
			$daftar['jenis'] = 'Laki-Laki';
		else
			$daftar['jenis'] = 'Perempuan';

		$kd_agama = $daftar['kd_agama'];
		$status_kawin = $daftar['status_kawin'];
		$pernikahan = $daftar['status_kawin'] != 1 ? "Belum Kawin" : "Kawin";
		//$stkawin = 'Kawin';



		$queryjenis = mysql_query("select * from jenis_pegawai where id_jns_pegawai='" . $daftar['id_jns_pegawai'] . "'");
		if ($queryjenis) {
			while ($daftarjenis = mysql_fetch_array($queryjenis)) {
				$id_jns_pegawai = $daftarjenis['id_jns_pegawai'];
				$jenis_pegawai = $daftarjenis['jenis_pegawai'];
			}
		}


		//			$jenis_pegawai=$daftar['id_jns_pegawai'];



		//jenis_pegawai
		if ($daftar['status_pegawai'] == 1)
			$status_pegawai = 'CPNS';
		elseif ($daftar['status_pegawai'] == 2)
			$status_pegawai = 'PNS';
		elseif ($daftar['status_pegawai'] == 3)
			$status_pegawai = 'Kontrak';
		elseif ($daftar['status_pegawai'] == 4)
			$status_pegawai = 'Surat Tugas';
		else
			$status_pegawai = "SK";

		$id_golpangkat_terakhir = $daftar['id_golpangkat_terakhir'];
		//$pangkat;
		//$golongan



		$id_jabatan_terakhir = $daftar['id_jabatan_terakhir'];
		$querylink = mysql_query("select * from jabatan where id_jabatan='" . $daftar['id_jabatan_terakhir'] . "'");
		if ($querylink) {
			while ($daftarjabatan = mysql_fetch_array($querylink)) {
				$jabatan = $daftarjabatan['nama_jabatan'];
				$queryJBT = mysql_query("select riwayat_jabatan.*,unit_kerja.nama_unit from riwayat_jabatan,unit_kerja 
				where kd_pegawai='" . $kd_pegawai . "' and id_jabatan='" . $daftar['id_jabatan_terakhir'] . "' 
				and aktif='1' and riwayat_jabatan.kode_unit=unit_kerja.kode_unit");
				if ($queryJBT) {
					while ($daftarDETjabatan = mysql_fetch_array($queryJBT)) {
						$tmtjabatan = $daftarDETjabatan['tmt_jabatan'];
						$unitjabatan = $daftarDETjabatan['nama_unit'];
					}
				}
			}
		}

		$kode_unit = $daftar['kode_unit'];
		//$unit_kerja
		$tgl_lahir = $daftar['tgl_lahir'];
		//$usia = $this->datediff($daftar['tgl_lahir'],$tgl);
		//$umur = $usia['years']." tahun ".$usia['months']." bulan";
		$tmt_cpns = $daftar['tmt_cpns'];

		$tgl = date('Y-m-d');
		$masa_kerja = $daftar['tmt_cpns'];
		$masa_kerja = datediff($daftar['tmt_cpns'], $tgl);
		$mk_tambahan_th = $daftar['mk_tambahan_th'];
		$mk_tambahan_bl = $daftar['mk_tambahan_bl'];
		$masa_kerja_seluruhnya_th = $masa_kerja['years'] + $mk_tambahan_th;
		$masa_kerja_seluruhnya_bl = $masa_kerja['months'] + $mk_tambahan_bl;
		$masa_kerja_seluruhnya = $masa_kerja_seluruhnya_th . " tahun " . $masa_kerja_seluruhnya_bl . " bulan";

		$photo = 'https://simpega.um.ac.id/admin/public/photo/photo_' . $kd_pegawai . '.jpg';
	}


?>


<form name="pegawaidetails" id="pegawaidetails" method="POST" action="">
	<table cellpadding="1" cellspacing="0" width="100%">
		<tr>
			<h4>I. KETERANGAN PERORANGAN</h4>
		</tr>
		<tr width="100" rowspan="5"><img src="<?php echo $photo; ?>" align="right" border="1" height="150" width="120"></tr>

		<td align="right">&nbsp;</td>
		
		<td></td>
	</table>
	<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1">

		<tr>
			<td width="25" align="left">1. </td>
			<td align="left" width="1000" colspan="2">Nama Lengkap</td>
			<td align="left" width="2000"><?php echo $nama_pegawai_saja ?></td>
		</tr>

		<tr>
			<td width="25" align="left">2. </td>
			<td width="1000" align="left" colspan="2">NIP</td>
			<td align="left" width="2000"><?php echo $NIP ?></td>
		</tr>

		<tr>
			<td width="25" align="left">3. </td>
			<td width="1000" align="left" colspan="2">Pangkat dan Golongan Ruang</td>
			<td width="2000" align="left"><?php echo $pangkat . ' - ' . $golongan ?></td>
		</tr>

		<tr>
			<td width="25" align="left">4. </td>
			<td width="1000" align="left" colspan="2">Tempat Lahir/Tgl. Lahir</td>
			<td width="2000" align="left"><?php echo $namalahir . ',' . date("d-m-Y", strtotime($tgl_lahir)); ?></td>
		</tr>

		<tr>
			<td width="25" align="left">5. </td>
			<td width="1000" align="left" colspan="2">Jenis Kelamin</td>
			<td width="2000" align="left"><?php echo $jns_kelamin ?></td>
		</tr>

		<tr>
			<td width="25" align="left">6. </td>
			<td width="1000" align="left" colspan="2">Agama</td>
			<td width="2000" align="left"><?php echo $agama ?></td>
		</tr>

		<tr>
			<td width="25" align="left">7. </td>
			<td width="1000" align="left" colspan="2">Status Perkawinan</td>
			<td width="2000" align="left"><?php echo $pernikahan ?></td>
		</tr>

		<tr>
			<td width="25" align="left">8. </td>
			<td width="1000" rowspan="5" align="middle">Alamat Rumah</td>
			<td align="left">a. Jalan</td>
			<td width="2000" align="left"><?php echo $alamat ?></td>
		</tr>

		<tr>
			<td width="25" align="left">9. </td>
			<td width="1000" align="left">b. Kelurahan/Desa</td>
			<td width="2000" align="left"><?php echo $kelurahan ?></td>
		</tr>

		<tr>
			<td width="25" align="left">10. </td>
			<td width="1000" align="left">c. Kecamatan</td>
			<td width="2000" align="left"><?php echo $kecamatan ?></td>
		</tr>

		<tr>
			<td width="25" align="left">11. </td>
			<td width="1000" align="left">d. Kabupaten/Kota</td>
			<td width="2000" align="left"><?php echo $kabupaten ?></td>
		</tr>

		<tr>
			<td width="25" align="left">12. </td>
			<td width="1000" align="left">e. Provinsi</td>
			<td width="2000" align="left"><?php echo $provinsi ?></td>
		</tr>

		<tr>
			<td width="25" align="left">13. </td>
			<td width="1000" rowspan="7" align="middle">Keterangan Badan</td>
			<td width="1000" align="left">a. Tinggi (cm)</td>
			<td width="1000" align="left"><?php echo $drh_tinggi ?></td>
		</tr>

		<tr>
			<td width="25" align="left">14. </td>
			<td width="1000" align="left">b. Berat Badan (kg)</td>
			<td width="1000" align="left"><?php echo $drh_bb ?></td>
		</tr>

		<tr>
			<td width="25" align="left">15. </td>
			<td width="1000" align="left">c. Rambut</td>
			<td width="1000" align="left"><?php echo $drh_rambut ?></td>
		</tr>

		<tr>
			<td width="25" align="left">16. </td>
			<td width="1000" align="left">d. Bentuk Muka</td>
			<td width="1000" align="left"><?php echo $drh_bentuk_muka ?></td>
		</tr>

		<tr>
			<td width="25" align="left">17. </td>
			<td width="1000" align="left">e. Warna Kulit</td>
			<td width="1000" align="left"><?php echo $drh_warna_kulit ?></td>
		</tr>

		<tr>
			<td width="25" align="left">18. </td>
			<td width="1000" align="left">f. Ciri-ciri Khas</td>
			<td width="1000" align="left"><?php echo $drh_ciri_ciri ?></td>
		</tr>

		<tr>
			<td width="25" align="left">19. </td>
			<td width="1000" align="left">g, Cacat Tubuh</td>
			<td width="1000" align="left"><?php echo $drh_cacat ?></td>
		</tr>

		<tr>
			<td width="25" rowspan="5" align="left">20. </td>
			<td width="1000" rowspan="5" colspan="2" align="middle">Kegemaran(Hobby)</td>
			<td width="1000" align="left">&nbsp;</td>
		</tr>

		<!--hobby kesenian  -->
		<tr>
			<td width="2000" align="left"><?php if (empty($hobi_kesenian)) {
												echo '-';
											} else {
												echo $hobi_kesenian;
											} ?></td>
		</tr>

		<!-- hobby olahraga -->
		<tr>
			<td width="2000" align="left"><?php if (empty($hobi_olahraga)) {
												echo '-';
											} else {
												echo $hobi_olahraga;
											} ?></td>
		</tr>

		<!-- hobby lain -->
		<tr>
			<td width="2000" align="left"><?php if (empty($hobi_lain)) {
												echo '-';
											} else {
												echo $hobi_lain;
											} ?></td>
		</tr>

		<tr>
			<td width="2000" align="left">&nbsp;</td>
		</tr>

	</table>
	<br/>


	<table class="bentuktulisan">
		<tr>
			<h4>II. Pendidikan</h4>
			<td>1. Pendidikan di Dalam dan di Luar Negeri</td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center">No</td>
				<th class="trhead" width="90" align="center">Tingkat</td>
				<th class="trhead" width="300" align="center">Nama Pendidikan</td>
				<th class="trhead" width="130" align="center">Jurusan</td>
				<th class="trhead" width="79" align="center">STTB/Tanda Lulus/Ijasah Tahun</td>
				<th class="trhead" width="98" align="center">Tempat</td>
				<th class="trhead" width="150" align="center">Nama Kepala Sekolah/Direktur/ Dekan/Promotor</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
				<td align="center">6</td>
				<td align="center">7</td>
			</tr>
			<?
	$i = 0;
	$query = mysql_query("select * from riwayat_pendidikan where kd_pegawai='".$kd_pegawai."' order by th_lulus");	
			while ($pendidikan=mysql_fetch_array($query)) { 
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	   $querydidik = mysql_query("select * from pendidikan where id_pendidikan='".$pendidikan['id_pendidikan']."'");					
		if ($querydidik) {
			$datadidik=mysql_fetch_array($querydidik); 
			$didik = $datadidik['nama_pendidikan'];
		}
	?>
			<tr>
				<td align="center" class="colEvn"><?= $i; ?></td>
				<td align="left"><?= $didik; ?></td>
				<td align="left"><?= $pendidikan['universitas']; ?></td>
				<td align="left"><?= $pendidikan['prodi']; ?></td>
				<td align="center">
					<?php
					if ($pendidikan['tanggal_ijazah'] == "0000-00-00") {
						echo "-";
					} else {
						echo date('Y', strtotime($pendidikan['tanggal_ijazah']));
					}
					?></td>
				<td align="left"><?= $pendidikan['kota']; ?></td>
				<td align="left"><?= $pendidikan['nama_kepalasekolah']; ?></td>
			</tr>
			<? }?>
		</table>
	</table>
	<br/>
	<table class="bentuktulisan">
		<tr>
			<br>
			<td>2. Kursus/Latihan di Dalam dan di Luar Negeri</td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center">No</td>
				<th class="trhead" width="90" align="center">Nama/Kursus/Latihan</td>
				<th class="trhead" width="300" align="center">Lamanya Tgl/Bln/Thn <br>s/d Tgl/Bln/Thn</td>
				<th class="trhead" width="100" align="center">Ijasah/Tanda Lulus/Surat Keterangan Tahun</td>
				<th class="trhead" width="79" align="center">Tempat</td>
				<th class="trhead" width="98" align="center">Keterangan</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
				<td align="center">6</td>
			</tr>
			<?php
				$i = 0;
				$query = mysql_query("select * from riwayat_pelatihan where kd_pegawai='".$kd_pegawai."' order by id_riwayat_pelatihan");	
						while ($pelatihans=mysql_fetch_array($query)) {
						$i++;
						if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
				
				if(empty($pelatihans)){ ?>
					<tr>
						<td align="left">&nbsp;</td>
						<td align="left">&nbsp;</td>
						<td align="left">&nbsp;</td>
						<td align="left">&nbsp;</td>
						<td align="left">&nbsp;</td>
						<td align="left">&nbsp;</td>
					</tr>
				<?php } else { ?>
			<tr>
				<td align="left"><?php echo $i;?></td>
				<td align="left"><?php echo $pelatihans['nama_pelatihan']; ?></td>
				<td align="left"><?php echo date('d M Y', strtotime($pelatihans['tgl_mulai'])).' - ' . date('d M Y', strtotime($pelatihans['tgl_selesai']));  ?></td>
				<td align="left"><?php echo $pelatihans['nomor_sertifikat']; ?></td>
				<td align="left"><?php echo $pelatihans['tempat_pelatihan']; ?></td>
				<td align="left"><?php echo $pelatihans['keterangan']; ?></td>
			</tr>
			<?php } }?>
		</table>
				<br/>
	</table>
	<table class="bentuktulisan">
		<tr>
			<h4>III. Riwayat Pekerjaan</h4>
			<td>1. Riwayat kepangkatan golongan ruang penggajian</td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center" rowspan="2">No</th>
				<th class="trhead" width="150" align="center" rowspan="2">Pangkat</th>
				<th class="trhead" width="100" align="center" rowspan="2">Gol. ruang Penggajian</th>
				<th class="trhead" width="100" align="center" rowspan="2">Berlaku terhitung mulai tgl.</th>
				<th class="trhead" width="79" align="center" rowspan="2">Gaji Pokok</th>
				<th class="trhead" width="98" align="center" colspan="3">Surat Keputusan</th>
				<th class="trhead" width="98" align="center" rowspan="2">Peraturan yang dijadikan dasar</th>
			</tr>
			<tr>
				<th>Pejabat</td>
				<th>Nomor</td>
				<th>Tanggal</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
				<td align="center">6</td>
				<td align="center">7</td>
				<td align="center">8</td>
				<td align="center" ">9</td>
		  </tr>
		  <?
	$i = 0;
	$query = mysql_query("select * from riwayat_gol_kepangkatan where kd_pegawai='".$kd_pegawai."' AND keterangan NOT LIKE 'KGB' order by aktif ,status_pegawai, id_golpangkat, tmt_pangkat");	
			while ($jns_pangkat=mysql_fetch_array($query)) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
        if ($jns_pangkat['tgl_SK_pangkat']=='0000-00-00') {
                $str_sk_pangkat = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_pangkat['tgl_SK_pangkat']);
                $str_sk_pangkat = $d . "-" .$m . "-" . $y;
            }
        $jns_pangkat['tgl_SK_pangkat']=$str_sk_pangkat;
        if ($jns_pangkat['tmt_pangkat']=='0000-00-00') {
                $str_tmt_pangkat = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_pangkat['tmt_pangkat']);
                $str_tmt_pangkat = $d . "-" .$m. "-" . $y;
            }
        $jns_pangkat['tmt_pangkat']=$str_tmt_pangkat;
		$querygol = mysql_query("select * from golongan_pangkat where id_golpangkat='".$jns_pangkat['id_golpangkat']."'");					
		if ($querygol) {
			$datagol=mysql_fetch_array($querygol); 
			$pangkat = $datagol['pangkat'];
			$golongan = $datagol['golongan'];
		}
		
	?>
		  <tr>
				<td><?php echo $i;?></td>
				<td><?php echo $pangkat; ?></td>
				<td><?php echo $golongan; ?></td>
				<td><?php echo $jns_pangkat['tmt_pangkat'];?></td>
				<td><?php echo number_format($jns_pangkat['gaji_pokok']); ?></td>
				<td><?php echo $jns_pangkat['pejabat'];?></td>
				<td><?php echo $jns_pangkat['no_SK_pangkat']; ?></td>
				<td><?php echo $jns_pangkat['tgl_SK_pangkat']; ?></td>
				<td><?php echo $jns_pangkat['keterangan']; ?>
			</tr>
	<?php }?>
		</table>
	</table>
	<table class="bentuktulisan">
		<tr>
			<br>
			<td>2. Pengalaman Jabatan Fungsional Tertentu</td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center" rowspan="2">No</th>
				<th class="trhead" width="150" align="center" rowspan="2">Jabatan</th>
				<th class="trhead" width="100" align="center" rowspan="2">Tanggal SK</th>
				<th class="trhead" width="100" align="center" rowspan="2">Golongan Ruang Penggajian</th>
				<th class="trhead" width="79" align="center" rowspan="2">Gaji Pokok</th>
				<th class="trhead" width="98" align="center" colspan="3">Surat Keputusan</th>
			</tr>
			<tr>
				<th>Pejabat</td>
				<th>Nomor</td>
				<th>Tanggal</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
				<td align="center">6</td>
				<td align="center">7</td>
				<td align="center">8</td>
			</tr>
			<?
	$i = 0;
	$queryjabatan = mysql_query("select * from riwayat_jabatan where kd_pegawai='".$kd_pegawai."' order by id_jabatan,tmt_jabatan,id_riwayat_jabatan");	
	while ($jns_jabatan=mysql_fetch_array($queryjabatan)) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
       if ($jns_jabatan['tgl_sk_jabatan']=='0000-00-00') {
                $str_sk_jabatan = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_jabatan['tgl_sk_jabatan']);
                $str_sk_jabatan = $d . "/" . $m . "/" . $y;
            }
        $jns_jabatan['tgl_sk_jabatan']=$str_tmt_jabatan;
		
		if ($jns_jabatan['tmt_jabatan']=='0000-00-00') {
                $str_tmt_jabatan = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_jabatan['tmt_jabatan']);
                $str_tmt_jabatan = $d . "/" . $m . "/" . $y;
            }
        $jns_jabatan['tmt_jabatan']=$str_tmt_jabatan;
		
        if ($jns_jabatan['tgl_selesai']=='0000-00-00') {
                $str_tgl_selesai = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_jabatan['tgl_selesai']);
                $str_tgl_selesai = $d . "/" . $m . "/" . $y;
            }
		$jns_jabatan['tgl_selesai']=$str_tgl_selesai;
		$querygol = mysql_query("select * from golongan_pangkat where id_golpangkat='".$jns_jabatan['id_golpangkat']."'");					
		if ($querygol) {
			$datagol=mysql_fetch_array($querygol); 
			$pangkat = $datagol['pangkat'];
			$golongan = $datagol['golongan'];
		}
		
	?>
			<tr>
				<td><?php $i; ?></td>
				<?php
				  $query = mysql_query("select * from jabatan where id_jabatan='".$jns_jabatan['id_jabatan']."'");					
					if ($query) {
						$datakab=mysql_fetch_array($query); 
						$nama_jabatan = $datakab['nama_jabatan'];
					}
			 ?>
				<td><?php $nama_jabatan; ?></td>
				<td><?php $jns_jabatan['tgl_sk_jabatan']; ?></td>
				<td><?php $golongan; ?></td>
				<td><?php number_format($jns_jabatan['gaji_pokok']); ?></td>
				<td><?php $jns_jabatan['pejabat']; ?></td>
				<td><?php $jns_jabatan['no_sk_jabatan'];?></td>
				<td><?php $jns_jabatan['tmt_jabatan'];?></td>
			</tr>
		<?php } ?>
		</table>
	</table>
	<table class="bentuktulisan">
		<tr>
			<h4>IV. TANDA JASA / PENGHARGAAN</h4>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center">No</td>
				<th class="trhead" width="300" align="center">Nama Bintang/Satya Lencana/ <br>Penghargaan</td>
				<th class="trhead" width="90" align="center">Tahun Perolehan</td>
				<th class="trhead" width="150" align="center">Nama Negara/Instansi yang memberi</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
			</tr>
			<?
				$i = 0;
				$querypenghargaan = mysql_query("select * from riwayat_penghargaan where kd_pegawai='".$kd_pegawai."' order by id_riwayat_penghargaan");	
				while ($penghargaan=mysql_fetch_array($querypenghargaan)) {
				$i++;
				if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			?>
			<tr>
				<td align="center"><?php echo $i; ?></td>
				<?php
					  $query = mysql_query("select * from jenis_penghargaan where id_jns_penghargaan='".$penghargaan['jenis_penghargaan']."'");					
						if ($query) {
							$dataunit=mysql_fetch_array($query); 
							$nama_penghargaan = $dataunit['nama_penghargaan'];
						}
				 ?>
				<td align="left"><?php echo $nama_penghargaan; ?></td>
				<td align="left"><?php echo $penghargaan['tahun']; ?></td>
				<td align="left"><?php echo $penghargaan['dari_instansi']; ?></td>
			</tr>
				<?php } ?>
		</table>
	</table>

	<table class="bentuktulisan">
		<tr>
			<h4>V. PENGALAMAN</h4>
			<td>1. Kunjungan ke Luar Negeri</td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center">No</td>
				<th class="trhead" width="150" align="center">Negara</td>
				<th class="trhead" width="150" align="center">Tujuan Kunjungan</td>
				<th class="trhead" width="50" align="center">Lamanya</td>
				<th class="trhead" width="150" align="center">Yang membiayai</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
			</tr>
			<?
				$i = 0;
				$querypengalaman = mysql_query("select * from riwayat_pengalaman where kd_pegawai='".$kd_pegawai."' order by kd_pengalaman");	
				while ($pengalaman=mysql_fetch_array($querypengalaman)) {
				$i++;
				if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			?>

			<tr>
				<td align="center"><?php $i; ?></td>
				<td align="left"><?php $pengalaman['negara']; ?></td>
				<td align="left"><?php $pengalaman['tujuan_kunjungan']; ?></td>
				<td align="left"><?php $pengalaman['lamanya']; ?></td>
				<td align="left"><?php $pengalaman['yang_membiayai']; ?></td>
			</tr>
				<?php } ?>
		</table>
	</table>

	<table class="bentuktulisan">
		<tr>
			<h4>VI. KETERANGAN KELUARGA</h4>
			<td>1. Isteri/Suami </td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center">No</td>
				<th class="trhead" width="150" align="center">Nama</td>
				<th class="trhead" width="150" align="center">Tempat Lahir</td>
				<th class="trhead" width="70" align="center">Tanggal Lahir</td>
				<th class="trhead" width="70" align="center">Tanggal Nikah</td>
				<th class="trhead" width="150" align="center">Pekerjaan</td>
				<th class="trhead" width="150" align="center">Keterangan</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
				<td align="center">6</td>
				<td align="center">7</td>
			</tr>
			<?
			$i = 0;
			$query = mysql_query("SELECT
	*
FROM
	(
		SELECT
			pegawai.kd_pegawai,
			pegawai.NIP,
			pegawai.nama_pegawai,
			pegawai.tempat_lahir,
			pegawai.tgl_lahir,
			pegawai.status_kawin
		FROM
			pegawai
	) pegawai
LEFT JOIN (
	SELECT
		pasangan_pegawai.nama_pasangan,
		pasangan_pegawai.pekerjaan pekerjaanistri,
		pasangan_pegawai.tempat_lahir tempat_lahir_pasangan,
		pasangan_pegawai.tanggal_lahir tgl_lahir_pasangan,
		pasangan_pegawai.tanggal_nikah,
		pasangan_pegawai.no_karis_karsu,
		pasangan_pegawai.tgl_karis_karsu,
		pasangan_pegawai.keterangan keteranganistri,
		pasangan_pegawai.aktif,
		pasangan_pegawai.kd_pegawai,
		pasangan_pegawai.kd_status_keluarga,
		status_keluarga.status_keluarga statuspasangan
	FROM
		pasangan_pegawai,
		status_keluarga
	WHERE
		pasangan_pegawai.kd_status_keluarga = status_keluarga.kd_status_keluarga
	and pasangan_pegawai.aktif = '1'
) AS pasangan_keluarga ON pasangan_keluarga.kd_pegawai = pegawai.kd_pegawai
LEFT JOIN (
	SELECT
		anak_pegawai.kd_pegawai,
		anak_pegawai.nama_anak,
		anak_pegawai.tgl_lahir tgl_lahir_anak,
		anak_pegawai.tempat_lahir tempat_lahir_anak,
		anak_pegawai.jns_kelamin,
		anak_pegawai.pekerjaan,
		anak_pegawai.urut,
		status_keluarga.status_keluarga statusanak
	FROM
		anak_pegawai,
		status_keluarga
	WHERE
		anak_pegawai.kd_status_keluarga = status_keluarga.kd_status_keluarga
) AS anak_pegawai ON anak_pegawai.kd_pegawai = pegawai.kd_pegawai
LEFT JOIN (
	SELECT
		riwayat_keluarga.nama_keluarga,
		riwayat_keluarga.tempat_lahir tempat_lahir_keluarga,
		riwayat_keluarga.tgl_lahir tgl_lahir_keluarga,
		riwayat_keluarga.jns_kelamin,
		riwayat_keluarga.pekerjaan,
		riwayat_keluarga.keterangan,
		status_keluarga.status_keluarga statuskeluarga,
		riwayat_keluarga.kd_pegawai
	FROM
		riwayat_keluarga,
		status_keluarga
	WHERE
		riwayat_keluarga.kd_status_keluarga = status_keluarga.kd_status_keluarga
) AS pegawai_keluarga ON pegawai_keluarga.kd_pegawai = pegawai.kd_pegawai
WHERE pegawai.kd_pegawai = '".$kd_pegawai."' GROUP BY pegawai.kd_pegawai");	
			while ($list_pasangan=mysql_fetch_array($query)) { 
	   		$i++;
	   		if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			if ($list_pasangan['tgl_lahir_pasangan']=='0000-00-00') {
                $str_tanggal_lahir = '-';
            }else if(empty($list_pasangan['tgl_lahir_pasangan'])){
				$str_tanggal_lahir = '-';
			}
            else {
                $str_tanggal_lahir = date('d-M-Y', strtotime($list_pasangan['tgl_lahir_pasangan']));
            }
            $list_pasangan['tgl_lahir_pasangan']=$str_tanggal_lahir;
			
			if ($list_pasangan['tanggal_nikah']=='0000-00-00') {
                $str_tanggal_nikah = '-';
            }else if (empty($list_pasangan['tanggal_nikah'])){
				$str_tanggal_nikah = '-';
			}
            else {
                $str_tanggal_nikah = date('d-M-Y', strtotime($list_pasangan['tanggal_nikah']));
            }
            $list_pasangan['tanggal_nikah']=$str_tanggal_nikah;
		 ?>
			<tr>
				<td align="center"><?php echo $i; ?></td>
				<td align="left"><?php echo empty($list_pasangan['nama_pasangan']) ? "-" : $list_pasangan['nama_pasangan'];  ?></td>
				<td align="left"><?php echo empty($list_pasangan['tempat_lahir_pasangan'])? "-" : $list_pasangan['tempat_lahir_pasangan'];?></td>
				<td align="left"><?php echo $list_pasangan['tgl_lahir_pasangan'];?></td>
				<td align="left"><?php echo $list_pasangan['tanggal_nikah'];?></td>
				<td align="left"><?php echo empty($list_pasangan['pekerjaanistri'])? "-" : $list_pasangan['pekerjaanistri'];?></td>
				<td align="left"><?php echo empty($list_pasangan['keteranganistri'])? "-" : $list_pasangan['keteranganistri']; ?></td>
			</tr>
		<?php }?>
		</table>
	</table>
	<br>
	<table class="bentuktulisan">
		<tr>
			<td>2. Anak</td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center">No</td>
				<th class="trhead" width="150" align="center">Nama</td>
				<th class="trhead" width="150" align="center">Jenis Kelamin</td>
				<th class="trhead" width="70" align="center">Tempat Lahir</td>
				<th class="trhead" width="70" align="center">Tanggal Lahir</td>
				<th class="trhead" width="150" align="center">Pekerjaan</td>
				<th class="trhead" width="150" align="center">Keterangan</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
				<td align="center">6</td>
				<td align="center">7</td>
			</tr>
			<?
			$i = 0;
			$query = mysql_query("SELECT
	*
FROM
	(
		SELECT
			pegawai.kd_pegawai,
			pegawai.NIP,
			pegawai.nama_pegawai,
			pegawai.tempat_lahir,
			pegawai.tgl_lahir,
			pegawai.status_kawin
		FROM
			pegawai
	) pegawai
LEFT JOIN (
	SELECT
		pasangan_pegawai.nama_pasangan,
		pasangan_pegawai.pekerjaan,
		pasangan_pegawai.tempat_lahir tempat_lahir_pasangan,
		pasangan_pegawai.tanggal_lahir tgl_lahir_pasangan,
		pasangan_pegawai.tanggal_nikah,
		pasangan_pegawai.no_karis_karsu,
		pasangan_pegawai.tgl_karis_karsu,
		pasangan_pegawai.keterangan,
		pasangan_pegawai.aktif,
		pasangan_pegawai.kd_pegawai,
		pasangan_pegawai.kd_status_keluarga,
		status_keluarga.status_keluarga statuspasangan
	FROM
		pasangan_pegawai,
		status_keluarga
	WHERE
		pasangan_pegawai.kd_status_keluarga = status_keluarga.kd_status_keluarga
	and pasangan_pegawai.aktif = '1'
) AS pasangan_keluarga ON pasangan_keluarga.kd_pegawai = pegawai.kd_pegawai
LEFT JOIN (
	SELECT
		anak_pegawai.kd_pegawai,
		anak_pegawai.nama_anak,
		anak_pegawai.tgl_lahir tgl_lahir_anak,
		anak_pegawai.tempat_lahir tempat_lahir_anak,
		anak_pegawai.jns_kelamin,
		anak_pegawai.pekerjaan pekerjaan_anak,
		anak_pegawai.urut,
		anak_pegawai.keterangan keterangananak,
		status_keluarga.status_keluarga statusanak
	FROM
		anak_pegawai,
		status_keluarga
	WHERE
		anak_pegawai.kd_status_keluarga = status_keluarga.kd_status_keluarga
) AS anak_pegawai ON anak_pegawai.kd_pegawai = pegawai.kd_pegawai
LEFT JOIN (
	SELECT
		riwayat_keluarga.nama_keluarga,
		riwayat_keluarga.tempat_lahir tempat_lahir_keluarga,
		riwayat_keluarga.tgl_lahir tgl_lahir_keluarga,
		riwayat_keluarga.jns_kelamin,
		riwayat_keluarga.pekerjaan,
		riwayat_keluarga.keterangan,
		status_keluarga.status_keluarga statuskeluarga,
		riwayat_keluarga.kd_pegawai
	FROM
		riwayat_keluarga,
		status_keluarga
	WHERE
		riwayat_keluarga.kd_status_keluarga = status_keluarga.kd_status_keluarga
) AS pegawai_keluarga ON pegawai_keluarga.kd_pegawai = pegawai.kd_pegawai
WHERE pegawai.kd_pegawai = '".$kd_pegawai."' GROUP BY pegawai.kd_pegawai");	
			while ($list_anak=mysql_fetch_array($query)) { 
	   		$i++;
	   		if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			if ($list_anak['tgl_lahir_anak']=='0000-00-00') {
                $str_tanggal_lahir = '-';
            }else if(empty($list_anak['tgl_lahir_anak'])){
				$str_tanggal_lahir = '-';
			}
            else {
                $str_tanggal_lahir = date('d-M-Y', strtotime($list_anak['tgl_lahir_anak']));
            }
            $list_anak['tgl_lahir_anak']=$str_tanggal_lahir;
		 ?>
			<tr>
				<td align="center"><?php echo $i; ?></td>
				<td align="left"><?php echo empty($list_anak['nama_anak']) ? "-" : $list_anak['nama_anak'];  ?></td>
				<td align="left"><?php echo $list_anak['jns_kelamin'] == '1' ? "Laki-Laki" : ($list_anak['jns_kelamin'] == '2' ? "Perempuan" : "-"); ?></td>
				<td align="left"><?php echo empty($list_anak['tempat_lahir_anak']) ? "-" : $list_anak['tempat_lahir_anak'];?></td>
				<td align="left"><?php echo $list_anak['tgl_lahir_anak'];?></td>
				<td align="left"><?php echo empty($list_anak['pekerjaan_anak'])? "-" : $list_anak['pekerjaan_anak'];?></td>
				<td align="left"><?php echo empty($list_anak['keterangananak'])? "-" : $list_anak['keterangananak']; ?></td>
			</tr>
		<?php } ?>
		</table>
	</table>
	<br>
	<table class="bentuktulisan">
		<tr>
			<td>3. Bapak dan Ibu Kandung</td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center">No</td>
				<th class="trhead" width="200" align="center">Nama</td>
				<th class="trhead" width="100" align="center">Tgl. Lahir/umur</td>
				<th class="trhead" width="100" align="center">Pekerjaan</td>
				<th class="trhead" width="100" align="center">Keterangan</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
			</tr>
			<?
			$i = 0;
			$query = mysql_query("SELECT
	*
FROM
	(
		SELECT
			pegawai.kd_pegawai,
			pegawai.NIP,
			pegawai.nama_pegawai,
			pegawai.tempat_lahir,
			pegawai.tgl_lahir,
			pegawai.status_kawin
		FROM
			pegawai
	) pegawai
LEFT JOIN (
	SELECT
		pasangan_pegawai.nama_pasangan,
		pasangan_pegawai.pekerjaan,
		pasangan_pegawai.tempat_lahir tempat_lahir_pasangan,
		pasangan_pegawai.tanggal_lahir tgl_lahir_pasangan,
		pasangan_pegawai.tanggal_nikah,
		pasangan_pegawai.no_karis_karsu,
		pasangan_pegawai.tgl_karis_karsu,
		pasangan_pegawai.keterangan,
		pasangan_pegawai.aktif,
		pasangan_pegawai.kd_pegawai,
		pasangan_pegawai.kd_status_keluarga,
		status_keluarga.status_keluarga statuspasangan
	FROM
		pasangan_pegawai,
		status_keluarga
	WHERE
		pasangan_pegawai.kd_status_keluarga = status_keluarga.kd_status_keluarga
	AND pasangan_pegawai.aktif = '1'
) AS pasangan_keluarga ON pasangan_keluarga.kd_pegawai = pegawai.kd_pegawai
LEFT JOIN (
	SELECT
		anak_pegawai.kd_pegawai,
		anak_pegawai.nama_anak,
		anak_pegawai.tgl_lahir tgl_lahir_anak,
		anak_pegawai.tempat_lahir tempat_lahir_anak,
		anak_pegawai.jns_kelamin,
		anak_pegawai.pekerjaan,
		anak_pegawai.urut,
		status_keluarga.status_keluarga statusanak
	FROM
		anak_pegawai,
		status_keluarga
	WHERE
		anak_pegawai.kd_status_keluarga = status_keluarga.kd_status_keluarga
) AS anak_pegawai ON anak_pegawai.kd_pegawai = pegawai.kd_pegawai
LEFT JOIN (
	SELECT
		riwayat_keluarga.nama_keluarga,
		riwayat_keluarga.tempat_lahir tempat_lahir_keluarga,
		riwayat_keluarga.tgl_lahir tgl_lahir_keluarga,
		riwayat_keluarga.jns_kelamin,
		riwayat_keluarga.pekerjaan pekerjaankeluarga,
		riwayat_keluarga.keterangan keterangankeluarga,
		riwayat_keluarga.kd_status_keluarga,
		status_keluarga.status_keluarga statuskeluarga,
		riwayat_keluarga.aktif,
		riwayat_keluarga.kd_pegawai
	FROM
		riwayat_keluarga,
		status_keluarga
	WHERE
		riwayat_keluarga.kd_status_keluarga = status_keluarga.kd_status_keluarga
and riwayat_keluarga.kd_status_keluarga = '6' OR riwayat_keluarga.kd_status_keluarga = '7' 
) AS pegawai_keluarga ON pegawai_keluarga.kd_pegawai = pegawai.kd_pegawai
WHERE
	pegawai.kd_pegawai = '".$kd_pegawai."'
GROUP BY
	pegawai.kd_pegawai");	
			while ($list_bapakibu=mysql_fetch_array($query)) { 
	   		$i++;
	   		if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			if ($list_bapakibu['tgl_lahir_keluarga']=='0000-00-00') {
                $str_tanggal_lahir = '-';
            }else if(empty($list_bapakibu['tgl_lahir_keluarga'])){
				$str_tanggal_lahir = '-';
			}
            else {
                $str_tanggal_lahir = date('d-M-Y', strtotime($list_bapakibu['tgl_lahir_keluarga']));
            }
            $list_bapakibu['tgl_lahir_keluarga']=$str_tanggal_lahir;
		 ?>
			<tr>
				<td align="center"><?php echo $i; ?></td>
				<td align="left"><?php echo empty($list_bapakibu['nama_keluarga']) ? "-" : $list_bapakibu['nama_keluarga'];  ?></td>
				<td align="left"><?php echo $list_bapakibu['tgl_lahir_keluarga'];?></td>
				<td align="left"><?php echo empty($list_bapakibu['pekerjaankeluarga'])? "-" : $list_bapakibu['pekerjaankeluarga'];?></td>
				<td align="left"><?php echo empty($list_bapakibu['keterangankeluarga'])? "-" : $list_bapakibu['keterangankeluarga']; ?></td>
			</tr>
		<?php } ?>
		</table>
	</table>
	<br>
	<table class="bentuktulisan">
		<tr>
			<td>4. Bapak dan Ibu mertua</td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center">No</td>
				<th class="trhead" width="200" align="center">Nama</td>
				<th class="trhead" width="100" align="center">Tgl. Lahir/umur</td>
				<th class="trhead" width="100" align="center">Pekerjaan</td>
				<th class="trhead" width="100" align="center">Keterangan</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
			</tr>
			<?
			$i = 0;
			$query = mysql_query("SELECT
	*
FROM
	(
		SELECT
			pegawai.kd_pegawai,
			pegawai.NIP,
			pegawai.nama_pegawai,
			pegawai.tempat_lahir,
			pegawai.tgl_lahir,
			pegawai.status_kawin
		FROM
			pegawai
	) pegawai
LEFT JOIN (
	SELECT
		pasangan_pegawai.nama_pasangan,
		pasangan_pegawai.pekerjaan,
		pasangan_pegawai.tempat_lahir tempat_lahir_pasangan,
		pasangan_pegawai.tanggal_lahir tgl_lahir_pasangan,
		pasangan_pegawai.tanggal_nikah,
		pasangan_pegawai.no_karis_karsu,
		pasangan_pegawai.tgl_karis_karsu,
		pasangan_pegawai.keterangan,
		pasangan_pegawai.aktif,
		pasangan_pegawai.kd_pegawai,
		pasangan_pegawai.kd_status_keluarga,
		status_keluarga.status_keluarga statuspasangan
	FROM
		pasangan_pegawai,
		status_keluarga
	WHERE
		pasangan_pegawai.kd_status_keluarga = status_keluarga.kd_status_keluarga
	AND pasangan_pegawai.aktif = '1'
) AS pasangan_keluarga ON pasangan_keluarga.kd_pegawai = pegawai.kd_pegawai
LEFT JOIN (
	SELECT
		anak_pegawai.kd_pegawai,
		anak_pegawai.nama_anak,
		anak_pegawai.tgl_lahir tgl_lahir_anak,
		anak_pegawai.tempat_lahir tempat_lahir_anak,
		anak_pegawai.jns_kelamin,
		anak_pegawai.pekerjaan,
		anak_pegawai.urut,
		status_keluarga.status_keluarga statusanak
	FROM
		anak_pegawai,
		status_keluarga
	WHERE
		anak_pegawai.kd_status_keluarga = status_keluarga.kd_status_keluarga
) AS anak_pegawai ON anak_pegawai.kd_pegawai = pegawai.kd_pegawai
LEFT JOIN (
	SELECT
		riwayat_keluarga.nama_keluarga,
		riwayat_keluarga.tempat_lahir tempat_lahir_keluarga,
		riwayat_keluarga.tgl_lahir tgl_lahir_keluarga,
		riwayat_keluarga.jns_kelamin,
		riwayat_keluarga.pekerjaan pekerjaankeluarga,
		riwayat_keluarga.keterangan keterangankeluarga,
		riwayat_keluarga.kd_status_keluarga,
		status_keluarga.status_keluarga statuskeluarga,
		riwayat_keluarga.aktif,
		riwayat_keluarga.kd_pegawai
	FROM
		riwayat_keluarga,
		status_keluarga
	WHERE
		riwayat_keluarga.kd_status_keluarga = status_keluarga.kd_status_keluarga
and riwayat_keluarga.kd_status_keluarga = '8' OR riwayat_keluarga.kd_status_keluarga = '9' 
) AS pegawai_keluarga ON pegawai_keluarga.kd_pegawai = pegawai.kd_pegawai
WHERE
	pegawai.kd_pegawai = '".$kd_pegawai."'
GROUP BY
	pegawai.kd_pegawai");	
			while ($list_bapakibu=mysql_fetch_array($query)) { 
	   		$i++;
	   		if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			if ($list_bapakibu['tgl_lahir_keluarga']=='0000-00-00') {
                $str_tanggal_lahir = '-';
            }else if(empty($list_bapakibu['tgl_lahir_keluarga'])){
				$str_tanggal_lahir = '-';
			}
            else {
                $str_tanggal_lahir = date('d-M-Y', strtotime($list_bapakibu['tgl_lahir_keluarga']));
            }
            $list_bapakibu['tgl_lahir_keluarga']=$str_tanggal_lahir;
		 ?>
			<tr>
				<td align="center"><?php echo $i; ?></td>
				<td align="left"><?php echo empty($list_bapakibu['nama_keluarga']) ? "-" : $list_bapakibu['nama_keluarga'];  ?></td>
				<td align="left"><?php echo $list_bapakibu['tgl_lahir_keluarga'];?></td>
				<td align="left"><?php echo empty($list_bapakibu['pekerjaankeluarga'])? "-" : $list_bapakibu['pekerjaankeluarga'];?></td>
				<td align="left"><?php echo empty($list_bapakibu['keterangankeluarga'])? "-" : $list_bapakibu['keterangankeluarga']; ?></td>
			</tr>
		<?php } ?>
		</table>
	</table>
	<br>
	<table class="bentuktulisan">
		<tr>
			<td>5. Saudara Kandung</td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center">No</td>
				<th class="trhead" width="200" align="center">Nama</td>
				<th class="trhead" width="50" align="center">Jenis Kelamin</td>
				<th class="trhead" width="90" align="center">Tanggal Lahir/<br>umur</td>
				<th class="trhead" width="100" align="center">Pekerjaan</td>
				<th class="trhead" width="100" align="center">Keterangan</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
				<td align="center">6</td>
			</tr>
			<?
			$i = 0;
			$query = mysql_query("SELECT
	*
FROM
	(
		SELECT
			pegawai.kd_pegawai,
			pegawai.NIP,
			pegawai.nama_pegawai,
			pegawai.tempat_lahir,
			pegawai.tgl_lahir,
			pegawai.status_kawin
		FROM
			pegawai
	) pegawai
LEFT JOIN (
	SELECT
		pasangan_pegawai.nama_pasangan,
		pasangan_pegawai.pekerjaan,
		pasangan_pegawai.tempat_lahir tempat_lahir_pasangan,
		pasangan_pegawai.tanggal_lahir tgl_lahir_pasangan,
		pasangan_pegawai.tanggal_nikah,
		pasangan_pegawai.no_karis_karsu,
		pasangan_pegawai.tgl_karis_karsu,
		pasangan_pegawai.keterangan,
		pasangan_pegawai.aktif,
		pasangan_pegawai.kd_pegawai,
		pasangan_pegawai.kd_status_keluarga,
		status_keluarga.status_keluarga statuspasangan
	FROM
		pasangan_pegawai,
		status_keluarga
	WHERE
		pasangan_pegawai.kd_status_keluarga = status_keluarga.kd_status_keluarga
	AND pasangan_pegawai.aktif = '1'
) AS pasangan_keluarga ON pasangan_keluarga.kd_pegawai = pegawai.kd_pegawai
LEFT JOIN (
	SELECT
		anak_pegawai.kd_pegawai,
		anak_pegawai.nama_anak,
		anak_pegawai.tgl_lahir tgl_lahir_anak,
		anak_pegawai.tempat_lahir tempat_lahir_anak,
		anak_pegawai.jns_kelamin,
		anak_pegawai.pekerjaan,
		anak_pegawai.urut,
		status_keluarga.status_keluarga statusanak
	FROM
		anak_pegawai,
		status_keluarga
	WHERE
		anak_pegawai.kd_status_keluarga = status_keluarga.kd_status_keluarga
) AS anak_pegawai ON anak_pegawai.kd_pegawai = pegawai.kd_pegawai
LEFT JOIN (
	SELECT
		riwayat_keluarga.nama_keluarga,
		riwayat_keluarga.tempat_lahir tempat_lahir_keluarga,
		riwayat_keluarga.tgl_lahir tgl_lahir_keluarga,
		riwayat_keluarga.jns_kelamin jns_kelaminkeluarga,
		riwayat_keluarga.pekerjaan pekerjaankeluarga,
		riwayat_keluarga.keterangan keterangankeluarga,
		riwayat_keluarga.kd_status_keluarga,
		status_keluarga.status_keluarga statuskeluarga,
		riwayat_keluarga.aktif,
		riwayat_keluarga.kd_pegawai
	FROM
		riwayat_keluarga,
		status_keluarga
	WHERE
		riwayat_keluarga.kd_status_keluarga = status_keluarga.kd_status_keluarga
and riwayat_keluarga.kd_status_keluarga = '10' 
) AS pegawai_keluarga ON pegawai_keluarga.kd_pegawai = pegawai.kd_pegawai
WHERE
	pegawai.kd_pegawai = '".$kd_pegawai."'
GROUP BY
	pegawai.kd_pegawai");	
			while ($list_bapakibu=mysql_fetch_array($query)) { 
	   		$i++;
	   		if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			if ($list_bapakibu['tgl_lahir_keluarga']=='0000-00-00') {
                $str_tanggal_lahir = '-';
            }else if(empty($list_bapakibu['tgl_lahir_keluarga'])){
				$str_tanggal_lahir = '-';
			}
            else {
                $str_tanggal_lahir = date('d-M-Y', strtotime($list_bapakibu['tgl_lahir_keluarga']));
            }
            $list_bapakibu['tgl_lahir_keluarga']=$str_tanggal_lahir;
		 ?>
			<tr>
				<td align="center"><?php echo $i; ?></td>
				<td align="left"><?php echo empty($list_bapakibu['nama_keluarga']) ? "-" : $list_bapakibu['nama_keluarga'];  ?></td>
				<td align="left"><?php echo $list_bapakibu['jns_kelaminkeluarga'] == '1' ? "Laki-Laki" : ($list_bapakibu['jns_kelaminkeluarga'] == '2' ? "Perempuan" : "-"); ?></td>
				<td align="left"><?php echo $list_bapakibu['tgl_lahir_keluarga'];?></td>
				<td align="left"><?php echo empty($list_bapakibu['pekerjaankeluarga'])? "-" : $list_bapakibu['pekerjaankeluarga'];?></td>
				<td align="left"><?php echo empty($list_bapakibu['keterangankeluarga'])? "-" : $list_bapakibu['keterangankeluarga']; ?></td>
			</tr>
		<?php } ?>
		</table>
	</table>
	<br>
	<table class="bentuktulisan">
		<tr>
			<h4>VII. KETERANGAN ORGANISASI</h4>
			<td>1. Semasa mengikuti pendidikan pada SLTA ke bawah</td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center">No</td>
				<th class="trhead" width="170" align="center">Nama Organisasi</td>
				<th class="trhead" width="130" align="center">Kedudukan dalam Organisasi</td>
				<th class="trhead" width="70" align="center">Dalam Th. s/d Th.</td>
				<th class="trhead" width="70" align="center">Tempat</td>
				<th class="trhead" width="130" align="center">Nama Pimpinan Organisasi</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
				<td align="center">6</td>
			</tr>
			<tr>
				<td align="center">1.</td>
				<td align="left">-</td>
				<td align="left">-</td>
				<td align="left">-</td>
				<td align="left">-</td>
				<td align="left">-</td>
			</tr>
		</table>
	</table>
	<br>
	<table class="bentuktulisan">
		<tr>
			<td>2. Semasa mengikuti pendidikan pada perguruan tinggi</td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center">No</td>
				<th class="trhead" width="170" align="center">Nama Organisasi</td>
				<th class="trhead" width="130" align="center">Kedudukan dalam Organisasi</td>
				<th class="trhead" width="70" align="center">Dalam Th. s/d Th.</td>
				<th class="trhead" width="70" align="center">Tempat</td>
				<th class="trhead" width="130" align="center">Nama Pimpinan Organisasi</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
				<td align="center">6</td>
			</tr>
			<tr>
				<td align="center">1.</td>
				<td align="left">-</td>
				<td align="left">-</td>
				<td align="left">-</td>
				<td align="left">-</td>
				<td align="left">-</td>
			</tr>
		</table>
	</table>
	<br>
	<table class="bentuktulisan">
		<tr>
			<td>3. Sesudah selesai pendidikan dan atau selama menjadi pegawai/karyawan</td>
		</tr>
		<table style="width: 100%;" border="1" cellspacing="0" cellpadding="1" class="bentuktulisan">
			<tr class="trhead">
				<th class="trhead" width="25" align="center">No</td>
				<th class="trhead" width="170" align="center">Nama Organisasi</td>
				<th class="trhead" width="130" align="center">Kedudukan dalam Organisasi</td>
				<th class="trhead" width="70" align="center">Dalam Th. s/d Th.</td>
				<th class="trhead" width="70" align="center">Tempat</td>
				<th class="trhead" width="130" align="center">Nama Pimpinan Organisasi</td>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">2</td>
				<td align="center">3</td>
				<td align="center">4</td>
				<td align="center">5</td>
				<td align="center">6</td>
			</tr>
			<tr>
				<td align="center">1.</td>
				<td align="left">-</td>
				<td align="left">-</td>
				<td align="left">-</td>
				<td align="left">-</td>
				<td align="left">-</td>
			</tr>
		</table>
	</table>
	<br>
	<br>
	<br>
	<table class="bentuktulisan">
		<tr>
			<td>Demikian Daftar Riwayat Hidup ini saya buat dengan sesungguhnya, dan apabila dikemudian hari terdapat keterangan yang tidak benar, saya bersedia dituntut di muka pengadilan serta bersedia menerima segala tindakan yang diambil oleh Pemerintah.</td>
		</tr>
	</table>
	<table cellpadding="1" cellspacing="0" border="0" width="100%">
		<tr>
			<td align="right" width="50%">&nbsp;</td>
			<td align="center" width="15%">Malang , <?php echo date('d-m-Y');?></td>
		</tr>
		<tr>
			<td align="right" width="2000">&nbsp;</td>
			<td align="center">Yang membuat,</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td align="right">&nbsp;</td>
			<td align="center">(<?php echo $nama_pegawai_saja ?>)</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><b><u>PERHATIAN :</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	<table cellpadding="1" cellspacing="0" border="0" width="100%">
		<tr>
			<td width="25">1.</td>
			<td width="200" colspan="2">Jika ada yang salah harus dicoret, yang dicoret tersebut tetap terbaca, kemudian <br>yang benar dituliskan diatas atau dibawahnya dan diparaf.</td>
		</tr>
		<tr>
			<td width="25">2.</td>
			<td width="200" colspan="2">Kolom yang kosong diberi tanda (-).</td>
		</tr>
	</table>
        <?php }?>
        
    <script>
        window.print();
    </script>

</body>

</html>