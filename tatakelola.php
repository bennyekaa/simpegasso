<!DOCTYPE html>
<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
		body {
			font-family: Arial;
		}

		/* Style the tab */
		.tab {
			overflow: hidden;
			border: 1px solid #ccc;
			background-color: #f1f1f1;
		}

		/* Style the buttons inside the tab */
		.tab button {
			background-color: inherit;
			float: left;
			border: none;
			outline: none;
			cursor: pointer;
			padding: 14px 16px;
			transition: 0.3s;
			font-size: 17px;
		}

		/* Change background color of buttons on hover */
		.tab button:hover {
			background-color: #ddd;
		}

		/* Create an active/current tablink class */
		.tab button.active {
			background-color: #ccc;
		}

		/* Style the tab content */
		.tabcontent {
			display: none;
			padding: 12px 12px;
			border: 1px solid #ccc;
			border-top: none;
		}
	</style>
</head>

<body>

	<h2>Tata Kelola Kegiatan Dosen</h2>
	<p></p>

	<div class="tab">
		<button class="tablinks" onclick="openCity(event, 'London')">PT LN QS 100</button>
		<button class="tablinks" onclick="openCity(event, 'Paris')">PT NON-QS 100/PT DN KLASTER 1 DIKTI</button>
		<button class="tablinks" onclick="openCity(event, 'Tokyo')">PRAKISI</button>
	</div>

	<div id="London" class="tabcontent">
		<h3>PT LN QS 100</h3>
		<td align="left"><br><a href="#" onClick="window.open('datadirieditpelatihan.php?id=<?= $kd_pegawai ?>','Form DRH');"><b><button class="btn btn-dark">Tambah Data</button></a></br></td>
		<table class="table table-hover table-striped" style="font-size:14px">
			<thead>
				<tr>
					<th width="30">No.</th>
					<th>Nama Pelatihan</th>
					<th>Jenis Pelatihan</th>
					<th width="100">Tahun Ajaran</th>
					<th width="100">Semester</th>
					<th width="100">SKS</th>
					<th>Tempat</th>
					<th width="25">&nbsp;</th>
				</tr>
			</thead>
			<tbody class="table-condensed">
				<?php
				$i = 1;
				$bgcol = "warning";
				$query = mysql_query("select rp.*,p.nama_pelatihan as jenis_pelatihan from riwayat_pelatihan rp, jenis_pelatihan p where rp.jenis_pelatihan=p.jns_pelatihan and kd_pegawai='" . $kd_pegawai . "' order by id_riwayat_pelatihan");
				while ($pelatihan = mysql_fetch_array($query)) {
				?>
					<tr class="<?= $bgColor ?>">
						<td style="text-align:center"><?= $i; ?>.</td>
						<td><?= $pelatihan['nama_pelatihan']; ?></td>
						<td><?= $pelatihan['jenis_pelatihan']; ?></td>
						<td style="text-align:center"><?= $pelatihan['tahun']; ?></td>
						<td><?= $pelatihan['semester']; ?></td>
						<td><?= $pelatihan['sks']; ?></td>
						<td><?= $pelatihan['tempat_pelatihan']; ?></td>
						<!-- <td style="text-align:center"><a href="pelatihan.php?kd=<?php echo $pelatihan['id_riwayat_pelatihan']; ?>&sts=del" title="Hapus"><img src="images/delete.gif" border="0"></a></td> -->
					</tr>
				<?php
					if ($i % 2 == 1) {
						$bgcol = "error";
					} else {
						$bgcol = "warning";
					}
					$i++;
				}
				?>

			</tbody>
		</table>
	</div>

	<div id="Paris" class="tabcontent">
		<h3>PT NON-QS 100/PT DN Klaster 1 DIKTI</h3>
		<td align="left"><br><a href="#" onClick="window.open('datadirieditpelatihan.php?id=<?= $kd_pegawai ?>','Form DRH');"><b><button class="btn btn-dark">Tambah Data</button></a></br></td>
		<table class="table table-hover table-striped" style="font-size:14px">
			<thead>
				<tr>
					<th width="30">No.</th>
					<th>Nama Pelatihan</th>
					<th>Jenis Pelatihan</th>
					<th width="100">Tahun Ajaran</th>
					<th width="100">Semester</th>
					<th width="100">SKS</th>
					<th>Tempat</th>
					<th width="25">&nbsp;</th>
				</tr>
			</thead>
			<tbody class="table-condensed">
				<?php
				$i = 1;
				$bgcol = "warning";
				$query = mysql_query("select rp.*,p.nama_pelatihan as jenis_pelatihan from riwayat_pelatihan rp, jenis_pelatihan p where rp.jenis_pelatihan=p.jns_pelatihan and kd_pegawai='" . $kd_pegawai . "' order by id_riwayat_pelatihan");
				while ($pelatihan = mysql_fetch_array($query)) {
				?>
					<tr class="<?= $bgColor ?>">
						<td style="text-align:center"><?= $i; ?>.</td>
						<td><?= $pelatihan['nama_pelatihan']; ?></td>
						<td><?= $pelatihan['jenis_pelatihan']; ?></td>
						<td style="text-align:center"><?= $pelatihan['tahun']; ?></td>
						<td><?= $pelatihan['semester']; ?></td>
						<td><?= $pelatihan['sks']; ?></td>
						<td><?= $pelatihan['tempat_pelatihan']; ?></td>
						<!-- <td style="text-align:center"><a href="pelatihan.php?kd=<?php echo $pelatihan['id_riwayat_pelatihan']; ?>&sts=del" title="Hapus"><img src="images/delete.gif" border="0"></a></td> -->
					</tr>
				<?php
					if ($i % 2 == 1) {
						$bgcol = "error";
					} else {
						$bgcol = "warning";
					}
					$i++;
				}
				?>

			</tbody>
		</table>
	</div>

	<div id="Tokyo" class="tabcontent">
		<h3>PRAKRISI</h3>
		<td align="left"><br><a href="#" onClick="window.open('datadirieditpelatihan.php?id=<?= $kd_pegawai ?>','Form DRH');"><b><button class="btn btn-dark">Tambah Data</button></a></br></td>
		<table class="table table-hover table-striped" style="font-size:14px">
			<thead>
				<tr>
					<th width="30">No.</th>
					<th>Nama Pelatihan</th>
					<th>Jenis Pelatihan</th>
					<th width="100">Tahun Ajaran</th>
					<th width="100">Semester</th>
					<th width="100">SKS</th>
					<th>Tempat</th>
					<th width="25">&nbsp;</th>
				</tr>
			</thead>
			<tbody class="table-condensed">
				<?php
				$i = 1;
				$bgcol = "warning";
				$query = mysql_query("select rp.*,p.nama_pelatihan as jenis_pelatihan from riwayat_pelatihan rp, jenis_pelatihan p where rp.jenis_pelatihan=p.jns_pelatihan and kd_pegawai='" . $kd_pegawai . "' order by id_riwayat_pelatihan");
				while ($pelatihan = mysql_fetch_array($query)) {
				?>
					<tr class="<?= $bgColor ?>">
						<td style="text-align:center"><?= $i; ?>.</td>
						<td><?= $pelatihan['nama_pelatihan']; ?></td>
						<td><?= $pelatihan['jenis_pelatihan']; ?></td>
						<td style="text-align:center"><?= $pelatihan['tahun']; ?></td>
						<td><?= $pelatihan['semester']; ?></td>
						<td><?= $pelatihan['sks']; ?></td>
						<td><?= $pelatihan['tempat_pelatihan']; ?></td>
						<!-- <td style="text-align:center"><a href="pelatihan.php?kd=<?php echo $pelatihan['id_riwayat_pelatihan']; ?>&sts=del" title="Hapus"><img src="images/delete.gif" border="0"></a></td> -->
					</tr>
				<?php
					if ($i % 2 == 1) {
						$bgcol = "error";
					} else {
						$bgcol = "warning";
					}
					$i++;
				}
				?>

			</tbody>
		</table>
	</div>

	<script>
		function openCity(evt, cityName) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById(cityName).style.display = "block";
			evt.currentTarget.className += " active";
		}
	</script>

</body>

</html>