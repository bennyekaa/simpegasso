<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

// $path = "/var/www/simpega"; server
$path = "";
require_once($path . "/simplesamlphp/lib/_autoload.php");
$as = new \SimpleSAML\Auth\Simple("ssoum-sp");

if (isset($_REQUEST['logout'])) {
	// die('test');
	session_unset();
	$url_asal = "http://simpega-dev.web.com";
	// $url_asal = ( isset($_GET["url_asal"])? $_GET["url_asal"] : $GLOBALS["SERVER"] );
	$as->logout(array(
		'ReturnTo' => $url_asal
	));
} else {
	$as->requireAuth();
	$attributes = $as->getAttributes();
	// print_r($attributes);exit;

	$_SESSION["user_id"] = $attributes["user_id"]["0"];
	$_SESSION["username"] = $attributes["username"]["0"];
	$_SESSION["app_skt"] = $attributes["app_skt"];
	$_SESSION["app_name"] = $attributes["app_nm"];
	$_SESSION["app_url"] = $attributes["url"];

	// print_r($_SESSION);exit;
}

function logout()
{
	// die('stop');
	session_unset();
	$url_asal = "http://simpega-dev.web.com";
	// $url_asal = ( isset($_GET["url_asal"])? $_GET["url_asal"] : $GLOBALS["SERVER"] );
	$as->logout(array(
		'ReturnTo' => $url_asal
	));
}

include("config.php");
koneksi();
$querylink = mysql_query("select * from pegawai where nip='" . $attributes["username"]["0"] . "'");
if ($querylink) {
	while ($daftar = mysql_fetch_array($querylink)) {
		// var_dump($daftar); die;
		$kd_pegawai = $daftar['kd_pegawai'];
		$nama_pegawai =  ucwords(strtolower($daftar['nama_pegawai']));
		$nama_gelar = $daftar['gelar_depan'] . " " . ucfirst($daftar['nama_pegawai']) . " " . $daftar['gelar_belakang'];
		$NIP = $daftar['NIP'];
		$photo = 'https://simpega.um.ac.id/admin/public/photo/photo_' . $kd_pegawai . '.jpg';
		$tempat = $daftar['tempat_lahir'];
		$tanggal = $daftar['tgl_lahir'];
		$alamat = $daftar['alamat'];
		$status_kawin = $daftar['status_kawin'];
		$email = $daftar['email'];
		$emailresmi = $daftar['email_resmi'];
		$hape = $daftar['hp'];
		$ket_pendidikan = $daftar['ket_pendidikan'];
		$id_pendidikan_terakhir = $daftar['id_pendidikan_terakhir'];
		if ($daftar['jns_kelamin'] == 1)
			$jenis = 'Pak ';
		else
			$jenis = 'Bu ';
	}
}

$query = mysql_query("select nama_kabupaten from kabupaten where kd_kabupaten='" . $tempat . "'");
if ($query) {
	$datakab = mysql_fetch_array($query);
	$tempat_lahir = $datakab['nama_kabupaten'];
} else {
	$tempat_lahir = $tempat;
}
if ($tempat_lahir == "Lain-lain") {
	$tempat_lahir = "";
}

// $querylink = mysql_query("select * from kelurahan where kd_kelurahan='".$kd_kelurahan."'");
// if ($querylink) {
// 	while ($daftarkel=mysql_fetch_array($querylink)) { 
// 	$kelurahan=$daftarkel['nama_kelurahan'].' - ';
// 	}
// }

$query = mysql_query("select status_perkawinan from status_perkawinan where status_kawin='" . $status_kawin . "'");
if ($query) {
	$datastkawin = mysql_fetch_array($query);
	$status_kawin = ', Status : ' . $datastkawin['status_perkawinan'];
} else {
	$status_kawin = '';
}

?>


<html>

<head>
	<title>SIMPEGA UM</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
	<style type="text/css">
		body {
			padding-top: 60px;
			padding-bottom: 20px;
		}

		.sidebar-nav {
			padding: 9px 0;
		}

		.menutop {
			background-color: #2D2D2D;
			border-color: #000000;
			min-width: 100%;
			opacity: 1;
			padding: 5px 0px;
			position: absolute;
			top: 0;
		}

		.listmenu {
			list-style: none outside none;
			margin: 0;
		}

		.listmenu li {
			list-style-type: none;
			display: inline-block;
			padding: 0px 10px;
		}

		.listmenu li a {
			color: #b8b9bd;
			opacity: 0.8;
		}

		.listmenu li a:hover {
			color: #fff;
			opacity: 1;
		}

		.ident {
			float: right;
			margin-right: 30px;
		}

		.ident1 {
			float: right;
			margin-right: 10px;
			margin-top: 4px;
		}

		.top1 {
			margin-top: -25px;
		}
	</style>
	<link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link rel="shortcut icon" href="images/device.ico">

</head>

<body>
	<div class="menutop">
		<ol class="listmenu">
			<li><a href="https://profil.um.ac.id">PROFIL</a></li>
			<li><a href="https://eoffice.um.ac.id">EOFFICE</a></li>
			<li><a href="https://kinerja.um.ac.id">KINERJA</a></li>
			<li><a href="https://remun.um.ac.id">REMUNERASI</a></li>
			<li><a href="https://sipadu.um.ac.id/sirkulasi">PERPUSTAKAAN</a></li>
			<li><a href="https://gsuite.google.com/dashboard">GOOGLE APPS</a></li>
			<li><a href="https://gate.um.ac.id/apps">APLIKASI LAINNYA</a></li>

			<li class="ident"><a href="?logout">Logout</a></li>
		</ol>
	</div>
	<div class="navbar navbar-inverse top1">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="brand" href="?lp=<?php echo encrypt_url('home'); ?>"><img src="images/log.png" align="absmiddle">&nbsp;The Learning University</a>
				<div class="nav-collapse collapse">
					<p class="navbar-text pull-right">
						<b>Selamat Datang, <?= $nama_pegawai . '[' . $attributes["username"]["0"] . ']' ?></b>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span3">
				<div class="well sidebar-nav">
					<ul class="nav nav-list">
						<!-- <li class="nav-header">Data Dosen</li>-->
						<li><a href="?lp=<?php echo encrypt_url('home'); ?>">Halaman Depan</a></li>
						<li class="nav-header">================</li>
						<li><a href="?lp=<?php echo encrypt_url('datadiri'); ?>">Data Diri</a></li>
						<!-- <li><a href="#">Usul Perubahan Data</a></li> -->
						<li><a href="?lp=<?php echo encrypt_url('pelatihan'); ?>">Pelatihan</a></li>
					</ul>
					<ul class="nav nav-list">
						<li class="nav-header">================</li>
						<li><a href="#" onClick="window.open('kp4.php?id=<?php echo $kd_pegawai; ?>','Form KP4');">Cetak KP4</a></li>
						<li><a href="#" onClick="window.open('drhprint.php?id=<?php echo $kd_pegawai; ?>','Form DRH');">Cetak DRH</a></li>
						<li class="nav-header">================</li>
					</ul>
				</div>
				<!--/.well -->
			</div>
			<!--/span-->
			<div class="span9">
				<?php
				$loadpage = $_REQUEST['lp'];
				if ($loadpage = encrypt_url("datadiri")) {
				} else {
				?>
					<div class="hero-unit" style="padding:5px">
						<h3>&nbsp;<?= $nama_gelar ?>
							<!--/NIP <?= $NIP ?>-->
						</h3>
						<table>
							<tr>
								<td width="10">&nbsp;</td>
								<td width="100" rowspan="5"><img src="<?php echo $photo; ?>" border="1" align="top" height="150" width="120"></td>
								<td width="20">&nbsp;</td>
								<td style="text-align:justify" width="500">NIP : <?= $NIP ?>
								</td>

							</tr>
							<tr>
								<td width="10">&nbsp;</td>
								<td width="20">&nbsp;</td>
								<td style="text-align:justify">
									Tempat, Tgl Lahir : <?= $tempat_lahir ?>, <?= date('d M Y', strtotime($tanggal)); ?>
								</td>
							</tr>
							<?php
							$query = mysql_query("select * from pendidikan where id_pendidikan='" . $id_pendidikan_terakhir . "'");
							if ($query) {
								$datapend = mysql_fetch_array($query);
								$pend = $datapend['nama_pendidikan'] . ' - ';
							} else {
								$pend = '';
							}
							?>
							<tr>
								<td width="10">&nbsp;</td>
								<td width="20">&nbsp;</td>
								<td style="text-align:justify">
									Pendidikan Terakhir : <?= $pend ?> <?= $ket_pendidikan ?>
									<!-- <?= $rt ?><?= $rw ?> <?= $kelurahan ?> <?= $alamat; ?>-->
								</td>
							</tr>
							<tr>
								<td width="10">&nbsp;</td>
								<td width="20">&nbsp;</td>
								<td style="text-align:justify">
									HP : <?= $hape; ?>
								</td>
							</tr>
							<tr>
								<td width="10">&nbsp;</td>
								<td width="20">&nbsp;</td>
								<td style="text-align:justify" colspan="2">
									Email : <?= $email; ?>
								</td>
							</tr>
							<tr>
								<td width="10">&nbsp;</td>
								<td width="20">&nbsp;</td>
								<td width="20">&nbsp;</td>
								<td style="text-align:justify" colspan="2">
									Email Resmi : <?= $emailresmi; ?>
								</td>
							</tr>
							<tr>
								<td width="10">&nbsp;</td>
								<td width="20">&nbsp;</td>
								<td colspan="2">&nbsp;</td>
							</tr>
						</table>
					</div>
				<?php } ?>

				<div class="row-fluid">
					<div class="span12" style="text-align:justify">

						<?php
						$loadpage = $_REQUEST['lp'];
						if ($loadpage == "") {
							$loadpage = encrypt_url("home");
						} else {
							$loadpage = $loadpage;
						}

						$hasildes = decrypt_url($loadpage);

						include($hasildes . ".php");
						?>


					</div>
					<!--/span-->
				</div>
				<!--/row-->
			</div>
			<!--/span-->
		</div>
		<!--/row-->

		<hr>

		<footer>
			<p class="muted credit">Copyright &copy; 2012 - All Rights Reserved Universitas Negeri Malang</p>
		</footer>

	</div>

	<script src="js/jquery-latest.js"></script>
	<script src="js/bootstrap-transition.js"></script>
	<script src="js/bootstrap-alert.js"></script>
	<script src="js/bootstrap-modal.js"></script>
	<script src="js/bootstrap-dropdown.js"></script>
	<script src="js/bootstrap-scrollspy.js"></script>
	<script src="js/bootstrap-tab.js"></script>
	<script src="js/bootstrap-tooltip.js"></script>
	<script src="js/bootstrap-popover.js"></script>
	<script src="js/bootstrap-button.js"></script>
	<script src="js/bootstrap-collapse.js"></script>
	<script src="js/bootstrap-carousel.js"></script>
	<script src="js/bootstrap-typeahead.js"></script>
</body>

</html>