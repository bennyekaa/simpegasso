<?php
include '/var/www/simpega/josso-php-inc/josso.php';
/**
 * PHP Josso lib.  Include this in all pages you want to use josso.
 *
 * @package  org.josso.agent.php
 *
 * @version $Id: josso.php 340 2006-02-09 17:02:13Z sgonzalez $
 * @author Sebastian Gonzalez Oyuela <sgonzalez@josso.org>
 */

/**
JOSSO: Java Open Single Sign-On

Copyright 2004-2008, Atricore, Inc.

This is free software; you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation; either version 2.1 of
the License, or (at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this software; if not, write to the Free
Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
02110-1301 USA, or see the FSF site: http://www.fsf.org.
*/
$currentUrl = $_REQUEST['josso_current_url'];
$_SESSION['dosensiakadsession'] = array();
session_unset();
session_unset($_SESSION['id_user']);
session_unset($_SESSION['id_user']);
session_unset($_SESSION['username_ident']);
session_unset($_SESSION['stdsiakadsession']);
session_unset($_SESSION['foto']);
session_unset($_SESSION['nama_prodi']);
session_unset($_SESSION['fak_kd']);
session_unset($_SESSION['data_akademik']);
session_unset($_SESSION['reg_akademik']);
session_unset($_SESSION['sks_ambil']);
$_SESSION['stdbkdsession'] = array();
session_unset($_SESSION['stdbkdsession']);
$_SESSION['stdprofilsession'] = array();
session_unset($_SESSION['stdprofilsession']);
 unset($_SESSION["PeopleID"]);
                unset($_SESSION["GroupId"]);
                unset($_SESSION["GroupName"]);
                unset($_SESSION["PeopleUsername"]);
                unset($_SESSION["Name"]);
                unset($_SESSION["NamaJabatan"]);
                unset($_SESSION["PrimaryRoleId"]);
                unset($_SESSION["NamaBagian"]);
                unset($_SESSION["menu"]);
                unset($_SESSION["sess_id"]);
                unset($_SESSION["NamaInstansi"]);
                unset($_SESSION["AppKey"]);
                session_destroy();

jossoRequestLogoutForUrl($currentUrl);
?>
