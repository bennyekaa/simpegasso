<?php
include("config.php");
koneksi();

$data = $_POST['data'];
$id = $_POST['id'];

$query = "SELECT
	provinsi.id,
	provinsi.nama,
	kabupaten.nama_kabupaten,
	kabupaten.kd_kabupaten,
	kecamatan.kd_kecamatan,
	kecamatan.nama_kecamatan,
	kelurahan.kd_kelurahan,
	kelurahan.nama_kelurahan 
FROM
	provinsi
	INNER JOIN kabupaten ON provinsi.id = kabupaten.kd_provinsi
	INNER JOIN kecamatan ON kabupaten.kd_kabupaten = kecamatan.kd_kabupaten
	INNER JOIN kelurahan ON kecamatan.kd_kecamatan = kelurahan.kd_kecamatan
WHERE
	kelurahan.kd_kelurahan = ".$id;

$result = mysql_query($query);
$data = array();
while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
    array_push($data, $row);
}

if($data) {
    echo json_encode(array('status' => 1, 'data' => $data));
}
else {
    echo json_encode(array('status' => 0));
}
die();
