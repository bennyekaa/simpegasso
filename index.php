<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

// $path = "/var/www/simpega"; server
$path = "";
require_once($path . "/simplesamlphp/lib/_autoload.php");
$as = new \SimpleSAML\Auth\Simple("ssoum-sp");

if (isset($_REQUEST['logout'])) {
    // die('test');
    session_unset();
    $url_asal = "http://simpega-dev.web.com";
    // $url_asal = ( isset($_GET["url_asal"])? $_GET["url_asal"] : $GLOBALS["SERVER"] );
    $as->logout(array(
        'ReturnTo' => $url_asal
    ));
} else {
    $as->requireAuth();
    $attributes = $as->getAttributes();
    // print_r($attributes);exit;

    $_SESSION["user_id"] = $attributes["user_id"]["0"];
    $_SESSION["username"] = $attributes["username"]["0"];
    $_SESSION["app_skt"] = $attributes["app_skt"];
    $_SESSION["app_name"] = $attributes["app_nm"];
    $_SESSION["app_url"] = $attributes["url"];

    // print_r($_SESSION);exit;
}

function logout()
{
    // die('stop');
    session_unset();
    $url_asal = "http://simpega-dev.web.com";
    // $url_asal = ( isset($_GET["url_asal"])? $_GET["url_asal"] : $GLOBALS["SERVER"] );
    $as->logout(array(
        'ReturnTo' => $url_asal
    ));
}

include("config.php");
koneksi();
$querylink = mysql_query("select * from pegawai where nip='" . $attributes["username"]["0"] . "'");
if ($querylink) {
    while ($daftar = mysql_fetch_array($querylink)) {
        // var_dump($daftar); die;
        $kd_pegawai = $daftar['kd_pegawai'];
        $nama_pegawai =  ucwords(strtolower($daftar['nama_pegawai']));
        $nama_gelar = $daftar['gelar_depan'] . " " . ucfirst($daftar['nama_pegawai']) . " " . $daftar['gelar_belakang'];
        $NIP = $daftar['NIP'];
        $photo = 'https://simpega.um.ac.id/admin/public/photo/photo_' . $kd_pegawai . '.jpg';
        $tempat = $daftar['tempat_lahir'];
        $tanggal = $daftar['tgl_lahir'];
        $alamat = $daftar['alamat'];
        $status_kawin = $daftar['status_kawin'];
        $email = $daftar['email'];
        $emailresmi = $daftar['email_resmi'];
        $hape = $daftar['hp'];
        $ket_pendidikan = $daftar['ket_pendidikan'];
        $id_pendidikan_terakhir = $daftar['id_pendidikan_terakhir'];
        if ($daftar['jns_kelamin'] == 1)
            $jenis = 'Pak ';
        else
            $jenis = 'Bu ';
    }
}

$query = mysql_query("select nama_kabupaten from kabupaten where kd_kabupaten='" . $tempat . "'");
if ($query) {
    $datakab = mysql_fetch_array($query);
    $tempat_lahir = $datakab['nama_kabupaten'];
} else {
    $tempat_lahir = $tempat;
}
if ($tempat_lahir == "Lain-lain") {
    $tempat_lahir = "";
}

// $querylink = mysql_query("select * from kelurahan where kd_kelurahan='".$kd_kelurahan."'");
// if ($querylink) {
// 	while ($daftarkel=mysql_fetch_array($querylink)) { 
// 	$kelurahan=$daftarkel['nama_kelurahan'].' - ';
// 	}
// }

$query = mysql_query("select status_perkawinan from status_perkawinan where status_kawin='" . $status_kawin . "'");
if ($query) {
    $datastkawin = mysql_fetch_array($query);
    $status_kawin = ', Status : ' . $datastkawin['status_perkawinan'];
} else {
    $status_kawin = '';
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIMPEGA - Sistem Informasi Kepegawaian Universitas Negeri Malang</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">



    <!-- Custom styles for this template -->
    <!-- <link href="css/simple-sidebar.css" rel="stylesheet"> -->

</head>

<body>

    <div class="d-flex" id="wrapper">

        <!-- 
     <div class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 text-center">
               <img src="https://profil.um.ac.id/images/logo.png" alt="" />
            </div>
            <div class="col-md-10 text-right" style="padding: 20px 0;">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-header">&#9776;</button>
                <a href="https://profil.um.ac.id" class="navbar-brand" style="font-size: 24px;">&nbsp;Profil Saya</a>
            </div>
        </div>
      </div>
    </div> -->

        <!-- Sidebar -->
        <div class="bg-light border-right" id="sidebar-wrapper">
            <div class="navbar navbar-inverse navbar-static-top">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-2 text-center">
                            <img src="https://profil.um.ac.id/images/logo.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="list-group list-group-flush">
                <a href="?lp=<?php echo encrypt_url('home'); ?>" class="list-group-item list-group-item-action bg-light">Dashboard</a>
                <a href="?lp=<?php echo encrypt_url('datadiri'); ?>" class="list-group-item list-group-item-action bg-light">Data Diri</a>
                <a href="?lp=<?php echo encrypt_url('pelatihan'); ?>" class="list-group-item list-group-item-action bg-light">Pelatihan</a>
                <a href="?lp=<?php echo encrypt_url('tatakelola'); ?>" class="list-group-item list-group-item-action bg-light">Tata Kelola Kegiatan Dosen</a>
                <a href="#" onClick="window.open('kp4.php?id=<?php echo $kd_pegawai; ?>','Form KP4');" class="list-group-item list-group-item-action bg-light">Cetak KP4</a>
                <a href="#" onClick="window.open('drhprint.php?id=<?php echo $kd_pegawai; ?>','Form DRH');" class="list-group-item list-group-item-action bg-light">Cetak DRH</a>
                <!-- <a href="#" class="list-group-item list-group-item-action bg-light">Cetak Kontrak</a> -->
            </div>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="row">
                <div class="col-md-10 text-right" style="padding: 10px 5;">
                    <p class="navbar-text pull-right">
                        <b>Selamat Datang, <?= $nama_pegawai . '[' . $attributes["username"]["0"] . ']' ?></b>
                    </p>
                </div>
                <div class="col-md-2 text-right" style="padding: 10px 5;">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-header">&#9776;</button>
                </div>
                </>
                <nav class="navbar-collapse collapse inverse" id="navbar-header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-11 col-xs-12">
                                Aplikasi lainnya :
                                &nbsp;<a href="https://sigap.um.ac.id" class="btn-sm btn-secondary" title="SIGAP">SIGAP</a>&nbsp;<a href="https://survei.um.ac.id" class="btn-sm btn-secondary" title="Survei untuk dosen, tenaga kependidikan dan mahasiswa">SURVEI</a>&nbsp;<a href="https://profil.um.ac.id" class="btn-sm btn-secondary" title="Profil">PROFIL</a>&nbsp;<a href="https://simpega.um.ac.id" class="btn-sm btn-primary" title="Kepegawaian">KEPEGAWAIAN</a>&nbsp;<a href="https://eoffice.um.ac.id" class="btn-sm btn-secondary" title="Tata Persuratan">EOFFICE</a>&nbsp;<a href="https://kinerja.um.ac.id" class="btn-sm btn-secondary" title="Penilaian Prestasi Kerja Pegawai">KINERJA</a>&nbsp;<a href="https://gate.um.ac.id/apps" class="btn-sm btn-secondary" title="APLIKASI LAINNYA">APLIKASI LAINNYA</a>&nbsp; </div>
                            <div class="col-md-1 col-xs-12 text-right">
                                <a href="?logout" class="btn-sm btn-secondary text-right"><i class="fa fa-sign-out" aria-hidden="true"></i> LOGOUT</a>
                            </div>
                        </div>
                    </div>
                </nav>
                </nav>

                <div class="container-fluid">
                    <div class="span12" style="text-align:justify">

                        <?php
                        $loadpage = $_REQUEST['lp'];
                        if ($loadpage == "") {
                            $loadpage = encrypt_url("home");
                        } else {
                            $loadpage = $loadpage;
                        }

                        $hasildes = decrypt_url($loadpage);

                        include($hasildes . ".php");
                        ?>


                    </div>
                    <!--/span-->
                </div>
            </div>
            <!-- /#page-content-wrapper -->
            <footer>
                <p class="muted credit">Copyright &copy; 2021 - All Rights Reserved Universitas Negeri Malang</p>
            </footer>

        </div>
        <!-- /#wrapper -->

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>

</body>

</html>