<?php

$lang['upload_userfile_not_set'] = "Tidak dapat menemukan variabel posting disebut USERFILE.";
$lang['upload_file_exceeds_limit'] = "File upload melebihi ukuran maksimum yang diizinkan di konfigurasi file PHP.";
$lang['upload_file_exceeds_form_limit'] = "File upload melebihi ukuran maksimal yang diperbolehkan oleh sistem aplikasi.";
$lang['upload_file_partial'] = "File hanya ter-upload sebagian.";
$lang['upload_no_temp_directory'] = "Folder Sementara (the temporery folder) hilang.";
$lang['upload_unable_to_write_file'] = "File tidak dapat disimpan ke komputer.";
$lang['upload_stopped_by_extension'] = "Upload file dihentikan oleh sistem.";
$lang['upload_no_file_selected'] = "Anda belum memilih file untuk diupload.";
$lang['upload_invalid_filetype'] = "Tipe file yang Anda coba upload tidak diperbolehkan.";
$lang['upload_invalid_filesize'] = "File yang Anda coba upload lebih besar daripada ukuran yang diijinkan.";
$lang['upload_invalid_dimensions'] = "Gambar yang Anda coba upload melebihi lebar atau ketinggian maksimum.";
$lang['upload_destination_error'] = "Terdapat masalah ketika mencoba untuk memindahkan file upload ke tujuan akhir.";
$lang['upload_no_filepath'] = "Path upload tampaknya tidak valid.";
$lang['upload_no_file_types'] = "Anda belum menententukan jenis file yang diperbolehkan.";
$lang['upload_bad_filename'] = "Nama file yang Anda kirimkan sudah ada di server.";
$lang['upload_not_writable'] = "Folder tujuan upload tidak dapat dibuat.";


/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */