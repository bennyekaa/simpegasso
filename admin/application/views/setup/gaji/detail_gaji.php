<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/gaji/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href = "<?=site_url('setup/gaji')?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<? } ?>
<div class="content-data">
	<form name="gajidetail" id="gajidetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr align="left">
				<td width ="100px">Kode</td>
				<td> : </td>
				<td>
					<?= $id_gaji?>
				</td>
			</tr>
			<tr align="left">
				<td width ="100px">Golongan</td>
				<td> : </td>
				<td>
				<?php
					echo form_dropdown('id_golpangkat',$golongan_assoc,$id_golpangkat);
					echo form_error('id_golpangkat'); ?> 
				</td>
			</tr>
			<tr align="left">
				<td width ="100px">Masa Kerja</td>
				<td> : </td>
				<td>
					<input type='text' class="js_required required" name='masa_kerja' id='masa_kerja' onkeyup="gsub_num(this)" onchange="gsub_num(this)" 
					onblur="gsub_num(this)" value='<?=$masa_kerja;?>'/>
					<span style="color:#F00;">* <?php echo form_error('masa_kerja'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="100px">Gaji Pokok</td>
				<td> : </td>
				<td>
					<input type='text' class="js_required required" name='gaji_pokok' id='gaji_pokok' onkeyup="gsub_num(this)" onchange="gsub_num(this)" 
					onblur="gsub_num(this)" value='<?=$gaji_pokok;?>'/>
					<span style="color:#F00;">* <?php echo form_error('gaji_pokok'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="100px">Tahun</td>
				<td> : </td>
				<td>
					<input size="4" type='text' class="js_required required" name='tahun' id='tahun' onkeyup="gsub_num(this)" onchange="gsub_num(this)" onblur="gsub_num(this)" value='<?=$tahun;?>' maxlength="4" />
					<span style="color:#F00;">* <?php echo form_error('tahun'); ?></span>
				</td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center;"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>