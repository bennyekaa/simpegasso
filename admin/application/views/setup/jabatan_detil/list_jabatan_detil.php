<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?>&nbsp;"<?=$nama_jabatan[$id_jabatan]?>"</div>
	<div id="module-menu">
	<a class="icon" href="<?=site_url("setup/jabatan/browse/$id_jenis_jabatan_assoc[$id_jabatan]")?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a> &nbsp;
<a class="icon" href="<?=site_url("setup/jabatan_detil/add/$id_jabatan")?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>No </th>
		<th width='300' >Nama Detil Jabatan</th>
		<th width='200' >Pangkat,Golongan</th>
		<th width='200' >Angka Kredit Minimal</th>
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_jabatan_detil!=FALSE){
	foreach ($list_jabatan_detil as $jns_jabatan) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$i;?></td>
		<td align="left" ><?= $jns_jabatan['nama_jabatan_detil']; ?></td>
		<td align="left" ><?=$golongan_pangkat_assoc[$jns_jabatan['id_golpangkat']]?></td>
		<td align="center" ><?= $jns_jabatan['ak_minimal']; ?></td>
		<td align="center" class="colEvn">	
			<?php if($this->user->user_group!="Staf") {	?>		
			<a href="<?=site_url("setup/jabatan_detil/edit/".$jns_jabatan['id_jabatan_detil']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/jabatan_detil/delete/".$jns_jabatan['id_jabatan_detil']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
			
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>