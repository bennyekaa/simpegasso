<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
			<a class="icon" href="<?=site_url('setup/jenis_mutasi/add')?>" title="Tambah">
			<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode</th>
		<th width='200' class="colEvn">Nama Jenis Mutasi</th>
		<th width='500'>Keterangan</th>			
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_jenis_mutasi!=FALSE){
	foreach ($list_jenis_mutasi as $jns_jenis_mutasi) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$jns_jenis_mutasi['jns_mutasi'];?></td>
		<td align="left" ><?= $jns_jenis_mutasi['nama_mutasi']; ?></td>
		<td align="left" ><?=$jns_jenis_mutasi['keterangan'];?></td>
		<td align="center" class="colEvn">	
			<?php if($this->user->user_group!="Staf") {	?>
			<a href="<?=site_url("setup/jenis_mutasi/edit/".$jns_jenis_mutasi['jns_mutasi']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/jenis_mutasi/delete/".$jns_jenis_mutasi['jns_mutasi']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>