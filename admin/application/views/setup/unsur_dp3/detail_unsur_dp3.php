<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/unsur_dp3/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/unsur_dp3');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"/></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="unsur_dp3detail" id="unsur_dp3detail" method="POST" action="<?= $action_url; ?>">
		<table class="general"> <input type="hidden" name="id_unsur_dp3" value="<?=$id_unsur_dp3?>" />
			<tr align="left">
				<td width ="10px">No Urut</td>
				<td> : </td>
				<td>
				<input type='text' name='urutan' id='urutan' value='<?= $urutan?>' style='width:5em' maxlength="2"/>
				<?php echo form_error('urutan'); ?>  </td>
			</tr>				
			<tr align="left">
				<td width ="100px">Nama Unsur DP3</td>
				<td> : </td>
				<td>
				<input type='text' name='nama_unsur_dp3' id='nama_unsur_dp3' class="js_required required" value='<?= $nama_unsur_dp3?>' style='width:18em'/>
				<span style="color:#F00;">* <?php echo form_error('nama_unsur_dp3'); ?></span>
				</td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>