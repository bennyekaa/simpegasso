<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/unit/$action/";
if (!async_request())
{
?>

<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/unit/browse/'. $id_group);?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>

<? } ?>
<br/>
<div class="content-data">
	<form name="unit_kerjadetail" id="unit_kerjadetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
		<tr align="left">
			<td width ="100px">Kode Unit Kerja</td>
			<td> : </td>
			<td>
				<input type='hidden' name='kode_unit_lama' id='kode_unit_lama' value='<?= $kode_unit; ?>' >
				<input type='text' name='kode_unit' onKeyUp="gsub_num(this)" id='kode_unit' value='<?= set_value('kode_unit', $kode_unit); ?>' />
					<span style="color:#F00;">* <?php echo form_error('kode_unit'); ?></span>
			</td>
		</tr>
		<tr align="left">
			<td width ="100px">Unit Kerja</td>
			<td> : </td>
			<td>
				<input type='text' name='nama_unit' id='nama_unit' class="js_required required" value='<?= $nama_unit?>' style='width:25em'/>
				<span style="color:#F00;">* <?php echo form_error('nama_unit'); ?></span>
			</td>
		</tr>
			<input type='hidden' name='id_group' id='id_group' class="js_required required" value='<?= $id_group?>' style='width:20em'/>
		<tr align="left">
			<td width ="100px">Alamat</td>
			<td> : </td>
			<td>
			<input type='text' name='alamat' id='alamat' value='<?= $alamat?>' style='width:25em'/>
			<?php echo form_error('alamat'); ?>  </td>
		</tr>
		
		<tr>
			<td width ="150px" align="left">Parent</td>
			<td> : </td>
			<td>
				
				<?php
						echo form_dropdown('kode_unit_general', $unit_kerja_assoc, $kode_unit_general);
						echo form_error('kode_unit_general');
					?>
			</td>
		</tr>
		<tr>
			<td width ="150px" align="left">General/Detil</td>
			<td> : </td>
			<td>
					<select name="GD">
						<option value="G" <?php echo ($unit_kerja['GD']=="G")?"selected=selected":""; ?>>G</option>
						<option value="D" <?php echo ($unit_kerja['GD']=="D")?"selected=selected":""; ?>>D</option>
					</select>
					
					<!--<?php
					echo form_dropdown('GD', $GD_assoc, $unit_kerja['GD']);
					echo form_error('GD'); ?>-->
		</td>
		</tr>	
		<tr>
			<td width ="150px" align="left">Tingkat</td>
			<td> : </td>
			<td>
					<input type='text' name='tingkat'  onKeyUp="gsub_num(this)"  id='tingkat' value='<?= $tingkat?>' style='width:5em'/>
			<span style="color:#F00;">* <?php echo form_error('tingkat'); ?></span>
		</td>
		</tr>			
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
});

$(document).ready(function(){
	$("#kode_unit").parent().append("<label style='display:none' class='inp_error'>Duplicate Entry</label>");
});


$('#kode_unit').keyup(function(){
	$(this).stopTime();
	$(this).oneTime(200,function(){
		if($("#kode_unit").val()!=$("#kode_unit_lama").val()){
		$.ajax({
			url 		: "<?=site_url('setup/unit/cek_duplicate_kode_unit/')?>/"+$("#kode_unit").val(),
			dataType	: "json",
			success 	:function(result){
							if(result){
								$("#kode_unit").siblings(':label.inp_error').show();
								$("#kode_unit").addClass("inp_error");
							}
							else{
								$("#kode_unit").siblings(':label.inp_error').hide();
								$("#kode_unit").removeClass("inp_error");
							}
						}
			});
		}else{
			$("#kode_unit").siblings(':label.inp_error').hide();
			$("#kode_unit").removeClass("inp_error");
		}
	});
});
</script>