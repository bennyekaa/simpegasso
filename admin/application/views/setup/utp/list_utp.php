<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
			<a class="icon" href="<?=site_url('setup/utp/add')?>" title="Tambah">
			<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode</th>
		<th width='350' class="colEvn">Nama Jenis Pegawai</th>
		
		<!--<th width='200' >Keterangan</th>		-->
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = $start;
	if ($list_utp!=FALSE){
	foreach ($list_utp as $jns_utp) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$i;//$jns_utp['id_utp'];?></td>
		<td align="left" ><?= $jns_utp['nama_utp']; ?></td>
		<!--<td align="left" ><?=$jns_utp['keterangan'];?></td>-->
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>
			<a href="<?=site_url("setup/utp/edit/".$jns_utp['id_utp']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/utp/delete/".$jns_utp['id_utp']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
			<a href="<?=site_url("setup/utp_detil/browse/".$jns_utp['id_utp']); ?>" title="Jenis UTP" class="view action">Kelola Rincian UTP</a>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>