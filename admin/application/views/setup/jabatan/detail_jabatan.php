<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/jabatan/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?> &nbsp;"<?=$nama_jenis_jabatan[$id_jenis_jabatan]?>"</div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url()."/setup/jabatan/$action_list/";?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="jabatandetail" id="jabatandetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr align="left">
				<td width ="100px">Kode</td>
				<td> : </td>
				<td>
					<input type='text' name='id_jabatan' id='id_jabatan' class="js_required required" value='<?= $id_jabatan?>' style='width:5em'/>
					<span style="color:#F00;">* <?php echo form_error('id_jabatan'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Nama Jabatan</td>
				<td> : </td>
				<td>
					<input type='text' name='nama_jabatan' id='nama_jabatan' class="js_required required" value='<?= $nama_jabatan?>' style='width:20em'/>
					<span style="color:#F00;">* <?php echo form_error('nama_jabatan'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Pangkat,Golongan</td>
				<td> : </td>
				<td>
					<?php
							echo form_dropdown('id_golpangkat_minimal', $golongan_pangkat_assoc, $id_golpangkat_minimal);
							echo form_error('id_golpangkat_minimal');
						?>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Batas usia Pensiun</td>
				<td> : </td>
				<td>
					<input type='text' name='batas_maks_pensiun' id='batas_maks_pensiun' onKeyUp="gsub_num(this)"  class="js_required required" value='<?= $batas_maks_pensiun?>' style='width:5em'/>
					<span style="color:#F00;">* <?php echo form_error('batas_maks_pensiun'); ?></span>
				</td>
			</tr>
					<input type='hidden' name='id_jenis_jabatan' id='id_jenis_jabatan' class="js_required required" value='<?= $id_jenis_jabatan?>' style='width:20em'/>
		
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>