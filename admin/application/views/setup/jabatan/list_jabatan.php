<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?> &nbsp;"<?=$nama_jenis_jabatan[$id_jenis_jabatan]?>"</div>
	<div id="module-menu">
	<a class="icon" href="<?=site_url('setup/jenis_jabatan');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a> &nbsp;
<a class="icon" href="<?=site_url("setup/jabatan/add/$id_jenis_jabatan")?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode </th>
		<th width='300' >Nama Jabatan</th>
		<th width='200' >Pangkat, Golongan</th>
		<th width='200' >Batas Usia Pensiun</th>
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_jabatan!=FALSE){
	foreach ($list_jabatan as $jns_jabatan) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$i;?></td>
		<td align="left" ><?= $jns_jabatan['nama_jabatan']; ?></td>
		<td align="left" ><?=$golongan_pangkat_assoc[$jns_jabatan['id_golpangkat_minimal']]?></td>
		<td align="center" ><?= $jns_jabatan['batas_maks_pensiun']; ?></td>
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>
			<a href="<?=site_url("setup/jabatan/edit/".$jns_jabatan['id_jabatan']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/jabatan/delete/".$jns_jabatan['id_jabatan']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
			<a href="<?=site_url("setup/jabatan_detil/browse/".$jns_jabatan['id_jabatan']); ?>" title="Rinci jabatan" class="view action">Rincian Jabatan</a>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>