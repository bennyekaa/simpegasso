<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/group_unit/add')?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>

<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode</th>
		<th width='200' class="colEvn">Nama Unit Kerja</th>
		<th width='400' >Keterangan</th>			
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_group_unit!=FALSE){
	foreach ($list_group_unit as $jns_group_unit) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$jns_group_unit['id_group'];?></td>
		<td align="left" ><?= $jns_group_unit['nama_group']; ?></td>
		<td align="left" ><?=$jns_group_unit['keterangan'];?></td>
		<td align="center" class="colEvn">
			<?php if($this->user->user_group!="Staf") {	?>			
			<a href="<?=site_url("setup/group_unit/edit/".$jns_group_unit['id_group']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/group_unit/delete/".$jns_group_unit['id_group']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
			<a href="<?=site_url("setup/unit/browse/".$jns_group_unit['id_group']); ?>" title="Unit Kerja" class="view action">Kelola Unit Kerja</a>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>