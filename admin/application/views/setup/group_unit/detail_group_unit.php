<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/group_unit/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
<a class="icon" href="<?=site_url('setup/group_unit');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>

<? } ?>
<br/>
<div class="content-data">
	<form name="group_unitdetail" id="group_unitdetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr align="left">
				<td width ="100px">Kode</td>
				<td> : </td>
				<td>
					<?= $id_group?>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Unit Kerja</td>
				<td> : </td>
				<td>
					<input type='text' name='nama_group' id='nama_group' class="js_required required" value='<?= $nama_group?>' style='width:15em'/>
					<span style="color:#F00;">* <?php echo form_error('nama_group'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="300px">Keterangan</td>
				<td> : </td>
				<td>
					<input type='text' name='keterangan' id='keterangan' class="js_required required" value='<?= $keterangan?>' style='width:20em'/>
					<span style="color:#F00;">* <?php echo form_error('keterangan'); ?></span>
				</td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>