<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/kabupaten/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/kabupaten');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="kabupatendetail" id="kabupatendetail" method="POST" action="<?= $action_url; ?>">
		<table class="general" >
			<tr align="left">
				<td width ="100px">Kode</td>
				<td> : </td>
				<td>
					<input type='text' name='kd_kabupaten' id='kd_kabupaten' class="js_required required" value='<?= $kd_kabupaten?>' style='width:5em'/>
					<span style="color:#F00;">* <?php echo form_error('kd_kabupaten'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="100px">Nama Kabupaten</td>
				<td> : </td>
				<td>
					<input type='text' name='nama_kabupaten' id='nama_kabupaten' class="js_required required" value='<?= $nama_kabupaten?>' style='width:20em'/>
					<span style="color:#F00;">* <?php echo form_error('nama_kabupaten'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="100px">Nama Propinsi</td>
				<td> : </td>
				<td>
					<input type='text' name='nama_propinsi' id='nama_propinsi' class="js_required required" value='<?= $nama_propinsi?>' style='width:20em'/>
					<span style="color:#F00;">* <?php echo form_error('nama_propinsi'); ?></span>
				</td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>