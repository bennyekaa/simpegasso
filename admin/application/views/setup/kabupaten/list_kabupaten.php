<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
			<a class="icon" href="<?=site_url('setup/kabupaten/add')?>" title="Tambah">
			<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode</th>
		<th width='100' class="colEvn">Nama Kabupaten</th>
		<th width='200' >Nama Propinsi</th>			
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_kabupaten!=FALSE){
	foreach ($list_kabupaten as $jns_kabupaten) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$i;?></td>
		<td align="left" >
			<a href="<?=site_url("setup/kecamatan/browse/".$jns_kabupaten['kd_kabupaten']); ?>"title="View Kecamatan" ><?=$jns_kabupaten['nama_kabupaten'];?></a> 
		</td>
		<td align="left" ><?=$jns_kabupaten['nama_propinsi'];?></td>
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>
				<a href="<?=site_url("setup/kabupaten/edit/".$jns_kabupaten['kd_kabupaten']); ?>" class="edit action" >Ubah</a>
				<a href="<?=site_url("setup/kabupaten/delete/".$jns_kabupaten['kd_kabupaten']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
			<a href="<?=site_url("setup/kecamatan/browse/".$jns_kabupaten['kd_kabupaten']); ?>" title="Kecamatan" class="view action">Kecamatan</a>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>