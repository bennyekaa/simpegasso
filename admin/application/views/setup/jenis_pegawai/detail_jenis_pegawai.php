<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/jenis_pegawai/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/jenis_pegawai');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="jenis_pegawaidetail" id="jenis_pegawaidetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr align="left">
				<td width ="150px">Kode</td>
				<td> : </td>
				<td>
					<input type='text' name='id_jns_pegawai' id='id_jns_pegawai' class="js_required required" value='<?= $id_jns_pegawai?>' style='width:5em'/>
					<span style="color:#F00;">* <?php echo form_error('id_jns_pegawai'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="150px">Nama Jenis Pegawai</td>
				<td> : </td>
				<td>
					<input type='text' name='jenis_pegawai' id='jenis_pegawai' class="js_required required" value='<?= $jenis_pegawai?>' style='width:20em'/>
					<span style="color:#F00;">* <?php echo form_error('jenis_pegawai'); ?></span>
				</td>
			</tr>
		
					<input type='hidden' name='usia_pensiun'  onKeyUp="gsub_num(this)"   id='usia_pensiun' value='<?= $usia_pensiun?>' style='width:5em'/>
			<tr align="left">
				<td width ="150px">Parameter</td>
				<td> : </td>
				<td>
					<?php
					echo form_dropdown('id_parameter',$parameter_assoc,$id_parameter);
					echo form_error('id_parameter'); ?> 
				</td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <? if ($action=="add"){?> disabled="disabled"  <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>