<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
<a class="icon" href="<?=site_url('setup/pendidikan/add')?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"/></a>
	</div>
	<div class="clear"></div>
</div>

<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50' class="colEvn">Kode</th>
		<th width='150' class="trhead"  align="left" >Pendidikan</th>
		<th width='200' class="colEvn"  align="left" >Keterangan</th>
		<th width='150' align="left" >Golongan Awal</th>
		<th width='150' class="colEvn" >Pangkat Akhir/Gol</th>
		<th class="trhead">Edit</th>
	</tr>
	<?
	$i = 0;
	if ($list_pendidikan!=FALSE){
	foreach ($list_pendidikan as $jns_pendidikan) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$i;?></td>
		<td align="left" class="colEvn"><?=$jns_pendidikan['nama_pendidikan'];?></td>
		<td align="left" class="colEvn"><?=$jns_pendidikan['keterangan'];?></td>
		<td align="left" ><?=$golongan_assoc[$jns_pendidikan['id_golpangkat_awal']]?></td>
		<td align="left" ><?=$golongan_pangkat_assoc[$jns_pendidikan['id_golpangkat_akhir']]?></td>
		<td align="center" class="colEvn">		
			<?php if($this->user->user_group!="Staf") {	?>	
			<a href = "<?=site_url("setup/pendidikan/edit/".$jns_pendidikan['id_pendidikan']); ?>" class="edit action" >Ubah</a>
			<a href = "<?=site_url("setup/pendidikan/delete/".$jns_pendidikan['id_pendidikan']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>