<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
<a class="icon" href="<?=site_url('setup/jenjang/add')?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"/></a>
	</div>
	<div class="clear"></div>
</div>

<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50' class="colEvn">Kode</th>
		<th width='250' align="left" >Gol/Pangkat Awal</th>
		<th width='250' align="left" class="colEvn">Gol/Pangkat Akhir</th>
		
		<th >Edit</th>
	</tr>
	<?
	$i = 0;
	if ($list_jenjang!=FALSE){
	foreach ($list_jenjang as $jns_jenjang) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$jns_jenjang['id_jenjang_kenaikan'];?></td>
		<td align="left" ><?=$golongan_pangkat_assoc[$jns_jenjang['id_golpangkat_awal']]?></td>
		<td align="left" ><?=$golongan_pangkat_assoc[$jns_jenjang['id_golpangkat_akhir']]?></td>
		
		<td align="center" class="colEvn">			
			<a href = "<?=site_url("setup/jenjang/edit/".$jns_jenjang['id_jenjang_kenaikan']); ?>" class="edit action" >Ubah</a>
			<a href = "<?=site_url("setup/jenjang/delete/".$jns_jenjang['id_jenjang_kenaikan']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
		</td>
	</tr>


	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>