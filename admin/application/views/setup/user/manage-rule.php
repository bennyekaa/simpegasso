<?
$option_group=array(0=>"Semua Grup") + $this->usergroup->get_group();
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div class="clear"></div>
</div>
<br/>
<div class="paging"><?=$page_links?></div>
<div>
	<table class="table-list">
	<tr class="trhead" align="center">
		<th width="20">No.</th>
		<th width="100" >Nama</th>
		<th width="400" >Keterangan</th>
		<th width="25" >Edit</th>
	</tr>
	<?
	$i = 0;
	if ($group_list!=FALSE){
	foreach ($group_list as $group) {
		$i++;
		if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>   
	<tr class="<?= $bgColor; ?>">
		<td align="center" nowrap="nowrap"><strong><?=$i; ?></strong></td>
		<td class="colEvn" align="left" nowrap="nowrap"><strong><?=$group['title']; ?></strong></td>
		<td align="left"><?=$group['description']; ?></td>
		<td class="colEvn" align="center" nowrap="nowrap">
			<a href="<?=site_url("staf/rule/assigned_user/".$group['group_id']); ?>" class="manage action" >Pengaturan Hak Akses</a>
		</td>
	</tr>
	<? }
	}
	else {
		echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
	}?>
	</table>
	<div class="paging"><?=$page_links?></div>
</div>
</div>