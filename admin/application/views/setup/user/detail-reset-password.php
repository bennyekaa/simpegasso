<?
$this->load->helper('url');
$this->load->library('form_validation');
?>
<div class="content">
<div id="content-header">
	<div class="ch-title"><h3><?=$judul;?></h3></div>
	<div class="clear"></div>
</div>
<br/>
<div class="content-data">
	<div span>
		Reset password akan menyebabkan user tidak bisa lagi menggunakan password lama.
	</div>
	<br/>
	<form name="resetpassword" id="resetpassword" method="POST" action="<?= $action_url; ?>">
		<table class="general">
		<tr align="left">
			<td width ="100px">Nama</td>
			<td> : </td>
			<td>
				<?= $name?></td>
		</tr>
		<tr align="left">
			<td width ="100px">Username</td>
			<td> : </td>
			<td>
				<?= $username?>
			</td>
		</tr>
		<tr align="left">
			<td width ="100px">Password</td>
			<td> : </td>
			<td>
			<input type='text' class="js_required required" name='password' id='password' value='' style='width:12em'/>
			<span style="color:#F00;">* <?php echo form_error('password'); ?></span>  </td>
		</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Reset  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>