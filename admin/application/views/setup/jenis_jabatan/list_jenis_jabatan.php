<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?> &nbsp;"<?=$nama_jenis_pegawai[$id_jns_pegawai]?>"</div>
	<div id="module-menu">
	<a class="icon" href="<?=site_url('setup/jenis_pegawai');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a> &nbsp;
<a class="icon" href="<?=site_url("setup/jenis_jabatan/add/$id_jns_pegawai")?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode </th>
		<th width='300' >Nama Jenis Jabatan</th>
		<th width='200' >Keterangan</th>
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_jenis_jabatan!=FALSE){
	foreach ($list_jenis_jabatan as $jns_jabatan) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$i;?></td>
		<td align="left" ><?= $jns_jabatan['nama_jenis_jabatan']; ?></td>
		<td align="center" ><?= $jns_jabatan['keterangan']; ?></td>
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>
			<a href="<?=site_url("setup/jenis_jabatan/edit/".$jns_jabatan['id_jenis_jabatan']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/jenis_jabatan/delete/".$jns_jabatan['id_jenis_jabatan']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
			<a href="<?=site_url("setup/jabatan/browse/".$jns_jabatan['id_jenis_jabatan']); ?>" title="Detail jabatan" class="view action">Detail Jabatan</a>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>