<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
			<a class="icon" href="<?=site_url('setup/parameter/add')?>" title="Tambah">
			<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>Kode</th>
		<th width='300' class="colEvn">Nama Parameter Kenaikan Pangkat</th>
		<th width='150' >Periode Minimal</th>	
		<th width='150' class="colEvn">DP3 Minimal</th>
		<th width='150' >AK Minimal</th>		
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_parameter!=FALSE){
	foreach ($list_parameter as $jns_parameter) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$jns_parameter['id_parameter'];?></td>
		<td align="left" ><?= $jns_parameter['parameter']; ?></td>
		<td align="center" ><?=$jns_parameter['periode_minimal'];?></td>
		<td align="center" ><?=$jns_parameter['DP3_minimal'];?></td>
		<td align="center" ><?=$jns_parameter['AK_minimal'];?></td>
		<td align="center" class="colEvn">			
			<?php if($this->user->user_group!="Staf") {	?>
			<a href="<?=site_url("setup/parameter/edit/".$jns_parameter['id_parameter']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("setup/parameter/delete/".$jns_parameter['id_parameter']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>