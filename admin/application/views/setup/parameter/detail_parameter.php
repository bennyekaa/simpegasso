<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/setup/parameter/$action/";
if (!async_request())
{
?>
<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/parameter');?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div class="content-data">
	<form name="parameterdetail" id="parameterdetail" method="POST" action="<?= $action_url; ?>">
		<table class="general">
			<tr align="left">
				<td width ="100px">Kode</td>
				<td> : </td>
				<td>
					<input type='text' name='id_parameter' id='id_parameter' class="js_required required" value='<?= $id_parameter?>' style='width:5em'/>
					<span style="color:#F00;">* <?php echo form_error('id_parameter'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="150px">Nama Parameter Kenaikan Pangkat</td>
				<td> : </td>
				<td>
					<input type='text' name='parameter' id='parameter' class="js_required required" value='<?= $parameter?>' style='width:20em'/>
					<span style="color:#F00;">* <?php echo form_error('parameter'); ?></span>
				</td>
			</tr>
			<tr align="left">
				<td width ="150px">Batas Periode Minimal</td>
				<td> : </td>
				<td>
					<input type='text' name='periode_minimal'  onKeyUp="gsub_num(this)"   id='periode_minimal' value='<?= $periode_minimal?>' style='width:5em'/>
				</td>
			</tr>
			<tr align="left">
				<td width ="150px">DP3 Minimal</td>
				<td> : </td>
				<td>
					<input type='text' name='DP3_minimal'  onKeyUp="gsub_num(this)"   id='DP3_minimal' value='<?= $DP3_minimal?>' style='width:5em'/>
				</td>
			</tr>
			<tr align="left">
				<td width ="150px">AK Minimal</td>
				<td> : </td>
				<td>
					<input type='text' name='AK_minimal'  onKeyUp="gsub_num(this)"   id='AK_minimal' value='<?= $AK_minimal?>' style='width:5em'/>
				</td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>