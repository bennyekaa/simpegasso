<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('setup/kepegawaian/add/'.$cat)?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div class="paging"><?=$page_links?></div>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50' >Kode</th>
		<th width='200' class="colEvn">Jenis Pegawai</th>
		<th width='150'>Keterangan</th>
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_pegawai!=FALSE){
	foreach ($list_pegawai as $jns_pegawai) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center"><?=$jns_pegawai['kd_jenis_pegawai'];?></td>
		<td align="left" class="colEvn"><?= $jns_pegawai['jenis_pegawai']; ?></td>
		<td align="left" ><?= $jns_pegawai['ket_jenis_pegawai']; ?></td>
		<td align="center" class="colEvn">		
			<?php if($this->user->user_group!="Staf") {	?>	
			<a href = "<?=site_url("setup/kepegawaian/edit/".$cat."/".$jns_pegawai['kd_jenis_pegawai']); ?>" class="edit action" >Ubah</a>
			<a href = "<?=site_url("setup/kepegawaian/delete/".$cat."/".$jns_pegawai['kd_jenis_pegawai']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
</div>
</div>