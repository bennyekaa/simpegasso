<script type="text/javascript" charset="utf-8">

var CONF = {
	BASE_URL : '<?php echo rtrim(site_url(), '/') . '/'; ?>',
	EDITOR : '<?php echo ($editor) ? 1 : 0 ?>',
	ENABLE_RATINGS : <?php echo isset($star_rating) ? 'true' : 'false'; ?>,
	<?php if(isset($rating_id)) { echo 'ENTRY_ID : '.$rating_id."\n"; } ?>
};

</script>