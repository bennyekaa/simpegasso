<div>
	<?php if ($flash_msg = $this->session->flashdata('error')): ?>
	<div class="error"><span class="message_content"><?php echo $flash_msg?></span></div>
	<?php endif ?>
	<?php if($flash_msg = $this->session->flashdata('success')): ?>
	<div class="success"><span class="message_content"><?php echo $flash_msg?></span></div>
	<?php endif ?>
</div>