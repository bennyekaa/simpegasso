<?php
function right($value, $count){
    return substr($value, ($count*-1));
}

function left($string, $count){
    return substr($string, 0, $count);
}

function datediff($d1, $d2)
{  
	$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
	$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
	$diff_secs = abs($d1 - $d2);  
	$base_year = min(date("Y", $d1), date("Y", $d2));  
	$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
	return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
}							

?>
<?php
	$querylink = mysql_query("select * from pegawai where kd_pegawai='".$kd_pegawai."'");
	if ($querylink) {
		while ($daftar=mysql_fetch_array($querylink)) {
			
			$kd_pegawai = $daftar['kd_pegawai'];
			$nama_pegawai = $daftar['gelar_depan']." ".strtolower($daftar['nama_pegawai'])." ".$daftar['gelar_belakang'];
			$NIP = $daftar['NIP'];
			$no_KTP = $daftar['no_KTP'];
			$nama_peg = $daftar['nama_pegawai'];
			$tempat = $daftar['tempat_lahir'];
			$tanggal = $daftar['tgl_lahir'];
			$alamat = $daftar['alamat'];   
			$alamat_asal = $daftar['alamat_asal'];
			$hp = $daftar['hp'];
			$email = $daftar['email'];
			$npwp = $daftar['npwp'];
			$no_ASKES = $daftar['no_ASKES'];
			$taspen = $daftar['taspen'];
			$karpeg = $daftar['karpeg'];
			$loker = $daftar['loker'];
			$kode_pos = $daftar['kode_pos'];
			$kd_agama = $daftar['kd_agama'];
			$kd_kelurahan = $daftar['kd_kelurahan'];
			$rt="";
			$rw="";
			if ($daftar['RT'] != '')
			$rt = $daftar['RT'];
			if ($daftar['RW'] != '')
			$rw = $daftar['RW'];
			$gol_darah = $daftar['gol_darah'];
			$id_pendidikan_terakhir = $daftar['id_pendidikan_terakhir'];
			$drh_tinggi = ltrim($daftar['drh_tinggi']);
			$drh_bb = ltrim($daftar['drh_bb']);
			$drh_rambut = ltrim($daftar['drh_rambut']);
			$drh_bentuk_muka = ltrim($daftar['drh_bentuk_muka']);
			$drh_warna_kulit = ltrim($daftar['drh_warna_kulit']);
			$drh_ciri_ciri = ltrim($daftar['drh_ciri_ciri']);
			$drh_cacat = $daftar['drh_cacat'];
			$hobi_kesenian = ltrim($daftar['hobi_kesenian']);
			$hobi_olahraga = ltrim($daftar['hobi_olahraga']);
			$hobi_lain = ltrim($daftar['hobi_lain']);
			$drh_nama_kecil = $daftar['drh_nama_kecil'];
			if ($daftar['jns_kelamin']==1)
				$daftar['jenis'] = 'Laki-Laki';
			else
				$daftar['jenis'] = 'Perempuan';
	
			$kd_agama = $daftar['kd_agama'];
			$status_kawin = $daftar['status_kawin'];	
			//$stkawin = 'Kawin';
			
			
			
			$queryjenis = mysql_query("select * from jenis_pegawai where id_jns_pegawai='".$daftar['id_jns_pegawai']."'");
			if ($queryjenis) {
				while ($daftarjenis=mysql_fetch_array($queryjenis)) { 
				$jenis_pegawai=$daftarjenis['jenis_pegawai'];
				 }	
			}
			
			
//			$jenis_pegawai=$daftar['id_jns_pegawai'];
			
			
			
			//jenis_pegawai
			if ($daftar['status_pegawai']==0)
				$status_pegawai = 'Honorer';
			elseif ($daftar['status_pegawai']==1)
				$status_pegawai = 'CPNS';
			else
				$status_pegawai = 'PNS';
				
			$id_golpangkat_terakhir=$daftar['id_golpangkat_terakhir'];
			//$pangkat;
			//$golongan
			
			$id_jabatan_terakhir=$daftar['id_jabatan_terakhir'];
			$querylink = mysql_query("select * from jabatan where id_jabatan='".$daftar['id_jabatan_terakhir']."'");
			if ($querylink) {
				while ($daftarjabatan=mysql_fetch_array($querylink)) { 
				$jabatan=$daftarjabatan['nama_jabatan'];
				$queryJBT = mysql_query("select riwayat_jabatan.*,unit_kerja.nama_unit from riwayat_jabatan,unit_kerja 
				where kd_pegawai='".$kd_pegawai."' and id_jabatan='".$daftar['id_jabatan_terakhir']."' 
				and aktif='1' and riwayat_jabatan.kode_unit=unit_kerja.kode_unit");
					if ($queryJBT) {
						while ($daftarDETjabatan=mysql_fetch_array($queryJBT)) { 
						$tmtjabatan=$daftarDETjabatan['tmt_jabatan'];
						$unitjabatan=$daftarDETjabatan['nama_unit'];
						 }	
					}
				
				 }	
			}
			
			$kode_unit=$daftar['kode_unit'];
			//$unit_kerja
			$tgl_lahir=$daftar['tgl_lahir'];
			//$usia = $this->datediff($daftar['tgl_lahir'],$tgl);
			//$umur = $usia['years']." tahun ".$usia['months']." bulan";
			$tmt_cpns=$daftar['tmt_cpns'];
				
			$tgl = date('Y-m-d');
			$masa_kerja =$daftar['tmt_cpns'];
			$masa_kerja = datediff($daftar['tmt_cpns'],$tgl);
			$mk_tambahan_th=$daftar['mk_tambahan_th'];
			$mk_tambahan_bl=$daftar['mk_tambahan_bl'];	
			$masa_kerja_seluruhnya_th=$masa_kerja['years'] + $mk_tambahan_th;
			$masa_kerja_seluruhnya_bl=$masa_kerja['months'] + $mk_tambahan_bl;
			$masa_kerja_seluruhnya = $masa_kerja_seluruhnya_th." tahun ".$masa_kerja_seluruhnya_bl." bulan";
			
			$photo = 'https://simpega.um.ac.id/admin/public/photo/photo_' . $kd_pegawai . '.jpg';

		}	
	}
	
	
?>


 <form name="pegawaidetails" id="pegawaidetails" method="POST" action="">	
		<table cellpadding="1" cellspacing="0" width="100%" >
		<tr>
		  <td colspan="3" align="left"><div align="left" class="ch-title"><h3>Data Pribadi</h3></div> </td>
		  <td align="left">&nbsp;</td>
		  <td colspan="3" align="left"><div align="left" class="ch-title"><h3>Data Kepegawaian</h3></div></td>
		  </tr>
		<tr>
		  <td width="80" align="left">Jenis Pegawai</td>
		  <td align="left" width="15">:</td>
		  <td align="left" width="300"><?=$jenis_pegawai;?></td>
		  <td align="left" width="25">&nbsp;</td>
		  <td align="left" width="120">Karpeg</td>
		  <td align="left" width="15">:</td>
		  <td align="left"><?=$karpeg?></td>
		</tr>
		
	<?php
		  $query = mysql_query("select status_perkawinan from status_perkawinan where status_kawin='".$status_kawin."'");					
			if ($query) {
				$datastkawin=mysql_fetch_array($query); 
				$status_kawin = $datastkawin['status_perkawinan'];
			}
			else{
				$status_kawin = '';
			}
	?>
		
		<tr>
		  <td width="126" align="left">Nama Kecil</td>
		  <td width="7">:</td>
		  <td width="230" align="left"><input name="drh_nama_kecil" id="drh_nama_kecil" width="200px" type="text" size="50" value="<?=$drh_nama_kecil; ?>" /> </td>
		  <td align="left">&nbsp;</td>
		  <td align="left">Loker</td>
		  <td align="left">:</td>
		  <td align="left"><?=$loker?></td>
		</tr>
		<tr>
		   <td width="126" align="left">Agama</td>
		  <td width="7">:</td>
		  <td width="230" align="left">
		 
		  <select name="kd_agama" class="kotakteks3" id="kd_agama" onBlur="this.style.backgroundColor='#fafafa'" onFocus="this.style.backgroundColor='#ffffff'">
			 <?php
				$querylink = mysql_query("select * from agama");
				if ($querylink) {
					while ($daftaragama=mysql_fetch_array($querylink)) { ?>
					<option value="<?=$daftaragama['kd_agama']?>"  <?php echo ($kd_agama==$daftaragama['kd_agama'])?"selected=selected":""; ?>><?=$daftaragama['agama']?></option>			
					<? }	
				}
			?>
		</select>			  </td>
		  <td align="left">&nbsp;</td>
		  <td align="left">Status Pegawai </td>
		  <td align="left">:</td>
		  <td align="left"><?=$status_pegawai?></td>
		</tr>
		<tr>
			 <td align="left">Golongan Darah</td>
		  <td>:</td>
		  <td align="left">
		  <select name="gol_darah" class="kotakteks3" id="gol_darah" onBlur="this.style.backgroundColor='#fafafa'" onFocus="this.style.backgroundColor='#ffffff'">
					<option value="O"  <?php echo ($gol_darah=="O")?"selected=selected":""; ?>>O</option>
					<option value="A"  <?php echo ($gol_darah=="A")?"selected=selected":""; ?>>A</option>
					<option value="B"  <?php echo ($gol_darah=="B")?"selected=selected":""; ?>>B</option>
					<option value="AB"  <?php echo ($gol_darah=="AB")?"selected=selected":""; ?>>AB</option>			
		</select>		 	  </td>
			<td align="left">&nbsp;</td>
			<td align="left">TMT CPNS </td>
			<td align="left">:</td>
			<td align="left"><?
            list($y, $m, $d) = explode('-', $tmt_cpns);
            $str_tmt_cpns = $d . "-" . $m . "-" . $y;
			$tmt_cpns=$str_tmt_cpns;          
            echo $tmt_cpns?></td>
		</tr>
		<tr>
			<td align="left">No KTP</td>
		  <td>:</td>
		  <td align="left"><input type="text" name="no_KTP" id="no_KTP"  value="<?=$no_KTP?>"	/></td>
			<td align="left">&nbsp;</td>
			<td align="left">Masa Kerja </td>
			<td align="left">:</td>
			<td align="left"><?=$masa_kerja_seluruhnya?></td>
		</tr>
		<tr>
		  <td align="left">HP Utama</td>
		  <td>:</td>
		  <td align="left"> <input name="hp" id="hp" width="50px" type="text" size="20" value="<?=$hp;?>" maxlength="15" />	
		  <!--<font color="#FF0000"><br />* nomor utama dan diharapkan tidak berganti-ganti</font>	-->  </td>
		  <td align="left">&nbsp;</td>
		  <td align="left">&nbsp;</td>
		  <td align="left">&nbsp;</td>
		  <td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left">No. Telp</td>
		  <td>:</td>
		  <td align="left"> <input name="no_telp" id="no_telp" width="50px" type="text" size="30" value="<?=$no_telp;?>" /></td>
			<td align="left">&nbsp;</td>
			<td align="left">Jabatan</td>
			<td align="left">:</td>
			<td align="left"><?=$jabatan?></td>
		</tr>
		<tr>
			 <td align="left">Email</td>
		  <td>:</td>
		  <td align="left"> <input name="email" id="email" width="200px" type="text" size="50" value="<?= $email; ?> " onSubmit="return Validasi();" /></td>
			<td align="left">&nbsp;</td>
			<td align="left">TMT Jabatan </td>
			<td align="left">:</td>
			<td align="left"><?
            list($y, $m, $d) = explode('-', $tmtjabatan);
            $str_tmt_jabatan_terakhir = $d . "-" . $m . "-" . $y;
			$tmtjabatan=$str_tmt_jabatan_terakhir;
            if ($tmtjabatan==0)
            {
                $tmtjabatan='-';
            }          
            echo $tmtjabatan?>            </td>
		</tr>
		<tr>
			<td align="left">Alamat Asal</td>
			<td> : </td>
			<td rowspan="2" align="left"><textarea rows="3" id="alamat_asal" name="alamat_asal" cols="50"><?=$alamat_asal?> </textarea></td>
			<td align="left">&nbsp;</td>
			  <td align="left">Unit Kerja</td>
			  <td align="left">:</td>
			  <td align="left"><?=$unitjabatan?></td>
		</tr>
		<tr>
		  <td align="left">&nbsp;</td>
		  <td>&nbsp;</td>
		  <td align="left">&nbsp;</td>
		  <td align="left">&nbsp;</td>
		  <td align="left">&nbsp;</td>
		  <td align="left">&nbsp;</td>
		</tr>
		<tr>
		  <td align="left">Alamat di Malang</td>
		  <td>:</td>
		  <td rowspan="4" align="left"> RT : <input size="5px" type="text" name="rt" id="rt" value="<?= $rt?>" /> 
		  <br />RW: <input size="5px" type="text" name="rw" id="rw" value="<?=$rw?>" />
		  <br />Desa/Kelurahan : 
		   <select name="kd_kelurahan" class="kotakteks3" id="kd_kelurahan" onBlur="this.style.backgroundColor='#fafafa'" onFocus="this.style.backgroundColor='#ffffff'">
			 <?php
				$querylink = mysql_query("select * from kelurahan");
				if ($querylink) {
					while ($daftarkel=mysql_fetch_array($querylink)) { ?>
					<option value="<?=$daftarkel['kd_kelurahan']?>"  <?php echo ($kd_kelurahan==$daftarkel['kd_kelurahan'])?"selected=selected":""; ?>><?=$daftarkel['nama_kelurahan']?></option>			
					<? }	
				}
			?>
		</select>
		 
		  <br />
		  <textarea rows="3" id="alamat" name="alamat" cols="50"><?=$alamat?> </textarea>
		  <br />
		  Kode Pos : <input name="kode_pos" id="kode_pos" width="20px" type="text" size="5"  value="<?=$kode_pos;?>" />	</td>
		  <td align="left">&nbsp;</td>
		   <td align="left">NPWP</td>
		   <td align="left">:</td>
		   <td align="left"><?=$npwp?></td>
		</tr>
	<tr>
		  <td align="left">&nbsp;</td>
		  <td>&nbsp;</td>
		  <td align="left">&nbsp;</td>
		  <td align="left">ASKES</td>
		  <td align="left">:</td>
		  <td align="left"><?=$no_ASKES?></td>
	</tr>
		<tr>
		  <td align="left">&nbsp; </td>
		  <td>&nbsp;</td>
		  <td align="left">&nbsp;</td>
		  <td align="left">Taspen</td>
		  <td align="left">:</td>
		  <td align="left"><?=$taspen?></td>
		</tr>
		<tr>
		  <td align="left">&nbsp;</td>
		  <td>&nbsp;</td>
		  <td align="left">&nbsp;</td>
		  <td align="left">&nbsp;</td>
		  <td align="left">&nbsp;</td>
		  <td align="left">&nbsp;</td>
		</tr>
		<tr>
		  <td align="center" colspan="7"><input id="submit" size="100px" name="submitdatapegawai" type="submit" value="  Simpan " /></td>
		</tr>
		</table>
		
		<table class="bentuktulisan">
		<tr><td>

		</td></tr>
          <tr>
            <td width="500" ><div class="ch-title">
              <h3>Ciri-ciri Fisik dan Kegemaran</h3>
            </div></td>
          </tr>
        </table>
		<table  class="bentuktulisan">
		<tr>
                    <td colspan="5"  align="left"><div align="left" class="ch-title"><h3>Data Keterangan Badan</h3></div></td>
          </tr>
                <tr>
                    <td  align="left">Tinggi</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
					<input name="drh_tinggi" id="drh_tinggi" width="200px" type="text" size="5" value="<?= $drh_tinggi; ?> " /> cm
					</td>
                </tr>	
				<tr>
                    <td  align="left">Berat Badan</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
					<input name="drh_bb" id="drh_bb" width="200px" type="text" size="5" value="<?= $drh_bb; ?> " /> kg
					</td>
                </tr>
				<tr>
                    <td  align="left">Rambut</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
					<input name="drh_rambut" id="drh_rambut"  type="text" size="50" value="<?= $drh_rambut; ?> " />
					</td>
                </tr>
				<tr>
                    <td  align="left">Bentuk Muka</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
					<input name="drh_bentuk_muka" id="drh_bentuk_muka" type="text" size="50" value="<?=$drh_bentuk_muka; ?> " />
					</td>
                </tr>
				<tr>
                    <td  align="left">Warna Kulit</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
					<input name="drh_warna_kulit" id="drh_warna_kulit" type="text" size="50" value="<?= $drh_warna_kulit; ?> " />
					</td>
                </tr>
				<tr>
                    <td  align="left">Ciri-ciri</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
					<input name="drh_ciri_ciri" id="drh_ciri_ciri" type="text" size="50" value="<?= $drh_ciri_ciri; ?> " />
					</td>
                </tr>
				<tr>
                    <td  align="left">Cacat Tubuh</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
					<input name="drh_cacat" id="drh_cacat" type="text" size="50" value="<?= $drh_cacat; ?> " />
					</td>
                </tr>
				
				<tr>
                    <td colspan="5"  align="left">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5"  align="left"><div align="left" class="ch-title"><h3>Data Hobi/Kegemaran</h3></div></td>
                </tr>
                <tr>
                    <td  align="left">Hobi Kesenian</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
					<textarea name="hobi_kesenian" id="hobi_kesenian" cols="100" rows="2"><?= $hobi_kesenian; ?> </textarea>
					</td>
                </tr>	
				<tr>
                    <td  align="left">Hobi Olahraga</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
					<textarea name="hobi_olahraga" id="hobi_olahraga" cols="100" rows="2"><?=$hobi_olahraga; ?> </textarea>
					</td>
                </tr>
				<tr>
                    <td  align="left">Hobi Lainnya</td>
                    <td> : </td>
                    <td colspan="3"  align="left">
					<textarea name="hobi_lain" id="hobi_lain" cols="100" rows="2"><?= $hobi_lain; ?> </textarea>
					</td>
                </tr>	
				<tr>
				  <td align="center" colspan="7"><input id="submit" size="100px" name="submitdatapegawai" type="submit" value="  Simpan " /></td>
				</tr>
		</table>
		<table  class="bentuktulisan">
		<tr><td>
		
		</td></tr>
          <tr>
            <td width="143" ><div class="ch-title">
              <h3>Data Keluarga </h3>
            </div></td>
          </tr>
        </table>

		<table border="1" cellpadding="0" cellspacing="0"  class="bentuktulisan">
          <tr class="trhead">
            <th class="trhead" width="38" align="left">No
            </th>
            <th class="trhead" width="209">Nama            </th>
            <th class="trhead" width="137" align="center">Tanggal Lahir </th>
            <th class="trhead" width="119" align="center">Tanggal Menikah</th>
            <th class="trhead" width="129" align="left">Status Keluarga 
                         
            </th>
          </tr>
          <?
			$i = 0;
			$query = mysql_query("select * from pasangan_pegawai where kd_pegawai='".$kd_pegawai."' and aktif='1' order by aktif");	
			while ($list_pasangan=mysql_fetch_array($query)) { 
	   		$i++;
	   		if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			if ($list_pasangan['tanggal_lahir']=='0000-00-00') {
                $str_tanggal_lahir = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $list_pasangan['tanggal_lahir']);
                $str_tanggal_lahir = $d . "-" . $m . "-" . $y;
            }
            $list_pasangan['tanggal_lahir']=$str_tanggal_lahir;
			
			if ($list_pasangan['tanggal_nikah']=='0000-00-00') {
                $str_tanggal_nikah = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $list_pasangan['tanggal_nikah']);
                $str_tanggal_nikah = $d . "-" . $m . "-" . $y;
            }
            $list_pasangan['tanggal_nikah']=$str_tanggal_nikah;
			
			$querysk = mysql_query("select * from status_keluarga where kd_status_keluarga='".$list_pasangan['kd_status_keluarga']."'");	
			while ($listsk=mysql_fetch_array($querysk)) { 
				$status_keluarga=$listsk['status_keluarga']; 
			}
		 ?>
          <tr class="<?=$bgColor?>">
            <td align="left" class="colEvn"><?=$i?></td>
            <td><?= $list_pasangan['nama_pasangan']; ?></td>
            <td align="center"><?= $list_pasangan['tanggal_lahir']; ?></td>
            <td align="center"><?= $list_pasangan['tanggal_nikah']; ?></td>
            <td align="left"><?=$status_keluarga?></td>
          </tr>
          <? } ?>
         
	<?
			$j = $i;
			$query = mysql_query("select * from anak_pegawai where kd_pegawai='".$kd_pegawai."' order by kd_anak");	
			while ($jns_anak=mysql_fetch_array($query)) { 
		   $j++;
		   if (($j%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			if ($jns_anak['tgl_lahir']=='0000-00-00') {
                $str_tgl_lahir = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_anak['tgl_lahir']);
                $str_tgl_lahir = $d . "-" .$m . "-" . $y;
            }
            $jns_anak['tgl_lahir']=$str_tgl_lahir;
			
			$querysk = mysql_query("select * from status_keluarga where kd_status_keluarga='".$jns_anak['kd_status_keluarga']."'");	
			while ($listsk=mysql_fetch_array($querysk)) { 
				$status_keluarga=$listsk['status_keluarga']; 
			}
		 ?>
          <tr class="<?=$bgColor?>">
            <td align="left" class="colEvn"><?=$j;?></td>
            <td><?= $jns_anak['nama_anak']; ?></td>
            <td align="center"><?= $jns_anak['tgl_lahir']; ?></td>
            <td align="center">-</td>
            <td align="left"><?=$status_keluarga?>
			</td>
          </tr>
          <? } ?>
          
        </table>
		<br />
		
		<table  class="bentuktulisan">
		<tr><td>
		
		</td></tr>
          <tr>
            <td colspan="7"  align="left">
              <div class="ch-title"><h3>Data Riwayat </h3></div>
            </td>
            
          </tr>
		 <tr>
            <td colspan="3" align="center">
              <h4>Riwayat Pendidikan </h4>
          </td>
            
          </tr>
	  </table>
	
		  
		  <table border="1" cellpadding="0" cellspacing="0"  class="bentuktulisan">
          <tr class="trhead">
            <th class="trhead" width="38" align="left">No</td>
            <th class="trhead" width="95">Pendidikan
            <th class="trhead" width="192" align="left">Keterangan<!--<th class="trhead" width="130" align="left">Jurusan</td>-->
            <th class="trhead" width="79" align="left">Tahun Lulus </td>
            <th class="trhead" width="98" align="left">Tempat</td>            </tr>
<?
	$i = 0;
	$query = mysql_query("select * from riwayat_pendidikan where kd_pegawai='".$kd_pegawai."' order by th_lulus");	
			while ($pendidikan=mysql_fetch_array($query)) { 
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	   $querydidik = mysql_query("select * from pendidikan where id_pendidikan='".$pendidikan['id_pendidikan']."'");					
		if ($querydidik) {
			$datadidik=mysql_fetch_array($querydidik); 
			$didik = $datadidik['nama_pendidikan'];
		}
	?>
          <tr class="<?=$bgColor?>">
            <td align="left" class="colEvn"><?=$i;?></td>
            <td><?= $didik; ?></td>
            <td align="left">&nbsp;<?=$pendidikan['keterangan']; ?></td>
            <td align="left"><?= $pendidikan['th_lulus']; ?></td>
            <td align="left"><?= $pendidikan['tempat_studi']?></td>
            </tr>
			<? } ?>
	
        </table>
		<br />
<?
					if ($id_jns_pegawai == 5){
                ?>
                 <h4 align="center">Riwayat Tugas Belajar </h4>
		<table border="1" cellpadding="0" cellspacing="0"  class="bentuktulisan">
		<tr class="trhead">
			<th width="20" >No</th>
			<th class="colEvn" width="103" >TMT Tugas Belajar </th>
			<th  width="134">Batas Akhir Tugas Belajar </th>
			<th  width="97">Tingkat</th>
			<th  width="181">Bidang Studi </th>
			<th  width="216">Tempat Studi </th>
			<th  width="167">Biaya</th>	
			
		</tr>
	
		<?
		$i = 0;
		$query = mysql_query("select * from riwayat_tugas_belajar where kd_pegawai='".$kd_pegawai."' order by id_riwayat_tb");	
			while ($tugas_belajar=mysql_fetch_array($query)) { 
		   $i++;
		   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			list($y, $m, $d) = explode('-', $tugas_belajar['tmt_tb']);
			$str_tmt_tb = $d . "-" . $m . "-" . $y;
			$tugas_belajar['tmt_tb']=$str_tmt_tb;
			list($y, $m, $d) = explode('-', $tugas_belajar['batas_akhir_tb']);
			$str_batas_tb = $d . "-" . $m . "-" . $y;
			$tugas_belajar['batas_akhir_tb']=$str_batas_tb;			
			$querydidik = mysql_query("select * from pendidikan where id_pendidikan='".$tugas_belajar['id_pendidikan_tb']."'");					
			if ($querydidik) {
				$datadidik=mysql_fetch_array($querydidik); 
				$pendidikan = $datadidik['nama_pendidikan'];
			}
		?>
		<tr class="<?=$bgColor?>">
			<td align="center" class="colEvn"><?=$i;?></td>
			<td align="left" ><?= $tugas_belajar['tmt_tb']; ?></td>
			<td align="center" ><?= $tugas_belajar['batas_akhir_tb'];?></td>
			<td align="center" ><?= $pendidikan; ?></td>
			<td align="center" ><?= $tugas_belajar['bidang_studi_tb']?></td>
			<td align="center" ><?= $tugas_belajar['tempat_studi_tb']?></td>
			<td align="center" ><?= $tugas_belajar['biaya_tb']; ?></td>
		</tr>
		<? } ?>
	</table>
                <? } ?>
				<br />
		  <h4 align="center">Riwayat Kepangkatan/Golongan Ruang Penggajian</h4>
		  <table border="1" cellpadding="0" cellspacing="0"  class="bentuktulisan">
            <tr class="trhead">
              <th width="38" height="31" align="left" class="trhead">No
                </td>              </th>
              <th width="175" class="trhead">Pangkat, Gol/Ruang</th>
              <th class="trhead" width="85" align="left">TMT</th>
              <th class="trhead" width="96" align="left">Gaji Pokok </th>
              <th class="trhead" width="164" align="left">Nomor SK </th>
              <th class="trhead" width="109" align="left">Tanggal SK </th>
              <th class="trhead" width="72" align="left">Masa Kerja </th>
              <th class="trhead" width="72" align="left">Keterangan</th>
            </tr>
			<?
	$i = 0;
	$query = mysql_query("select * from riwayat_gol_kepangkatan where kd_pegawai='".$kd_pegawai."' order by aktif ,status_pegawai, id_golpangkat, tmt_pangkat");	
			while ($jns_pangkat=mysql_fetch_array($query)) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
        if ($jns_pangkat['tgl_SK_pangkat']=='0000-00-00') {
                $str_sk_pangkat = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_pangkat['tgl_SK_pangkat']);
                $str_sk_pangkat = $d . "-" .$m . "-" . $y;
            }
        $jns_pangkat['tgl_SK_pangkat']=$str_sk_pangkat;
        if ($jns_pangkat['tmt_pangkat']=='0000-00-00') {
                $str_tmt_pangkat = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_pangkat['tmt_pangkat']);
                $str_tmt_pangkat = $d . "-" .$m. "-" . $y;
            }
        $jns_pangkat['tmt_pangkat']=$str_tmt_pangkat;
		$querygol = mysql_query("select * from golongan_pangkat where id_golpangkat='".$jns_pangkat['id_golpangkat']."'");					
		if ($querygol) {
			$datagol=mysql_fetch_array($querygol); 
			$gol = $datagol['pangkat'].", ".$datagol['golongan'];
		}
		
	?>
            <tr class="<?=$bgColor?>">
              <td align="center" class="colEvn"><?=$i;?></td>

              <td><?=$gol;?></td>
              <td align="left"><?= $jns_pangkat['tmt_pangkat']; ?></td>
              <td align="right"><?= number_format($jns_pangkat['gaji_pokok']); ?></td>
              <td align="left"><?= $jns_pangkat['no_SK_pangkat']; ?></td>
              <td align="left"><?= $jns_pangkat['tgl_SK_pangkat']; ?></td>
              <td align="left"><?= $jns_pangkat['mk_tahun']; ?>
th
  <?= $jns_pangkat['mk_bulan']; ?>
bl</td>
              <td align="left"><?= $jns_pangkat['keterangan'];?></td>
            </tr>
			<? } ?>
	
          </table>
		  <br />
		  <h4 align="center">Riwayat Mutasi </h4>
	<table border="1" cellpadding="0" cellspacing="0"  class="bentuktulisan">
	<tr class="trhead">
		<th >No</th>
		<th width="150" class="colEvn" >Jenis Mutasi</th>
		<th >No. SK </th>
		<th class="colEvn" >Tanggal SK </th>
		<th >TMT Mutasi</th>
		<th >Gol/Ruang</th>
		<th width="300" class="colEvn" >Keterangan Mutasi</th>
		<th >Unit Kerja Asal</th>
		<th class="colEvn" >Unit Kerja Baru </th>
		
	</tr>

		<?
		$i = 0;
		$query = mysql_query("select * from mutasi where kd_pegawai='".$kd_pegawai."' order by id_mutasi");	
			while ($jns_mutasi=mysql_fetch_array($query)) {
		   $i++;
		   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
		   if ($jns_mutasi['tgl_SK']=='0000-00-00') {
					$str_sk_mutasi = '-';
				}
				else {
					list($y, $m, $d) = explode('-', $jns_mutasi['tgl_SK']);
					$str_sk_mutasi = $d . "-" .$m. "-" . $y;
				}
			$jns_mutasi['tgl_SK']=$str_sk_mutasi;
			if ($jns_mutasi['tmt_SK']=='0000-00-00') {
					$str_tmt_SK = '-';
				}
				else {
					list($y, $m, $d) = explode('-', $jns_mutasi['tmt_SK']);
					$str_tmt_SK = $d . "-" .$m. "-" . $y;
				}
			$jns_mutasi['tmt_SK']=$str_tmt_SK;
		?>
		<tr class="<?=$bgColor?>">
			<td align="center" class="colEvn"><?=$i;?></td>
			<td align="left" ><?= $jns_mutasi['nama_mutasi']; ?></td> <!--$mutasi_assoc[$jns_mutasi['nama_mutasi']]-->
			<td align="left" ><?= $jns_mutasi['no_SK']; ?></td>
			<td align="center" ><?= $jns_mutasi['tgl_SK']; ?></td>
			<td align="center" ><?= $jns_mutasi['tmt_SK']; ?></td>
			<td align="left" ><?= $jns_mutasi['id_golpangkat']; ?></td>
			<td align="left" ><?= $jns_mutasi['ket_mutasi']; ?></td>
			 <?php
				  $query = mysql_query("select * from unit_kerja where kode_unit='".$jns_mutasi['kode_unit_kerja_asal']."'");					
					if ($query) {
						$dataunit=mysql_fetch_array($query); 
						$unit_kerja_asal = $dataunit['nama_unit'];
					}
			 ?>
			<td align="left" ><?= $unit_kerja_asal; ?></td>
			<?php
				  $query = mysql_query("select * from unit_kerja where kode_unit='".$jns_mutasi['kode_unit_kerja_baru']."'");					
					if ($query) {
						$dataunit=mysql_fetch_array($query); 
						$unit_kerja_baru = $dataunit['nama_unit'];
					}
			 ?>
			<td align="left" ><?= $unit_kerja_baru; ?></td>
			
		</tr>
		<? } ?>

	</table>
		  <br />
		  <?
					if ($id_jns_pegawai != 1){
                ?>
		<!--  <h4 align="center">Riwayat Angka Kredit </h4>
		<table border="1" cellpadding="0" cellspacing="0"  class="bentuktulisan">
		<tr class="trhead">
			<th >No</th>
			<th class="colEvn" width="100" >Tanggal</th>
			<th  width="250">Angka Kredit</th>	
		</tr>
	
		<?
		$i = 0;
		$query = mysql_query("select * from pegawai_rincian_ak where kd_pegawai='".$kd_pegawai."' order by id_rincian_ak");	
			while ($angka_kredit=mysql_fetch_array($query)) {
		   $i++;
		   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			if ($angka_kredit['tmt_ak']=='0000-00-00') {
					$str_ak = '-';
				}
				else {
					list($y, $m, $d) = explode('-', $angka_kredit['tmt_ak']);
					$str_ak = $d . "/" . $m . "/" . $y;
				}
			$angka_kredit['tmt_ak']=$str_ak;
			
		?>
		<tr class="<?=$bgColor?>">
			<td align="center" class="colEvn"><?=$i;?></td>
			<td align="center" ><?= $angka_kredit['tmt_ak']; ?></td>
			<td align="center" ><?= $angka_kredit['nilai']; ?></td>
		</tr>
	
		<? } ?>
		<tr class="trhead">
			<td align="center" colspan="2" >JUMLAH</td>	
			<td align="center" class="colEvnRight">
				<?php
			
						$query = mysql_query("
						SELECT sum(nilai) AS jumlah FROM `pegawai_rincian_ak` WHERE (kd_pegawai='".$kd_pegawai."') group by kd_pegawai");							
	
						if ($query) {
								$data=mysql_fetch_array($query);
								
								$jumlahx=($data['jumlah']== 0)?"-":$data['jumlah'];
								echo $jumlahx;				
								}
						else
							echo "-";					
						
				?>
			</td>
		</tr>
		
	</table>-->
		  
		  <br />
		  <h4 align="center">Riwayat Jabatan Fungsional </h4>
		  <table border="1" cellpadding="0" cellspacing="0"  class="bentuktulisan">
            <tr class="trhead">
              <th class="trhead" width="38" align="left">No
                  </td>              </th>
              <th width="146" class="trhead">Jabatan</th>
              <th class="trhead" width="68" align="left">TMT</th>
              <th class="trhead" width="183" align="left">Nomor SK </th>
              <th class="trhead" width="86" align="left">Unit Kerja </th>
            </tr>
			<?
	$i = 0;
	$queryjabatan = mysql_query("select * from riwayat_jabatan where kd_pegawai='".$kd_pegawai."' order by id_jabatan,tmt_jabatan,id_riwayat_jabatan");	
	while ($jns_jabatan=mysql_fetch_array($queryjabatan)) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
       if ($jns_jabatan['tgl_sk_jabatan']=='0000-00-00') {
                $str_sk_jabatan = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_jabatan['tgl_sk_jabatan']);
                $str_sk_jabatan = $d . "/" . $m . "/" . $y;
            }
        $jns_jabatan['tgl_sk_jabatan']=$str_tmt_jabatan;
		
		if ($jns_jabatan['tmt_jabatan']=='0000-00-00') {
                $str_tmt_jabatan = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_jabatan['tmt_jabatan']);
                $str_tmt_jabatan = $d . "/" . $m . "/" . $y;
            }
        $jns_jabatan['tmt_jabatan']=$str_tmt_jabatan;
		
        if ($jns_jabatan['tgl_selesai']=='0000-00-00') {
                $str_tgl_selesai = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_jabatan['tgl_selesai']);
                $str_tgl_selesai = $d . "/" . $m . "/" . $y;
            }
        $jns_jabatan['tgl_selesai']=$str_tgl_selesai;
		
	?>
            <tr class="<?=$bgColor?>">
              <td align="left"class="colEvn"><?=$i;?></td>
			  <?php
				  $query = mysql_query("select * from jabatan where id_jabatan='".$jns_jabatan['id_jabatan']."'");					
					if ($query) {
						$datakab=mysql_fetch_array($query); 
						$nama_jabatan = $datakab['nama_jabatan'];
					}
			 ?>
              <td><?= $nama_jabatan; ?></td>
              <td align="left"><?= $jns_jabatan['tmt_jabatan']; ?></td>
              <td align="left"><?= $jns_jabatan['no_sk_jabatan']; ?></td>
				<?php
					  $query = mysql_query("select * from unit_kerja where kode_unit='".$jns_jabatan['kode_unit']."'");					
						if ($query) {
							$dataunit=mysql_fetch_array($query); 
							$unit_kerja = $dataunit['nama_unit'];
						}
				 ?>
              <td align="left"><?= $unit_kerja; ?></td>
            </tr>
	<? } 
	}?>
	
          </table>
	
		  <br />
		  <h4 align="center">Riwayat Pelatihan </h4>
		  <table border="1" cellpadding="0" cellspacing="0"  class="bentuktulisan">
            <tr class="trhead">
              <th class="trhead" width="38" align="left">No
                </td>              </th>
              <th class="trhead" width="230">Nama Pelatihan </th>
              <th class="trhead" width="57" align="left">Tahun </td>              </th>
              <!--<th class="trhead" width="130" align="left">Lama Pelatihan </th>-->
              <th class="trhead" width="242" align="left">Tempat</th>
            </tr>
			<?
	$i = 0;
	$query = mysql_query("select * from riwayat_pelatihan where kd_pegawai='".$kd_pegawai."' order by id_riwayat_pelatihan");	
			while ($pelatihans=mysql_fetch_array($query)) {
			   $i++;
			   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
			?>
            <tr class="<?=$bgColor?>">
				  <td align="left" class="colEvn"><?=$i;?></td>
				  <td><?= $pelatihans['nama_pelatihan']; ?></td>
				  <td align="left"><?= $pelatihans['tahun']; ?></td>
				  <!--<td align="left"><?= $pelatihans['lama_hari']; ?></td>-->
				  <td align="left"><?= $pelatihans['tempat_pelatihan']; ?></td>
            </tr>
			<? } ?>
	
          </table>
		  <h4 align="center">Riwayat DP3 </h4>
	<table border="1" cellpadding="0" cellspacing="0"  class="bentuktulisan">
	<tr class="trhead">
		<th width="121" >No</th>
		<th width="191" class="colEvn" >Nilai DP3</th>
		<th width="104" >Tahun  </th>
	</tr>
	<?
	$i = 0;
	if ($list_dp3!=FALSE){
	foreach ($list_dp3 as $dp3) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$i;?></td>
		<td align="left" ><?= $dp3['nilai_dp3']; ?></td>
		<td align="center" ><?= $dp3['tahun']; ?></td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
		  <br />
		  <h4 align="center">Riwayat Penghargaan </h4>
		 <table border="1" cellpadding="0" cellspacing="0"  class="bentuktulisan">
            <tr class="trhead">
              <th class="trhead" width="38" align="left">No
                </td>              </th>
              <th class="trhead" width="230">Nama Penghargaan </th>
              <th class="trhead" width="57" align="left">Tahun </td>              </th>
              <!--<th class="trhead" width="130" align="left">Lama Pelatihan </th>-->
              <th class="trhead" width="242" align="left">Keterangan</th>
            </tr>
			<?
	$i = 0;
	$querypenghargaan = mysql_query("select * from riwayat_penghargaan where kd_pegawai='".$kd_pegawai."' order by id_riwayat_penghargaan");	
	while ($penghargaan=mysql_fetch_array($querypenghargaan)) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
            <tr class="<?=$bgColor?>">
              <td align="left" class="colEvn"><?=$i;?></td>
			  <?php
					  $query = mysql_query("select * from jenis_penghargaan where id_jns_penghargaan='".$penghargaan['jenis_penghargaan']."'");					
						if ($query) {
							$dataunit=mysql_fetch_array($query); 
							$nama_penghargaan = $dataunit['nama_penghargaan'];
						}
				 ?>
              <td width="350"><?= $nama_penghargaan; ?></td>
              <td align="left"><?= $penghargaan['tahun']; ?></td>
              <td align="left"><?= $penghargaan['keterangan']; ?></td>
            </tr>
			<? } ?>
	
    </table>
	<!--	  <?
					if ($id_jns_pegawai != 1){
                ?>
		  <h4 align="center">Riwayat Karya Ilmiah </h4>
		 <table border="1" cellpadding="0" cellspacing="0"  class="bentuktulisan">
            <tr class="trhead">
              <th width="31" >No</th>
              <th width="162" class="colEvn" >Jenis Karya</th>
              <th width="150" >Judul </th>
              <th width="150" class="colEvn">Bidang Ilmu </th>
              <th width="128" >Dimuat di </th>
              <th width="78" class="colEvn">Tahun </th>
              <th  width="130">Akreditasi </th>
             
            </tr>
            <?
	$i = 0;
	$query = mysql_query("select * from riwayat_karya_ilmiah where kd_pegawai='".$kd_pegawai."' order by id_riwayat_karya");	
			while ($penghargaan=mysql_fetch_array($query)) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
       
	?>
            <tr class="<?=$bgColor?>">
              <td align="center" class="colEvn"><?=$i;?></td>
			  <?php
					  $query = mysql_query("select * from jenis_karya_ilmiah where id_jenis='".$karya_ilmiah['jenis_karya']."'");					
						if ($query) {
							$dataunit=mysql_fetch_array($query); 
							$nama_karya = $dataunit['nama_jenis'];
						}
				 ?>
              <td align="center" ><?= $nama_karya; ?></td>
              <td align="center" ><?= $karya_ilmiah['judul_karya']; ?></td>
              <td align="center" ><?= $karya_ilmiah['bidang_ilmu']; ?></td>
              <td align="center" ><?= $karya_ilmiah['dimuat']; ?></td>
              <td align="left" ><?= $karya_ilmiah['tahun']; ?></td>
              <td align="left" ><?= $karya_ilmiah['akreditasi']; ?></td>
             
            </tr>
            <? } ?>
           
    </table>-->

		   <br />

      </form>		   
		  <p>&nbsp;</p>

 
<?php
}				
if((isset($_POST['submitdatapegawai'])))	
{
$sql="UPDATE `pegawai`  SET  no_KTP='".$_POST['no_KTP']."' ,
`kd_agama`='".$_POST['kd_agama']."',`alamat`='".$_POST['alamat']."',`rt`='".$_POST['rt']."',`rw`='".$_POST['rw']."',`kd_kelurahan`='".$_POST['kd_kelurahan']."',`kode_pos`='".$_POST['kode_pos']."',`no_telp`='".$_POST['no_telp']."',`alamat_asal`='".$_POST['alamat_asal']."',`email`='".$_POST['email']."',`hp`='".$_POST['hp']."', `drh_nama_kecil`='".$_POST['drh_nama_kecil']."', `drh_tinggi`='".$_POST['drh_tinggi']."'
, `drh_bb`='".$_POST['drh_bb']."', `drh_rambut`='".$_POST['drh_rambut']."', `drh_bentuk_muka`='".$_POST['drh_bentuk_muka']."', `drh_warna_kulit`='".$_POST['drh_warna_kulit']."', `drh_ciri_ciri`='".$_POST['drh_ciri_ciri']."', `drh_cacat`='".$_POST['drh_cacat']."', `hobi_kesenian`='".$_POST['hobi_kesenian']."', `hobi_olahraga`='".$_POST['hobi_olahraga']."', `hobi_lain`='".$_POST['hobi_lain']."'
WHERE kd_pegawai = '".$kd_pegawai."'";
	$query = mysql_query($sql);
	//redirect('pegawai/caripegawai/view/'.$kd_pegawai);
	echo $sql;
}
?>
