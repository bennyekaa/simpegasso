<div class="row" style="padding-bottom: 0px;">
    <div class="col-md-4">
        <div class="center-block text-center">
            <!-- <img src="<?php echo URL_IMG ?>_default_avatar_male.jpg" class="img-responsive" alt="Foto Pegawai" style="margin-top: 10px;"> -->
            <img src="<?php echo base_url() ?>public/photo/photo_<?php echo $detail['photo'] ?>.jpg" class="img-responsive img-thumbnail" alt="Foto <?php echo $detail['nama_pegawai']; ?>" style="margin-top: 10px;">
        </div>
        <?php if ($detail['id_jns_pegawai'] == '5') {
            $RG = $detail['nama_pegawai'];
            $namapencarian = str_replace(' ', '-', $RG);
        ?>
            <div class="col-md-12 text-center">
                <button type="button" onclick="window.open('https://sinta.ristekbrin.go.id/authors/detail?id=<?php echo $id_sinta; ?>&view=overview', '')" class="btn btn-primary btn-lg btn-block">Sinta</button>
                <button type="button" onclick="window.open('https://scholar.google.co.id/citations?user=<?php echo $id_google_scholar; ?>&hl=en','')" class="btn btn-primary btn-lg btn-block">Google Scholar</button>
                <button type="button" onclick="window.open('https://www.researchgate.net/profile/<?php echo $id_research_gate; ?>','')" class="btn btn-primary btn-lg btn-block">Reseachgate</button>
                <button type="button" onclick="window.open('https://pakar.um.ac.id/Data/Peneliti/u=<?php echo $keyuser; ?>','')" class="btn btn-primary btn-lg btn-block">Kepakaran UM</button>
            </div>
        <?php } ?>
    </div>
    <div class="col-md-8" style="padding-top: 10px;">
        <h3><?php echo $detail['gelar_depan']; ?><?php echo $detail['nama_pegawai']; ?><?php echo $detail['gelar_belakang']; ?></h3>

        <hr style="margin-top: 10px;">
        <div class="form-horizontal form-custom">
            <div class="form-group">
                <label class="col-sm-2 text-left">NIP</label>
                <div class="col-sm-9">
                    <span><?php echo ($detail['NIP']) ? $detail['NIP'] : "-" ?></span>
                </div>
            </div>

            <?php if ($detail['id_jns_pegawai'] == '5') { ?>
                <div class="form-group">
                    <label class="col-sm-2 text-left">NIDN</label>
                    <div class="col-sm-9">
                        <span><?php echo ($detail['NIDN']) ? $detail['NIDN'] : "-" ?></span>
                    </div>
                </div>
            <?php } ?>

            <div class="form-group">
                <label class="col-sm-2 text-left">Alamat</label>
                <div class="col-sm-9">
                    <span><?php echo ($detail['alamat']) ? $detail['alamat'] : "-" ?></span>
                </div>
            </div>

            <?php if ($detail['status_pegawai'] < 3) { ?>
                <div class="form-group">
                    <label class="col-sm-2">Jabatan, Golongan</label>
                    <div class="col-sm-9">
                        <span><?php echo (($detail['nama_jabatan']) ? $detail['nama_jabatan'] : "-") . " , " . $detail['pangkat'] . ', ' . $detail['golongan'] ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">Jabatan Struktural</label>
                    <div class="col-sm-9">
                        <span><?php
                                if ($detail['nama_jabatan_s'] == null) {
                                    echo "Jabatan Sudah Tidak Aktif";
                                } else {
                                    echo $detail['nama_jabatan_s'];
                                }
                                ?>
                        </span>
                    </div>
                </div>
            <?php } ?>

            <div class="form-group">
                <label class="col-sm-2">NPWP</label>
                <div class="col-sm-9">
                    <span><?php echo ($detail['npwp']) ? $detail['npwp'] : '-'; ?></span>
                </div>
            </div>

            <?php if ($detail['id_jns_pegawai'] == '5') { ?>
                <div class="form-group">
                    <label class="col-sm-2">Riwayat Pendidikan</label>
                    <div class="col-sm-9">
                        <ul>
                            <?php
                            // var_dump($riwayat); die();
                            foreach ($riwayat as $data) {
                                echo '<li>' . $data['universitas'] . '(' . $data['th_lulus'] . ')</li>';
                            }

                            ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>

            <div class="form-group">
                <label class="col-sm-2">Unit Kerja</label>
                <div class="col-sm-9">
                    <span><?php echo ($detail['nama_unit']) ? $detail['nama_unit'] : '-'; ?></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2">Email</label>
                <div class="col-sm-9">
                    <span><?php echo ($detail['email']) ? $detail['email_resmi'] : $detail['email']; ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center">
        <hr>
        <p class="text-muted" style="font-size:90%;"><i>Jika ada kesalahan informasi, silahkan melaporkan melalui email
                kepegawaian@um.ac.id atau menghubungi bagian kepegawaian di pesawat 1142 (Dosen) / 1146
                (T.Kependidikan)</i></p>
    </div>
</div>