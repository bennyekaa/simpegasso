
<ul class="tab-list" id='detail'>
				<li class="tab-tulisan">
					<div class="grid">
						<div class="row">
							<h2 class="sub-title">Detail Data Pegawai</h2>
			                <div class="g-1">
			                    <ul class="education">
								    <li><div class="station"><strong><?php echo $detail[0]['gelar_depan']; ?><?php echo $detail[0]['nama_pegawai']; ?><?php echo $detail[0]['gelar_belakang']; ?></strong></div>
			                            <div class="position-desc">
											<ul class="container-left-info-tabs">
												<li>	
													<ul class="personal-info">
															<table border="0" cellpadding="3" width="400">
															<tbody>
															<tr align="left" valign="middle">
																<td rowspan="7">
																	<img src="https://simpega.um.ac.id/admin/public/photo/photo_<?php echo $detail[0]['kd_pegawai']?>.jpg" border="0" width="100" height="150" style="margin-right: 10px"> 												
																</td>
															</tr>
															<tr align="left" valign="middle"><td width="90">
															<span>NIP </span></td><td width="200"><span><?php echo $detail[0]['NIP'] ?></span>
															</td></tr>
															<tr align="left" valign="middle"><td>
															<span>Gol </span></td><td><span><?php echo $detail[0]['pangkat'].", ".$detail[0]['golongan'] ?></span>
															</td></tr>
															<tr align="left" valign="middle"><td>
															
															<span>Jabatan </span></td><td><span><?php echo $detail[0]['nama_jabatan_s'] ?></span>
															</td></tr>
															<tr align="left" valign="middle"><td>
															<span>NPWP </span></td><td><span><?php echo $detail[0]['npwp']; ?></span>
															</td></tr>
															<tr align="left" valign="middle"><td>
															<span>Unit Kerja </span></td><td><span><?php echo $detail[0]['nama_unit'] ?></span>
															</td></tr>
															<tr align="left" valign="middle"><td>
															<span>Email </span></td><td><span><?php echo $detail[0]['email'] ?></span>
															</td></tr>
															</tbody></table>
			                                                </ul>
			                                    </li>
											</ul>	
			                            </div>
												 
			                            <div class="time"><a href="#" class="link-intern" id='kembali'>Kembali</a></div>
			                        </li>
									<li style="border-top:1px solid #bebebe">
											Jika ada kesalahan informasi silahkan melaporkan melalui email<a href="mailto:kepegawaian@um.ac.id" class="link-intern"> <strong>kepegawaian@um.ac.id</strong> </a>, atau menghubungi bagian kepegawaian di pesawat 142(Dosen) / 146(T.Kependidikan)
									</li>
								</ul>
			                </div>
						</div>
					</div>
				</li>
			</ul>

			<script>
					$('#kembali').click(function(){
						$("#detail").remove();
						$("html, body").animate({
							scrollTop:$("#tmpdetailpegawai").offset().top
						}, 500);
							
						});
			</script>