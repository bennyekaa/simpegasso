$(document).ready(function() {

	///////////////////////////////////
	// Fancybox
	///////////////////////////////////

	$(".fancybox").fancybox({
		openEffect: 'elastic',
		closeEffect: 'elastic',
		helpers : {
    		title : {
    			type : 'inside'
    		}
    	}
	});

	$(".fancybox").append('<div class="mask"><span class="icon-search"></span></div>');

	///////////////////////////////////
	// Scroll Effect
	///////////////////////////////////

	$('a[href*=#]').bind("click", function(event) {
		event.preventDefault();
		var target = $(this).attr("href");

		$('html,body').animate({
			scrollTop: $('a[name='+target.replace(/#/g, "")+']').offset().top
		}, 1500 , function (){location.hash = target;});
	});	

	///////////////////////////////////
	// Show Info on Mobile
	///////////////////////////////////

	menuIcon = $(".show-info-mobile");
	info = $(".container-left-info");

	menuIcon.click(function() {
		if (info.attr("data-status") == "active") {
			info.slideUp(500).removeAttr("data-status");
		} else {
			info.slideDown(500).attr("data-status","active");
		}
	});

	///////////////////////////////////
	// Calculate Pie Chart
	///////////////////////////////////

	barArray = $(".pie-bar");
	heightsArray = $(".pie-inner");

	for (i=0; i <= barArray.length-1; i++) {
		height = heightsArray.eq(i).attr("data-height");
		heightPx = Math.round((height/100)*160);
		heightPx = 160 - heightPx;
		barArray.eq(i).css("clip","rect(0px 160px "+ heightPx +"px 0)");
	}

	///////////////////////////////////
	// Scrollbar
	///////////////////////////////////

	function setInfoHeight() {
		var avatarHeight = $(".container-left-picture").height();
		var viewportHeight = $(window).height();
		var infoHeight = viewportHeight - avatarHeight;

		$(".container-left-info").css("height",infoHeight);
	}

	if ($("body").innerWidth() > 1070) {
		setInfoHeight();
		$(".container-left-info").perfectScrollbar();
	} else {
		$(".container-left-info").perfectScrollbar('destroy');
	}

	///////////////////////////////////
	// Mobile Settings
	///////////////////////////////////

	function displayInfo() {
		$(".container-left-info").show();
	}

	window.onresize = function() {
		// display info
		if ($("body").innerWidth() > 1070) {
			displayInfo();
			setInfoHeight();
			$(".container-left-info").perfectScrollbar();
			$(".container-left-info").perfectScrollbar('update');
		}
		// destroy scrollbar at second breakpoint
		if ($("body").innerWidth() <= 1070) {
			$(".container-left-info").perfectScrollbar('destroy');
			$(".container-left-info").css("height","auto");
		}
	};

});
