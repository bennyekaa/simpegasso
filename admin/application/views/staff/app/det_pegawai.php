<?php 
	
	include "../config/conf.php";
	
	$actionData = anti_injection("act");
	
	$dataYangDiCari = anti_injection("dataYangDiCari");
	$halaman = anti_injection("page");
	
	$dataPerPage = 20;	
	if(isset($halaman)) {
		$noPage = $halaman;
	} 
	else $noPage = 1;
	$offset = ($noPage - 1) * $dataPerPage;

	$queryjml 	= "SELECT COUNT(*) AS jumData from pegawai, golongan_pangkat, unit_kerja WHERE pegawai.kode_unit = unit_kerja.kode_unit
AND pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat AND (pegawai.nama_pegawai like '%".$dataYangDiCari."%' or pegawai.nip like '%".$dataYangDiCari."%')";
	$hasil  = mysql_query($queryjml);
	$data   = mysql_fetch_array($hasil);

	$jumData = $data['jumData'];
	 
	$jumPage = ceil($jumData/$dataPerPage);

	$query = mysql_query("SELECT pegawai.kd_pegawai, pegawai.NIP, pegawai.gelar_depan, pegawai.nama_pegawai,
pegawai.gelar_belakang, golongan_pangkat.golongan, unit_kerja.nama_unit, pegawai.npwp, pegawai.email, (select jenis_pegawai from jenis_pegawai where pegawai.id_jns_pegawai = jenis_pegawai.id_jns_pegawai) as jenis_pegawai, (select agama from agama where pegawai.kd_agama = agama.kd_agama) as agama FROM pegawai, golongan_pangkat, unit_kerja WHERE pegawai.kode_unit = unit_kerja.kode_unit AND pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat and nip not like '' and nip not like '-' and (status_pegawai<3)  AND (pegawai.nama_pegawai like '%".$dataYangDiCari."%' or pegawai.nip like '%".$dataYangDiCari."%') order by pegawai.id_golpangkat_terakhir, pegawai.nip LIMIT ".$offset.", ". $dataPerPage);
	$jmlquery = mysql_num_rows($query);
?>
<p class="portfolio-intro" align="right">Terdapat <b><?php echo $jumData; ?></b> data pegawai.</p>
<table class="table table-hover table-striped table-bordered" style="font-size:0.77777em">
	<thead>
		<tr>
			<th width="30" style="text-align:center">No.</th>
			<th width="50" style="text-align:center">NIP</th>
			<th style="text-align:center">Nama Pegawai</th>
			<th width="30" style="text-align:center">Gol</th>
			<th style="text-align:center">Unit Kerja</th>
			<!--<th width="150" style="text-align:center">NPWP</th>-->
            <th width="30" style="text-align:center">Detail</th>
            <!-- <th width="30" style="text-align:center">&nbsp;</th> -->
		</tr>
	</thead>
	<tbody class="table-condensed">
	<?php
		$x = 1;
		if ($query) {
			while ($dataquery=mysql_fetch_array($query)) {
	?>
				<tr>
					<td style="text-align:left;vertical-align:top"><?php echo $x; ?>.</td>
					<td style="text-align:left"><?php echo $dataquery['NIP']; ?></td>
					<td style="text-align:justify"><?php echo $dataquery['gelar_depan'];?><?php echo ' '.$dataquery['nama_pegawai'];?>										<?php echo $dataquery['gelar_belakang']; ?></td>
					<td style="text-align:center"><?php echo $dataquery['golongan']; ?></td>
					<td style="text-align:left"><?php echo $dataquery['nama_unit']; ?></td>
					<!--<td style="text-align:left"><?php //echo $dataquery['npwp']; ?></td>-->
                    <td style="text-align:center"><a href="#detailcekpengawai" class="link-intern" onClick="detailcekpegawai(<?php echo $dataquery['kd_pegawai']; ?>)" title="Detail <?php echo $dataquery['nama_pegawai']; ?>"><span class="glyphicon glyphicon-book"></span></a></td>
				</tr>			
	<?php			
				$x++;		
			}
		}
	?>
	</tbody>
</table>
<div class="pagination  pagination-right">
	<ul>
		<?php
			if ($noPage > 1) {
		?>										      
				<li><a href="#pegawai" onclick="ShowPegawai(<?php echo $noPage-1; ?>,document.getElementById('frminputcaridataPegawai').value)"  class="link-intern">&laquo;</a></li>
		<?php
			}
			
			for($page = 1; $page <= $jumPage; $page++) {
				if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) {   
					if (($showPage == 1) && ($page != 2))  echo "<li><a href='#' class=\"active\">..</a></li>"; 
					if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  echo "<li><a href='#' class=\"active\">..</a></li>";
					if ($page == $noPage) {
						?>
						<li class="disabled"><a href="#"><?php echo $page; ?></a></li>
		<?php
					} else {
		?>
						<li><a href="#pegawai" onclick="ShowPegawai(<?php echo $page; ?>,document.getElementById('frminputcaridataPegawai').value)" class="link-intern"><?php echo $page; ?></a></li>
		<?php
					}
					$showPage = $page;
				}
			}
			
			if ($noPage < $jumPage) {
		?>
				<li><a href="#pegawai" onclick="ShowPegawai(<?php echo $noPage+1; ?>,document.getElementById('frminputcaridataPegawai').value)" class="link-intern">&raquo;</a></li>
		<?php
			}
		?>
	</ul>
</div>
