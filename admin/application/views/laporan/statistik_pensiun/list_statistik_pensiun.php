<?php 
function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}  
 ?>
<div class="content">
<div id="content-header">

	<div id="module-title"><h3>
	  DAFTAR NOMINASI PNS UNIVERSITAS NEGERI MALANG
	</h3>
	</div>
	<div id="module-menu">
		<div id="module-menu">

			<a href="<?=site_url('laporan/statistik_pensiun/printtoxls/'.$unit_kerja.'/'.$id_jns_pegawai.'/list_statistik_pensiun')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>	
	</div>
	<div class="clear"></div>
</div>

<div id="content-data">
	<div id='detail' style="position:absolute z-index"></div>
		<center>
			<form name="pegawaidetails" id="pegawaidetails" method="POST" action="<?=site_url('laporan/statistik_pensiun/browse')?>" >
				Jenis Pegawai:
					<?php echo form_dropdown('id_jns_pegawai', $get_jenis_pegawai_all, $id_jns_pegawai); ?>
				<label>Unit Kerja : </label>
				<?=form_group_dropdown('unit_kerja',$unitkerja_assoc,$unit_kerja);?>
				<input name="mulai" type="submit" value="Cari">
			</form>
	  	</center>
		<br/>
		<?php
			if ($this->input->post('mulai')){
		?>	
		<?php 
			//cek idjenis pegawainya apa
			$query = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'");					
			if ($query)	{
				$dataku=mysql_fetch_array($query);  
				$id_tenaga_dosen= $dataku['id_var'];
				}
			
			$query = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_adm'");					
			if ($query)	{
				$dataku=mysql_fetch_array($query);  
				$id_tenaga_adm= $dataku['id_var'];
				}
				
			if ($id_jns_pegawai=='All'){
				$kriteria_jenis = " and id_jns_pegawai > 0 ";
			}
			/*elseif ($id_jns_pegawai=='nAll'){
				$kriteria_jenis = " and id_jns_pegawai not like '".$id_tenaga_adm."'  and status_pegawai < 3 ";
			}*/
			elseif ($id_jns_pegawai=='nAllUnit'){
				$kriteria_jenis = " and pegawai.id_jns_pegawai not like '".$id_tenaga_dosen."'  and status_pegawai < 3";
			}
			else {
				$kriteria_jenis = " and pegawai.id_jns_pegawai='".$id_jns_pegawai."'  and status_pegawai < 3 ";
			}
			$grandtotal=0;
			
		?>	
		
		<?php 
		 	if ($unit_kerja == 'Semua'){
		?>
		
		<table class="table-list" width="100%">

				<?php 		
				$query = mysql_query("select *, replace(kode_unit,'0','') as tingkatan from unit_kerja order by kode_unit");					
				if ($query) {
					while ($data_unit=mysql_fetch_array($query)) {
							$kd_unit = $data_unit['kode_unit'];
							$tingkatan = strlen($data_unit['tingkatan']);
							$pembanding=left($kd_unit,$tingkatan);		
				?>
				<?php
				
					
					
					if ($data_unit['tingkat'] == 1 and $data_unit['GD'] == 'G')   {
						
						$sql =" SELECT pegawai.nama_pegawai,pegawai.NIP,pegawai.tgl_lahir, pegawai.tmt_unit_terakhir, pegawai.tmt_cpns, pendidikan.id_pendidikan,pendidikan.nama_pendidikan,
								CASE  WHEN pendidikan.id_pendidikan>3  THEN riwayat_pendidikan.prodi ELSE ''  END 
								as jurusan, count(pegawai.kd_pegawai) as jml_pegawai,pegawai.kode_unit,
								pegawai.kode_unit_induk,pegawai.id_jns_pegawai, 
								(select unit_kerja.nama_unit from unit_kerja where unit_kerja.kode_unit=pegawai.kode_unit)as nama_unit 
								FROM pegawai Inner Join riwayat_pendidikan ON pegawai.kd_pegawai 
								= riwayat_pendidikan.kd_pegawai AND pegawai.id_pendidikan_terakhir = riwayat_pendidikan.id_pendidikan
								Inner Join pendidikan ON riwayat_pendidikan.id_pendidikan = pendidikan.id_pendidikan
								WHERE (left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '".$kd_unit."' 
								OR pegawai.kode_unit_induk = '".$kd_unit."') and (pegawai.eselon not like 'non-eselon') ".$kriteria_jenis."
								and  `riwayat_pendidikan`.`aktif`='1'
								GROUP BY pegawai.kd_pegawai,pendidikan.id_pendidikan, pendidikan.nama_pendidikan, jurusan, tempat_studi,kode_unit,
								pegawai.kode_unit_induk 
								ORDER BY pendidikan.id_pendidikan desc,tempat_studi,jurusan,kode_unit ";
						}
					else{
						$sql =" SELECT pegawai.nama_pegawai,pegawai.NIP,pegawai.tgl_lahir, pegawai.tmt_unit_terakhir, pegawai.tmt_cpns, pendidikan.id_pendidikan,pendidikan.nama_pendidikan,
								CASE  WHEN pendidikan.id_pendidikan>3  THEN riwayat_pendidikan.prodi ELSE ''  END 
								as jurusan,CASE  WHEN pendidikan.id_pendidikan>4  THEN riwayat_pendidikan.universitas ELSE pendidikan.nama_pendidikan END 
								as tempat_studi, count(pegawai.kd_pegawai) as jml_pegawai,pegawai.kode_unit,
								pegawai.kode_unit_induk,pegawai.id_jns_pegawai,riwayat_pendidikan.th_lulus, 
								(select unit_kerja.nama_unit from unit_kerja where unit_kerja.kode_unit=pegawai.kode_unit)as nama_unit 
			   				    FROM pegawai Inner Join riwayat_pendidikan 
								ON pegawai.kd_pegawai 
								= riwayat_pendidikan.kd_pegawai AND pegawai.id_pendidikan_terakhir = riwayat_pendidikan.id_pendidikan
								Inner Join pendidikan ON riwayat_pendidikan.id_pendidikan = pendidikan.id_pendidikan
								WHERE (pegawai.kode_unit = '".$kd_unit."'  OR pegawai.kode_unit_induk = '".$kd_unit."')
								and (pegawai.eselon like 'non-eselon') ".$kriteria_jenis."
								and  `riwayat_pendidikan`.`aktif`='1'
								GROUP BY pegawai.kd_pegawai,pendidikan.id_pendidikan, pendidikan.nama_pendidikan, jurusan, tempat_studi,kode_unit,
								pegawai.kode_unit_induk 
								ORDER BY pendidikan.id_pendidikan desc,tempat_studi,jurusan,kode_unit ";				
						}	
					
					//echo $sql;
					
					$query_pegawai = mysql_query($sql);			
					$row_count = mysql_num_rows($query_pegawai);
					//echo $row_count.'__';
					$j=0;
						 	
					if (($query_pegawai) and $row_count>0 ){	
							$query_anak = mysql_query("select * from unit_kerja where kode_unit_general='".$kd_unit."'");
							if ($data_unit['tingkat'] == 1 and $data_unit['GD'] == 'G')   {
								$ada_anak = '';}
							else if ($query_anak) {
								$data_anak=mysql_fetch_array($query_anak);
								if ($data_anak!=FALSE) {
									$ada_anak = $data_anak['kode_unit'];
								}
								else {
									$ada_anak = '';
									}
								}
					if ($ada_anak =='') {		
						echo "<tr><td colspan='12'>&nbsp;</td></tr>";
						if (($data_unit['nama_unit'] == $nama_induk) and ($data_unit['id_group']=='1')) {
							echo "<tr><td colspan='12'><strong> PUSAT (".$data_unit['nama_unit'].")</strong></td></tr>";}
						else if (($data_unit['nama_unit'] == $nama_induk) and ($data_unit['id_group']=='2')) {
							echo "<tr><td colspan='12'><strong> FAKULTAS (".$data_unit['nama_unit'].")</strong></td></tr>";}
						else {
							echo "<tr><td colspan='12'><strong>".$nama_induk." (".$data_unit['nama_unit'].")</strong></td></tr>";
						}
					?>
				<tr class="trhead">
					<td rowspan="2" align="center">No</td>
					<td rowspan="2" align="center">Nama Pegawai<BR />
					NIP</td>
					<td rowspan="2" align="center" class="colEvn">Usia<br />Masa Kerja</td>
					<td rowspan="2" align="center" class="colEvn">TMT Tugas Di Unit </td>
					<td colspan="2" align="center">Pendidikan</td>
				</tr>
				<tr class="trhead">
					
					<td align="center" class="colEvn">Jenis</td>
					<td align="center" class="colEvn">Jurusan</td>
				</tr>
								
				<?php 		
					$j=0;
					$sumsubtotal = 0;					
					while ($data_pegawai=mysql_fetch_array($query_pegawai)) {
						$j++;
						if (($j%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
					?>
				<tr class="<?= $class; ?>">
					<td  align="center" class="colEvnRight"><?= $j;?> </td>
					<td align="left" class="colEvnRight"><?= $data_pegawai['nama_pegawai'];?><BR /><?= $data_pegawai['NIP'];?></td>
					<td align="center" class="colEvnRight"><?php 
					$a = datediff($data_pegawai['tgl_lahir'], date('Y-m-d'));
					echo $a[years].' thn '.$a[months].' bln ';
				?> <br /><?php 
					$a = datediff($data_pegawai['tmt_cpns'], date('Y-m-d'));
					echo $a[years].' thn '.$a[months].' bln ';
				?></td>
					<td align="left" class="colEvnRight">	<?= $data_pegawai['tmt_unit_terakhir'];?></td>
					<td align="left" class="colEvnRight"><?= $data_pegawai['nama_pendidikan'];?></td>
					
					<td  align="left" class="colEvnRight"><?= $data_pegawai['jurusan'];$sumsubtotal=$sumsubtotal+$data_pegawai['jml_pegawai'];
					$grandtotal=$grandtotal+$data_pegawai['jml_pegawai']; ?> </td>
				</tr>
				<?php		
						}
					?>
				<!--<tr class="trhead">
					<td  colspan="5" align="left" class="colEvn">Sub Total</td>
					
					<td align="center" class="colEvnRight"><?= $sumsubtotal; ?></td>
					
					<td  align="center" class="colEvnRight">&nbsp; </td>
					
				</tr>-->
				<?php 
					
					} //penutup jika ada anak
							}
						$j=0;
						
						}
					
					}
				 ?>
			<tr class="trOdd">
				<td colspan="7"  align="left">&nbsp;</td>
			</tr> 
			<tr class="trhead">
				<td colspan="5"  align="left"><strong>TOTAL KESELURUHAN</strong></td>
				<td  align="center" class="colEvn"><?= $grandtotal; ?> </td>
			</tr>
		 </table>	
<!--jika unit kerja yang dipilih adalah per unit maka;-->		  
<?php
	}
	else {
		$query = mysql_query("select nama_unit from unit_kerja where (tingkat = '1' and left(kode_unit,2)='".left($unit_kerja,2)."')");		
		//echo "select nama_unit from unit_kerja where (tingkat = '1' and left(kode_unit,2)='".left($unit_kerja,2)."')";	
		if ($query) 
			{
			$data_induk=mysql_fetch_array($query);
			$nama_induk = $data_induk['nama_unit'];
			if ($nama_induk!='') { 
				$nama_induk = $data_induk['nama_unit'];

				}
			else
				{
					$query = mysql_query("select nama_unit from unit_kerja where (tingkat = '1' and left(kode_unit,1)='".left($unit_kerja,1)."')");					
					if ($query) {
						$data_induk=mysql_fetch_array($query); 
						$nama_induk = $data_induk['nama_unit'];}
				}
		}
		$query = mysql_query("select id_group,nama_unit from unit_kerja where kode_unit='".$unit_kerja."'");					
		if ($query) {
			$data_unit=mysql_fetch_array($query); 
			$nama_unit = $data_unit['nama_unit'];
			if (($data_unit['nama_unit'] == $nama_induk) and ($data_unit['id_group']=='1')) {
				$nama_induk ='PUSAT';}
			}	
			
		$nama_unit = $nama_induk.' ('.$nama_unit.')';
?>
		<table class="table-list" width="100%">
		<tr class="trOdd"><td colspan="7"  align="center">
			<font size="+1"> <strong><?=$judul;?></strong></font>
			</td></tr>
			<tr class="trOdd"><td colspan="7"  align="left">
			<?=$nama_unit;?>
			</td></tr>
			<tr class="trhead">
					<td rowspan="2" align="center">No</td>
					<td rowspan="2" align="center">Nama Pegawai<BR />
					NIP</td>
					<td rowspan="2" align="center" class="colEvn">Usia<br />Masa Kerja</td>
					<td rowspan="2" align="center" class="colEvn">TMT Tugas Di Unit </td>
					<td colspan="2" align="center">Pendidikan</td>
					
		  </tr>
				<tr class="trhead">
					
					<td align="center" class="colEvn">Jenis</td>
					<td align="center" class="colEvn">Jurusan</td>
				</tr>
			<?php
				$query = mysql_query("select *, replace(kode_unit,'0','') as tingkatan from unit_kerja 
				where kode_unit= '".$unit_kerja."' ");					
				$data_unit=mysql_fetch_array($query);
				$row_count = mysql_num_rows($query);
				if ($row_count>0){	
					$kd_unit = $data_unit['kode_unit'];
					$tingkatan = strlen($data_unit['tingkatan']);
					$pembanding=left($kd_unit,$tingkatan);		
				}
				
				
			if ($data_unit['tingkat'] == 1 and $data_unit['GD'] == 'G')   {
						
			$sql =" SELECT pegawai.nama_pegawai,pegawai.NIP, pegawai.tgl_lahir,pegawai.tmt_unit_terakhir, pegawai.tmt_cpns, pendidikan.id_pendidikan,pendidikan.nama_pendidikan,
								CASE  WHEN pendidikan.id_pendidikan>3  THEN riwayat_pendidikan.prodi ELSE ''  END 
								as jurusan,CASE  WHEN pendidikan.id_pendidikan>4  THEN riwayat_pendidikan.universitas ELSE pendidikan.nama_pendidikan END 
					as tempat_studi, count(pegawai.kd_pegawai) as jml_pegawai,pegawai.kode_unit,
					pegawai.kode_unit_induk,pegawai.id_jns_pegawai,riwayat_pendidikan.th_lulus , 
					(select unit_kerja.nama_unit from unit_kerja where unit_kerja.kode_unit=pegawai.kode_unit)as nama_unit 
					FROM pegawai Inner Join riwayat_pendidikan ON pegawai.kd_pegawai 
					= riwayat_pendidikan.kd_pegawai AND pegawai.id_pendidikan_terakhir = riwayat_pendidikan.id_pendidikan
					Inner Join pendidikan ON riwayat_pendidikan.id_pendidikan = pendidikan.id_pendidikan
					WHERE (left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '".$kd_unit."' 
					OR pegawai.kode_unit_induk = '".$kd_unit."') and (pegawai.eselon not like 'non-eselon') ".$kriteria_jenis."
					and  `riwayat_pendidikan`.`aktif`='1'
					GROUP BY pegawai.kd_pegawai, pendidikan.id_pendidikan, pendidikan.nama_pendidikan, jurusan, tempat_studi,kode_unit,
					pegawai.kode_unit_induk 
					ORDER BY pendidikan.id_pendidikan desc,tempat_studi,jurusan,kode_unit ";
			}
		else{
			$sql =" SELECT pegawai.nama_pegawai,pegawai.NIP,pegawai.tgl_lahir, pegawai.tmt_unit_terakhir, pegawai.tmt_cpns, pendidikan.id_pendidikan,pendidikan.nama_pendidikan,
								CASE  WHEN pendidikan.id_pendidikan>3  THEN riwayat_pendidikan.prodi ELSE ''  END 
								as jurusan,CASE  WHEN pendidikan.id_pendidikan>4  THEN riwayat_pendidikan.universitas ELSE pendidikan.nama_pendidikan END 
					as tempat_studi, count(pegawai.kd_pegawai) as jml_pegawai,pegawai.kode_unit,
					pegawai.kode_unit_induk,pegawai.id_jns_pegawai,riwayat_pendidikan.th_lulus, 
					(select unit_kerja.nama_unit from unit_kerja where unit_kerja.kode_unit=pegawai.kode_unit)as nama_unit 
					FROM pegawai Inner Join riwayat_pendidikan ON pegawai.kd_pegawai 
					= riwayat_pendidikan.kd_pegawai AND pegawai.id_pendidikan_terakhir = riwayat_pendidikan.id_pendidikan
					Inner Join pendidikan ON riwayat_pendidikan.id_pendidikan = pendidikan.id_pendidikan
					WHERE (pegawai.kode_unit = '".$kd_unit."'  OR pegawai.kode_unit_induk = '".$kd_unit."')
					and (pegawai.eselon like 'non-eselon') ".$kriteria_jenis."
					and  `riwayat_pendidikan`.`aktif`='1'
					GROUP BY pegawai.kd_pegawai, pendidikan.id_pendidikan, pendidikan.nama_pendidikan, jurusan, tempat_studi,kode_unit,
					pegawai.kode_unit_induk 
					ORDER BY pendidikan.id_pendidikan desc,tempat_studi,jurusan,kode_unit ";				
			}	
			
			
				//echo $sql;
				
				$query_pegawai = mysql_query($sql);		
				$i = 0;			
				$subtotal = 0;
				$sumsubtotal = 0;
				while ($data_pegawai=mysql_fetch_array($query_pegawai)) {
					$i++;
					if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				?>
			<tr class="<?= $class; ?>">
					<td  align="center" class="colEvnRight"><?= $j;?> </td>
					<td align="left" class="colEvnRight"><?= $data_pegawai['nama_pegawai'];?><BR /><?= $data_pegawai['NIP'];?></td>
					<td align="center" class="colEvnRight"><?php 
					$a = datediff($data_pegawai['tgl_lahir'], date('Y-m-d'));
					echo $a[years].' thn '.$a[months].' bln ';
				?> <br /><?php 
					$a = datediff($data_pegawai['tmt_cpns'], date('Y-m-d'));
					echo $a[years].' thn '.$a[months].' bln ';
				?></td>
					<td align="left" class="colEvnRight">	<?= $data_pegawai['tmt_unit_terakhir'];?></td>
					<td align="left" class="colEvnRight"><?= $data_pegawai['nama_pendidikan'];?></td>
					<td  align="left" class="colEvnRight"><?= $data_pegawai['jurusan'];$sumsubtotal=$sumsubtotal+$data_pegawai['jml_pegawai'];
					$grandtotal=$grandtotal+$data_pegawai['jml_pegawai']; ?> </td>
					
		  </tr>
			<? } ?>	
			<!--<tr class="trhead">
				<td  colspan="5" align="left" class="colEvn">Sub Total</td>
				<td  align="center" class="colEvn"><?= $grandtotal; ?> </td>
				<td  align="center" class="colEvnRight">&nbsp;</td>

			</tr>
			<tr class="trOdd">
				<td colspan="7"  align="left">&nbsp;</td>
			</tr> -->
			<tr class="trhead">
				<td colspan="5"  align="left"><strong>TOTAL KESELURUHAN</strong></td>
				<td align="center">
					<font size="+1"> <strong><?=$grandtotal;?></strong></font>
				</td>
				
			</tr>  
		</table>			
		<? }} ?>
		<br />
		<div class="paging"><?=$page_links?></div>
  </div>
</div>
