<?
function ucname($string) {
    $string =ucwords(strtolower($string));

    foreach (array('-', '\'') as $delimiter) {
      if (strpos($string, $delimiter)!==false) {
        $string =implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
      }
    }
    return $string;
}
?>
<div class="content">
	<div id="content-header">
		<div id="module-title"><h3><?=$judul?></h3></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/pensiun/printtopdf/'.$group.'/'.$tahun.'/'.$jns_pegawai)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/pensiun/printtoxls/'.$group.'/'.$tahun.'/'.$jns_pegawai.'/daftar_pensiun.xls')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<br/>
	<div id="content-data">
		<center>
			<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('laporan/pensiun/browse')?>" >
				<label>Jenis Pegawai : </label><?=form_dropdown('jns_pegawai',$jenis_pegawai_assoc,$jns_pegawai);?>
				<label>Bulan: </label><?=form_dropdown('group',$group_assoc,$group);?><label> Tahun: </label><?=form_dropdown('tahun',$tahun_assoc,$tahun);?>
				<input type="submit" value=" Cari  " name="mulai">
			</form>
		</center>
		<br/>
		<?php
			if ($this->input->post('mulai')){
		?>
		<table class="table-list">
			<tr class="trhead" align="center">	
				<th width="41" rowspan="2" align="center" class="colEvn">No</th> 
				<th width="114" rowspan="2" >NIP</th>
				<th width="189" rowspan="2" class="colEvn" >Nama Pegawai</th>
				<th width="130" rowspan="2" align="center" class="trhead"><span class="colEvn">Tempat/Tanggal Lahir</span></th>
				<th width="130" rowspan="2" align="center" class="trhead">Pangkat, Gol/Ruang</th>
				<th width="154" rowspan="2" >Tanggal Pensiun</th>
				
				<th width="220" nowrap class="colEvn"><span class="trhead">Jabatan</span></th>
				<!--<th width="86" rowspan="2" nowrap>Batas Pensiun </th>-->
				<?php //if($group == ''){ ?>
				<?php //} ?>
			</tr>
			<tr class="trhead" align="center">
			  <th nowrap class="colEvn">Unit Kerja</th>
		  </tr>
			
			<?
			$i = 0;
			
			if ($pensiun_list!=FALSE){
			
			foreach ($pensiun_list as $pensiun) {
				//if ($pensiun['usia']=='56') {
				$i++;
				if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				list($y, $m, $d) = explode('-', $pensiun['tgl_lahir']);
				$str_tgl_lahir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
				$pensiun['tgl_lahir']=$str_tgl_lahir;
				
				list($y, $m, $d) = explode('-', $pensiun['tgl_resign']);
				$str_tgl_resign = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
				$pensiun['tgl_resign']=$str_tgl_resign;
				
				if ($pensiun['nama_jabatan']=='Fungsional Umum')
				{
					$pensiun['nama_jabatan']='-';
				}
			?>   
			<tr class="<?= $class; ?>">
				<td rowspan="2" align="center" class="colEvn"><?= $i; ?></td>
				<td rowspan="2" align="left" nowrap="nowrap" class="trhead"><?= $pensiun['NIP']; ?></td>
				<td rowspan="2" align="left" nowrap="nowrap" class="colEvn"><?= $pensiun['gelar_depan']." ".strtoupper($pensiun['nama_pegawai'])." ".$pensiun['gelar_belakang']; ?></td>
				<?php
						  $query = mysql_query("select nama_kabupaten from kabupaten where kd_kabupaten='".$pensiun['tempat_lahir']."'");					
							if ($query) {
								$datakab=mysql_fetch_array($query); 
								$tempat_lahir = $datakab['nama_kabupaten'];
							}
							else{
								$tempat_lahir = $jabatan['tempat_lahir'];
							}
							if ($tempat_lahir =="Lain-lain"){ 
								$tempat_lahir="";
							}		
					?>
				<td rowspan="2" align="left" class="trhead">
				  <?=$tempat_lahir?>
			  ,
			  <?=$pensiun['tgl_lahir']?>			  </td>
				<td rowspan="2" align="left" class="trhead"><?=$pensiun['nama_pangkat'];?>, <?=$pensiun['nama_golongan'];?>				  </td>
				<td rowspan="2" align="center" nowrap="nowrap" class="trhead"><?=$pensiun['tgl_resign'];?></td>
				<td width="220" align="center" nowrap class="trhead"><?
				if (($pensiun['id_jns_pegawai']== 1) or ($pensiun['id_jns_pegawai']== 5))
				{
					echo $jabatan_struktural_assoc[$pensiun['id_jabatan_struktural']];
				}
				else
				{
					echo $jabatan_assoc[$pensiun['id_jabatan_terakhir']];
				}?></td>
				<!--<td width="86" rowspan="2" align="center" nowrap class="trhead"><?=$pensiun['batas'];?></td>-->
			</tr>
			<tr class="<?= $class; ?>">
			  <td align="center" nowrap class="trhead"><?=$pensiun['unit_kerja'];?></td>
		  </tr>
			
			<? } }//}
			else {
				$conspan = ($group == '') ? 7 : 6;
				echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
			}?>
		</table>
		<div class="paging"><?=$page_links?></div>
	</div>
</div>
<? }?>