<?php 
function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}  
 ?>
<div class="content">
<div id="content-header">

	<div id="module-title"><h3>
	  Statistik Pegawai Berdasarkan Usia dan Tingkat Pendidikan
	</h3>
	</div>
	<div id="module-menu">
		<div id="module-menu">

			<a href="<?=site_url('laporan/statistik_usia/printtoxls/'.$unit_kerja.'/'.$id_jns_pegawai.'/list_perunit_statistik_usia')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>	
	</div>
	<div class="clear"></div>
</div>

<div id="content-data">
	<div id='detail' style="position:absolute z-index"></div>
		<center>
			<form name="pegawaidetails" id="pegawaidetails" method="POST" action="<?=site_url('laporan/statistik_usia/browse')?>" >
				Jenis Pegawai:
					<?php echo form_dropdown('id_jns_pegawai', $get_jenis_pegawai_all, $id_jns_pegawai); ?>
				<label>Unit Kerja : </label>
				<?=form_group_dropdown('unit_kerja',$unitkerja_assoc,$unit_kerja);?>
				<input name="mulai" type="submit" value="Cari">
			</form>
	  	</center>
		<br/>
		<?php
			if ($this->input->post('mulai')){
		?>	
		<?php 
			//cek idjenis pegawainya apa
			$query = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'");					
			if ($query)	{
				$dataku=mysql_fetch_array($query);  
				$id_tenaga_dosen= $dataku['id_var'];
				}
			
			$query = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_adm'");					
			if ($query)	{
				$dataku=mysql_fetch_array($query);  
				$id_tenaga_adm= $dataku['id_var'];
				}
				
			if ($id_jns_pegawai=='All'){
				$kriteria_jenis = " and id_jns_pegawai > 0 ";
			}
			/*elseif ($id_jns_pegawai=='nAll'){
				$kriteria_jenis = " and id_jns_pegawai not like '".$id_tenaga_adm."'  and status_pegawai < 3";
			}*/
			elseif ($id_jns_pegawai=='nAllUnit'){
				$kriteria_jenis = " and id_jns_pegawai not like '".$id_tenaga_dosen."' and status_pegawai < 3 ";
			}
			else {
				$kriteria_jenis = " and id_jns_pegawai='".$id_jns_pegawai."'  and status_pegawai < 3 ";
			}
			$grandtotal=0;
			
		?>	
		
		<?php 
		 	if ($unit_kerja == 'Semua'){
		?>
		<table class="table-list" width="80%">
				<tr class="trOdd">
					<td colspan="<?=12+(count($pendidikan_assoc))?>"  align="center">
						<font size="+1"> <strong><?=$judul;?></strong></font>
					</td>
				</tr>
				<tr class="trOdd">
					<td colspan="<?=12+(count($pendidikan_assoc))?>"  align="center">
						<font size="+1"> <strong><?=$judul_unit;?></strong></font>
					</td>
				</tr>
				<?php 		
				$query = mysql_query("select *, replace(kode_unit,'0','') as tingkatan from unit_kerja order by kode_unit");					
				if ($query) {
					while ($data_unit=mysql_fetch_array($query)) {
							$kd_unit = $data_unit['kode_unit'];
							$tingkatan = strlen($data_unit['tingkatan']);
							$pembanding=left($kd_unit,$tingkatan);		
				?>
				<?php
				
					if ($this->input->post('nama_search')!=''){
						$search_nama = " ((nama_pegawai like '%".$this->input->post('nama_search')."%') or (NIP like '".$this->input->post('nama_search')."'))";	
					}
					else{
						$search_nama = " (nama_pegawai like '%%')";	
					}
					
					if ($data_unit['tingkat'] == 1 and $data_unit['GD'] == 'G')   {
						$sql ="select  * from (select  (year(curdate())-year(tgl_lahir))  as umur from pegawai where 
						(left(kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR kode_unit = '".$kd_unit."' 
						OR kode_unit_induk = '".$kd_unit."') and (eselon not like 'non-eselon') and ".$search_nama."".$kriteria_jenis."
						group by (year(curdate())-year(tgl_lahir)) 
						UNION
						select  (year(curdate())-year(tgl_lahir))  as umur from pegawai where 
						kode_unit = '".$kd_unit."' and kode_unit_induk = '0' and ".$search_nama."".$kriteria_jenis."
						group by (year(curdate())-year(tgl_lahir))) as GABUNGAN order by umur desc";
   					    $nama_induk = $data_unit['nama_unit'];
						}
					else{
						$sql = "select  (year(curdate())-year(tgl_lahir))  as umur from pegawai where 
						(kode_unit = '".$kd_unit."'  OR kode_unit_induk = '".$kd_unit."')
						and (eselon like 'non-eselon')  and ".$search_nama."".$kriteria_jenis."
						group by (year(curdate())-year(tgl_lahir)) order by kode_unit,(year(curdate())-year(tgl_lahir)) desc";							
						}	
					
					//echo $sql;
					
					$query_pegawai = mysql_query($sql);			
					$row_count = mysql_num_rows($query_pegawai);
					//echo $row_count.'__';
					$j=0;
						 	
					if (($query_pegawai) and $row_count>0 ){	
							$query_anak = mysql_query("select * from unit_kerja where kode_unit_general='".$kd_unit."'");
							if ($data_unit['tingkat'] == 1 and $data_unit['GD'] == 'G')   {
								$ada_anak = '';}
							else if ($query_anak) {
								$data_anak=mysql_fetch_array($query_anak);
								if ($data_anak!=FALSE) {
									$ada_anak = $data_anak['kode_unit'];
								}
								else {
									$ada_anak = '';
									}
								}
					if ($ada_anak =='') {		
						echo "<tr><td colspan='12'>&nbsp;</td></tr>";
						if (($data_unit['nama_unit'] == $nama_induk) and ($data_unit['id_group']=='1')) {
							echo "<tr><td colspan='12'><strong> PUSAT (".$data_unit['nama_unit'].")</strong></td></tr>";}
						else if (($data_unit['nama_unit'] == $nama_induk) and ($data_unit['id_group']=='2')) {
							echo "<tr><td colspan='12'><strong> FAKULTAS (".$data_unit['nama_unit'].")</strong></td></tr>";}
						else {
							echo "<tr><td colspan='12'><strong>".$nama_induk." (".$data_unit['nama_unit'].")</strong></td></tr>";
						}
					?>
				<tr class="trhead">
					<th width="20"  align="center" class="trhead">No</th>
					<th align="center"  class="trhead">Usia</th>
					<? $i=0;if($pendidikan_assoc)foreach($pendidikan_assoc as $komp){$i++; ?>
					<th style="min-width:20px" class="<?=$i % 2==0?"":"colEvnRight"?>" ><?=$komp['nama_pendidikan']?></th>
					<? } ?>
					<th align="center"  class="trhead">Jumlah</th>
				</tr>
								
				<?php 		
					$j=0;
					$subtotal = 0;
					$sumsubtotal = 0;					
					while ($data_pegawai=mysql_fetch_array($query_pegawai)) {
						$j++;
						if (($j%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
					?>
				<tr class="<?= $class; ?>">
					<td  align="left" class="colEvnRight"><?= $j;?> </td>
					
					<td align="center" class="colEvnRight">
						<?php 
						//$tanggal_lahir = $data_pegawai['tahun'].'-01-01';
						//$a = datediff($tanggal_lahir, date('Y-m-d'));
						//echo $a[years].' thn ';
						echo $data_pegawai['umur'];
					?> 
					</td>
					<? $i=1;if($pendidikan_assoc)	foreach($pendidikan_assoc as $komp){$i++;?>
					<td align="center" class="colEvnRight">
						<?php												
							
							if ($data_unit['tingkat'] == 1 and $data_unit['GD'] == 'G')   {
								//ambil pegawai dg pendidikan > s1
								$sql = "select count(kd_pegawai) as jml_didik from (select * from pegawai 
								where (left(kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR kode_unit = '".$kd_unit."' 
								OR kode_unit_induk = '".$kd_unit."') and (eselon not like 'non-eselon') and ".$search_nama." ".$kriteria_jenis."
								and  (year(curdate())-year(tgl_lahir))='".$data_pegawai['umur']."' and id_pendidikan_terakhir = '".$komp['id_pendidikan']."'
								UNION
								select * from pegawai 
								where (kode_unit = '".$kd_unit."') and ".$search_nama." ".$kriteria_jenis."
								and  (year(curdate())-year(tgl_lahir))='".$data_pegawai['umur']."' and id_pendidikan_terakhir = '".$komp['id_pendidikan']."')
								as GABUNGAN";
								//echo $sql;
								$query_pegawai_usia_unit = mysql_query($sql);
								}
								
							else{
								$sql = "select count(kd_pegawai) as jml_didik from pegawai 
								where (kode_unit = '".$kd_unit."'  OR kode_unit_induk = '".$kd_unit."')
								and (eselon like 'non-eselon')  and ".$search_nama."".$kriteria_jenis."
								and  (year(curdate())-year(tgl_lahir))='".$data_pegawai['umur']."' and id_pendidikan_terakhir = '".$komp['id_pendidikan']."'";
								$query_pegawai_usia_unit = mysql_query($sql);		
								}	
								
							$data_pegawai_usia_unit=mysql_fetch_array($query_pegawai_usia_unit);
							$row_count = mysql_num_rows($query_pegawai_usia_unit);
							if ($row_count>0){	
								if ($data_pegawai_usia_unit['jml_didik']>0){		
									echo $data_pegawai_usia_unit['jml_didik'];
									$subtotal = $subtotal + $data_pegawai_usia_unit['jml_didik'];
								}
								else{
									echo '-';
								}	
							}
							else{
								echo '-';
							}
							
							
						?>
					</td>
					<? } ?>
					<td  align="center" class="colEvn"><?= 
					$subtotal; 
					$sumsubtotal = $sumsubtotal + $subtotal; 
					$grandtotal=$grandtotal+$subtotal;
					$subtotal = 0;?> </td>
		
				</tr>
				<?php		
						}
					?>
				<tr class="trhead">
					<td  colspan="2" align="left" class="colEvn">Sub Total</td>
					<? $i=0;if($pendidikan_assoc)foreach($pendidikan_assoc as $komp){$i++; ?>
						<td align="center" class="colEvnRight">
							<?php												
								
								if ($data_unit['tingkat'] == 1 and $data_unit['GD'] == 'G')   {
									//ambil pegawai dg pendidikan > s1
									$query_pegawai_usia_unit = mysql_query("select count(kd_pegawai) as jml_didik from (
									select * from pegawai 
									where (left(kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR kode_unit = '".$kd_unit."' 
									OR kode_unit_induk = '".$kd_unit."') and (eselon not like 'non-eselon') and ".$search_nama." ".$kriteria_jenis."
									and id_pendidikan_terakhir = '".$komp['id_pendidikan']."' 
									UNION
									select * from pegawai 
									where kode_unit = '".$kd_unit."' and ".$search_nama." ".$kriteria_jenis."
									and id_pendidikan_terakhir = '".$komp['id_pendidikan']."') as GABUNGAN
									");
									}
								else{
									$query_pegawai_usia_unit = mysql_query("select count(kd_pegawai) as jml_didik from pegawai 
									where (kode_unit = '".$kd_unit."'  OR kode_unit_induk = '".$kd_unit."')
									and (eselon like 'non-eselon')  and ".$search_nama."".$kriteria_jenis."
									and id_pendidikan_terakhir = '".$komp['id_pendidikan']."'");		
									}	
								$data_pegawai_usia_unit=mysql_fetch_array($query_pegawai_usia_unit);
								$row_count = mysql_num_rows($query_pegawai_usia_unit);
								if ($row_count>0){	
									if ($data_pegawai_usia_unit['jml_didik']>0){		
										echo $data_pegawai_usia_unit['jml_didik'];
									}
									else{
										echo '-';
									}	
								}
								else{
									echo '-';
								}
							?>
						</td>
					<? } ?>
					<td  align="center" class="colEvnRight"><?= $sumsubtotal; ?> </td>
				</tr>
				<?php 
					
					} //penutup jika ada anak
							}
						$j=0;
						
						}
					
					}
				 ?>
			<tr class="trOdd">
				<td colspan="<?=3+(count($pendidikan_assoc))?>"  align="left">&nbsp;</td>
			</tr> 
			<tr class="trhead">
				<td colspan="<?=2+(count($pendidikan_assoc))?>"  align="left"><strong>TOTAL KESELURUHAN</strong></td>
				<td align="center">
					<font size="+1"> <strong><?=$grandtotal;?></strong></font>
				</td>
			</tr> 

		 </table>	
<!--jika unit kerja yang dipilih adalah per unit maka;-->		  
<?php
	}
	else {
		$query = mysql_query("select nama_unit from unit_kerja where (tingkat = '1' and left(kode_unit,2)='".left($unit_kerja,2)."')");		
		//echo "select nama_unit from unit_kerja where (tingkat = '1' and left(kode_unit,2)='".left($unit_kerja,2)."')";	
		if ($query) 
			{
			$data_induk=mysql_fetch_array($query);
			$nama_induk = $data_induk['nama_unit'];
			if ($nama_induk!='') { 
				$nama_induk = $data_induk['nama_unit'];

				}
			else
				{
					$query = mysql_query("select nama_unit from unit_kerja where (tingkat = '1' and left(kode_unit,1)='".left($unit_kerja,1)."')");					
					if ($query) {
						$data_induk=mysql_fetch_array($query); 
						$nama_induk = $data_induk['nama_unit'];}
				}
		}
		$query = mysql_query("select id_group,nama_unit from unit_kerja where kode_unit='".$unit_kerja."'");					
		if ($query) {
			$data_unit=mysql_fetch_array($query); 
			$nama_unit = $data_unit['nama_unit'];
			if (($data_unit['nama_unit'] == $nama_induk) and ($data_unit['id_group']=='1')) {
				$nama_induk ='PUSAT';}
			}	
			
		$nama_unit = $nama_induk.' ('.$nama_unit.')';
?>
		<table class="table-list" width="80%">
			<tr class="trOdd"><td colspan="<?=12+(count($pendidikan_assoc))?>"  align="center">
			<font size="+1"> <strong><?=$judul;?></strong></font>
			</td></tr>
			<tr class="trOdd"><td colspan="<?=12+(count($pendidikan_assoc))?>"  align="left">
			<?=$nama_unit;?>
			</td></tr>
			<tr class="trhead">
				<th width="20"  align="center" class="trhead">No</th>
				<th align="center"  class="trhead">Usia</th>
				<? $i=0;if($pendidikan_assoc)foreach($pendidikan_assoc as $komp){$i++; ?>
				<th style="min-width:20px" class="<?=$i % 2==0?"":"colEvnRight"?>" ><?=$komp['nama_pendidikan']?></th>
				<? } ?>
				<th align="center"  class="trhead">Jumlah</th>
			</tr>
			<?php
				$query = mysql_query("select *, replace(kode_unit,'0','') as tingkatan from unit_kerja 
				where kode_unit= '".$unit_kerja."' ");					
				$data_unit=mysql_fetch_array($query);
				$row_count = mysql_num_rows($query);
				if ($row_count>0){	
					$kd_unit = $data_unit['kode_unit'];
					$tingkatan = strlen($data_unit['tingkatan']);
					$pembanding=left($kd_unit,$tingkatan);		
				}
				
				if ($this->input->post('nama_search')!=''){
					$search_nama = " ((nama_pegawai like '%".$this->input->post('nama_search')."%') or (NIP like '".$this->input->post('nama_search')."'))";	
					}
				else{
					$search_nama = " (nama_pegawai like '%%')";	
				}
				
			
			if ($data_unit['tingkat'] == 1 and $data_unit['GD'] == 'G')   {
				$sql ="select  * from (
				select  (year(curdate())-year(tgl_lahir))  as umur from pegawai where 
				(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."'  OR kode_unit = '".$kd_unit."' 
				OR kode_unit_induk = '".$kd_unit."') and (eselon not like 'non-eselon') and ".$search_nama." ".$kriteria_jenis."
				group by (year(curdate())-year(tgl_lahir)) 
				UNION
				select  (year(curdate())-year(tgl_lahir))  as umur from pegawai where 
				kode_unit = '".$kd_unit."' and ".$search_nama." ".$kriteria_jenis."
				group by (year(curdate())-year(tgl_lahir))
				) as GABUNGAN order by umur desc";
				$nama_induk = $data_unit['nama_unit'];
				//echo $sql;
				}
			else{
				$sql = "select  (year(curdate())-year(tgl_lahir))  as umur from pegawai where 
				(kode_unit = '".$kd_unit."'  OR kode_unit_induk = '".$kd_unit."')
				and (eselon like 'non-eselon')  and ".$search_nama." ".$kriteria_jenis."
				group by (year(curdate())-year(tgl_lahir)) 
				order by (year(curdate())-year(tgl_lahir)) desc";							
				}	
				//echo $sql;
				
				$query_pegawai = mysql_query($sql);		
				$i = 0;			
				$subtotal = 0;
				$sumsubtotal = 0;
				while ($data_pegawai=mysql_fetch_array($query_pegawai)) {
					$i++;
					if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				?>
			<tr class="<?= $class; ?>">
				<td  align="left" class="colEvnRight"><?= $i;?> </td>
				<td align="center" class="colEvnRight"> 
				<?php 
					echo $data_pegawai['umur'];
				?> 
				</td>
				<? $j=1;if($pendidikan_assoc)	foreach($pendidikan_assoc as $komp){$j++;?>
				<td align="center" class="colEvnRight">
					<?php												
						
						if ($data_unit['tingkat'] == 1 and $data_unit['GD'] == 'G')   {
							
							$sql = "select count(kd_pegawai) as jml_didik from(
							select * from pegawai 
							where (left(kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR 
							kode_unit = '".$kd_unit."' 
							OR kode_unit_induk = '".$kd_unit."') and (eselon not like 'non-eselon') 
							and ".$search_nama." ".$kriteria_jenis."
							and  (year(curdate())-year(tgl_lahir))='".$data_pegawai['umur']."' and 
							id_pendidikan_terakhir = '".$komp['id_pendidikan']."'
							UNION
							select * from pegawai 
							where kode_unit = '".$kd_unit."' and ".$search_nama." ".$kriteria_jenis."
							and  (year(curdate())-year(tgl_lahir))='".$data_pegawai['umur']."' and 
							id_pendidikan_terakhir = '".$komp['id_pendidikan']."') as GABUNGAN
							";
							$query_pegawai_usia_unit = mysql_query($sql);
							}
						else{
							$sql = "select count(kd_pegawai) as jml_didik from pegawai 
							where (kode_unit = '".$kd_unit."'  OR kode_unit_induk = '".$kd_unit."')
							and (eselon like 'non-eselon')  and ".$search_nama."".$kriteria_jenis."
							and  (year(curdate())-year(tgl_lahir))='".$data_pegawai['umur']."' and 
							id_pendidikan_terakhir = '".$komp['id_pendidikan']."' ";
							$query_pegawai_usia_unit = mysql_query($sql);		
							}	
							
						$data_pegawai_usia_unit=mysql_fetch_array($query_pegawai_usia_unit);
						$row_count = mysql_num_rows($query_pegawai_usia_unit);
						if ($row_count>0){	
							if ($data_pegawai_usia_unit['jml_didik']>0){		
								echo $data_pegawai_usia_unit['jml_didik'];
								$subtotal = $subtotal + $data_pegawai_usia_unit['jml_didik'];
							}
							else{
								echo '-';
							}	
						}
						else{
							echo '-';
						}
						
						//echo $sql;
						
					?>
				</td>
				<? } ?>
				<td  align="center"class="colEvnRight">
				<?= $subtotal;
				$sumsubtotal = $sumsubtotal + $subtotal;
				$grandtotal=$grandtotal+$subtotal;
				$subtotal = 0; 
				?> </td>
			</tr>	
			<? } ?>	
			<tr class="trhead">
				<td  colspan="2" align="left" class="colEvn">Sub Total</td>
				<? $i=0;if($pendidikan_assoc)foreach($pendidikan_assoc as $komp){$i++; ?>
					<td align="center" class="colEvnRight">
						<?php												
							if ($data_unit['tingkat'] == 1 and $data_unit['GD'] == 'G')   {
								$sql="select count(kd_pegawai) as jml_didik from(
								select * from pegawai 
								where (left(kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR kode_unit = '".$kd_unit."' 
								OR kode_unit_induk = '".$kd_unit."') and (eselon not like 'non-eselon') and ".$search_nama." ".$kriteria_jenis."
								and id_pendidikan_terakhir = '".$komp['id_pendidikan']."'
								UNION
								select * from pegawai 
								where kode_unit = '".$kd_unit."' and ".$search_nama." ".$kriteria_jenis."
								and id_pendidikan_terakhir = '".$komp['id_pendidikan']."') as GABUNGAN
								";
								$query_pegawai_usia_unit = mysql_query($sql);
								}
							else{
								$sql="select count(kd_pegawai) as jml_didik from pegawai 
								where (kode_unit = '".$kd_unit."'  OR kode_unit_induk = '".$kd_unit."')
								and (eselon like 'non-eselon')  and ".$search_nama."".$kriteria_jenis."
								and id_pendidikan_terakhir = '".$komp['id_pendidikan']."'";
								$query_pegawai_usia_unit = mysql_query($sql);		
								}	
							$data_pegawai_usia_unit=mysql_fetch_array($query_pegawai_usia_unit);
							$row_count = mysql_num_rows($query_pegawai_usia_unit);
							if ($row_count>0){	
								if ($data_pegawai_usia_unit['jml_didik']>0){		
									echo $data_pegawai_usia_unit['jml_didik'];
								}
								else{
									echo '-';
								}	
							}
							else{
								echo '-';
							}
							//echo $sql;
						?>
					</td>
				<? } ?>
				<td  align="center" class="colEvn"><?= $sumsubtotal; ?> </td>

			</tr>
			<tr class="trOdd">
				<td colspan="<?=3+(count($pendidikan_assoc))?>"  align="left">&nbsp;</td>
			</tr> 
			<tr class="trhead">
				<td colspan="<?=2+(count($pendidikan_assoc))?>"  align="left"><strong>TOTAL KESELURUHAN</strong></td>
				<td align="center">
					<font size="+1"> <strong><?=$grandtotal;?></strong></font>
				</td>
			</tr>  
		</table>			
		<? }} ?>
		<br />
		<div class="paging"><?=$page_links?></div>
  </div>
</div>
