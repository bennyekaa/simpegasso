<font size="-3">
<table class="table-list" border="1">
		<tr  align="center">	
		    <th  width="20" align="center">No</th> 
			<th  width="74" align="center">NIP</th>
			<th  width="136" >Nama Pegawai</th>
            <th  width="44" >TMT KGB</th>
            <th  width="44" >TMT CPNS </th>
			<th  width="20" >GOL</th>
			<th  width="45" >MK</th>
            <th  width="45" >MKG</th>
			<th  width="38" >Gaji Pokok</th>
			<th  width="38" >Gaji Baru</th>
            <th  width="36" >Pendidikan</th>
    		<th  width="95" >Jabatan</th>
    		<th  width="130" >Unit Kerja</th>
		</tr>
		<?
		$i = $start++;
		
		if ($kerja_list!=FALSE){
		foreach ($kerja_list as $kerja) {
			$i++;
			if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
            if ($kerja['tmt_kgb']=='0000-00-00') {
                $str_tmt_kgb = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $kerja['tmt_kgb']);
                $str_tmt_kgb = $d . "/" . $m . "/" . $y;
                   
            }
            $kerja['tmt_kgb']=$str_tmt_kgb;
            
            if ($kerja['tmt_cpns']=='0000-00-00') {
                $str_tmt_cpns = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $kerja['tmt_cpns']);
                $str_tmt_cpns = $d . "/" . $m . "/" . $y;
                   
            }
            $kerja['tmt_cpns']=$str_tmt_cpns;
            if ($kerja['nama_jabatan']=='Fungsional Umum')
            {
				$kerja['nama_jabatan'] = '-';
			}
		?>  
		<tr>
		    <td width="20" align="center"><?= $i; ?></td>
			<td width="74" align="left">&nbsp;<?= $kerja['NIP']?></td>
            <td width="136" align="left">&nbsp;<?= trim($kerja['gelar_depan']." " .strtoupper($kerja['nama_pegawai'])." ".$kerja{'gelar_belakang'}) ?></td>
		    <td width="44" align="center"><?= $kerja['tmt_kgb']; ?></td>
			<td width="44" align="center"><?= $kerja['tmt_cpns']?></td>
			<td width="20" align="center"><?= $kerja['nama_golongan']; ?></td>
            <td width="45" align="center"><?= $kerja['masa_kerja']; ?></td>
			<td width="45" align="center"><?= $kerja['masa_kerja_golongan']; ?></td>
			<td width="38" align="right"><?= number_format($kerja['gajilama']); ?></td>
			<td width="38" align="right"><?= number_format($kerja['gajibaru']); ?></td>	
            <td width="36" align="center"><?= $kerja['nama_pendidikan']; ?></td>	
            <td width="95" align="left"><?= $kerja['nama_jabatan']?></td>
		 	 <td width="130" align="left"><?= $kerja['unit_kerja']; ?></td>	
		</tr>
		<? } }
		else {
			echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
		}?>
</table>
</font>

