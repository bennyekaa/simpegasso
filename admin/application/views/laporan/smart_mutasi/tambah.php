
<div class="content">
	<div id="content-header">
		<div id="module-title"><h3>Tambah Data Mutasi</h3></div>	
		<div class="clear"></div>
	</div>
</div>	
<div id="content-data"  style="padding-bottom:30px">
	<div style="text-align:center;font-weight:bolder"><?php echo $this->session->flashdata('msg') ?></div>
	<form action="<?php echo base_url() ?>laporan/smart_mutasi/simpan" method="POST">
		<table width="100%" border="0">
			
			<tr>
				<td colspan="3"><strong>Data SK</strong></td><td colspan="3"><strong>Data Pegawai</strong></td>
			</tr>
			<tr>
				<td width="20%">Nomor SK</td>
				<td width="0.5%">:</td>
				<td width="12%"><input type="text" name="nomor_sk" size="35"></td>

				<td width="20%">Nama</td>
				<td width="1%">:</td>
				<td width="65%">
					<input type ="text" name="nama" id="nama" size="35">
					<input type ="hidden" name="kd_pegawai" id="kd_pegawai">
					<input type ="hidden" name="id_golpangkat" id="id_golpangkat">
				</td>
			</tr>
			<tr>
				<td>Tanggal SK</td>
				<td>:</td>
				<td><input type="date" name="tgl_sk"></td>
				<td>Jabatan Lama</td>
				<td>:</td>
				<td>
					<input type="text" name="jabatan_lama" id="jabatan_lama" size="35">
					<input type="hidden" name="id_jabatan_lama" id="id_jabatan_lama">
				</td>
				
			</tr>
			<tr>
				<td>Tanggal Mutasi</td>
				<td>:</td>
				<td><input type="date" name="tgl_mutasi"></td>

				<td>OTK</td>
				<td>:</td>
				<td><select name="otk_lama" id="otk_lama">
						<option value="0">--Pilih OTK--</option>
						<option value="1983">OTK 1983</option>
						<option value="1999">OTK 1999</option>
						<option value="2012">OTK 2012</option>
				</select></td>
			</tr>
			<tr>
				
				<td>Mulai Berlaku SK</td>
				<td>:</td>
				<td><input type="date" name="tgl_berlaku_sk"></td>
				<td>Unit Kerja Lama</td>
				<td>:</td>
				<td>
					<!-- <input type="text" name="unit_kerja_lama" id='unit_kerja_lama' size="35"> -->
					<input type="hidden" name="id_unit_kerja_lama" id="id_unit_kerja_lama">
					<select name="unit_kerja_lama" id="unit_kerja_lama"><option value="">--Pilih OTK Sebelumnya--</option></select>
				</td>
				
				
			</tr>
			<tr>
				<td>Jenis Mutasi</td>
				<td>:</td>
				<td><?php //echo $jenis_mutasi ?></td>
				<td>Jenis Jabatan</td>
				<td>:</td>
				<td>
					<select name="jenis_jabatan" id="jenis_jabatan">
						<option value="">--Pilih Jenis Jabatan--</option>	
						<option value="struktural">Struktural</option>	
						<option value="fungsional_umum">Fungsional Tertentu</option>	
						<option value="fungsional_khusus">Fungsional Umum</option>	
					</select>
				</td>
				
			</tr>
			
			<tr>
				<td colspan="3"><?php echo $jenis_mutasi ?></td>
				<td>Jabatan Baru</td>
				<td>:</td>
				<td><select name="jabatan_baru" id="jabatan_baru"></select></td>
			</tr>
			<tr>
				<td style="vertical-align:top">Keterangan</td>
				<td style="vertical-align:top">:</td>
				<td></td>
				<td>Job Class</td>
				<td>:</td>
				<td><input type="text" name="job_class" id="job_class" disabled="true"></td>
			</tr>
			<tr>
				

				<td colspan="3" rowspan="4">
					<textarea name="keterangan_mutasi" id="" cols="50" rows="8" style="width:auto" placeholder="Keterangan Mutasi"></textarea>
				</td>
				<td>Job Value</td>
				<td>:</td>
				<td><input type="text" name="job_value" id="job_value" disabled="true"></td>
			</tr>
			<tr>
				<td>Pilih OTK</td>
				<td>:</td>
				<td>
					<select name="otk" id="pilih_otk">
						<option value="0">--Pilih OTK--</option>
						<option value="1983">OTK 1983</option>
						<option value="1999">OTK 1999</option>
						<option value="2012">OTK 2012</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Unit Kerja Baru</td>
				<td>:</td>
				<td><select name="unit_kerja_baru" id="unit_kerja_baru"><option value="">--Pilih OTK Sebelumnya--</option></select></td></td>
			</tr>
			<tr>
				
				
				<td>Status</td>
				<td>:</td>
				<td>
					<input type="radio" name="status" value="1"> Aktif 
					<input type="radio" name="status" value="0" checked=""> Nonaktif
				</td>
			</tr>

		
			<tr>
				<td colspan="3"></td><td colspan="3"><input type="submit" name="simpan" value="Simpan"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		<!-- <table width="45%" border="1">
			<tr>
				<td colspan="3"><strong>Data Pegawai</strong></td>
			</tr>
			<tr>
				<td>Nama</td>
				<td>:</td>
				<td>
					<input type ="text" name="nama" id="nama" size="24">
					<input type ="hidden" name="kd_pegawai" id="kd_pegawai">
					<input type ="hidden" name="id_golpangkat" id="id_golpangkat">
				</td>
			</tr>
			<tr>
				<td>Jenis Jabatan</td>
				<td>:</td>
				<td>
					<select name="jenis_jabatan" id="jenis_jabatan">
						<option value="">--Pilih Jenis Jabatan--</option>	
						<option value="struktural">Struktural</option>	
						<option value="fungsional_umum">Fungsional Umum</option>	
						<option value="fungsional_khusus">Fungsional Khusus</option>	
					</select>
				</td>
			</tr>
			<tr>
				<td>Jabatan Lama</td>
				<td>:</td>
				<td>
					<input type="text" name="jabatan_lama" id="jabatan_lama" size="24">
					<input type="hidden" name="id_jabatan_lama" id="id_jabatan_lama">
				</td>
			</tr>
			<tr>
				<td>Jabatan Baru</td>
				<td>:</td>
				<td><select name="jabatan_baru" id="jabatan_baru"></select></td>
			</tr>
			<tr>
				<td>Pilih OTK</td>
				<td>:</td>
				<td>
					<select name="otk" id="pilih_otk">
						<option value="">--Pilih OTK--</option>
						<option value="1983">OTK 1983</option>
						<option value="1999">OTK 1999</option>
						<option value="2012">OTK 2012</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Unit Kerja Lama</td>
				<td>:</td>
				<td>
					<input type="text" name="unit_kerja_lama" id='unit_kerja_lama' size="24">
					<input type="hidden" name="id_unit_kerja_lama" id="id_unit_kerja_lama">
				</td>
			</tr>
			<tr>
				<td>Unit Kerja Baru</td>
				<td>:</td>
				<td><select name="unit_kerja_baru" id="unit_kerja_baru"></select></td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td><input type="submit" name="simpan" value="Simpan"> </td>
			</tr>
		</table> -->
	</form>
	<div style="padding:10px;">
		<div>Data Mutasi</div>
	<table id="data_mutasi">
		<thead><tr>
			<td>No</td>
			<td>Nama</td>
			<td>NIP</td>
			<td>Gol/Pangkat</td>
			<td>Unit Kerja Asal</td>
			<td>Unit Kerja Baru</td>
		</tr></thead>
		<tbody>
			<?php echo $data_mutasi; ?>
		</tbody>
	</table>
	</div>
	
</div>


</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="<?php echo base_url() ?>public/js/jquery.autocomplete.js"></script>
<script src="<?php echo base_url() ?>pegawai/caripegawai/get_list_nama_pegawai"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#nama').autocomplete({
		lookup:nama,
		onSelect:function(suggestion){
			$("#kd_pegawai").val(suggestion.data.kd_pegawai);
			$("#id_golpangkat").val(suggestion.data.id_golpangkat);
			$("#jabatan_lama").val(suggestion.data.jabatan_lama);
			$("#unit_kerja_lama").val(suggestion.data.unit_kerja_lama);
			$("#id_jabatan_lama").val(suggestion.data.id_jabatan_lama);
			$("#id_unit_kerja_lama").val(suggestion.data.id_unit_kerja_lama);
			$("#otk_lama").val(suggestion.data.otk);
			fetch_unit_kerja('#unit_kerja_lama', suggestion.data.otk, suggestion.data.id_unit_kerja_lama);
			// $("#unit_kerja_lama").val(suggestion.data.id_unit_kerja_lama);

			// alert("You've selected :"+suggestion.value+", "+jabatan_lama+", "+unit_kerja_lama);
		}
	});
			$("#otk_lama").change(function(){
				var otk=$("#otk_lama").val();

				fetch_unit_kerja("#unit_kerja_lama", otk);
			});

		$('#jenis_jabatan').change(function(){
		var jenis_jabatan=$("#jenis_jabatan").val();
		var url="";
		if (jenis_jabatan==='struktural') {
			url="get_jabatan_struktural";
		}else if(jenis_jabatan==='fungsional_umum'){
			url="get_jabatan_fungsional_umum";
		}else if(jenis_jabatan==='fungsional_khusus'){
			url="get_jabatan_fungsional_khusus";
		}

			$.ajax({
				url:'<?php echo base_url() ?>laporan/smart_mutasi/'+url,
				dataType:'text',
				type:'GET',
			}).done(function(data){
				if (data!=null) {
					$("#jabatan_baru option").remove();
					$("#jabatan_baru").append(data);
				}
			});
			
		});

	$("#pilih_otk").change(function(){
		var otk=$("#pilih_otk").val();
		fetch_unit_kerja("#unit_kerja_baru", otk);
		
	});

	function fetch_unit_kerja(dom, otk_value, id_unit_kerja_lama=null){
		if (otk_value=='0') {
			$(dom+' option').remove();
				$(dom).append("<option value=''>--Pilih OTK Sebelumnya--</option>");
				return;
		}
		$.ajax({
			url:'<?php echo base_url() ?>laporan/smart_mutasi/get_unit_kerja_otk/'+otk_value,
			dataType:'text',
			type:'GET',
		}).done(function(data){
			if(data!=null){
				$(dom+' option').remove();
				$(dom).append(data);
				if (id_unit_kerja_lama!=null) {
					$(dom).val(id_unit_kerja_lama);
				}
			}
		});
	}

	$("#data_mutasi").DataTable();


	// menampilkan job clas & job value
	$("#jabatan_baru").change(function(){
		var jabatan_baru = $("#jabatan_baru").val();
		var golpangkat = $("#id_golpangkat").val();
			$.ajax({
				url:'<?php echo base_url() ?>laporan/smart_mutasi/getJVJC',
				dataType:'json',
				type:'POST',
				data: {jabatan_baru:jabatan_baru, golpangkat:golpangkat}
			}).done(function(data){
				// alert(data.job_class);
				if (data!="") {
					$("#job_class, #job_value").prop("disabled", false);
					$("#job_class").val(data.job_class);
					$("#job_value").val(data.job_value);
				}
			});
	});
});

</script>

