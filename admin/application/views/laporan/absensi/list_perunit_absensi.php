<div class="content">
<div id="content-header">

	<div id="module-title"><h3>
	  Laporan Absensi Pegawai
	</h3>
	</div>
	<div id="module-menu">
		<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/absensi/printtopdf/'.$group.'/'.$tahun.'/'.$unit_kerja)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/absensi/printtoxls/'.$group.'/'.$tahun.'/'.$unit_kerja.'/list_perunit_absensi.xls')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>	
	</div>
	<div class="clear"></div>
</div>
	
<div id="content-data">
	<div id='detail' style="position:absolute z-index"></div>
		<center>
			<form name="pegawaidetails" id="pegawaidetails" method="POST" action="<?=site_url('laporan/absensi/browse')?>" >
				<label>Bulan: </label>
				<?=form_dropdown('group',$group_assoc,$group);?>
				<label> Tahun: </label>
				<?=form_dropdown('tahun',$tahun_assoc,$tahun);?>
				<label>Unit Kerja : </label>
				<?=form_group_dropdown('unit_kerja',$unitkerja_assoc,$unit_kerja);?>
				<label>NIP/Nama Pegawai : </label>
				<input type="text" name="nama_search" size="25" value="<?=$nama_search;?>">
				<input name="mulai" type="submit" value="Cari">
			</form>
			
	  	</center>
		<br/>
		<?php
			if ($this->input->post('mulai')){
		?>	
		
		 <?php 
		 	//membaca jumlah cuti tahunan
			$query_cuti_tahunan  = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'jml_cuti_tahunan'");
			$datacuti=mysql_fetch_array($query_cuti_tahunan); 
			if ($datacuti!=FALSE){$jml_cuti_tahunan = $datacuti['id_var'];}
			else{$jml_cuti_tahunan = 0;	}
			
			//membaca jumlah minimal cuti
			$query_min_cuti  = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'min_cuti'");
			$datamin_cuti=mysql_fetch_array($query_min_cuti); 
			if ($datamin_cuti!=FALSE){$jml_min_cuti = $datamin_cuti['id_var'];}
			else{$jml_min_cuti = 0;	}
			
			//membaca bonus cuti jika tahun sebelumnya hanya mengambil maksimal sebebsar cuti minimal
			$query_bonus_cuti  = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'bonus_cuti'");
			$databonus_cuti=mysql_fetch_array($query_bonus_cuti); 
			if ($databonus_cuti!=FALSE){$jml_bonus_cuti = $databonus_cuti['id_var'];}
			else{$jml_bonus_cuti = 0; }
		 
		 	//membaca maksimla toleransi TK
			$query_maks_tk = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'maks_tk'");
			$datamaks_tk=mysql_fetch_array($query_maks_tk); 
			if ($datamaks_tk!=FALSE){$jml_maks_tk = $datamaks_tk['id_var'];}
			else{$jml_maks_tk = 0; }
		 
		 	if ($unit_kerja == 'Semua'){
			?>
			<table class="table-list" width="100%">
				<tr class="trOdd">
					<td colspan="15"  align="center">
						<font size="+1"> <strong><?=$judul;?></strong></font>
					</td>
				</tr>
				<tr class="trOdd">
					<td colspan="15"  align="center">
						<font size="+1"> <strong><?=$judul_unit;?></strong></font>
					</td>
				</tr>
				<?php 
				//$query = mysql_query("select *, replace(kode_unit,'0','') as tingkatan from unit_kerja where tingkat < 3 order by kode_unit");					
				$query = mysql_query("select *, replace(kode_unit,'0','') as tingkatan from unit_kerja order by kode_unit");					
				if ($query) {
					while ($data_unit=mysql_fetch_array($query)) {
							$kd_unit = $data_unit['kode_unit'];
							$tingkatan = strlen($data_unit['tingkatan']);
							$pembanding=left($kd_unit,$tingkatan);		
				?>
					
				<?php
				
					if ($this->input->post('nama_search')!=''){
						$search_nama = " ((nama_pegawai like '%".$this->input->post('nama_search')."%') or (NIP like '".$this->input->post('nama_search')."') and status_pegawai < 3)";	
					}
					else{
						$search_nama = " (nama_pegawai like '%%') and status_pegawai < 3";	
					}
					if ($data_unit['tingkat'] == 1){
						$sql ="select * from pegawai where 
						(left(kode_unit,LENGTH('".$pembanding."')) = '".$pembanding."' OR kode_unit = '".$kd_unit."' OR kode_unit_induk = '".$kd_unit."') 
						and (eselon not like 'non-eselon') and ".$search_nama." order by id_golpangkat_terakhir desc";
   					    $nama_induk = $data_unit['nama_unit'];
						}
					else{
						$sql = "select * from pegawai where (kode_unit = '".$kd_unit."'  OR kode_unit_induk = '".$kd_unit."')
						and (eselon like 'non-eselon')  and ".$search_nama." order by kode_unit,id_golpangkat_terakhir desc";							
						}	
					//echo $sql;
					$query_pegawai = mysql_query($sql);			
					$row_count = mysql_num_rows($query_pegawai);
					//echo $row_count.'__';
					$j=0;
						 	
					if (($query_pegawai) and $row_count>0 ){	
							$query_anak = mysql_query("select * from unit_kerja where kode_unit_general='".$kd_unit."'");
							
							if ($data_unit['tingkat'] == 1){							
								$ada_anak = '';}
							else if ($query_anak) {
								$data_anak=mysql_fetch_array($query_anak);
								if ($data_anak!=FALSE) {
									$ada_anak = $data_anak['kode_unit'];
								}
								else {
									$ada_anak = '';
									}
								}
							
					if ($ada_anak =='') {		
						echo "<tr><td colspan='12'>&nbsp;</td></tr>";
						if (($data_unit['nama_unit'] == $nama_induk) and ($data_unit['id_group']=='1')) {
							echo "<tr><td colspan='12'><strong> PUSAT (".$data_unit['nama_unit'].")</strong></td></tr>";}
						else if (($data_unit['nama_unit'] == $nama_induk) and ($data_unit['id_group']=='2')) {
							echo "<tr><td colspan='12'><strong> FAKULTAS (".$data_unit['nama_unit'].")</strong></td></tr>";}
						else {
							echo "<tr><td colspan='12'><strong>".$nama_induk." (".$data_unit['nama_unit'].")</strong></td></tr>";
						}
					?>

					<tr class="trhead">
						<th  width="20" rowspan="2" align="center" class="trhead">No</th>
						<th width="130" rowspan="2" align="center" class="trhead"><span class="trhead">NIP</span></th>
						<th width="260" rowspan="2" align="center" class="trhead">Nama Pegawai</th>
						<th width="27" rowspan="2" align="center" class="trhead">Ijin</th>
						
						<th width="27" rowspan="2" align="center" class="trhead">S</th>
						<th width="27" rowspan="2" align="center" class="trhead">SD</th>
						<th width="27" rowspan="2" align="center" class="trhead">CT</th>
						<th width="27" rowspan="2" align="center" class="trhead">TK</th>
						<th width="27" rowspan="2" align="center" class="trhead">TD</th>
						<th width="27" rowspan="2" align="center" class="trhead" >TB</th>
						<th width="27" rowspan="2" align="center" class="trhead">MPP</th>
						<th width="70" colspan="2" align="center" class="trhead">Jumlah</th>
						<th width="60" rowspan="2" align="center" class="trhead">%Hadir</th>
						<th width="150" rowspan="2" align="center" class="trhead">Tdk Hadir Bulan</th>
					</tr>
					<tr class="trhead">
						<th width="35" align="center" class="colEvn">Tdk Hadir</th>
						<th width="35" align="center" class="colEvn">Hadir</th>
					</tr>					
									
					<?php 				
						while ($data_pegawai=mysql_fetch_array($query_pegawai)) {
							if ($group=='0') {
								$query_absen = mysql_query("select  kd_pegawai, sum(Ijin) as Ijin, sum(TK) as TK, sum(TB) as TB, 
								sum(TD) as TD, sum(Sakit) as Sakit, sum(SD) as SD, sum(CT) as CT, sum(MPP) as MPP, 
								sum(total_absen) as total_absen 
								FROM absensi WHERE kd_pegawai='".$data_pegawai['kd_pegawai']."' and 
								kode_unit='".$data_pegawai['kode_unit']."' and left(periode,4) = '".$tahun."' 
								GROUP BY left(periode,4),kd_pegawai,kode_unit ");
								$query_hari_efektif = mysql_query("select  sum(jml_hari) as total_hari 
								FROM hari_efektif WHERE left(periode,4) = '".$tahun."' GROUP BY left(periode,4)");
								$query_bulan = mysql_query("select  distinct right(periode,2) as bulan FROM absensi WHERE left(periode,4) = '".$tahun."' 
								and kd_pegawai='".$data_pegawai['kd_pegawai']."' and total_absen>0 order by right(periode,2)");
								$array_bulan='';
								if ($query_bulan) {
								while ($data_bulan=mysql_fetch_array($query_bulan)) {
										$array_bulan = $array_bulan.''.$data_bulan['bulan'];
									}
								}
							}
							else 
								{
								$query_absen = mysql_query("select * from absensi where kd_pegawai='".$data_pegawai['kd_pegawai']."' and 
								kode_unit='".$data_pegawai['kode_unit']."' and	periode = '".$tahun.$group."'");
								$query_hari_efektif = mysql_query("select  sum(jml_hari) as total_hari 
								FROM hari_efektif WHERE periode = '".$tahun.$group."' GROUP BY periode");
								$query_bulan = mysql_query("select  distinct right(periode,2) as bulan FROM absensi WHERE left(periode,4) = '".$tahun."' 
								and right(periode,2)='".$group."' and kd_pegawai='".$data_pegawai['kd_pegawai']."' and total_absen>0 order by right(periode,2)");
								$array_bulan='';
								if ($query_bulan) {
								while ($data_bulan=mysql_fetch_array($query_bulan)) {
										$array_bulan = $array_bulan.''.$data_bulan['bulan'];
									}
								}
							}		
								
							$datahari=mysql_fetch_array($query_hari_efektif); 
							if ($datahari!=FALSE){$jml_hari = $datahari['total_hari'];}
							else{$jml_hari = 0;	}
											
							$datasen=mysql_fetch_array($query_absen); 
							if ($datasen!=FALSE){
									$Ijin = $datasen['Ijin'];
									$TK = $datasen['TK'];
									$TB = $datasen['TB'];
									$TD = $datasen['TD'];
									$Sakit = $datasen['Sakit'];
									$SD = $datasen['SD'];
									$CT = $datasen['CT'];
									$MPP = $datasen['MPP'];
									$jml_tk = $jml_tk + $TK ;
									$jml_ijin = $jml_ijin + $Ijin ;
									$jml_cuti = $jml_cuti + $Ijin + $CT + $TK  ;
									$total_absen = $datasen['total_absen'];
									if (trim($Ijin)=='0'){$Ijin='';}
									if (trim($TK)=='0'){$TK='';}
									if (trim($TB)=='0'){$TB='';}
									if (trim($TD)=='0'){$TD='';}
									if (trim($Sakit)=='0'){$Sakit='';}
									if (trim($SD)=='0'){$SD='';}
									if (trim($CT)=='0'){$CT='';}
									if (trim($MPP)=='0'){$MPP='';}
								}
							else {
									$Ijin = '';
									$TK = '';
									$TB = '';
									$TD = '';
									$Sakit = '';
									$SD = '';
									$CT = '';
									$MPP = '';
									$total_absen = '0';
							}
							$jml_hadir = $jml_hari - $total_absen ;
							if (trim($jml_hadir)<0){$jml_hadir='0';}
							if (($jml_hari>0) or ($jml_hadir>0)) {
								$prosen_jml_hadir = round(($jml_hadir/$jml_hari) * 100,2);							
							}
							else {$prosen_jml_hadir = 0;}
							//tambahan pembacaan cuti tahun lalu
							$tahun_lalu = $tahun - 1;
							$query_cuti_lalu = mysql_query("select  sum(Ijin+CT+TK)  as cuti_lalu 
							FROM absensi WHERE left(periode,4) = '".$tahun_lalu."' 
							and kd_pegawai='".$data_pegawai['kd_pegawai']."'");
							$data_cuti_lalu=mysql_fetch_array($query_cuti_lalu); 
							if ($data_cuti_lalu!=FALSE){$jml_cuti_lalu = $data_cuti_lalu['cuti_lalu'];}
							else{$jml_cuti_lalu = 0; }
							
							if ($jml_cuti_lalu > $jml_min_cuti)
								$bonus = 0; 
							else
								$bonus = $jml_bonus_cuti;
							
							if (($tahun_lalu=='2010') and ($bonus=6)) $bonus=0;
							$j++;
							if (($j%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
							if ($total_absen > 0) { //jika pernah absen
						?>
								<tr class="<?= $class; ?>">
									<td  align="center" class="colEvn"><?= $j;?> 
									</td>
									<td align="left" class="trhead"> <?=$data_pegawai['NIP']?> </td>
									<td align="left" class="trhead"><?=$data_pegawai['gelar_depan']." ".strtoupper($data_pegawai['nama_pegawai'])." ".$data_pegawai['gelar_belakang']?></td>
									<td align="center" class="trhead"><?=$Ijin;?></td>
									<td  align="center" class="trhead"><?=$Sakit;?></td>
									<td  align="center" class="trhead"><?=$SD;?></td>
									<td  align="center" class="trhead"><?=$CT;?></td>
									<td  align="center" class="colEvn"><?=$TK;?></td>
									<td  align="center" class="trhead"><?=$TD;?></td>
									<td align="center" class="trhead"><?=$TB;?></td>
									<td  align="center" class="trhead"><?=$MPP;?></td>
									<td  align="center" class="trhead"> <strong><?=$total_absen;?></strong></td>
									<td  align="center" class="trhead"> <strong><?=$jml_hadir;?></strong></td>
									<td  align="center" class="trhead"> <strong><?=$prosen_jml_hadir;?></strong></td>
									<td  align="center" class="trhead"> <?=$array_bulan;?></td>
								</tr>
							<?php
							$nama_peg = $data_pegawai['gelar_depan']." ".strtoupper($data_pegawai['nama_pegawai'])." ".$data_pegawai['gelar_belakang'];		
						}
						}//penutup jika pernah absen
						if ($row_count < 2) {
						?>
							<!--dihilangkan
							<tr class="trEvn">
								<td colspan="15">
								<table width="100%" border="1" class="table-list">
									<tr>
										<td width="27%" class="colEvn" align="left"><strong>Detail cuti :<?=$nama_peg?> &raquo;</strong> </td>
										<td width="10%" class="colEvn" align="center">TK <?=$tahun?>:&nbsp;&nbsp;<?=$jml_tk;?> hari</td>
										<td width="10%" class="colEvn" align="center">Ijin <?=$tahun?>:&nbsp;&nbsp;<?=$jml_ijin;?> hari</td>
										<td width="11%" class="colEvn" align="center">Cuti <?=$tahun?>:&nbsp;&nbsp;<?=$jml_cuti;?> hari</td>
										<td width="11%" class="colEvn" align="center">Cuti <?=$tahun_lalu?>:&nbsp;&nbsp;<?=$jml_cuti_lalu;?> hari</td>
										<td width="11%" class="colEvn" align="center">Sisa cuti <?=$tahun_lalu?>:&nbsp;&nbsp;<?=$bonus;?> hari</td>
										<td width="20%" class="colEvn" align="center">Cuti yg blm diambil:&nbsp;&nbsp;<strong><?=$jml_cuti_tahunan+$bonus-$jml_cuti;?> hari</strong></td>
									</tr>
									<?php
										if ($jml_tk>$jml_maks_tk){
										?>
										<tr>
											<td class="colEvn" align="center" colspan="7">
							<blink><strong><font color="#000000"> Tahun ini <?=$nama_peg?> telah tidak masuk tanpa keterangan sebanyak</font> <font color="#FF0000"><?=$jml_tk?> kali! </font></strong> </blink>
											</td>
										</tr>
									<? } ?>	
								</table>
								</td>
							</tr>-->
						<?php
						}
						$jml_tk = 0;
						$jml_cuti = 0;
					} //penutup jika ada anak
					
							}
						$j=0;
						
						}
					
					}
				 ?>
				
		  </table>
		  
		  
		  
<!--jika unit kerja yang dipilih adalah per unit maka;-->		  
		<?php
			}
			else {
				$query = mysql_query("select nama_unit from unit_kerja where (tingkat = '1' and left(kode_unit,2)='".left($unit_kerja,2)."')");					
				if ($query) 
					{
					$data_induk=mysql_fetch_array($query);
					$nama_induk = $data_induk['nama_unit'];
					if ($nama_induk='') { 
						$nama_induk = $data_induk['nama_unit'];}
					else
						{
							$query = mysql_query("select nama_unit from unit_kerja where (tingkat = '1' and left(kode_unit,1)='".left($unit_kerja,1)."')");					
							if ($query) {
								$data_induk=mysql_fetch_array($query); 
								$nama_induk = $data_induk['nama_unit'];}
						}
				}
				$query = mysql_query("select id_group,nama_unit from unit_kerja where kode_unit='".$unit_kerja."'");					
				if ($query) {
					$data_unit=mysql_fetch_array($query); 
					$nama_unit = $data_unit['nama_unit'];
					if (($data_unit['nama_unit'] == $nama_induk) and ($data_unit['id_group']=='1')) {
						$nama_induk ='PUSAT';}
					}	
				$nama_unit = $nama_induk.' ('.$nama_unit.')';
		?>
		
		
			<table class="table-list" width="100%">
				<tr class="trOdd"><td colspan="15"  align="center">
				<font size="+1"> <strong><?=$judul;?></strong></font>
				</td></tr>
				<tr class="trOdd"><td colspan="15"  align="left">
				<?=$nama_unit;?>
				</td></tr>
				<tr class="trhead">
					<th  width="20" rowspan="2" align="center" class="trhead">No</th>
					<th width="130" rowspan="2" align="center" class="trhead"><span class="trhead">NIP</span></th>
					<th width="260" rowspan="2" align="center" class="trhead">Nama Pegawai</th>
					<th width="27" rowspan="2" align="center" class="trhead">Ijin</th>
					
					<th width="27" rowspan="2" align="center" class="trhead">S</th>
					<th width="27" rowspan="2" align="center" class="trhead">SD</th>
					<th width="27" rowspan="2" align="center" class="trhead">CT</th>
					<th width="27" rowspan="2" align="center" class="trhead">TK</th>
					<th width="27" rowspan="2" align="center" class="trhead">TD</th>
					<th width="27" rowspan="2" align="center" class="trhead" >TB</th>
					<th width="27" rowspan="2" align="center" class="trhead">MPP</th>
					<th width="70" colspan="2" align="center" class="trhead">Jumlah</th>
					<th width="60" rowspan="2" align="center" class="trhead">%Hadir</th>
					<th width="150" rowspan="2" align="center" class="trhead">Tdk Hadir Bulan</th>
				</tr>
				<tr class="trhead">
					<th width="35" align="center" class="colEvn">Tdk Hadir</th>
					<th width="35" align="center" class="colEvn">Hadir</th>
				</tr>
				<?
				$i = $start++;
				if ($kerja_list!=FALSE){
				foreach ($kerja_list as $pegawai) {
					$i++;
					if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
					
				?>   
				<?php 
					$nama_peg = $pegawai['gelar_depan']." ".$pegawai['nama_pegawai']." ".$pegawai['gelar_belakang'];
					
						if ($group=='0') {
							$query = mysql_query("select  kd_pegawai, sum(Ijin) as Ijin, sum(TK) as TK, sum(TB) as TB, sum(TD) as TD, sum(Sakit) as Sakit,
							sum(SD) as SD, sum(CT) as CT, sum(MPP) as MPP, sum(total_absen) as total_absen 
							FROM absensi WHERE kd_pegawai='".$pegawai['kd_pegawai']."' and kode_unit='".$pegawai['kode_unit']."' and
							left(periode,4) = '".$tahun."' GROUP BY left(periode,4),kd_pegawai,kode_unit");
							$query_hari_efektif = mysql_query("select  sum(jml_hari) as total_hari 
							FROM hari_efektif WHERE left(periode,4) = '".$tahun."' GROUP BY left(periode,4)");
							$query_bulan = mysql_query("select  distinct right(periode,2) as bulan FROM absensi WHERE left(periode,4) = '".$tahun."' 
							and kd_pegawai='".$pegawai['kd_pegawai']."' and total_absen>0 order by right(periode,2)");
							$array_bulan='';
							if ($query_bulan) {
							while ($data_bulan=mysql_fetch_array($query_bulan)) {
									$array_bulan = $array_bulan.''.$data_bulan['bulan'];
								}
							}
						}
						
						else {
							$query = mysql_query("select * from absensi where kd_pegawai='".$pegawai['kd_pegawai']."' and kode_unit='".$pegawai['kode_unit']."' and
							periode = '".$tahun.$group."' ");	
							$query_hari_efektif = mysql_query("select  sum(jml_hari) as total_hari 
							FROM hari_efektif WHERE periode = '".$tahun.$group."' GROUP BY periode");
							$query_bulan = mysql_query("select  distinct right(periode,2) as bulan FROM absensi WHERE left(periode,4) = '".$tahun."' 
							and right(periode,2)='".$group."' and kd_pegawai='".$pegawai['kd_pegawai']."'  and total_absen>0 order by right(periode,2)");
							$array_bulan='';
							if ($query_bulan) {
							while ($data_bulan=mysql_fetch_array($query_bulan)) {
									$array_bulan = $array_bulan.''.$data_bulan['bulan'];
								}
							}
						}		
							
						$datahari=mysql_fetch_array($query_hari_efektif); 
						if ($datahari!=FALSE){$jml_hari = $datahari['total_hari'];}
						else{$jml_hari = 0;	}
						
						$datasen=mysql_fetch_array($query); 
						if ($datasen!=FALSE){
								$Ijin = $datasen['Ijin'];
								$TK = $datasen['TK'];
								$TB = $datasen['TB'];
								$TD = $datasen['TD'];
								$Sakit = $datasen['Sakit'];
								$SD = $datasen['SD'];
								$CT = $datasen['CT'];
								$MPP = $datasen['MPP'];
								$total_absen = $datasen['total_absen'];
								$jml_tk = $jml_tk + $TK ;
								$jml_ijin = $jml_ijin + $Ijin ;
								$jml_cuti = $jml_cuti + $Ijin + $CT + $TK ;
								if (trim($Ijin)=='0'){$Ijin='';}
								if (trim($TK)=='0'){$TK='';}
								if (trim($TB)=='0'){$TB='';}
								if (trim($TD)=='0'){$TD='';}
								if (trim($Sakit)=='0'){$Sakit='';}
								if (trim($SD)=='0'){$SD='';}
								if (trim($CT)=='0'){$CT='';}
								if (trim($MPP)=='0'){$MPP='';}
							}
						else 
							{
								$Ijin = '';
								$TK = '';
								$TB = '';
								$TD = '';
								$Sakit = '';
								$SD = '';
								$CT = '';
								$MPP = '';
								$total_absen = '0';
							}
						$jml_hadir = $jml_hari - $total_absen ;
						if (trim($jml_hadir)<0){$jml_hadir='0';}
						if (($jml_hari>0) or ($jml_hadir>0)) {
							$prosen_jml_hadir = round(($jml_hadir/$jml_hari) * 100,2);							
						}
						else {$prosen_jml_hadir = 0;}
						
						//tambahan pembacaan cuti tahun lalu
						$tahun_lalu = $tahun - 1;
						$query_cuti_lalu = mysql_query("select  sum(Ijin+CT+TK)  as cuti_lalu FROM absensi WHERE left(periode,4) = '".$tahun_lalu."' 
						and kd_pegawai='".$pegawai['kd_pegawai']."'");
						
						$data_cuti_lalu=mysql_fetch_array($query_cuti_lalu); 
						if ($data_cuti_lalu!=FALSE){$jml_cuti_lalu = $data_cuti_lalu['cuti_lalu'];}
						else{$jml_cuti_lalu = 0; }
						
						if ($jml_cuti_lalu > $jml_min_cuti)
							$bonus = 0; 
						else
							$bonus = $jml_bonus_cuti;
						
						if ($total_absen > 0) { //jika penah absen
					?>
				<tr class="<?= $class; ?>">				
					<td  align="center" class="colEvn">	<?= $i;?>
					</td>
					<td align="left" class="trhead"> <?=$pegawai['NIP']?> </td>
					<td align="left" class="trhead"> <?=$pegawai['gelar_depan']." ".strtoupper($pegawai['nama_pegawai'])." ".$pegawai['gelar_belakang']?> </td>
					
					<td align="center" class="trhead"><?=$Ijin;?></td>
					
					<td align="center" class="trhead"><?=$Sakit;?></td>
					<td align="center" class="trhead"><?=$SD;?></td>
					<td align="center" class="trhead"><?=$CT;?></td>
					<td align="center" class="trhead"><?=$TK;?></td>
					<td align="center" class="trhead"><?=$TD;?></td>
					<td align="center" class="trhead"><?=$TB;?></td>
					<td align="center" class="trhead"><?=$MPP;?></td>
					<td  align="center" class="trhead"> <strong><?=$total_absen;?></strong></td>
					<td  align="center" class="trhead"> <strong><?=$jml_hadir;?></strong></td>
					<td  align="center" class="trhead"> <strong><?=$prosen_jml_hadir;?></strong></td>
					<td  align="center" class="trhead"> <?=$array_bulan;?></td>
					
				</tr>
				
				<? } }}//penutup jika pernah absen
					else {
						$conspan = ($group == '') ? 7 : 6;
						echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
					} 
				if ($i < 2) {
				?>
				
				<!--dihilangkan
				<tr class="trOdd">
					<td colspan="15">
					<table width="100%" border="1" class="table-list">
						<tr>
							<td width="27%" class="colEvn" align="left"><strong>Detail cuti :<?=$nama_peg?> &raquo;</strong> </td>
							<td width="10%" class="colEvn" align="center">TK <?=$tahun?>:&nbsp;&nbsp;<?=$jml_tk;?> hari</td>
							<td width="10%" class="colEvn" align="center">Ijin <?=$tahun?>:&nbsp;&nbsp;<?=$jml_ijin;?> hari</td>
							<td width="11%" class="colEvn" align="center">Cuti <?=$tahun?>:&nbsp;&nbsp;<?=$jml_cuti;?> hari</td>
							<td width="11%" class="colEvn" align="center">Cuti <?=$tahun_lalu?>:&nbsp;&nbsp;<?=$jml_cuti_lalu;?> hari</td>
							<td width="11%" class="colEvn" align="center">Sisa cuti <?=$tahun_lalu?>:&nbsp;&nbsp;<?=$bonus;?> hari</td>
							<td width="20%" class="colEvn" align="center">Cuti yg blm diambil:&nbsp;&nbsp;<strong><?=$jml_cuti_tahunan+$bonus-$jml_cuti;?> hari</strong></td>
						</tr>
						<?php
						if ($jml_tk>$jml_maks_tk){
						?>
						<tr>
							<td class="colEvn" align="center" colspan="6">
							<blink><strong><font color="#000000"> Tahun ini <?=$nama_peg?> telah tidak masuk tanpa keterangan sebanyak</font> <font color="#FF0000"><?=$jml_tk?> kali! </font></strong> </blink>
							</td>
						</tr>
						<? } ?>			
					</table>
					</td>
				</tr>-->
				<? } ?>		
			</table>		
		<? } }?>
		<br />
		<div class="paging"><?=$page_links?></div>
  </div>
</div>
