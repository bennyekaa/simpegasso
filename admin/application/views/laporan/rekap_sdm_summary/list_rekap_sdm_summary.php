
<div class="content">
<div id="content-header">

	<div id="module-title">
		<h3>Daftar Pegawai</h3>
	</div>
	<div id="module-menu">
		<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/rekap_sdm_summary/printtopdf/'.$group.'/'.$unit_kerja)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/rekap_sdm_summary/printtoxls/'.$group.'/'.$unit_kerja.'/REKAP SDM PERJENIS PEGAWAI')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>	
	</div>
	<div class="clear"></div>
</div>
	
<div id="content-data">
	<div id='detail' style="position:absolute z-index"></div>
		<center>
			<form name="pegawaidetails" id="pegawaidetails" method="POST" action="<?=site_url('laporan/rekap_sdm_summary/browse')?>" >
				<label>Jenis Pegawai : </label>
				<?=form_dropdown('group',$adm_akd_assoc,$group);?>
				<input name="mulai" type="submit" value="Cari">
			</form>
	  	</center>
		<br/>
		<?php
		if ($this->input->post('mulai'))
		{
		 	if ($unit_kerja == 'Semua')
			{
		?>
	
			<table class="table-list" width="50%" align="center">
				<tr class="trOdd">
					<td colspan="4"  align="center">
						<font size="+1"> <strong><?=$judul;?></strong></font>
					</td>
				</tr>
				<tr class="trOdd">
					<td colspan="4"  align="left">
						<font size="+1"> Summary Pegawai perJenis Pegawai </strong></font>
					</td>
				</tr>
				<tr class="trhead" >
					<td width="46" align="center" class="trhead">No</td>
					<td width="250" colspan="2" align="center" class="trhead"><span class="trhead">Jabatan</span></td>
					<td width="100" align="center" class="trhead">Jumlah</td>
					
				</tr>	
					
				<?php 
				$nomor=0;
				$totalSDM = 0;
				$sql_summary = "select id_jns_pegawai, (select jenis_pegawai from jenis_pegawai 
				where id_jns_pegawai=pegawai.id_jns_pegawai)as jenis_pegawai,
				case WHEN status_pegawai=1 then 'CPNS' else (select nama_jabatan from jabatan
				where id_jabatan=pegawai.id_jabatan_terakhir) end as jabatan_fungsional, count(kd_pegawai)  as jumlah
				from pegawai WHERE status_pegawai< 3 and id_jns_pegawai ='".$_POST['group']."' 
				GROUP BY id_jns_pegawai, id_jabatan_terakhir ORDER BY id_jns_pegawai, id_jabatan_terakhir";
				
				//echo $sql_summary;
				
				$query_summary = mysql_query($sql_summary);
				while ($data_summary=mysql_fetch_array($query_summary)) 
				{
				$nomor++;
				if (($nomor%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				?>
				<tr class="<?=$class?>">
					<td  width="46" align="center" class="trhead"><?=$nomor?></td>
					<td width="250" colspan="2" align="left" class="trhead"><?=$data_summary['jabatan_fungsional']?></td>
					<td width="100" align="center" class="trhead"><?=$data_summary['jumlah']?></td>
	
				</tr>	
				<?
				$totalSDM = $totalSDM + $data_summary['jumlah'];
				}
				?>
				<tr class="trhead" >
					<td  width="296" colspan="3" align="left" class="trhead">Total SDM</td>
					<td width="100" align="center" class="trhead"><?=$totalSDM?></td>
					
				</tr>	
				
				
				<tr class="trOdd">
					<td colspan="4">&nbsp;</td>
				</tr>	
				
				<tr class="trOdd">
					<td colspan="4"  align="left">
						<font size="+1"> Summary Pegawai perGolongan </strong></font>
					</td>
				</tr>
				
				<tr class="trhead" >
					<td  width="46" align="center" class="trhead">No</td>
					<td width="175" align="center" class="trhead"><span class="trhead">Jabatan</span></td>
					<td width="75" align="center" class="trhead"><span class="trhead">Golongan</span></td>
					<td width="100" align="center" class="trhead">Jumlah</td>
					
				</tr>
				
				
				<?php 
				$nomor=0;
				$sql_summary = "select id_jns_pegawai, (select jenis_pegawai from jenis_pegawai 
				where id_jns_pegawai=pegawai.id_jns_pegawai)as jenis_pegawai,
				case WHEN status_pegawai=1 then 'CPNS' else
				(select nama_jabatan from jabatan
				where id_jabatan=pegawai.id_jabatan_terakhir) end as jabatan_fungsional, 
				(select  golongan_pangkat.golongan from golongan_pangkat 
				where golongan_pangkat.id_golpangkat=pegawai.id_golpangkat_terakhir) as golongan,
				count(kd_pegawai)  as jumlah
				from pegawai 
				WHERE status_pegawai< 3 and id_jns_pegawai ='".$_POST['group']."' 
				GROUP BY id_jns_pegawai, id_jabatan_terakhir, id_golpangkat_terakhir
				ORDER BY id_jns_pegawai, id_jabatan_terakhir, id_golpangkat_terakhir";
				
				//echo $sql_summary;
				
				$query_summary = mysql_query($sql_summary);
				$totalSDMGol =0;
				while ($data_summary=mysql_fetch_array($query_summary)) 
				{
				$nomor++;
				if (($nomor%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				?>
				<tr class="<?=$class?>">
					<td  width="46" align="center" class="trhead"><?=$nomor?></td>
					<td width="175" align="left" class="trhead"><?=$data_summary['jabatan_fungsional']?></td>
					<td width="75" align="center" class="trhead"><?=$data_summary['golongan']?></td>
					<td width="100" align="center" class="trhead"><?=$data_summary['jumlah']?></td>
	
				</tr>	
				
			
				<?
				$totalSDMGol = $totalSDMGol + $data_summary['jumlah'];
				}
				?>
				<tr class="trhead" >
					<td  width="296" colspan="3" align="left" class="trhead">Total SDM </td>
					<td width="100" align="center" class="trhead"><?=$totalSDMGol?></td>
					
				</tr>	
					
			</table>	

		<? }} ?>
		  

		
  </div>
</div>
