<table class="table-list" cellpadding="4" border="1">
	<tr bgcolor="#E1E1E1">
		  <th width="20" align="center" >No</th>
		  <th width="105" >NIP</th>
		  <th width="120" >Nama</th>
		  <th width="60" >TMT KP</th>
          <th width="60" >TMT CPNS </th>
		  <th width="60" >Gol/Ruang </th>
		  <th width="97" >Masa Kerja</th>
		  <th width="50" >Pend.</th>
		  <th width="73" >Jabatan</th>
		  <th width="133" >Unit Kerja</th>
  </tr>
		
		<?
		$i = $start++;
		if ($kerja_list!=FALSE){
		foreach ($kerja_list as $jabatan) {
			$i++;
			if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
			
            if ($jabatan['tmt_golpangkat_terakhir']=='0000-00-00') {
                $str_tmt_golpangkat_terakhir = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jabatan['tmt_golpangkat_terakhir']);
                $str_tmt_golpangkat_terakhir = $d . "/" . $m . "/" . $y;
                   
            }
            $jabatan['tmt_golpangkat_terakhir'] =$str_tmt_golpangkat_terakhir;
            if ($jabatan['tmt_cpns']=='0000-00-00') {
                $str_tmt_cpns = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jabatan['tmt_cpns']);
                $str_tmt_cpns = $d . "/" . $m . "/" . $y;
                   
            }
            $jabatan['tmt_cpns']=$str_tmt_cpns;
           
            if ($jabatan['nama_jabatan']=='Fungsional Umum')
			{
				$jabatan['nama_jabatan']='-';
			}
		?>   
		<tr bgcolor="#FCFBFB">
			<td width="20" align="center" ><?= $i; ?></td>
			<td width="105"align="left"><?=$jabatan['NIP']?></td>
			<td width="120" align="left"><?= $jabatan['gelar_depan']." ".$jabatan['nama_pegawai'].$jabatan['gelar_belakang']; ?></td>
			<td width="60" align="center"><?=$jabatan['tmt_golpangkat_terakhir']?></td>
			<td width="60" align="center"><?=$jabatan['tmt_cpns']?></td>
			<td width="60" align="center"><?= $jabatan['nama_golongan']; ?></td>
			<td width="97" align="left"><?= $jabatan['masa_kerja']?></td>
			<td width="50" align="center"><?= $jabatan['nama_pendidikan']?></td>
			<td width="73" align="left"><?= $jabatan['nama_jabatan']?></td>
			<td width="133" align="left"><?= $jabatan['unit_kerja']; ?></td>
  		</tr>
		
		<? } }
		else {
			echo "<tr><td colspan=10>Data tidak ditemukan</td></tr>";
		} ?>
		</table>