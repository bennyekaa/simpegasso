<?php 
function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}  
 ?>
<div class="content">
<div id="content-header">

	<div id="module-title"><h3>
	  Statistik Pegawai Berdasarkan Usia dan Tingkat Pendidikan
	</h3>
	</div>
	<div id="module-menu">
		<div id="module-menu">

			<a href="<?=site_url('laporan/statistik_usia_all/printtoxls/'.$unit_kerja.'/'.$id_jns_pegawai.'/list_statistik_usia_all')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>	
	</div>
	<div class="clear"></div>
</div>

<div id="content-data">
	<div id='detail' style="position:absolute z-index"></div>
		<center>
			<form name="pegawaidetails" id="pegawaidetails" method="POST" action="<?=site_url('laporan/statistik_usia_all/browse')?>" >
				Jenis Pegawai:
					<?php echo form_dropdown('id_jns_pegawai', $get_jenis_pegawai_all, $id_jns_pegawai); ?>
				<!--<label>Unit Kerja : </label>
				<?=form_group_dropdown('unit_kerja',$unitkerja_assoc,$unit_kerja);?>-->
				<input name="mulai" type="submit" value="Cari">
			</form>
	  	</center>
		<br/>
		<?php
			if ($this->input->post('mulai')){
		?>	
		<?php 
			//cek idjenis pegawainya apa
			$query = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'");					
			if ($query)	{
				$dataku=mysql_fetch_array($query);  
				$id_tenaga_dosen= $dataku['id_var'];
				}
			
			$query = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_adm'");					
			if ($query)	{
				$dataku=mysql_fetch_array($query);  
				$id_tenaga_adm= $dataku['id_var'];
				}
				
			if ($id_jns_pegawai=='All'){
				$kriteria_jenis = " and id_jns_pegawai > 0 ";
			}
			/*elseif ($id_jns_pegawai=='nAll'){
				$kriteria_jenis = " and id_jns_pegawai not like '".$id_tenaga_adm."'  and status_pegawai < 3";
			}*/
			elseif ($id_jns_pegawai=='nAllUnit'){
				$kriteria_jenis = " and id_jns_pegawai not like '".$id_tenaga_dosen."'  and status_pegawai < 3";
			}
			else {
				$kriteria_jenis = " and id_jns_pegawai='".$id_jns_pegawai."'  and status_pegawai < 3 ";
			}
			
			
		?>	
		
		
		<table class="table-list" width="80%">
			<tr class="trOdd"><td colspan="<?=12+(count($pendidikan_assoc))?>"  align="center">
			<font size="+1"> <strong><?=$judul;?></strong></font>
			</td></tr>
			
			<tr class="trhead">
				<!--<th width="20"  align="center" class="trhead">No</th>-->
				<th align="center"  class="trhead" width="80">Usia</th>
				<? $i=0;if($pendidikan_assoc)foreach($pendidikan_assoc as $komp){$i++; ?>
				<th style="min-width:20px" class="<?=$i % 2==0?"":"colEvnRight"?>" ><?=$komp['nama_pendidikan']?></th>
				<? } ?>
				<th align="center"  class="trhead">Jumlah</th>
			</tr>
			<?php
				
				
				if ($this->input->post('nama_search')!=''){
					$search_nama = " ((nama_pegawai like '%".$this->input->post('nama_search')."%') or (NIP like '".$this->input->post('nama_search')."'))";	
					}
				else{
					$search_nama = " (nama_pegawai like '%%')";	
				}
				
			
				$sql = "select count(kd_pegawai) as jumlah, (year(curdate())-year(tgl_lahir))  as umur 
				from pegawai where  ".$search_nama." ".$kriteria_jenis."
				group by (year(curdate())-year(tgl_lahir)) 
				order by (year(curdate())-year(tgl_lahir)) desc";							
					
				//echo $sql;
				
				$query_pegawai = mysql_query($sql);		
				$i = 0;			
				$totali = 0;
				$total_all = 0;
				$totald3 = 0;
				$totald2 = 0;
				while ($data_pegawai=mysql_fetch_array($query_pegawai)) {
					$i++;
					if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				?>
			<tr class="<?= $class; ?>">
				<!--<td  align="left" class="colEvnRight"><?= $i;?> </td>-->
				<td align="center" class="colEvnRight"> 
				<strong><?php 
					echo $data_pegawai['umur'];
				?></strong> 
				</td>
				<? $j=1;if($pendidikan_assoc)	foreach($pendidikan_assoc as $komp){$j++;?>
				<td align="center" class="colEvnRight">
					<?php												
						
							$sql = "select count(kd_pegawai) as jml_didik from pegawai 
							where  ".$search_nama."".$kriteria_jenis."
							and  (year(curdate())-year(tgl_lahir))='".$data_pegawai['umur']."' and 
							id_pendidikan_terakhir = '".$komp['id_pendidikan']."' ";
							$query_pegawai_usia_unit = mysql_query($sql);		
								
							
						$data_pegawai_usia_unit=mysql_fetch_array($query_pegawai_usia_unit);
						$row_count = mysql_num_rows($query_pegawai_usia_unit);
						if ($row_count>0){	
							if ($data_pegawai_usia_unit['jml_didik']>0){		
								echo $data_pegawai_usia_unit['jml_didik'];
								$totali = $totali + $data_pegawai_usia_unit['jml_didik'];
							}
							else{
								echo '-';
							}	
						}
						else{
							echo '-';
						}
						
						//echo $sql;
						
					?>
				</td>
				<? } ?>
				<td  align="center"class="colEvnRight"><strong><?= $totali;$total_all = $total_all + $totali;$totali = 0; ?></strong> </td>
			</tr>	
			<? } ?>	
			<tr class="trhead">
				<td align="left" class="colEvn"><strong>Sub Total</strong></td>
				<? $i=0;if($pendidikan_assoc)foreach($pendidikan_assoc as $komp){$i++; ?>
					<td align="center" class="colEvnRight">
						<strong><?php												
							
							$sql="select count(kd_pegawai) as jml_didik from pegawai 
							where   ".$search_nama."".$kriteria_jenis."
							and id_pendidikan_terakhir = '".$komp['id_pendidikan']."'";
							$query_pegawai_usia_unit = mysql_query($sql);		
						
							$data_pegawai_usia_unit=mysql_fetch_array($query_pegawai_usia_unit);
							$row_count = mysql_num_rows($query_pegawai_usia_unit);
							if ($row_count>0){	
								if ($data_pegawai_usia_unit['jml_didik']>0){		
									echo $data_pegawai_usia_unit['jml_didik'];
								}
								else{
									echo '-';
								}	
							}
							else{
								echo '-';
							}
							//echo $sql;
						?></strong>
					</td>
				<? } ?>
				<td  align="center" class="colEvn"><strong><?= $total_all; ?></strong> </td>
			</tr>
		</table>			
		<? } ?>
		<br />
		<div class="paging"><?=$page_links?></div>
  </div>
</div>
