<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('laporan/pegawai')?>" title="Kembali">
		<img class="noborder" src="<?=base_url().'public/images/back.jpg'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
	<div class="content-data">
		<table class="general">
		<tr>
			<td align="left">NIP</td>
			<td> : </td>
			<td align="left">
			<?=$nip;?>
			</td>
		</tr>
		<tr>
			<td align="left">Nama Pegawai</td>
			<td> : </td>
			<td align="left">
			<?=$nama_peg?>
			</td>
		</tr>
		<tr>
			<td align="left">Tempat / Tgl Lahir</td>
			<td> : </td>
			<td align="left">
			<?=$tempat?> / <?= date('d M Y',strtotime($tanggal)); ?>
			</td>
		</tr>
		<tr>
			<td align="left">Jenis Kelamin</td>
			<td> : </td>
			<td align="left" colspan="2">
			<?=$jenis;?>
			</td>
		</tr>
		<tr>
			<td align="left">Alamat</td>
			<td> : </td>
			<td align="left">
				<?=$alamat?>
			</td>
		</tr>
		<tr>
			<td align="left">Agama</td>
			<td> : </td>
			<td align="left">
				<?=$agama?>
			</td>
		</tr>
		<tr>
			<td align="left">Status Perkawinan</td>
			<td> : </td>
			<td align="left">
				<?=$stkawin?>
			</td>
		</tr>		
		</table>
		<br>
		<br>

		<div class="clear"></div>
	</div>
</div>