<?
$option_group=$this->lookup->get_group_laporan(0,'unit_kerja','kode_unit','unit_kerja');
?>

<div class="content">
	<div id="content-header">
		<div id="module-title"><h3><?=$judul;?></h3></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/pegawai/printtopdf/'.$group)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/pegawai/printtoxls/'.$group.'/buku_pegawai.xls')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	
	<div id="content-data">
		<div id='detail' style="position:absolute z-index"></div>
		<center>
			<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('laporan/pegawai/browse')?>" >
				<label>Jenis Pegawai : </label><?=form_dropdown('group',$adm_akd_assoc,$group);?>
				<!--<label>Unit Kerja : </label><?=form_group_dropdown('unit_kerja',$option_unit,$unit_kerja);?>-->
				<input type="submit" value="Filter">
			</form>
		</center>
		<br/>
		
		<?
			echo "<h3>Jenis Pegawai : ".$adm_akd_assoc[$group]."</h3>";
		?>
		<table class="table-list">
			<tr class="trhead">
				<th class="trhead" width="20" align="center" rowspan="2">No</th>
				<th class="trhead" width="170" align="center">Nama</th>
				<th width="107" rowspan="2" align="center" class="trhead">NIP</th>
				<th width="84" rowspan="2" align="center" class="trhead">Gol/Ruang</th>
				<th class="trhead" width="181" align="center" rowspan="2"><span class="colEvn">TMT</span></th>
				<th class="trhead" width="181" align="center" rowspan="2"><span class="colEvn">Karpeg</span></th>
				<th class="trhead" width="181" align="center" rowspan="2">KGB</th>
				<th class="trhead" width="181" align="center" rowspan="2">Pendidikan</th>
				<th class="trhead" width="155" align="center" rowspan="2">Alamat</th>
				<th class="trhead" width="55" align="center" rowspan="2">Loker</th>
				<th class="trhead" width="186" align="center" rowspan="2">Subag</th>
				<th class="trhead" width="186" align="center" rowspan="2">Unit</th>
				<th class="trhead" width="85" align="center" rowspan="2">Pensiun</th>
			</tr>
			<tr class="trheadEvn">
				<th class="trheadEvn" width="170" align="center">Tempat/Tanggal Lahir</th>
			</tr>
				
			<?
			$i = $start++;
			if ($kerja_list!=FALSE){
			foreach ($kerja_list as $pegawai) {
				$i++;
				if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				list($y, $m, $d) = explode('-', $pegawai['tgl_resign']);
				$str_tgl_resign = $d . "/" . $m . "/" . $y;
				$pegawai['tgl_resign']=$str_tgl_resign;
				
				list($y, $m, $d) = explode('-', $pegawai['tgl_lahir']);
				$str_tgl_lahir = $d . "-" . lookup_model::shortmonthname($m) . "-" . $y;
				$pegawai['tgl_lahir']=$str_tgl_lahir;
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_cpns']);
				$str_tmt_cpns = $d . "-" . lookup_model::shortmonthname($m) . "-" . $y;
				$pegawai['tmt_cpns']=$str_tmt_cpns;
				if ($pegawai['tmt_cpns']=='00/00/0000')
				{
					$pegawai['tmt_cpns']='-';
				}
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_kgb']);
				$str_tmt_kgb = $d . "-" . lookup_model::shortmonthname($m) . "-" . $y;
				$pegawai['tmt_kgb']=$str_tmt_kgb;
				if ($pegawai['tmt_kgb']=='00/00/0000')
				{
					$pegawai['tmt_kgb']='-';
				}
				
				
			?>   
			<tr class="<?= $class; ?>">
				<td align="right" class="colEvn" rowspan="2"><?= $i; ?></td>
				<!--<td align="left" class="trhead" ><?=$pegawai['NIP']?></td>-->
				<td  align="left" class="trhead"><?=strtoupper($pegawai['nama_pegawai'])." ".$pegawai['gelar_depan']."".$pegawai{'gelar_belakang'} ?></td>
				<td align="left" class="colEvn"><?=$pegawai['NIP']?></td>
				<td rowspan="2" align="center" class="colEvn"><?=$golongan_assoc[$pegawai['id_golpangkat_terakhir']]?></td>
				<td align="left" class="trhead" rowspan="2">
				  <?=$pegawai['tmt_cpns']?>
				</td>
				<td align="left" class="trhead" rowspan="2">
				  <?=$pegawai['karpeg']?>
				</td>
				<td align="left" class="trhead" rowspan="2"><?=$pegawai['tmt_kgb']?></td>
				<td align="left" class="trhead" rowspan="2"><?=$pendidikan_assoc[$pegawai['id_pendidikan_terakhir']]?></td>
				<!--<td rowspan="2" align="center" class="trhead">			
				   <a href = "<?=site_url("laporan/pegawai/view/".$pegawai['id_kerja']); ?>" class="view action" >Lihat Detil</a>
				</td>
			</tr>
			<tr class="<?= $class; ?>">
				<td align="left" class="trhead"><?=$pegawai['gelar_depan']." " .$pegawai['nama_pegawai'].$pegawai{'gelar_belakang'} ?></td>-->
				<td align="left" class="trhead" rowspan="2"><?=$pegawai['alamat']?></td>
				<td align="left" class="trhead" rowspan="2"><?=$pegawai['loker']?></td>
				<td rowspan="2" align="center" class="colEvn"><?=$unitkerja_assoc[$pegawai['kode_unit']]?></td>
				<?php 
			$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
								where  u.kode_unit='".$pegawai['kode_unit']."'");					
			if ($query) 
				{
					$data=mysql_fetch_array($query);  
					$tingkat_unit = $data['tingkat'];
					$namaunit = $data['nama_unit'];
					$kodeunit = $data['kode_unit'];
					$kodeunitg = $data['kode_unit_general'];
				}
				if ($tingkat_unit!=1) {
					$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
										where  u.kode_unit'".$kodeunitg."'");					
					if ($query) {
						$data=mysql_fetch_array($query);  
						$tingkat_unit = $data['tingkat'];
						$namaunit = $data['nama_unit'];
						$kodeunit = $data['kode_unit'];
						$kodeunitg = $data['kode_unit_general'];
					}
					if ($tingkat_unit!=1) {
						$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
											where  u.kode_unit='".$kodeunitg."'");					
						if ($query) {
							$data=mysql_fetch_array($query);  
							$tingkat_unit = $data['tingkat'];
							$namaunit = $data['nama_unit'];
							$kodeunit = $data['kode_unit'];
							$kodeunitg = $data['kode_unit_general'];
						}
						if ($tingkat_unit!=1) {
							$query = mysql_query("SELECT u.kode_unit, u.nama_unit, u.tingkat,u.kode_unit_general FROM unit_kerja  u 
												where  u.kode_unit='".$kodeunitg."'");					
							if ($query) {
								$data=mysql_fetch_array($query);  
								$tingkat_unit = $data['tingkat'];
								$namaunit = $data['nama_unit'];
								$kodeunit = $data['kode_unit'];
								$kodeunitg = $data['kode_unit_general'];
							}
							if ($tingkat_unit!=1) {
								$namaunit = $pegawai['unit_kerja'];
							}													
						}
					}
				}					
				
		  		
		  ?>
				<td rowspan="2" align="center" class="colEvn"><?= $namaunit;?></td>
				
				<td rowspan="2" align="left" class="colEvn"><?=$pegawai['tgl_resign']?></td>
				<!--<td align="left" class="colEvn"><?=$pegawai['wilayah']?></td>-->
			</tr>
					 				<?php
						  $query = mysql_query("select nama_kabupaten from kabupaten where kd_kabupaten='".$pegawai['tempat_lahir']."'");					
							if ($query) {
								$datakab=mysql_fetch_array($query); 
								$tempat_lahir = $datakab['nama_kabupaten'];
							}
							else{
								$tempat_lahir = $jabatan['tempat_lahir'];
							}
							if ($tempat_lahir =="Lain-lain"){ 
								$tempat_lahir="";
							}		
					?>

			<tr class="<?= $class; ?>">	
				<td align="left" class="trhead"><?=$tempat_lahir?>
				  , <?=$pegawai['tgl_lahir']?></td>
				<td align="left" class="colEvn"><?=$pegawai['nip_lama']?></td>
			</tr>
			
		  
		
		
		
			<? } }
			else {
				echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
			} ?>
		</table>
		<div class="paging">
		  <?=$page_links?>
		</div>
	</div>
</div>

