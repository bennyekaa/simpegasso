<div class="content">
	<div id="content-header">
		<div id="module-title"><h3><?=$judul?></h3></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('laporan/tugas_belajar_dosen/printtopdf/'.$group.'/'.$tahun.'/'.$unit_kerja)?>" title="Cetak">
			<img class="noborder" src="<?=base_url().'public/images/print.jpg'?>"></a>
			<a href="<?=site_url('laporan/tugas_belajar_dosen/printtoxls/'.$group.'/'.$tahun.'/'.$unit_kerja.'/daftar_tb.xls')?>"  title="Eksport ke Excel">
		<img align="top" class="noborder" src="<?=base_url().'public/images/xls2.jpg'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<br/>
	<div id="content-data">
		<center>
			<form name="userdetails" id="userdetails" method="POST" action="<?=site_url('laporan/tugas_belajar_dosen/browse')?>" >
				<label>Fakultas</label>
				<?=form_dropdown('unit_kerja',$unit_kerja_assoc,$unit_kerja);?>
				<br />
				<label> Keadaan  </label>
				<?=form_dropdown('group',$group_assoc,$group);?>
				
				<?=form_dropdown('tahun',$tahun_assoc,$tahun);?>
				<input type="submit" value=" Cari  " name="mulai">
			</form>
		</center>
		<br/>
		<?php
			if ($this->input->post('mulai')){
		?>
		<table class="table-list" width="70%">
			<tr class="trhead" align="center">	
				<th  align="center" >No</th>
				<th  >NIP</th> 
				<th  >Nama Pegawai</th>
				<th  align="center" class="trhead">Jurusan</th>
				<th  align="center" class="trhead">Jabatan</th>
				<th  align="center" class="trhead">Golongan</th>
				<th  align="center" class="trhead">Jenjang Pendidikan </th>
				<th  align="center" class="trhead">Bidang Studi </th>
				<th  align="center" class="trhead">Tempat Studi </th>
				<th  >TMT Tugas Belajar </th>
				
				<th  >Batas Akhir Tugas Belajar </th>
				<th  >Biaya</th>
				<th  >No. Dasar Penerimaan </th>
				<th  >Tgl Dasar Penerimaan </th>
				<th  >No. SK TB </th>
				<th  >Tgl SK TB </th>
				<th  >Keterangan</th>
			</tr>
			
			
			<?
			$i = 0;
			
			if ($tb_list!=FALSE){
			
			foreach ($tb_list as $tb) {
				//if ($pensiun['usia']=='56') {
				$i++;
				if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				
				list($y, $m, $d) = explode('-', $tb['tmt_tb']);
				$str_tmt_tb = $d . "-" . $m . "-" . $y;
				$tb['tmt_tb']=$str_tmt_tb;
				
				list($y, $m, $d) = explode('-', $tb['batas_akhir_tb']);
				$str_batas_tb = $d . "-" . $m . "-" . $y;
				$tb['batas_akhir_tb']=$str_batas_tb;
				
				
			?>   
			<tr class="<?= $class; ?>">
				<td  align="center" ><?= $i; ?></td>
				<td align="left" ><?= $tb['NIP']; ?></td>
				<td align="left" ><?= $tb['gelar_depan']." ".$tb['nama_pegawai']." ".$tb['gelar_belakang']; ?></td>	
				<td  align="left" ><?=$tb['nama_unit']?></td>
				<td  align="left" ><?=$tb['nama_jabatan']?></td>
				<td  align="left" ><?=$tb['golongan']?></td>
				<td  align="left" ><?=$tb['id_pendidikan_tb']?></td>
				<td  align="left" ><?=$tb['bidang_studi_tb']?></td>
				<td  align="left" ><?= $tb['tempat_studi_tb']?></td>
				<td  align="center" ><?=$tb['tmt_tb'];?></td>
				<td  align="center" ><?=$tb['batas_akhir_tb'];?></td>
				<td  align="center" ><?=$tb['biaya_tb'];?></td>
				
				<td  align="center" ><?=$tb['no_dasar_penerimaan'];?></td>
				<td  align="center" ><?=$tb['tgl_dasar_penerimaan'];?></td>
				<td  align="center" ><?=$tb['no_SK_tb'];?></td>
				<td  align="center" ><?=$tb['tgl_SK_tb'];?></td>
				
				<td  align="center" ><?=$tb['keterangan']?></td>
			</tr>
			
			
			<? } }//}
			else {
				$conspan = ($group == '') ? 7 : 6;
				echo "<tr><td colspan=6>Data tidak ditemukan</td></tr>";
			}?>
	  </table>
		<div class="paging"><?=$page_links?></div>
	</div>
</div>
<? }?>