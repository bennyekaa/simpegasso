<?
$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url("pegawai/riwayatpelatihan/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
        <?php	
					if($this->user->user_group=="Staf") {
				?>
                <a class="icon" href="<?=site_url('pegawai/caripegawai/view/' .$kd_pegawai)?>" title="Batal">
                <img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
            
            <?php } else { ?>
        <a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
            <? } ?>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
	<form name="pelatihandetails" id="pelatihandetails" method="POST" action="<?=site_url('pegawai/riwayatpelatihan/browse')?>" >
	</form>
	</center>
	<br/>
	<table class="table-list">
	<tr class="trhead">
		<th >No</th>
		<th class="colEvn" >Nama Pelatihan </th>
		<th >Tahun  </th>
		<th class="colEvn">Jenis Pelatihan </th>
		<th >Lama</th>
		<th width="250" class="colEvn">Tempat Pelatihan</th>
		<th >Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_pelatihan!=FALSE){
	foreach ($list_pelatihan as $pelatihans) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$i;?></td>
		<td align="left" ><?= $pelatihans['nama_pelatihan']; ?></td>
		<td align="center" ><?= $pelatihans['tahun']; ?></td>
		<td align="left" ><?= $jenis_pelatihan_assoc[$pelatihans['jenis_pelatihan']];?></td>
		<td align="center" ><?= $pelatihans['lama_hari']; ?></td>
		<td align="left" ><?= $pelatihans['tempat_pelatihan']; ?></td>
		<td align="center" class="colEvn">			
			<a href="<?=site_url("pegawai/riwayatpelatihan/edit/".$pelatihans['id_riwayat_pelatihan']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("pegawai/riwayatpelatihan/delete/".$pelatihans['id_riwayat_pelatihan']); ?>" class="delete action" >Hapus</a>		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

