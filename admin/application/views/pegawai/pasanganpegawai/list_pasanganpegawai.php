<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
	<a class="icon" href="<?=site_url("pegawai/pasanganpegawai/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
		<a class="icon" href="<?=site_url("pegawai/pegawai/view/" . $pegawai['kd_pegawai']); ?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div class="paging"><?=$page_links?></div>
<div>
	<table width="891" class="table-list">
	<tr class="trhead">
		<th width='44' class="colEvn">Kode</th>
		
		<th width='137' align="left" ><div align="center">Nama</div></th>
		
		<th width='128' align="left" ><div align="center">Tanggal Lahir </div></th>
		<th width='141' align="left" ><div align="center">Tanggal Menikah </div></th>
		<th width='233' align="left" ><div align="center">Keterangan</div></th>
				
		<th width="20" class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_pasangan!=FALSE){
	foreach ($list_pasangan as $jns_keluarga) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	   if ($jns_keluarga['tanggal_lahir']=='0000-00-00') {
                $str_tgl_lahir = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_lahir']);
                $str_tgl_lahir = $d . "-" . $m . "-" . $y;
            }
        $jns_keluarga['tanggal_lahir']=$str_tgl_lahir;
		if ($jns_keluarga['tanggal_nikah']=='0000-00-00') {
                $str_tgl_nikah = '-';
            }
            else {
                list($y, $m, $d) = explode('-', $jns_keluarga['tanggal_nikah']);
                $str_tgl_nikah = $d . "-" . $m . "-" . $y;
            }
        $jns_keluarga['tanggal_nikah']=$str_tgl_nikah;
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn">
			<?=$jns_keluarga['id_pasangan_pegawai'];?></td>
		<td align="left" ><div align="center">
		  	<?= $jns_keluarga['nama_pasangan']; ?></div></td>
		<td align="left" ><div align="center">
		  	<?= $jns_keluarga['tempat_lahir']; ?>, <?= $jns_keluarga['tanggal_lahir']; ?></div></td>
		<td align="left" ><div align="center">
		  	<?= $jns_keluarga['tanggal_nikah']; ?></div></td>
		<td align="left" ><div align="center">
		  	<?php if (trim($jns_keluarga['keterangan'])=="")
			{?>
				<?=$jns_keluarga['pekerjaan'];?>
			<?php }
			else { ?>
				<?=$jns_keluarga['keterangan'];?>
				<? } ?></div></td>
		<td align="center" class="colEvn">			
			<a href = "<?=site_url("pegawai/pasanganpegawai/edit/".$jns_keluarga['id_pasangan_pegawai']); ?>" class="edit action" >Ubah</a>
			<? if($jns_keluarga['aktif']=='0'){?>
			<a href = "<?=site_url("pegawai/pasanganpegawai/delete/".$jns_keluarga['id_pasangan_pegawai']); ?>" class="nyroModal delete action" >Hapus</a>
			<? } ?>		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
</div>
</div>

