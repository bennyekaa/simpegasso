<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url("pegawai/riwayatmutasi/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
        <a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
	<form name="mutasidetails" id="mutasidetails" method="POST" action="<?=site_url('pegawai/riwayatmutasi/browse')?>" >
	</form>
	</center>
	<br/>
	
	<table class="table-list">
	<tr class="trhead">
		<th >No</th>
		<th width="150" class="colEvn" >Jenis Mutasi</th>
		<th >No. SK </th>
		<th class="colEvn" >Tanggal SK </th>
		<th >TMT Mutasi</th>
		<th >Gol/Ruang</th>
		<th width="300" class="colEvn" >Keterangan Mutasi</th>
		<th >Unit Kerja Asal</th>
		<th class="colEvn" >Unit Kerja Baru </th>
		<th >Edit</th>
	</tr>

		<?
		$i = 0;
		if ($list_mutasi!=FALSE){
		foreach ($list_mutasi as $jns_mutasi) {
		   $i++;
		   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
		   if ($jns_mutasi['tgl_SK']=='0000-00-00') {
					$str_sk_mutasi = '-';
				}
				else {
					list($y, $m, $d) = explode('-', $jns_mutasi['tgl_SK']);
					$str_sk_mutasi = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
				}
			$jns_mutasi['tgl_SK']=$str_sk_mutasi;
			if ($jns_mutasi['tmt_SK']=='0000-00-00') {
					$str_tmt_SK = '-';
				}
				else {
					list($y, $m, $d) = explode('-', $jns_mutasi['tmt_SK']);
					$str_tmt_SK = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
				}
			$jns_mutasi['tmt_SK']=$str_tmt_SK;
		?>
		<tr class="<?=$bgColor?>">
			<td align="center" class="colEvn"><?=$i;?></td>
			<td align="left" ><?= $jns_mutasi['nama_mutasi']; ?></td> <!--$mutasi_assoc[$jns_mutasi['nama_mutasi']]-->
			<td align="left" ><?= $jns_mutasi['no_SK']; ?></td>
			<td align="center" ><?= $jns_mutasi['tgl_SK']; ?></td>
			<td align="center" ><?= $jns_mutasi['tmt_SK']; ?></td>
			
			<?
			if ($status_pegawai_assoc[$jns_mutasi['status_pegawai']]=='CPNS')
			{
			?>
				<td align="left" ><?= $gol_assoc[$jns_mutasi['id_golpangkat']]; ?></td>
			<?
			}else
			{
			?>
			 	<td align="left" ><?= $golongan_assoc[$jns_mutasi['id_golpangkat']]; ?></td>
			<? } ?>
			
			<td align="left" ><?= $jns_mutasi['ket_mutasi']; ?></td>
			<td align="left" ><?= $unit_kerja_assoc[$jns_mutasi['kode_unit_kerja_asal']]; ?></td>
			<td align="left" ><?= $unit_kerja_assoc[$jns_mutasi['kode_unit_kerja_baru']]; ?></td>
			<td align="center" class="colEvn">			
				<a href="<?=site_url("pegawai/riwayatmutasi/edit/".$jns_mutasi['id_mutasi']); ?>" class="edit action" >Ubah</a>
				<a href="<?=site_url("pegawai/riwayatmutasi/delete/".$jns_mutasi['id_mutasi']); ?>" class="delete action" >Hapus</a>		
			</td>
		</tr>
		<? } ?>
	<? }
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

