<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayattridharma/$action/";
if (!async_request())
{
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/riwayattridharma/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
		
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div>
	<form name="tridharmadetail" id="tridharmadetail" method="POST" action="<?= $action_url; ?>">
		<?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
		<table class="general">
			<tr align="left">
			  <td>Nama Kegiatan Tridharma</td>
			  <td> : </td>
			  <td >
			  <textarea name="nama_tridharma" id="nama_tridharma" rows="2" cols="50"><?=set_value('nama_tridharma',$nama_tridharma);?></textarea> 
				  <span style="color:#F00;">* <?php echo form_error('nama_tridharma'); ?></span> </td>
			  </tr>
<input type='hidden' name='jns_tridharma' id='jns_tridharma' value="-" />
<input type='hidden' name='nomor_surat_tugas' id='nomor_surat_tugas' value='<?=set_value('nomor_surat_tugas',$nomor_surat_tugas);?>'/>
		  <tr align="left">
			  <td>Semester</td>
			  <td>:</td>
			  <td ><?
						echo form_dropdown('semester', $semester_assoc, $semester);
					?>
					<?php echo form_error('semester'); ?></td>
		  </tr>
		<tr align="left">
		  <td>Tahun Ajaran</td>
		  <td>:</td>
		  <td ><input type='text' name='tahun' id='tahun' value='<?=set_value('tahun',$tahun);?>'/>
			  <span style="color:#F00;">* <?php echo form_error('tahun'); ?></span></td>
		</tr>
		<tr align="left">
			<td width ="200">SKS</td>
			<td> : </td>
			<td ><input type='text' name='sks' id='sks' value='<?= $sks?>'  class="required"/> sks</td>
		</tr>
		  <input type='hidden' name='tgl_mulai' id='tgl_mulai' value="0000-00-00" />
		  <input type='hidden' name='tgl_selesai' id='tgl_selesai' value="0000-00-00" />
			<tr align="left">
				<td width ="200">Masa Penugasan</td>
				<td> : </td>
				<td ><input type='text' name='lama_hari' id='lama_hari' value='<?= $lama_hari?>'  class="required"/>
				  <span style="color:#F00;">*<?php echo form_error('lama_hari'); ?> hari </span></td>
			</tr>
			<input type='hidden' name='jml_jam' id='jml_jam' value='<?= $jml_jam?>'  class="required"/> </td>
		  <tr align="left">
				<td width ="200">Bukti Penugasan</td>
				<td> : </td>
				<td >
				<textarea name='bukti_penugasan' id='bukti_penugasan' rows="1" cols="40"><?= $bukti_penugasan?></textarea>
				 </td>
			</tr>
			
			<tr align="left">
				<td>Capaian</td>
				<td>:</td>
				<td ><?
					echo form_dropdown('capaian', $capaian_assoc, $capaian);
					?>
					<?php echo form_error('capaian'); ?></td>
			</tr>
		
				<input type="hidden" name='tempat_tridharma' id='tempat_tridharma' value="-" />
			<tr align="left">
			  <td><em>Outcome</em></td>
			  <td> : </td>
			  <td> 
			<textarea name='keterangan' id='keterangan' rows="1" cols="40"><?= $keterangan?></textarea>
				<?php echo form_error('keterangan'); ?> </td>	  
				   </td>
			  </tr>
		</table>
		<br/>
		<!--<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>-->
		<div class="submit" style="text-align:center"><input id="submit" size="100px" name="submit" type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>