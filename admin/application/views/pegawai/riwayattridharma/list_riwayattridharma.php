<?
$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url("pegawai/riwayattridharma/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
        <?php	
					if($this->user->user_group=="Staf") {
				?>
                <a class="icon" href="<?=site_url('pegawai/caripegawai/view/' .$kd_pegawai)?>" title="Batal">
                <img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
            
            <?php } else { ?>
        <a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
            <? } ?>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
	<form name="tridharmadetails" id="tridharmadetails" method="POST" action="<?=site_url('pegawai/riwayattridharma/browse')?>" >
	</form>
	</center>
	<br/>
	<table class="table-list">
	<tr class="trhead">
		<th >No</th>
		<th class="colEvn" >Nama Kegiatan </th>
		<th >Tahun Ajaran</th>
		<th class="colEvn">Semester </th>
		<th >S K S</th>
		<th width="250" class="colEvn"><em>Outcome</em></th>
		<th >Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_tridharma!=FALSE){
	foreach ($list_tridharma as $tridharmas) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$i;?></td>
		<td align="left" ><?= $tridharmas['nama_tridharma']; ?></td>
		<td align="center" ><?= $tridharmas['tahun']; ?></td>
		<td align="left" ><?= $semester_assoc[$tridharmas['semester']];?></td>
		<td align="center" ><?= $tridharmas['sks']; ?></td>
		<td align="left" ><?= $tridharmas['keterangan']; ?></td>
		<td align="center" class="colEvn">			
			<a href="<?=site_url("pegawai/riwayattridharma/edit/".$tridharmas['id_riwayat_tridharma']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("pegawai/riwayattridharma/delete/".$tridharmas['id_riwayat_tridharma']); ?>" class="delete action" >Hapus</a>		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

