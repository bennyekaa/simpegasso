<?
$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
	<!--v3 tambahan tb_ib-->	
	<?php 
	if ($tb_ib=='tb'){
	?>
	<!--end v3 tambahan tb_ib-->	
		<a class="icon" href="<?=site_url("pegawai/riwayattugasbelajar/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
	<?php } else {		?>
		<a class="icon" href="<?=site_url("pegawai/riwayatijinbelajar/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
	<?}?>
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
        <a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
		<form name="tbdetails" id="tbdetails" method="POST" action="<?=site_url('pegawai/riwayattugasbelajar/browse')?>" >
		</form>
	</center>
	<br/>
	<table class="table-list">
		<tr class="trhead">
			<th width="20" >No</th>
	<?php 
	if ($tb_ib=='tb'){
	?>
			<th class="colEvn" width="103" >TMT Tugas Belajar </th>
			<th  width="134">Batas Akhir Tugas Belajar </th>
	<? } else { ?>
			<th class="colEvn" width="103" >TMT Ijin Belajar </th>
	<?}?>
			<th  width="97">Nomor Surat</th>
			<th  width="97">Tingkat</th>
			<th  width="181">Bidang Studi </th>
			<th  width="216">Tempat Studi </th>
			<th  width="167">Biaya</th>	
			<th width="128" >Edit</th>
		</tr>
	
		<?
		$i = 0;
		if ($list_tb!=FALSE){
		foreach ($list_tb as $tugas_belajar) {
		   $i++;
		   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
		   list($y, $m, $d) = explode('-', $tugas_belajar['tmt_tb']);
           $str_tmt_tb = $d . "-" . $m . "-" . $y;
           $tugas_belajar['tmt_tb']=$str_tmt_tb;
		   list($y, $m, $d) = explode('-', $tugas_belajar['batas_akhir_tb']);
           $str_batas_tb = $d . "-" . $m . "-" . $y;
           $tugas_belajar['batas_akhir_tb']=$str_batas_tb;
		?>
		<tr class="<?=$bgColor?>">
			<td align="center" class="colEvn"><?=$i;?></td>
			<td align="left" ><?= $tugas_belajar['tmt_tb']; ?></td>
			<?php 
	if ($tb_ib=='tb'){
	?>
			<td align="center" ><?= $tugas_belajar['batas_akhir_tb'];?></td>
	<?}?>
			<td align="center" ><?= $tugas_belajar['no_SK_tb'];?></td>
			<td align="center" ><?= $pendidikan_assoc[$tugas_belajar['id_pendidikan_tb']]; ?></td>
			<td align="center" ><?= $tugas_belajar['bidang_studi_tb']?></td>
			<td align="center" ><?= $tugas_belajar['tempat_studi_tb']?></td>
			<td align="center" ><?= $tugas_belajar['biaya_tb']; ?></td>
			<td align="center" class="colEvn">		
<!--v3 tambahan tb_ib-->	
<?php 
	if ($tb_ib=='tb'){
?>
<!--end v3 tambahan tb_ib-->		
			<a href="<?=site_url("pegawai/riwayattugasbelajar/edit/".$tugas_belajar['id_riwayat_tb']); ?>" class="edit action" >Ubah</a>
<?php 
} 
else 
{ 
?>
			<a href="<?=site_url("pegawai/riwayatijinbelajar/edit/".$tugas_belajar['id_riwayat_tb']); ?>" class="edit action" >Ubah</a>
<?}?>
			<?php if($tugas_belajar['aktif']=='0') {?>

			<a href="<?=site_url("pegawai/riwayattugasbelajar/delete/".$tugas_belajar['id_riwayat_tb']); ?>" class="delete action" >Hapus</a>
			<? } ?>		
			<!--<a href="<?=site_url("pegawai/detiltugasbelajar/browse/".$tugas_belajar['id_riwayat_tb']); ?>" title="Detil Tugas Belajar" class="view action">Detil Tugas Belajar </a>-->
		</td>
		</tr>
	
		<? } ?>
		<?}
		else {
			echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
		}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

