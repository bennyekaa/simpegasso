<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/penghargaan/$action/";
if (!async_request())
{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/penghargaan/index')?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel-icon.png'?>"></a>
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
		<form name="penghargaandetail" id="penghargaandetail" method="POST" action="<?= $action_url; ?>">
			<table class="general">
				<tr align="left">
					<td width ="200px">Kode</td>
					<td> : </td>
					<td width ="200px">
					<?= $id_riwayat_penghargaan?></td>
				</tr>
				<tr align="left">
					<td width ="200px">Nama Penghargaan</td>
					<td> : </td>
					<td width ="200px">
					<?php
						echo form_dropdown('FK_jenis_penghargaan',$jenis_penghargaan_assoc, $FK_jenis_penghargaan);
						echo form_error('FK_jenis_penghargaan'); ?>
					</td>
				</tr>
				<tr align="left">
					<td width ="200px">Tanggal Penerimaan</td>
					<td> : </td>
					<td width ="200px">
					<input type="text" name="tgl_penghargaan" id="tgl_penghargaan" class="js_required" value="<?= $tgl_penghargaan?>"  class="datepicker required"/>
					<?php echo form_error('tgl_penghargaan'); ?>  </td>
				</tr>
				<tr align="left">
					<td width ="200px">Keterangan</td>
					<td> : </td>
					<td width ="200px">
					<input type="text" name="ket_riwayat_penghargaan" id="ket_riwayat_penghargaan" value="<?= $ket_riwayat_penghargaan?>" />
					<?php echo form_error('ket_riwayat_penghargaan'); ?>  </td>
				</tr>
			</table>
			<br/>
			<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
		</form>
	</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>