<?
$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
		<a class="icon" href="<?=site_url("pegawai/riwayattugas/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
        
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
		<form name="tbdetails" id="tbdetails" method="POST" action="<?=site_url('pegawai/riwayattugas/browse')?>" >
		</form>
	</center>
	<br/>
	<table class="table-list">
		<tr class="trhead">
			<th width="20" >No</th>
			<th class="colEvn" width="103" >Nama Unit </th>
			<th  width="167">Tugas</th>
			<th  width="138">Tanggal Mulai </th>
			<th  width="149">Tanggal Selesai </th>
			<th  width="167">Keterangan</th>	
			<th width="128" >Edit</th>
		</tr>
	
		<?
		$i = 0;
		if ($list_tugas!=FALSE){
		foreach ($list_tugas as $tugas) {
		   $i++;
		   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
		   list($y, $m, $d) = explode('-', $tugas['tgl_mulai_tugas']);
           $str_tgl_mulai = $d . "-" . $m . "-" . $y;
           $tugas['tgl_mulai_tugas']=$str_tgl_mulai;
		   list($y, $m, $d) = explode('-', $tugas['tgl_selesai_tugas']);
           $str_tgl_selesai = $d . "-" . $m . "-" . $y;
           $tugas['tgl_selesai_tugas']=$str_tgl_selesai;
		?>
		<tr class="<?=$bgColor?>">
			<td align="center" class="colEvn"><?=$i;?></td>
			<td align="left" ><?= $unit_kerja_assoc[$tugas['kode_unit']]; ?></td>
			<td align="center" ><?= $tugas['nama_tugas'];?></td>
			<td align="center" ><?= $tugas['tgl_mulai_tugas']?></td>
			<td align="center" ><?= $tugas['tgl_selesai_tugas']?></td>
			<td align="center" ><?= $tugas['keterangan']; ?></td>
			<td align="center" class="colEvn">			
			<a href="<?=site_url("pegawai/riwayattugas/edit/".$tugas['id_riwayat_tugas']); ?>" class="edit action" >Ubah</a>
			<?php if($tugas['aktif']=='0') {?>
			<a href="<?=site_url("pegawai/riwayattugas/delete/".$tugas['id_riwayat_tugas']); ?>" class="delete action" >Hapus</a>
			<? } ?>		
			<a href="<?=site_url("pegawai/detiltugas/browse/".$tugas['id_riwayat_tugas']."/".$tugas['kd_pegawai']); ?>" title="Detil Tugas Belajar" class="view action">Detil Tugas </a>		</td>
		</tr>
	
		<? } ?>
		<?}
		else {
			echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
		}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

