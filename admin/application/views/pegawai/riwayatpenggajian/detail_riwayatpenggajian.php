<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayatpenggajian/$action/";
if (!async_request())
{
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/riwayatpenggajian/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
		
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div>
	<form name="gajidetail" id="gajidetail" method="POST" action="<?= $action_url; ?>">
		<?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
		<table class="general">
			<tr align="left">
				<td width ="200px">Kode</td>
				<td> : </td>
				<td >
				<?= $id_rincian_kgb?></td>
			</tr>
			<tr align="left">
					<td width ="200px">Gol/Ruang </td>
					<td> : </td>
					<td >
					<?php
						echo form_dropdown('id_golpangkat',$golongan_assoc, $id_golpangkat);
						?>
						<? echo form_error('id_golpangkat'); ?> </td>
				</tr>
			<tr align="left">
				<td width ="200">Pejabat</td>
				<td> : </td>
				<td>
					<input type='text' size="30" name='atas_nama' id='atas_nama' value='<?= $atas_nama?>' />
				
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Nomor KGB</td>
				<td> : </td>
				<td >
					<input type="text" size="30" name="nomor_kgb" id="nomor_kgb" class="js_required" value='<?= $nomor_kgb?>'  class="required" />
					<span style="color:#F00;">*<?php echo form_error('nomor_kgb'); ?></span>			</td>
			</tr>
			<tr align="left">
				<td width ="200px">Tanggal surat KGB</td>
				<td> : </td>
				<td >
					<input type="text" class="datepicker" name="tgl_surat_kgb" id="tgl_surat_kgb" value='<?=set_value('tgl_surat_kgb',$tgl_surat_kgb);?>'/>
					<?php echo form_error('tgl_surat_kgb'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>	</td>
			</tr>
			
			<tr align="left">
				<td width ="200px">TMT KGB</td>
				<td> : </td>
				<td >
					<input type="text" class="datepicker" name="tmt_kgb" id="tmt_kgb" value='<?=set_value('tmt_kgb',$tmt_kgb);?>' />
					<?php echo form_error('tmt_kgb'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>	</td>
			</tr>
			<tr align="left">
				<td width ="200">Gaji Pokok</td>
				<td> : </td>
				<td >
					<input type="text" name="gaji_pokok" id="gaji_pokok" class="js_required" value='<?= $gaji_pokok?>'  class="required" />
					<span style="color:#F00;">*<?php echo form_error('gaji_pokok'); ?></span>		</td>
			</tr>
			
			<tr align="left">
				<td width ="200">Masa Kerja Tahun</td>
				<td> : </td>
				<td>
					<input type="text" name="mk_tahun" id="mk_tahun" class="js_required" value='<?= $mk_tahun?>'  class="required" />
					<span style="color:#F00;">*<?php echo form_error('mk_bulan'); ?></span>	
				</td>
			</tr>
			
			<tr align="left">
				<td width ="200">Masa Kerja Bulan</td>
				<td> : </td>
				<td>
					<input type="text" name="mk_bulan" id="mk_bulan" value='<?= $mk_bulan?>' />
					<span style="color:#F00;">*<?php echo form_error('mk_bulan'); ?></span>	
				</td>
			</tr>

			<tr align="left">
				<td width ="200">Nama Pejabat</td>
				<td> : </td>
				<td>
					<input type='text' size="30" name='nama_pejabat' id='nama_pejabat' value='<?= $nama_pejabat?>' width="150px" />
				</td>
			</tr>
			<tr align="left">
				<td width ="200">NIP Pejabat</td>
				<td> : </td>
				<td>
					<input type='text' size="30" name='nip_pejabat' id='nip_pejabat' value='<?= $nip_pejabat?>' />
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Aktif</td>
				<td> : </td>
				<td width ="200px">
				<?php
					echo form_dropdown('aktif',$status_assoc, $aktif);
					echo form_error('aktif'); ?>  </td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>