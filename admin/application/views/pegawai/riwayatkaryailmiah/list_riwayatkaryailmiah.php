<?
//$group_option=array(0=>'1 Januari', 1=>'1 April', 2=>'1 Juli', 3=>'1 Oktober');
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url("pegawai/riwayatkaryailmiah/add/" . $pegawai['kd_pegawai'])?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
        <a class="icon" href="<?=site_url('pegawai/pegawai/view/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<center>
	<form name="karyadetails" id="karyadetails" method="POST" action="<?=site_url('pegawai/riwayatkaryailmiah/browse')?>" >
	</form>
	</center>
	<br/>
	<table width="926" class="table-list">
	<tr class="trhead">
		<th width="31" >No</th>
		<th width="162" class="colEvn" >Jenis Karya</th>
		<th width="150" >Judul </th>
		<th width="150" class="colEvn">Bidang Ilmu </th>
		<th width="128" >Dimuat di </th>
		<th width="78" class="colEvn">Tahun </th>
		<th  width="130">Akreditasi </th>
		
		<th width="61" >Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_karya_ilmiah!=FALSE){
	foreach ($list_karya_ilmiah as $karya_ilmiah) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
       
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" class="colEvn"><?=$i;?></td>
		<td align="center" ><?= $jenis_karya_assoc[$karya_ilmiah['jenis_karya']]; ?></td>
		<td align="center" ><?= $karya_ilmiah['judul_karya']; ?></td>
		<td align="center" ><?= $karya_ilmiah['bidang_ilmu']; ?></td>
		<td align="center" ><?= $karya_ilmiah['dimuat']; ?></td>
		<td align="left" ><?= $karya_ilmiah['tahun']; ?></td>
		<td align="left" ><?= $karya_ilmiah['akreditasi']; ?></td>
		
		<td align="center" class="colEvn">			
			<a href="<?=site_url("pegawai/riwayatkaryailmiah/edit/".$karya_ilmiah['id_riwayat_karya']); ?>" class="edit action" >Ubah</a>
			
			<a href="<?=site_url("pegawai/riwayatkaryailmiah/delete/".$karya_ilmiah['id_riwayat_karya']); ?>" class="delete action" >Hapus</a>		</td>
		
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

