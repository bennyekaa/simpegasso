<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayatkaryailmiah/$action/";
if (!async_request())
{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/riwayatkaryailmiah/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
			
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
		<form name="karyadetail" id="karyadetail" method="POST" action="<?= $action_url; ?>">
			 <?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
			<table class="general">
				<tr align="left">
					<td width ="200">Kode</td>
					<td> : </td>
					<td >
					<?= $id_riwayat_karya?></td>
				</tr>
				<tr align="left">
				  <td>Jenis Karya</td>
				  <td> : </td>
				  <td ><?php
						echo form_dropdown('jenis_karya',$jenis_karya_assoc, $jenis_karya);
						?> <? echo form_error('jenis_karya'); ?> </td>
				  </tr>
				<tr align="left">
				<td width ="200">Judul</td>
				<td> : </td>
				<td >
					<textarea rows="3" id="judul_karya" name="judul_karya" cols="25" class="js_required"><?=set_value('judul_karya',$judul_karya);?> </textarea>   
					
				  <span style="color:#F00;"><?php echo form_error('judul_karya'); ?></span>			</td>
			</tr>
				
				
				
				
				
							<tr align="left">
				  <td>Bidang Ilmu</td>
				  <td> : </td>
				  <td >
						<input size="45" type="text" name="bidang_ilmu" id="bidang_ilmu" value="<?= $bidang_ilmu?>" /></td>
				  </tr>
				  <tr align="left">
				  <td>Dimuat dalam </td>
				  <td> : </td>
				  <td >
						<input size="45" type="text" name="dimuat" id="dimuat" value="<?= $dimuat?>" /></td>
				  </tr>
				<tr align="left">
				  <td>Tahun</td>
				  <td> : </td>
				  <td >
						<input size="45" type="text" name="tahun" id="tahun" value="<?= $tahun?>" />
					  	 </td>
				  </tr>
				  <tr align="left">
				  <td>Akreditasi</td>
				  <td> : </td>
				  <td >
						<input size="45" type="text" name="akreditasi" id="akreditasi" value="<?= $akreditasi?>" /></td>
				  </tr>
				
			</table>
			<br/>
			<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
		</form>
	</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>