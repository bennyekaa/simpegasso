<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/terminasi/')?>" title="Kembali">
		<img class="noborder" src="<?=base_url().'public/images/back.jpg'?>" /></a>
		<a class="icon" href="<?=site_url('pegawai/terminasi/add/'.$id_peg)?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/add-icon.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table class="table-list">
	<tr class="trhead">
		<th width='50' >No</th>
		<th width='100' class="colEvn">Tanggal Berhenti</th>
		<th width='150' >No. SK</th>
		<th width='100' class="colEvn">Tanggal SK</th>
		<th width='150' >Alasan Terminasi</th>	
		<th class="colEvn">Edit</th>
	</tr>

	<?
	$i = 0;
	if ($list_terminasi!=FALSE){
	foreach ($list_terminasi as $jns_terminasi) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$i;?></td>
		<td align="center" class="colEvn"><?= $jns_terminasi['tmt_terminasi']; ?></td>
		<td align="left" ><?= $jns_terminasi['no_sk']; ?></td>
		<td align="center" class="colEvn"><?= $jns_terminasi['tgl_sk']; ?></td>
		<td align="left" ><?= $jns_terminasi['jenis_terminasi']; ?></td>
		<td align="center" class="colEvn">			
			<a href = "<?=site_url("pegawai/terminasi/edit/".$jns_terminasi['id_terminasi']); ?>" class="edit action" >Ubah</a>
			<a href = "<?=site_url("pegawai/terminasi/delete/".$jns_terminasi['id_terminasi']); ?>" class="nyroModal delete action" >Hapus</a>
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/><br/>
<div class="paging"><?=$page_links?></div>
</div>
</div>

