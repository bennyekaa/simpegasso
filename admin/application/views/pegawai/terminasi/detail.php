<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/terminasi/$action/";
if (!async_request())
{
?>
<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/terminasi/view/'.$FK_kd_pegawai)?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel-icon.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<? } ?>
<br/>
<div>
	<form name="terminasidetail" id="terminasidetail" method="POST" action="<?= $action_url; ?>">
		<input type='hidden' name='FK_kd_pegawai' id='FK_kd_pegawai' value='<?=$FK_kd_pegawai?>'/>
		<table class="general">
			<tr align="left">
				<td width ="200px">Kode</td>
				<td> : </td>
				<td width ="200px">
				<?= $id_terminasi?></td>
			</tr>
			<tr align="left">
				<td width ="200px">TMT Berhenti</td>
				<td> : </td>
				<td width ="200px">
					<input type='text' class="datepicker" name='tmt_terminasi' id='tmt_terminasi' value='<?=set_value('tmt_terminasi',$tmt_terminasi);?>'/>
					<?php echo form_error('tmt_terminasi'); ?>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Nomor SK</td>
				<td> : </td>
				<td width ="200px">
				<input type='text' name='no_sk' id='no_sk' class="js_required" value='<?= $no_sk?>'  class="required"/>
				<?php echo form_error('no_sk'); ?>  </td>
			</tr>
			<tr align="left">
				<td width ="200px">Tanggal SK</td>
				<td> : </td>
				<td width ="200px">
				<input type='text' class='datepicker' name='tgl_sk' id='tgl_sk' class="js_required" value='<?= $tgl_sk?>'  class="required"/>
				<?php echo form_error('tgl_sk'); ?>  </td>
			</tr>
			<tr align="left">
				<td width ="200px">Alasan Terminasi</td>
				<td> : </td>
				<td width ="200px">
				<?php
					echo form_dropdown('FK_jenis_terminasi',$terminasi_assoc, $FK_jenis_terminasi);
					echo form_error('FK_jenis_terminasi'); ?>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Disetujui Oleh</td>
				<td> : </td>
				<td width ="200px">
				<input type='text' name='persetujuan' id='persetujuan' value='<?= $persetujuan?>' />
				<?php echo form_error('persetujuan'); ?>  </td>
			</tr>
			<tr align="left">
				<td width ="200px">Status Pegawai</td>
				<td> : </td>
				<td width ="200px">
				<?php
					echo form_dropdown('aktif',$status_assoc, $aktif);
					echo form_error('aktif'); ?>
				</td>
			</tr>
			<tr align="left">
				<td width ="200px">Keterangan</td>
				<td> : </td>
				<td width ="200px">
				<input type='text' name='ket_terminasi' id='ket_terminasi' value='<?= $ket_terminasi?>'/>
				<?php echo form_error('ket_terminasi'); ?>  </td>
			</tr>
		</table>
		<br/>
		<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
	</form>
</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>