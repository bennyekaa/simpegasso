<div class ="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
	<a class="icon" href="<?=site_url("pegawai/riwayattugasbelajar/index/" . $pegawai['kd_pegawai'])?>" title="Batal">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a> &nbsp;
<a class="icon" href="<?=site_url("pegawai/detiltugasbelajar/add/")?>" title="Tambah">
		<img class="noborder" src="<?=base_url().'public/images/new2.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div>
	<table  class="table-list">
	<tr class="trhead">
		<th width='50'>No </th>
		<th width='300' >Nomor SK Penunjang</th>
		<th width='200' >Tanggal SK Penunjang</th>
		<th width='200' >Keterangan</th>
		<th class="colEvn">Edit</th>
	</tr>
	<?
	$i = 0;
	if ($list_detil_tb!=FALSE){
	foreach ($list_detil_tb as $detil_tb) {
	   $i++;
	   if (($i%2)==0) { $bgColor = "trEvn"; } else { $bgColor = "trOdd"; }
	?>
	<tr class="<?=$bgColor?>">
		<td align="center" ><?=$i;?></td>
		<td align="left" ><?= $detil_tb['no_SK_penunjang']; ?></td>
		<td align="left" ><?=$detil_tb['tgl_SK_penunjang']?></td>
		<td align="center" ><?= $detil_tb['keterangan']; ?></td>
		<td align="center" class="colEvn">	
					
			<a href="<?=site_url("pegawai/detiltugasbelajar/edit/".$detil_tb['id_detil_riwayat_tb']); ?>" class="edit action" >Ubah</a>
			<a href="<?=site_url("pegawai/detiltugasbelajar/delete/".$detil_tb['id_detil_riwayat_tb']); ?>" class="delete action" >Hapus</a>
			
			
		</td>
	</tr>

	<? } ?>
	<?}
	else {
		echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
	}?>
	</table>
	<br/>
	<br/>
	<div class="paging"><?=$page_links?></div>
</div>
</div>
