<?
$this->load->helper('url');
$this->load->library('form_validation');
$action_url = site_url() . "/pegawai/riwayatangkakredit/$action/";
if (!async_request())
{
?>
<div class ="content">
	<div id="content-header">
		<div id="module-title"><?=$judul?></div>
		<div id="module-menu">
			<a class="icon" href="<?=site_url('pegawai/riwayatangkakredit/index/' . $pegawai['kd_pegawai'])?>" title="Batal">
			<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>"></a>
			
		</div>
		<div class="clear"></div>
	</div>
	<? } ?>
	<br/>
	<div>
		<form name="akdetail" id="akdetail" method="POST" action="<?= $action_url; ?>">
			 <?php echo form_hidden('kd_pegawai', $pegawai['kd_pegawai']); ?>
			<table class="general">
				<tr align="left">
					<td width ="200px">Kode</td>
					<td> : </td>
					<td >
					<?= $id_rincian_ak?></td>
				</tr>
					<tr align="left">
					<td width ="200px">Tanggal</td>
					<td> : </td>
					<td >
						<input type='text' class='datepicker' name='tmt_ak' id='tmt_ak' value='<?=set_value('tmt_ak',$tmt_ak);?>'/>
						<?php echo form_error('tmt_ak'); ?><span style="color:#F00;"><i> yyyy-mm-dd</i></span>	</td>
				</tr>
				<tr align="left">
					<td width ="200px">Keterangan</td>
					<td> : </td>
					<td >
					<input type="text" name="keterangan" id="keterangan" value="<?= $keterangan?>"/>
					<?php echo form_error('keterangan'); ?>  </td>
				</tr>
				<tr align="left">
					<td width ="200">Angka Kredit</td>
					<td> : </td>
					<td >
						<input type="text" name="angka_kredit" id="angka_kredit" class="js_required" value="<?= $angka_kredit?>" class="required" />
						<span style="color:#F00;">*<?php echo form_error('angka_kredit'); ?></span>		</td>
				</tr>
			
			
			</table>
			<br/>
			<div class="submit" style="text-align:center"><input id="submit"  <?if ($action=="add"){?> disabled="disabled" <? } ?> size="100px" name="submit" <?if ($action=="add"){?>disabled="disabled" <? } ?>type="submit" value="  Simpan  " /></div>
		</form>
	</div>
</div>
<script>
$(".js_required").keyup(function(){
			var tmp=new Array();
			var bool=false;
			$(".js_required").each(function(index){tmp[index]=$(this).val();});
			for (var i=0;i<tmp.length;i++)
				if (tmp[i]=="")
				{
					bool=true;
				}
			if (bool) $("#submit").attr('disabled', 'disabled');
			else $("#submit").removeAttr("disabled");
		});
</script>