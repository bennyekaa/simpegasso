<?
//$option_group=$this->lookup->get_group_laporan(0,'unit_kerja','kode_unit','unit_kerja');
?>

<div class="content">
<div id="content-header">

	<div id="module-title"><h3><?=$judul;?></h3></div>
	<div id="module-menu">
		<a class="icon" href="<?=site_url('pegawai/caripegawai/')?>" title="Kembali">
		<img class="noborder" src="<?=base_url().'public/images/cancel2.png'?>" /></a>
	</div>
	<div class="clear"></div>
</div>
	
	<div id="content-data">
	<div id='detail' style="position:absolute z-index"></div>
		<center>
		<!--<form name="pegawaidetails" id="pegawaidetails" method="POST" action="<?=site_url('pegawai/caripegawai/cari')?>" >
		<label>Jenis Pegawai : </label><?=form_dropdown('group',$jenis_pegawai_assoc,$group);?>
		<label>Unit Kerja : </label><?=form_dropdown('unit_kerja',$unitkerja_assoc,$unit_kerja);?>
		<input type="submit" value="Filter">
		</form>-->
		</center>
		<br/>
		
	<table class="table-list">
		<tr class="trhead">
			<th width="20" align="center" class="trhead">No</th>
			<th width="350" align="center" class="trhead"><span class="trhead">Nama</span></th>
			<th width="87" align="center" class="trhead">NIP</th>
			<th width="75" align="center" class="trhead">Gol/Ruang</th>
			<th width="124" align="center" class="trhead" >Unit Kerja </th>
			<th width="62" align="center" class="trhead" >Detil</th>
		</tr>
		
			
		<?
			$i = $start++;
			if ($pegawai_list!=FALSE){
			foreach ($pegawai_list as $pegawai) {
				$i++;
				if (($i%2)==0) { $class = "trEvn"; } else { $class = "trOdd"; }
				list($y, $m, $d) = explode('-', $pegawai['tgl_lahir']);
				$str_tgl_lahir = $d . " " . lookup_model::shortmonthname($m) . " " . $y;
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_golpangkat_terakhir']);
				$str_tmt_golpangkat_terakhir = $d . "/" . $m . "/" . $y;
				$pegawai['tmt_golpangkat_terakhir']=$str_tmt_golpangkat_terakhir;
				
				list($y, $m, $d) = explode('-', $pegawai['tmt_cpns']);
				$str_tmt_cpns = $d . "/" . $m . "/" . $y;
				$pegawai['tmt_cpns']=$str_tmt_cpns;
				
				if ($jabatan_assoc[$pegawai['id_jabatan_terakhir']]=='Fungsional Umum')
				{
					$jabatan_assoc[$pegawai['id_jabatan_terakhir']]='-';
				}
				if ($pegawai['tmt_golpangkat_terakhir']==0)
				{
					$pegawai['tmt_golpangkat_terakhir']='-';
				}
			?>   
		 <tr class="<?= $class; ?>">
			<td  align="right" class="colEvn"><?= $i; ?></td>
			<td align="left" class="trhead">
		    <?=$pegawai['gelar_depan']." ".$pegawai['nama_pegawai']." ".$pegawai['gelar_belakang']?></td>
			<td align="left" class="trhead"><?=$pegawai['NIP']?></td>
			<td  align="center" class="colEvn"><span class="trhead">
			  <?=$golongan_assoc[$pegawai['id_golpangkat_terakhir']]?>
			</span></td>
			<td align="center" class="trhead"><?=$unitkerja_assoc[$pegawai['kode_unit']]?></td>
			<td  align="center" class="trhead">			
		<a href="<?=site_url("pegawai/caripegawai/view/".$pegawai['kd_pegawai']); ?>" title="Detil Data Pegawai" class="view action">Lihat Detil</a>		</tr>
		
		<!--<tr class="<?= $class; ?>">
			<td align="left" class="trhead"><?=$golongan_assoc[$pegawai['id_golpangkat']]?></td>
			<td align="left" class="colEvn"><?=$pegawai['tmt']?></td>
			<td align="left" class="trhead"><?=$pegawai['id_pendidikan_terakhir']?></td>
			<td align="left" class="colEvn"><?=$pegawai['id_golpangkat_terakhir']?></td>
		</tr>-->
			<? } }
			else {
				echo "<tr><td colspan=6>Data Tidak Ditemukan</td></tr>";
			} ?>
		</table>
		<br />
		<div class="paging"><?=$page_links?></div>
	</div>
</div>