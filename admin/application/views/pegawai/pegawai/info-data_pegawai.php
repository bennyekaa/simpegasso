<div class="content">
<div id="content-header">
	<div id="module-title"><?=$judul?></div>
	<div id="module-menu">
            <a class="icon" href="<?=site_url('pegawai/pegawai/cari/')?>" title="Pencarian">
				<img class="noborder" src="<?=base_url().'public/images/search-icon.png'?>"></a>
			<a class="icon" href="<?=site_url('pegawai/pegawai/edit/'.$kd_pegawai)?>" title="Ubah">
				<img class="noborder" src="<?=base_url().'public/images/edit-icon.png'?>"></a>
			<a class="icon" href="<?=site_url('pegawai/pegawai/cetak/'.$kd_pegawai)?>" title="Cetak">
				<img class="noborder" src="<?=base_url().'public/images/print-icon.png'?>"></a>
			&nbsp; &nbsp; &nbsp; &nbsp;
            <a class="icon" href="<?=site_url('pegawai/keluarga/index/')?>" title="Riwayat Keluarga">
				<img class="noborder" src="<?=base_url().'public/images/keluarga-icon.png'?>"></a>
			<a class="icon" href="<?=site_url('pegawai/pendidikan/index/')?>" title="Riwayat Pendidikan">
				<img class="noborder" src="<?=base_url().'public/images/pendidikan-icon.png'?>"></a>
			<a class="icon" href="<?=site_url('pegawai/jabatan/index/')?>" title="Riwayat Jabatan">
				<img class="noborder" src="<?=base_url().'public/images/jabatan-icon.png'?>"></a>
			<a class="icon" href="<?=site_url('pegawai/penghargaan/index/')?>" title="Riwayat Penghargaan">
				<img class="noborder" src="<?=base_url().'public/images/penghargaan-icon.png'?>"></a>
			<a class="icon" href="<?=site_url('pegawai/pelatihan/index/')?>" title="Riwayat Pelatihan">
				<img class="noborder" src="<?=base_url().'public/images/pelatihan-icon.png'?>"></a>
	</div>
	<div class="clear"></div>
</div>
<br/>
<div class="content-data">
	<!--<input type='hidden' name='kd_pegawai' id='kd_pegawai' value='<?= $kd_pegawai; ?>' >-->
	<table class="general">
		<tr>
			<td>
				<img class="noborder" src="<?=base_url().'public/images/image/'.$gambar['image'];?>"></a>
			<td>
		<tr>
		<tr>
			<td  align="left">NIP</td>
			<td> : </td>
			<td width="300">
				<?=$pegawai['NIP'] ?>
			</td>
		</tr>
		<tr>
			<td  align="left">Nama</td>
			<td> : </td>
			<td width="300">
				<?=$pegawai['gelar_depan']." ".$pegawai['nama']." ".$pegawai['gelar_belakang']?>
			</td>
		</tr>
		<tr>
			<td align="left">Tempat dan Tanggal Lahir</td>
			<td> : </td>
			<td width="300">
			<?php
						  $query = mysql_query("select nama_kabupaten from kabupaten where kd_kabupaten='".$pegawai['tempat_lahir']."'");					
							if ($query) {
								$datakab=mysql_fetch_array($query); 
								$tempat_lahir = $datakab['nama_kabupaten'];
							}
							else{
								$tempat_lahir = $jabatan['tempat_lahir'];
							}
							if ($tempat_lahir =="Lain-lain"){ 
								$tempat_lahir="";
							}		
					?>
				<?=$tempat_lahir.", ".$tgl_lahir ?>
			</td>
		</tr>
		<tr>
			<td  align="left">Jenis Kelamin</td>
			<td> : </td>
			<td>
				<?=$gender?>
			</td>
		</tr>
		<tr>
			<td  align="left">Agama</td>
			<td> : </td>
			<td>
				<?=$pegawai['agama']?>
			</td>
		</tr>
		<tr>
			<td  align="left">Status Perkawinan</td>
			<td> : </td>
			<td>
				<?=$pegawai['status_perkawinan']?>
			</td>
		</tr>
		<!--<tr>
			<td  align="left" colspan="2"><br/><b>Riwayat Pegawai</b></td>
		</tr>
		<tr>
			<td  align="left">Jabatan</td>
			<td> : </td>
			<td>
				<?=$kerja['jabatan']?>
			</td>
		</tr>
		<tr>
			<td  align="left">Unit Kerja</td>
			<td> : </td>
			<td>
				<?=$kerja['unit_kerja'].", ".$kerja['departemen']?>
			</td>
		</tr>
		<tr>
			<td  align="left">Gol/Ruang - Pangkat</td>
			<td> : </td>
			<td>
				<?=$kerja['golongan']." - ".$kerja['pangkat']?>
			</td>
		</tr>
		<tr>
			<td  align="left">Pangkat TMT</td>
			<td> : </td>
			<td>
				<?=$tmt_golongan?>
			</td>
		</tr>

		<tr>
			<td  align="left">Status Kepegawaian</td>
			<td> : </td>
			<td>
				<?=$kerja['status_pegawai']?>
			</td>
		</tr>
		<tr>
			<td  align="left">Jenis Kepegawaian</td>
			<td> : </td>
			<td >
				<?=$kerja['jenis_pegawai']?>
			</td>
		</tr>-->
		
<!-- Pendidikan -->

		<tr>
			<td  align="left">Pendidikan Terakhir</td>
			<td> : </td>
			<td>
				<?=$tingkat_pendidikan." - ".$pegawai['jur_fak'].", ".$pegawai['nama_sekolah'].", ".$pegawai['tahun']?>
			</td>
		</tr>
		<!--</form>-->
	</table>
</div>
<div class="clear"></div>
</div>