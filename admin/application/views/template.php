<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" 
"http://www.w3.org/TR/html4/strict.dtd"> 
 
<html lang="en">
<head>
<link rel="shortcut icon" href="<?=base_url()?>/favicon.ico">
<title><?=$title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<meta name="Description" content="<?php echo ($meta_description)?>" /> 
<meta name="Keywords" content="<?php echo ($meta_keywords)?>" /> 
  
<!-- Framework CSS --> 
<link rel="stylesheet" href="<?=base_url()?>public/css/screen.css" type="text/css" media="screen, projection"> 
<link rel="stylesheet" href="<?=base_url()?>public/css/css.css" type="text/css" media="screen, projection"> 
<link rel="stylesheet" href="<?=base_url()?>public/css/custom.css" type="text/css" media="screen, projection"> 
<link rel="stylesheet" href="<?=base_url()?>public/css/nyroModal.css" type="text/css" media="screen, projection"> 
<link rel="stylesheet" href="<?=base_url()?>public/css/jquery.tooltip.css" type="text/css" media="screen, projection"> 
<link rel="stylesheet" href="<?=base_url()?>public/css/ui.theme.css" type="text/css" media="screen, projection"> 
<link rel="stylesheet" href="<?=base_url()?>public/css/ui.core.css" type="text/css" media="screen, projection"> 
<link rel="stylesheet" href="<?=base_url()?>public/css/ui.datepicker.css" type="text/css" media="screen, projection"> 
<link rel="stylesheet" href="<?=base_url()?>public/css/autocomplete.css" type="text/css" media="screen, projection"> 
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" type="text/css" media="screen, projection"> 
<!-- Import fancy-type plugin for the sample page. -->

<script type="text/javascript" src="<?=base_url();?>public/js/gsub.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/jquery.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/jquery.extension.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/jquery.nyroModal.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/jquery.nyroModal.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/jquery.nyroModal.pack.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/dropdown.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/jquery.tooltip.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/ui.core.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/ui.datepicker.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>public/js/jquery.dataTables.min.js"></script>

<?php
	echo $this->template->show_head();    
?>
<script>
	$(document).ready(function(){
		$("#data_mutasi").DataTable();
		if ($(".icon"))$(".icon").tooltip({showURL: false});
		
		if ($(".action")){
			$(".action").each(function(){
				$(this).attr('title',$(this).text());
			});
			
			$(".action").text("");
			$(".action").tooltip({showURL: false});
		}
		if($('.datepicker')) 
			$('.datepicker').datepicker();
			
		$(function() {
		
			$.nyroModalSettings({
				width: 300,//*Math.random()*1000,
				height: Math.random()*1000
			});

			$('#block').nyroModal({
				'blocker': '#blocker'
			});
				
			function preloadImg(image) {
				var img = new Image();
				img.src = image;
			}
			
			preloadImg('img/ajaxLoader.gif');
			preloadImg('img/prev.gif');
			preloadImg('img/next.gif');
		});
	});
</script>
<style type="text/css">
	#blocker {
		width: 300px;
		height: 300px;
		background: red;
		padding: 30px;
		border: 5px solid green;
	}
</style>
</head> 
 
<body> 
	<div id="top-container">
		<div id="header" >
			<div class="space"></div>
			<div id="header-img">&nbsp;</div>
		</div>
	</div>
	<div id="menu-container" >
		<div id="main-menu" class="span-27"><?php echo $side_content?><div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
	<div id="mid-container">	
		<div id="mid-container-ctr" class="span-26">
			<div id="main-content" class="span-23"> <!--untuk mengatur box pd conten utama-->
				<div class="box-body"><div class="box-left"><div class="box-right">
					<div class="box-top"><div class="box-bottom"><div class="box-top-left" >
					<div class="box-top-right" ><div class="box-bottom-left" >
					<div class="box-bottom-right"><div class="box">
					<div class="confirm"><?= $this->load->view($flashdata) ?>&nbsp;&nbsp;</div>
					<?= $content ?>
				</div></div></div></div></div></div></div></div></div></div>
			</div>
			
			
			<div class="clear"></div>
		</div>
		<div class="clear" ></div>
	</div> 
	<?php 

		// $query = mysql_query("select u.kd_pegawai,p.NIP,p.nama_pegawai,LTRIM(concat(p.gelar_depan,' ',p.nama_pegawai,p.gelar_belakang)) as namapeg from user_pegawai u,pegawai p where 
		// 					  p.kd_pegawai = u.kd_pegawai and u.user_id='".$this->user->user_id."'");					
		// if ($query) {
		// 		$data=mysql_fetch_array($query);  
				foreach ($data_pegawai as $data) {
							$id = $data['kd_pegawai']; 
							$NIP = $data['NIP']; 
							$nama_pegawai = $data['namapeg']; 
				}

		// }	
	?>
	<div id="bottom-container">
		<div id="footer">
			<!--<div class="left-foot">&nbsp;&nbsp;<a href="http://localhost/simpega/">SIMPEGA UM</a></div>-->
			<div class="left-foot">&nbsp;&nbsp;>>&nbsp;&nbsp;</div>
			<?php 					
				if ($nama_pegawai) {
						$nama_peg = $nama_pegawai; 
				}	
				else {
						$nama_peg = 'CALON PENGGUNA SIMPEGA'; 
				}
			?>
			<!--<marquee direction="right" scrollamount="3" bgcolor="#transparan" width="60%"  behavior="alternate"> Pengguna : <?= $nama_peg ?> </marquee> -->
			Pengguna : <?= $nama_peg ?>
			<div class="right-foot">
			<a href="http://www.um.ac.id">Universitas Negeri Malang &copy; <?php echo  date("Y") ?></a> &raquo; <a href="<?=site_url('staff/logout');?>" ><strong>KELUAR</strong></a>&nbsp;&nbsp;
			</div>
		<div class="clear"></div>
	</div>
	</div>  
<!-- 	<script type="text/javascript" src="<?=base_url()?>/application/views/staff/js/jquery.min.js"></script>
	

	<script>
			$(document).ready(function(){
				// datatable
				

			});
	</script> -->
</body> 
</html> 