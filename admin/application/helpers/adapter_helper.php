<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


function records_to_assoc($record, $key = 'id', $value = 'name', $default = array())
{
	$assoc = $default;
	
	if(record)foreach ($record as $row)
	{
		if (!in_array($row->$value,$assoc))
			$assoc[$row->$key] = $row->$value;
	}
	
	return $assoc;
}
//update
/******* ngabaiin kesamaan nama *********/
function records_to_assoc2($record, $key = 'id', $value = 'name', $default = array())
{
	$assoc = $default;
	
	if(record)foreach ($record as $row)
	{
		if (!in_array($row->$key,$assoc))
			$assoc[$row->$key] = $row->$value;
	}
	
	return $assoc;
}
/****************** hasil bisa 1 atau lebih field ***********************/
function records_to_assoc3($record, $key = 'id', $values = array('name'), $default = array())
{
	$assoc = $default;
	if(record)foreach ($record as $row)
	{
		if (!in_array($row->$key,$assoc))
			if($values && is_array($values))
				foreach($values as $value)
					$assoc[$row->$key][$value] = $row->$value;
	}
	return $assoc;
}

/*bisa lebih dari stu field*/
function records_to_assoc4($record, $key = 'id', $field1,$field2, $default = array())
{
	$assoc = $default;
	if($record)foreach ($record as $row)
	{
		if (!in_array($row->$key,$assoc))
			$assoc[$row->$key] = $row->$field2.', '.$row->$field1;
	}
	
	return $assoc;
}

function records_to_assoc5($record, $key = 'id', $value = 'name', $default = array())
{
		$assoc = $default;
	
	if($record)
        foreach ($record as $row)
    	{
            unset($tmp);$tmp = array();
            $row_value = '';
    		if (!in_array($row->$key,$assoc)){
                if(is_array($value)){
                    foreach($value as $v){
                        $tmp[$v] = $row->$v;
                    }
                    $row_value = $tmp;
                } else {
                    $row_value = $row->$value;
                }
    			$assoc[$row->$key] = $row_value;
            }
    	}
	
	return $assoc;
}

function arr_to_assoc ($record, $key = 'id', $value = 'name', $default = array())
{
	$assoc = $default;
	
	foreach ($record as $row)
	{
		//if (!in_array($row[$value],$assoc))
			$assoc[$row[$key]] = $row[$value];
	}
	
	return $assoc;
}
