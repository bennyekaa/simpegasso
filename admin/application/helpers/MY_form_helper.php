<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Form token based on Chris Shiflett's implementation
 * @see http://phpsecurity.org
*/

/**
 * Generate Form Token
 * 
 * @access	public
 * @return	string	unique form token
 * 
 */
function generate_token()
{
	$CI =& get_instance();
	
	$token = md5(uniqid(rand(), TRUE));
	$CI->session->set_userdata('token', $token);
	$CI->session->set_userdata('token_time', time());
	
	return $token;
}

// --------------------------------------------------------------------

/**
 * Check form token
 * 
 * @access	public
 * @param	string	submitted token
 * @param	integer	expiration time - default 5 minutes
 * @return	bool	token valid?
 * 
 */
function check_token($submitted_token, $expire = 300)
{
	$CI =& get_instance();
	
	$token_age = time() - $CI->session->userdata('token_time');
	$token = $CI->session->userdata('token');

	// 5 minutes to submit the form
	if ($token_age <= $expire)
	{
		if ($token === $submitted_token)
		{
			return TRUE;
		}		
	}
	
	// Expired or invalid
	$CI->session->unset_userdata('token');
	return FALSE;
}




function form_group_dropdown($name = '', $options = array(), $selected = '', $extra = '')
{
    if ($extra != '') $extra = ' '.$extra;
    
    $form = '<select name="'.$name.'"'.$extra.">\n";
    if($options) foreach($options as $key => $val)
    {
        if(is_array($val))
        {
            $form .= '<optgroup label="'.$key.'">'."\n";
            foreach ($val as $key2 => $val2)
            {
                $sel = ($selected != $key2) ? '' : ' selected="selected"';
                
                $form .= '<option value="'.$key2.'"'.$sel.'>'.$val2."</option>\n";
            }
            $form .= "</optgroup>\n";
        }
        else
        {
            $sel = ($selected != $key) ? '' : ' selected="selected"';
            $form .= '<option value="'.$key.'"'.$sel.'>'.$val."</option>\n";
        }
    }
    $form .= '</select>';
    
    return $form;
}
/* End of file MY_form_helper.php */
/* Location: ./application/helpers/MY_form_helper.php */