<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pasangan_Pegawai_model extends Model
{
    var $record_count;
    var $table_name= 'pasangan_pegawai';
    var $pkey = 'id_pasangan_pegawai';

	function Pasangan_Pegawai_model()
    {
        parent::Model();
    }

    function findAll($fields=NULL,$start = NULL, $count = NULL)
    {
        return $this->find($fields,NULL, NULL,$start, $count);
    }

    function findByFilter($filter_rules, $start = NULL, $count = NULL)
    {
      	return $this->find(NULL,$filter_rules, NULL,$start, $count);
    }

    function find($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
    {
      	$results = array();
		$this->_set_where($filters);

		$this->db->from($this->table_name);
		 $this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		$this->_set_order($filters);

		if ($start){
			if ($count) {
				$this->db->limit($start, $count);
			}
			else {
				$this->db->limit($start);
			}
		}
		//$this->db->join('pegawai','pegawai.kd_pegawai=pasangan_pegawai.kd_pegawai','left');
		$this->db->join('status_keluarga','status_keluarga.kd_status_keluarga=pasangan_pegawai.kd_status_keluarga','left');
		$query = $this->db->get( $this->table_name );
		if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return FALSE;
		}
	}

	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->from($this->table_name);
		$this->db->where($this->pkey, $id);
		$this->db->join('status_keluarga','status_keluarga.kd_status_keluarga=pasangan_pegawai.kd_status_keluarga','left');
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);
	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }
	    return $results;
	}
   
	function retrieve_by_idpeg($idField)
	{
		//$this->db->join('pegawai','pegawai.kd_pegawai=pasangan_pegawai.kd_pegawai','left');
		//$this->db->join('status_keluarga','status_keluarga.kd_status_keluarga=pasangan_pegawai.kd_status_keluarga','left');
		$this->db->where('kd_pegawai',$idField);
		$query = $this->db->get($this->table_name);
		//echo $this->db->last_query();
		$str = $query->result_array();
		return $str;
	}
	function retrieve_by_idpeg_aktif($idField)
	{
		$this->db->where('aktif','1');
		$this->db->where('kd_pegawai',$idField);
		$query = $this->db->get($this->table_name);
		//echo $this->db->last_query();
		$str = $query->result_array();
		return $str;
	}
	
    function add( $data )
    {
		$this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    function update($keyvalue, $data)
    {
        $this->db->where($this->pkey, $keyvalue);
        $this->db->update($this->table_name, $data);
    }
	function update_status($kd_pegawai, $data)
	{
		$this->db->where('kd_pegawai', $kd_pegawai);
		$this->db->update($this->table_name, $data);
	}
    function delete($idField)
    {
        $this->db->where($this->pkey, $idField);
        $this->db->delete($this->table_name);
        return true;
    }

    function _set_where($filters=NULL)
    {
        if ($filters)
		{
			if (is_string($filters))
			{
				$this->db->where($filters);
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
					foreach ($filters as $field => $value)
						$this->db->where($field, $value);
				}
			}
		}
    }

    function _set_order($order=NULL)
    {
        if ($order)
		{
            if (is_string($order))
			{
				$this->db->order_by($order);
			}
            elseif (is_array($order))
            {
                if (count($order) > 0)
                {
					foreach ($order as $field => $value)
                        $this->db->order_by($field, $value);
                }
			}
		}
    }
    
	function get_idpasangan_by_userid($userid)
	{
		$has = $this->db->get_where($this->table_name,array('kd_pegawai' => $userid));   
		$data = $has->row_array();
			
		return $data['id_pasangan_pegawai'];
	}
	
	function get_id_from_NIP($NIP)
	{
		$has = $this->db->get_where($this->table_name,array('NIP' => $NIP));   
		$data = $has->row_array();
			
		return $data['kd_pegawai'];
	}
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table_name."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function get_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
	}
}
?>