<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class jenis_pegawai_model extends Model {

	var $record_count;
    var $table		= 'jenis_pegawai';
	var $pkey 		= 'id_jns_pegawai';
	var $table_parameter		= 'parameter_kenaikan_pangkat';
	var $pkey_parameter 		= 'id_parameter';
	
	function jenis_pegawai_model()
	{
		parent::Model();
	}
	
	function findAll($jumlah = Null, $mulai = 0,$ordby = Null)
	{
		return $this->find(NULL, $jumlah, $mulai,$ordby);
	}
	
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->where($this->pkey, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);
	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }
	    return $results;
	}
	
	function find($filters = NULL, $jumlah = -1, $mulai = NULL,$ordby = Null)
    {
		$results = array();
		$this->_set_where($filters);
		$this->db->from($this->table);
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		//$this->db->join('pegawai','pegawai.id_jns_pegawai = jenis_pegawai.id_jns_pegawai');
		
		if ($jumlah) {
		   if ($mulai) {
			  $this->db->limit( $jumlah,$mulai);
		   }
		   else {
			  $this->db->limit($jumlah);
		   }
		}
		if($ordby)
		$this->db->order_by($ordby." asc");
		$query = $this->db->get( $this->table );
	
		if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else 
			{
				return FALSE;
		}
    }
	
	function add($data)
	{
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}
	
	function delete($id)
	{
		$this->db->delete($this->table, array($this->pkey => $id));
	}
	
	function update($id, $data)
	{
		$this->db->where($this->pkey, $id);
		$this->db->update($this->table, $data);
	}

	function _set_where($filters=NULL)
    {
        if ($filters) 
		{
            if ( is_string($filters) ) 
            {
                $this->db->where($filters);
            }
			elseif ( is_array($filters) ) 
            {
                if ( count($filters) > 0 ) 
                {
                    foreach ($filters as $field => $value) 
                        $this->db->where($field, $value);               
                }
			}
		}
    }
    
    function _set_order($order=NULL)
    {
        if ($order) 
		{
            if ( is_string($order) ) 
			{
                $this->db->order_by($order);
			}
            elseif ( is_array($order) ) 
            {
                if ( count($order) > 0 ) 
                {
					foreach ($order as $field => $value) 
                        $this->db->order_by($field, $value);               
                }
			}
		}
    }
	
/*	function get_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
	}*/
	function get_jenis_pegawai_assoc()
	{
		$this->load->helper('adapter');
		$this->db->order_by($this->pkey." asc");
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),"id_jns_pegawai","jenis_pegawai");
	}
	
	
	
	function get_assoc()
		{
		$this->load->helper('adapter');
		$this->db->order_by($this->pkey." asc");
		$q = $this->db->get($this->table);
		return records_to_assoc4($q->result(),"id_jns_pegawai","jenis_pegawai","id_jns_pegawai");
	}
	
		function get_assoc2()
	{
		$this->load->helper('adapter');
		$this->db->order_by($this->pkey." asc");
		$q = $this->db->get($this->table);
		return records_to_assoc($q->result(),"id_jns_pegawai","jenis_pegawai");
	}
	
	function get_jenispeg_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
	}

	
	function getAllJenisPegawai($id_jns_pegawai)
	{
		$query =$this->db->query("SELECT id_jns_pegawai,jenis_pegawai from jenis_pegawai where id_jns_pegawai not like '".$id_jns_pegawai."'  order by  id_jns_pegawai");
		if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
        else
			{
				return FALSE;
			}
	}
	function getAllJenisPegawaiIdem($id_jns_pegawai)
	{
		$query =$this->db->query("SELECT id_jns_pegawai,jenis_pegawai from jenis_pegawai where id_jns_pegawai like '".$id_jns_pegawai."'  	order by  id_jns_pegawai");
		if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
        else
			{
				return FALSE;
			}
	}

}