<?
class Lap_pensiun_model extends Model {
	var $table				= 'pegawai';
	var $primary_key 		= 'kd_pegawai';
	var $record_count;

	function Lap_pensiun_model()
	{
		parent::Model();
	}
	
	function findAll($jumlah = 10, $mulai = 0)
	{
	    return $this->find(NULL, $jumlah, $mulai);
	}

	function findById($key_value)
	{
	    return $this->find(array($this->primary_key => $key_value));
	}

	function findByFilter($filter_rules, $order = NULL, $start = NULL, $count = NULL)
    {
      	return $this->find(NULL, $filter_rules, $order, $start, $count);
		
    }
	
	/*function findByFilter($filter_rules, $jumlah = 10, $mulai = 0)
	{
	    //echo $mulai;
	    return $this->find($filter_rules, $jumlah, $mulai);
	}*/

	function find($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
	{
		$results = array();

		$this->_set_where($filters);
		$this->_set_order($order);
		
        $query = $this->db->get( $this->table );
        $this->record_count = $query->num_rows();

        $this->db->limit($start, $count);
		//$this->db->select('*, IF( MONTH( tgl_lahir ) =12, 1, MONTH( tgl_lahir ) +1 ) AS bulan_pensiun');
		$this->_set_where($filters);
		$this->_set_order($order);
		
		$query = $this->db->get( $this->table );
        //echo ($this->db->last_query());
		if ($query->num_rows() > 0)
		{
            return $query->result_array();
        }
        else
		{
            return FALSE;
		}
	}
	
	/*function find($filters = NULL, $jumlah = 1, $mulai = NULL)
	{

	    $results = array();
	    $this->_set_where($filters);
	    $this->db->from($this->table);
	    $this->record_count = $this->db->count_all_results();

	    //the real result
	    $this->_set_where($filters);
	    $this->_set_order($filters);

	    if ($jumlah) {
	       if ($mulai) {
		  $this->db->limit( $jumlah,$mulai);
	       }
	       else {
		  $this->db->limit($jumlah);
	       }
	    }
		
		//$this->db->select('pegawai.NIP, pegawai.nama_pegawai, pegawai.gelar_belakang, pegawai.gelar_depan, pegawai.tgl_lahir, pegawai.tempat_lahir, pegawai.kode_unit, IF( MONTH( tgl_lahir ) =12, 1, MONTH( tgl_lahir ) +1 ) AS bulan_pensiun');
	    //$this->db->select('*, IF( MONTH( tgl_lahir ) =12, 1, MONTH( tgl_lahir ) +1 ) AS bulan_pensiun');
        //$this->db->join('unit_kerja','unit_kerja.kode_unit = pegawai.kode_unit');
	    $query = $this->db->get( $this->table );
        //echo $this->db->last_query();
	    if ($query->num_rows() > 0)
	    {
			return $query->result_array();
	    }
	    else
	    {
			return FALSE;
	    }
	}*/
	

    function retrieve_by_pkey($idField) {

	    $results = array();
		$this->db->select('pegawai.NIP, pegawai.nama, pegawai.gelar_belakang, pegawai.gelar_depan, pegawai.tgl_lahir, pegawai.tempat_lahir, pegawai.kode_unit');
	    //$this->db->join('kerja','kerja.FK_pegawai = pegawai.kd_pegawai');
		$this->db->where( $this->primary_key, $idField);
	    $this->db->limit( 1 );
		$query = $this->db->get( $this->table );
	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
		   
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }
	    return $results;
    }

	function _set_where($filters=NULL)
	{
	    if ($filters)
	    {
			if ( is_string($filters) )
			{
				$this->db->where($filters);
                
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
				foreach ($filters as $field => $value)
					$this->db->where($field, $value);
				}
			}
	    }
	}

	function _set_order($order=NULL)
	{
	    if ($order)
	    {
			if ( is_string($order) )
			{
				$this->db->order_by($order);
			}
			elseif ( is_array($order) )
			{
				if ( count($order) > 0 )
				{
				   foreach ($order as $field => $value)
					$this->db->order_by($field, $value);
				}
			}
	    }
	}	
}
