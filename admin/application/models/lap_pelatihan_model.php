<?
class lap_pelatihan_model extends Model {
    var $record_count;
    var $table_name = 'riwayat_pelatihan';
    var $primary_key = 'id_riwayat_pelatihan';
	
    function pelatihanmodel()
    {
		parent::Model();
    }

    function findAll($jumlah = 10, $mulai = 0, $by = '')
    {
		return $this->find(NULL, $jumlah, $mulai,$by);
    }

    function findById($key_value)
    {
		return $this->find(array($this->primary_key => $key_value));
    }

    function findByFilter($filter_rules, $jumlah = NULL, $mulai = NULL , $by = '')
    {
		return $this->find($filter_rules, $jumlah, $mulai, $by);
    }

    function find($filters = NULL, $jumlah = -1, $mulai = NULL, $by = 'riwayat_pelatihan.id_riwayat_pelatihan asc')
    {
		if (!$by) $by = 'riwayat_pelatihan.id_riwayat_pelatihan asc';
		$results = array();
		$this->_set_where($filters);
		//$this->db->where(array('status'=>0));
		$this->db->from($this->table_name);
		$this->record_count = $this->db->count_all_results();
		//the real result
		$this->_set_where($filters);
		
		if ($jumlah) {
		   if ($mulai) {
			  $this->db->limit( $jumlah,$mulai);
		   }
		   else {
			  $this->db->limit($jumlah);
		   }
		}
		$this->db->order_by($by);
		//$this->db->where(array('status'=>0));
		$query = $this->db->get( $this->table_name );
		
		if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return FALSE;
    }

   function retrieve_by_pkey($idField) {

	$results = array();
  
	$this->db->where( $this->primary_key, $idField);
	$this->db->limit( 1 );
	$query = $this->db->get( $this->table_name );
  
  
	if ($query->num_rows() > 0)
	{
	   $row = $query->row_array();
	   $results		 = $row;
	}
	else
	{
	   $results = false;
	}
  
	return $results;
   }


    function add( $data )
    {

      $this->db->insert($this->table_name, $data);

      return $this->db->insert_id();
    }

    function modify($keyvalue, $data)
    {
		$this->db->where($this->primary_key, $keyvalue);
		$this->db->update($this->table_name, $data);
	}

    function delete_by_pkey($idField)
    {
		$this->db->where($this->primary_key, $idField);
		$this->db->delete($this->table_name);
		//$this->db->update($this->table_name,array('status'=>1));
		return true;
    }

    function _set_where($filters=NULL)
    {
        if ($filters) 
		{
            if ( is_string($filters) ) 
            {
                $this->db->where($filters);
            }
	    elseif ( is_array($filters) ) 
            {
                if ( count($filters) > 0 ) 
                {
                    foreach ($filters as $field => $value) 
                        $this->db->where($field, $value);               
                }
			}

		}
    }
    
    function _set_order($order=NULL)
    {
        if ($order) 
		{
			if ( is_string($order) ) 
			{
					$this->db->order_by($order);
			}
			elseif ( is_array($order) ) 
			{
				if ( count($order) > 0 ) 
				{
					foreach ($order as $field => $value) 
						$this->db->order_by($field, $value);               
				}
			}
		}
    }
}

?>
