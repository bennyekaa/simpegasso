<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class lookup_model extends Model{
	function lookup_model(){
        parent::Model();
    }
	
	function shortmonthname($month) {
		switch ($month) {
			case '01' : return 'Jan'; break;
			case '02' : return 'Feb'; break;
			case '03' : return 'Mar'; break;
			case '04' : return 'Apr'; break;
			case '05' : return 'Mei'; break;
			case '06' : return 'Jun'; break;
			case '07' : return 'Jul'; break;
			case '08' : return 'Agt'; break;
			case '09' : return 'Sep'; break;
			case '10' : return 'Okt'; break;
			case '11' : return 'Nov'; break;
			case '12' : return 'Des'; break;
		}
	}
	
	function longonthname($month) {
		switch ($month) {
			case '01' : return 'Januari'; break;
			case '02' : return 'Februari'; break;
			case '03' : return 'Maret'; break;
			case '04' : return 'April'; break;
			case '05' : return 'Mei'; break;
			case '06' : return 'Juni'; break;
			case '07' : return 'Juli'; break;
			case '08' : return 'Agustus'; break;
			case '09' : return 'September'; break;
			case '10' : return 'Oktober'; break;
			case '11' : return 'November'; break;
			case '12' : return 'Desember'; break;
		}
	}
	
	function _ubah_hari(){
		switch(date('N')) {
			case 1 : return 'Senin';
					 break;
			case 2 : return 'Selasa';
					 break;
			case 3 : return 'Rabu';
					 break;
			case 4 : return 'Kamis';
					 break;
			case 5 : return 'Jumat';
					 break;
			case 6 : return 'Sabtu';
					 break;
			case 7 : return 'Minggu';
					 break;
		}
	}
	
	function jenis_waktu($time){
		if($time <11) {
			return 'Pagi';
			break;
		}
		elseif($time <15) {
			return 'Siang';
			break;
		}
		elseif($time <18) {
			return 'Sore';
			break;
		}
		else {
			return 'Malam';
			break;
		}
	}
	
	function gender_assoc(){
		return array(1=>"Laki-laki",2=>"Perempuan");
	}
	
	function jenis_pendidikan_assoc(){
		return array(1=>"Formal",2=>"Non Formal");
	}
	
	function jenis_sertifikasi_assoc(){
		return array(1=>"Penilaian Portofolio", 2=>"Pemberian Sertifikat Langsung");
	}
	
	function status_assoc(){
		return array(0=>"Non-Aktif",1=>"Aktif");
	}
	function perpanjangan_assoc(){
		return array('Tidak Perpanjangan'=>'Tidak Perpanjangan','Perpanjangan'=>'Perpanjangan');
	}
	function tunjangan_assoc(){
		return array('Belum Dihentikan'=>'Belum Dihentikan','Dihentikan'=>'Dihentikan');
	}
    function gol_darah_assoc(){
         return array('O'=>'O','A'=>'A','B'=>'B','AB'=>'AB');
    }
	function ket_kp_assoc(){
         return array('KGB'=>'KGB','SK KP'=>'SK KP','SK KP Peny. Ijasah'=>'SK KP Peny. Ijasah','SK CPNS'=>'SK CPNS','SK PNS'=>'SK PNS','KGB Perbaikan'=>'KGB Perbaikan','KGB Batal'=>'KGB Batal','Inpassing'=>'Inpassing','SK Mutasi'=>'SK Mutasi','SK Jabatan'=>'SK Jabatan','Peny. Masa Kerja'=>'Peny. Masa Kerja','Penurunan Pangkat'=>'Penurunan Pangkat','Surat Tugas Honorer'=>'Surat Tugas Honorer','SK Honorer'=>'SK Honorer','-'=>'-');
    }
	function status_pasangan_assoc(){
		return array(1=>"Suami",2=>"Istri");
	}
	
	function status_hidup_assoc(){
		return array(1=>"Hidup",2=>"Meninggal");
	}
	
	function GD_assoc(){
		return array(G=>"G",D=>"D");
	}
		
	function status_anak_assoc(){
		return array(1=>"Anak Kandung",2=>"Anak Angkat");
	}
	
	function status_pegawai_assoc(){
		return array('2'=>"PNS", '1'=>"CPNS", '3'=>"Kontrak", '4'=>"Surat Tugas", '5'=>"SK");
	}
	
	function status_asal_assoc(){
		return array('1'=>"Umum",'2'=>"Honorer");
	}
	function sertifikasi_assoc(){
		return array(0=>"Belum",1=>"Sudah");
	}
	
	function penanggungjawab_assoc(){
		return array(1=>"Ya",0=>"Tidak");
	}
	
	function eselon_assoc(){
		return array('non-eselon'=>'non-eselon','II'=>'II','IIa'=>'IIa','III'=>'III','IIIa'=>'IIIa','IV'=>'IV','IVa'=>'IVa');
	}
	
	function semester_assoc(){
		return array('Gasal'=>'Gasal','Genap'=>'Genap');
	}
	
	function capaian_assoc(){
		return array('1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10',
		'11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20',
		'21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30',
		'31'=>'31','32'=>'32','33'=>'33','34'=>'34','35'=>'35','36'=>'36','37'=>'37','38'=>'38','39'=>'39','40'=>'40',
		'41'=>'41','42'=>'42','43'=>'43','44'=>'44','45'=>'45','46'=>'46','47'=>'47','48'=>'48','49'=>'49','50'=>'50',
		'51'=>'51','52'=>'52','53'=>'53','54'=>'54','55'=>'55','56'=>'56','57'=>'57','58'=>'58','59'=>'59','60'=>'60',
		'61'=>'61','62'=>'62','63'=>'63','64'=>'64','65'=>'65','66'=>'66','67'=>'67','68'=>'68','69'=>'69','70'=>'70',
		'71'=>'71','72'=>'72','73'=>'73','74'=>'74','75'=>'75','76'=>'76','77'=>'77','78'=>'78','79'=>'79','80'=>'80',
		'81'=>'81','82'=>'82','83'=>'83','84'=>'84','85'=>'85','86'=>'86','87'=>'87','88'=>'88','89'=>'89','90'=>'90',
		'91'=>'91','92'=>'92','93'=>'93','94'=>'94','95'=>'95','96'=>'96','97'=>'97','98'=>'98','99'=>'99','100'=>'100');
	}
	
	function jenis_pelatihan_assoc(){
		return array('1'=>'Struktural','2'=>'Fungsional','3'=>'Teknis','4'=>'Prajabatan');
	}
	function adm_akd_assoc(){
		return array(1=>" Tenaga Administrasi",2=>"Tenaga Akademik");
	}
	function tahun_assoc($y1, $y2){
		$arr_tahun = array();
		//die("$y1 => $y2");
		for($y = $y1; $y <= $y2; $y++)
		{
			$arr_tahun[$y] = $y;
		}
        return $arr_tahun;
    }
	function tahun_minus_assoc($y1, $y2){
		$arr_tahun = array();
		//die("$y1 => $y2");
		for($y = $y1; $y >= $y2; $y--)
		{
			$arr_tahun[$y] = $y;
		}
        return $arr_tahun;
    }
/*	function gd_assoc(){
         return array('G'=>'G','D'=>'D');
    }*/
	function get_tingkat_all()
    {
		$this->load->helper('adapter');
		$this->db->select('distinct tingkat');
		$q=$this->db->get('unit_kerja');

		$res=records_to_assoc($q->result(), 'tingkat',  'tingkat');
		
		return $res;
    }
	function month_assoc(){
        return array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember');
    }

	function get_pelatihan_all()
    {
		$this->load->helper('adapter');
		$q=$this->db->get('riwayat_pelatihan');

		$res=records_to_assoc($q->result(), 'id_riwayat_pelatihan',  'nama_pelatihan');
		return $res;
    }
	
	
	function get_idpegawai(){
		$this->load->helper('adapter');
		$this->db->select('kd_pegawai');
		$q=$this->db->get('riwayat_pelatihan');
		$res = $q->row_array();
		return $res['kd_pegawai'];
	}
	
	function get_pegbypelatihan($kd_pegawai){
		$this->load->helper('adapter');
		$this->db->select('nama_pegawai');
		$q=$this->db->get_where('pegawai',array('pegawai.kd_pegawai'=>$kd_pegawai));
		$res = $q->row_array();
		return $res['nama'];
	}
	
	function get_namapelatihan($id){
		$q=$this->db->get_where('riwayat_pelatihan',array('id_riwayat_pelatihan'=>$id));
		$res = $q->row_array();
		return $res['nama_pelatihan'];
	}
	
	function get_all_pelatihan(){
		$this->load->helper('adapter');
		$this->db->select('users.user_id , sekolah.nama as nama_sekolah');
		$this->db->join('members','members.id_sekolah=sekolah.id_sekolah');
		$this->db->join('users','members.user_id=users.user_id');
		$q=$this->db->get('sekolah');
	
		return records_to_assoc($q->result(), 'user_id', 'nama_sekolah');
	}
	
	function get_NIPpegawai($kd_pegawai)
    {
		$this->db->select('NIP');
		$this->db->where('kd_pegawai',$kd_pegawai);
		$q=$this->db->get('pegawai');
		$res=$q->row_array();
		return $res['NIP'];
    }	

	function get_namapegawai($kd_pegawai)
    {
		$this->db->select('nama_pegawai');
		$this->db->where('kd_pegawai',$kd_pegawai);
		$q=$this->db->get('pegawai');
		$res=$q->row_array();
		return $res['nama'];
    }		

	function get_namapenghargaan($FK_jenis_penghargaan)
    {
		$this->db->select('nama_penghargaan');
		$this->db->where('kd_jenis_penghargaan',$FK_jenis_penghargaan);
		$q=$this->db->get('master_jenis_penghargaan');
		$res=$q->row_array();
		return $res['nama_penghargaan'];
    }	
	
	function get_nama_pelatihan($id_riwayat_pelatihan)
    {
		$this->db->select('nama_pelatihan');
		$this->db->where('id_riwayat_pelatihan',$id_riwayat_pelatihan);
		$q=$this->db->get('riwayat_pelatihan');
		$res=$q->row_array();
		return $res['nama_pelatihan'];
    }	
	
	function get_ket_pelatihan($id_riwayat_pelatihan)
    {
		$this->db->select('ket_riwayat_pelatihan');
		$this->db->where('id_riwayat_pelatihan',$id_riwayat_pelatihan);
		$q=$this->db->get('riwayat_pelatihan');
		$res=$q->row_array();
		return $res['ket_riwayat_pelatihan'];
    }		
	
	function get_namadepartemen($kd_departemen)
    {
		$this->db->select('departemen');
		$this->db->where('kd_departemen',$kd_departemen);
		$q=$this->db->get('master_departemen');
		$res=$q->row_array();
		return $res['departemen'];
    }			

	function get_cari_field($tablename,$indexname,$fcari,$fkid)
    {
		if ($fkid!="*") {
			$this->db->select($fkid);
		}
		
		$this->db->where($indexname,$fcari);
		$q=$this->db->get($tablename);
		$res=$q->row_array();

		if($fkid!="*"){
			return $res[$fkid];		
		}else{
			return $res;
		}
    }	
	function get_cari_field2($tablename,$indexname1,$indexname2,$fcari1,$fcari2,$fkid)
    {
		$this->db->select($fkid);
		$this->db->where($indexname1,$fcari1);
		$this->db->where($indexname2,$fcari2);
		$q=$this->db->get($tablename);
		$res=$q->row_array();
		return $res[$fkid];
    }	
	
	function get_nmpenghargaan()
    {
		$this->load->helper('adapter');
		$q=$this->db->get('master_jenis_penghargaan');

		$res=records_to_assoc($q->result(), 'kd_jenis_penghargaan',  'nama_penghargaan', array(date('0') => '--Pilih Status Pegawai--'));
		return $res;
    }
	
    function get_group($all=0){
		$this->load->helper('adapter');
		$q=$this->db->get('unit_kerja');
			if($all==1)
				$res=records_to_assoc($q->result(), 'kode_unit',  'unit_kerja');
			else
				$res=records_to_assoc($q->result(), 'kode_unit',  'unit_kerja', 
					array('0' => 'Semua Group'));
		return $res;
    }	

    function get_group_laporan($all=0,$tabelname,$indexname,$idname){
		$this->load->helper('adapter');
		$q=$this->db->get($tabelname);
			if($all==1)
				$res=records_to_assoc($q->result(), $indexname,  $idname);
			else
				$res=records_to_assoc($q->result(),  $indexname,  $idname, 
					array('0' => 'Semua Group'));
		return $res;
    }

	function get_datafield($tabelname,$indexname,$idname)
    {
		$this->load->helper('adapter');
		$q=$this->db->get($tabelname);

		$res=records_to_assoc($q->result(), $indexname,  $idname);
		return $res;
    }	
	function get_datafield2($tabelname,$indexname,$idname)
    {
		$this->load->helper('adapter');
		$q=$this->db->get($tabelname);

		$res=records_to_assoc3($q->result(), $indexname,  $idname);
		return $res;
    }
	
}
?>
