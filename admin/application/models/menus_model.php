<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menus_model extends Model
{
    var $record_count;
    var $table_name = 'menus';
    var $primary_key = 'id_menu';
	
    function menugroups_model()
	{
		parent::Model();
    }
	
	function FindAll()
	{
		$this->db->join('rule','rule.id_rule=menus.id_rule','left');
		$data = $this->db->orderby('menus.parent')->orderby('menus.ordering')->get($this->table_name);
		if ($data->num_rows() > 0)
			return $data->result_array();
		else return false;
	}
	
	function find_by_parent($id)
	{
		$data = $this->db->orderby('menus.ordering')
						->get_where($this->table_name,
									array('parent'=>$id));
		if ($data->num_rows() > 0)
			return $data->result_array();
		else return false;
	}

	function retrieve_by_pkey($idField)
	{
		$this->db->where($this->primary_key, $idField);
		$this->db->limit( 1 );		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() > 0)
		{
		   $row = $query->row_array();
		   return $row;
		}
		else return false;
	}
	
    function add($data)
	{
		$this->db->insert($this->table_name, $data);
		return $this->db->insert_id();
    }

    function modify($keyvalue, $data)
	{
		$this->db->where($this->primary_key, $keyvalue);
		$this->db->update($this->table_name, $data);
    }
	
	function delete_by_pkey($idField)
	{
		$this->db->where($this->primary_key, $idField);
		$this->db->delete($this->table_name);
		return true;
    }
	
	function delete_any($idField)
	{
		if($idField){
			foreach($idField as $id=>$value)
			{
				if($id==0)$this->db->where($this->primary_key,$value);
				else if($value)$this->db->or_where($this->primary_key,$value);
			}
			$this->db->delete($this->table_name);
			return true;
		}
		else return false;
	}
	
	function get_newordering($parent)
	{
		$this->db->select_max("ordering");
		$q=$this->db->get_where($this->table_name,array("parent"=>$parent));
		$res = $q->row_array();
		return $res['ordering'];
	}
	
	function get_name($idField)
	{
		$this->db->where($this->primary_key, $idField);
		$this->db->limit( 1 );		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() > 0)
		{
		   $row = $query->row_array();
		   return $row['parent'];
		}
		else return false;
	}
	
	function get_menu()
	{
	    $this->db->select("id_menu,name,(SELECT menupar.name FROM menus menupar WHERE menupar.id_menu = menus.parent) AS parent_name");
		$q=$this->db->get($this->table_name);
		foreach ($q->result_array() as $value)
		{
            $res[strtoupper($value['parent_name'])][$value['id_menu']]=$value['name'];
        }
		return $res;
    }
	function _set_where($filters=NULL)
    {
        if ($filters) 
		{
            if ( is_string($filters) ) 
            {
                $this->db->where($filters);
				return true;
			}
			elseif ( is_array($filters) ) 
            {
                if ( count($filters) > 0 ) 
                {
                    foreach ($filters as $field => $value)
						if(is_int($field))
							$this->db->where($this->primary_key, $value);
						else
							$this->db->where($field, $value);               
                }
				return true;
			}
			else return false;
		}
    }
}
?>