<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class user_pegawai_model extends Model
{
    var $record_count;
    var $table_name= 'user_pegawai';
    var $key_fiedl = 'user_id';

	function user_pegawai_model()
    {
        parent::Model();
    }

    /*
    * get all data from table
    */
    function findAll($fields=NULL,$start = NULL, $count = NULL)
    {
        return $this->find($fields,NULL, NULL,$start, $count);
    }

    function findByFilter($filter_rules, $start = NULL, $count = NULL)
    {
      	return $this->find(NULL,$filter_rules, NULL,$start, $count);
    }

    function find($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
    {
      	$results = array();
		//finding number of search
		$this->_set_where($filters);

		$this->db->from($this->table_name);
		 $this->record_count = $this->db->count_all_results();

		//the real result
		$this->_set_where($filters);
		$this->_set_order($filters);

		if ($start){
			if ($count) {
				$this->db->limit($start, $count);
			}
			else {
				$this->db->limit($start);
			}
		}
		
		$this->db->join('pegawai','pegawai.kd_pegawai = user_pegawai.kd_pegawai');
		$this->db->join('users','users.username = user_pegawai.username');
	
      	$query = $this->db->get( $this->table_name );
		if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return FALSE;
		}
	}


   function retrieve_by_pkey($idField) {

	$results = array();

	$this->db->where( $this->key_fiedl, $idField);
	$this->db->limit( 1 );
	$query = $this->db->get( $this->table_name );


	if ($query->num_rows() > 0)
	{
	   $row = $query->row_array();
	   $results		 = $row;
	}
	else
	{
	   $results = false;
	}

	return $results;
   }
    function add( $data )
    {
		$this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    function update($keyvalue, $data)
    {
        $this->db->where($this->key_fiedl, $keyvalue);
        $this->db->update($this->table_name, $data);
    }
    
	function update_user($id, $data)
    {
        $this->db->where('user_id', $id);
        $this->db->update($this->table_name, $data);
    }
	
    function delete($idField)
    {
        $this->db->where($this->key_fiedl, $idField);
        $this->db->delete($this->table_name);
        return true;
    }

    function _set_where($filters=NULL)
    {
        if ($filters)
		{
			if (is_string($filters))
			{
				$this->db->where($filters);
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
					foreach ($filters as $field => $value)
						$this->db->where($field, $value);
				}
			}
		}
    }

    function _set_order($order=NULL)
    {
        if ($order)
		{
            if (is_string($order))
			{
				$this->db->order_by($order);
			}
            elseif (is_array($order))
            {
                if (count($order) > 0)
                {
					foreach ($order as $field => $value)
                        $this->db->order_by($field, $value);
                }
			}
		}
    }
    
	function get_idpegawai_by_userid($userid)
	{
		$this->db->where('user_id',$userid);
		$query = $this->db->get($this->table_name);   		
		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
			return $data['kd_pegawai'];
		}
		else
		{
			return FALSE;
		}
		//return $data;		
	} 
	
	function cek_id_peg($id_peg)
	{
		$query = $this->db->get_where($this->table_name,array('kd_pegawai' => $id_peg));   
		if ($query->num_rows() > 0)
		{
		   return TRUE;
		}
		else
		{
		   return FALSE;
		}
	}
}
?>