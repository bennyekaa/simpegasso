<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jenis_jabatan_uj2016_model extends Model {

	function __construct()
	{
		parent::__construct();
	}

	public function getJabatan()
	{
		$this->db->select("jenis_jabatan_uj_remun2016.id_jabatan_uj, jenis_jabatan_uj_remun2016.nama_jabatan_uj");
		$this->db->from("jenis_jabatan_uj_remun2016");
		$result=$this->db->get()->result_array();
		return $result;
	}

	public function getJVRendah($id_jabatan_uj)
	{
		$this->db->select("jenis_jabatan_uj_remun2016.jv_rendah");
		$this->db->from("jenis_jabatan_uj_remun2016");
		$this->db->where("id_jabatan_uj", $id_jabatan_uj);

		$result=$this->db->get()->row_array();

		return $result;
	}

	public function getJVTinggi($id_jabatan_uj)
	{
		$this->db->select("jenis_jabatan_uj_remun2016.jv_tinggi");
		$this->db->from("jenis_jabatan_uj_remun2016");
		$this->db->where("id_jabatan_uj", $id_jabatan_uj);

		$result=$this->db->get()->row_array();

		return $result;
	}

	public function getJvJc($jabatan_baru, $idgolpangkat){
		$jv = ($idgolpangkat<=8) ? "jenis_jabatan_uj_remun2016.jv_rendah" : "jenis_jabatan_uj_remun2016.jv_tinggi" ;
		$this->db->select("jenis_jabatan_uj_remun2016.job_class, ".$jv." as jv");
		$this->db->from("jenis_jabatan_uj_remun2016");
		$this->db->where("id_jabatan_uj", $jabatan_baru);

		$result = $this->db->get()->result_array();

		return $result;
	}
    
}
