<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pensiun_model extends Model {
    
	var $record_count;
    var $table = 'pegawai_pensiun';
    var $primary_key = 'kd_pegawai';
	
    function Pensiun_model(){
		parent::Model();
    }

	function get_assoc($field)
	{
		$this->load->helper('adapter');
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->primary_key,$field);
	}
	
  	function findAll($jumlah = Null, $mulai = 0,$ordby = Null)
	{
		return $this->find(NULL, $jumlah, $mulai,$ordby);
	}

    function findById($key_value)
    {
		return $this->find(array($this->primary_key => $key_value));
    }

    function findByFilter($filter_rules, $order = NULL, $start = NULL, $count = NULL)
    {
      	return $this->find(NULL, $filter_rules, $order, $start, $count);
		
    }

	function find($fields=NULL, $filters = NULL, $order=NULL, $start = NULL, $count = NULL)
	{
		$results = array();

		$this->_set_where($filters);
		$this->_set_order($order);
		//$this->db->join('pegawai','pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat');
        
        $query = $this->db->get( $this->table );
        $this->record_count = $query->num_rows();

        $this->db->limit($start, $count);
		/*if ($start){
			if ($count) {
				$this->db->limit($start, $count);
			}
			else {
				$this->db->limit($start);
			}
		}*/
		//$this->db->join('nama_unit','unit_kerja.kode_unit = kerja.FK_unit_kerja');
		$this->_set_where($filters);
		$this->_set_order($order);
		//$this->db->join('pegawai','pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat');
		$query = $this->db->get( $this->table );
        //echo ($this->db->last_query());
		if ($query->num_rows() > 0)
		{
            return $query->result_array();
        }
        else
		{
            return FALSE;
		}
	}


   function retrieve_by_pkey($idField)
   {
		$results = array();
		$this->db->where( $this->primary_key, $idField);
		$this->db->limit( 1 );
		$query = $this->db->get( $this->table );
	  
		if ($query->num_rows() > 0){
		   $row = $query->row_array();
		   $results		 = $row;
		}
		else{
		   $results = false;
		}	  
		return $results;
   }


    function add( $data )
    {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
    }

    function modify($keyvalue, $data)
    {
		$this->db->where($this->primary_key, $keyvalue);
		$this->db->update($this->table, $data);
    }

    function delete_by_pkey($idField)
    {
		$this->db->where($this->primary_key, $idField);
		$this->db->delete($this->table);

		return true;
    }

    function _set_where($filters=NULL)
	{
	    if ($filters)
	    {
			if ( is_string($filters) )
			{
				$this->db->where($filters);
                
			}
			elseif ( is_array($filters) )
			{
				if ( count($filters) > 0 )
				{
				foreach ($filters as $field => $value)
					$this->db->where($field, $value);
				}
			}
	    }
	}
    
    function _set_order($order=NULL)
    {
        if ($order) 
		{
            if ( is_string($order) ) 
			{
                $this->db->order_by($order);
			}
            elseif ( is_array($order) ) 
            {
                if ( count($order) > 0 ) 
                {
		   foreach ($order as $field => $value) 
                        $this->db->order_by($field, $value);               
                }
			}
		}
    }
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}	
	
	
	
	function get_namapegawai_by_idpegawai($id)
	{
		$this->db->where('kd_pegawai',$id);
		$query = $this->db->get($this->table_name);   		
		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
			return $data['kd_pegawai'];
		}
		else
		{
			return FALSE;
		}
		//return $data;		
	}
	function get_active($idField)
	{
		$results = array();
		$this->db->select('aktif');
		$this->db->where( $this->primary_key, $idField);
		$this->db->limit( 1 );
		$query = $this->db->get( $this->table );
	  
		if ($query->num_rows() > 0){
		   $row = $query->row_array();
		   $results		 = $row;
		}
		else{
		   $results = false;
		}	  
		return $results;
	}
	
	
	
}