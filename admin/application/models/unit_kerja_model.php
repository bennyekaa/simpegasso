<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Unit_kerja_model extends Model {

	var $table		= 'unit_kerja';
	var $pkey 		= 'kode_unit';
	var $table_name		= 'unit_kerja,pegawai';
	var $table_name1		= 'unit_kerja';
	
	function Unit_kerja_model()
	{
		parent::Model();
	}
	
	/* untuk paging */ 

	function findAll($jumlah = Null, $mulai = 0,$ordby = Null)
	{
		return $this->find(NULL, $jumlah, $mulai,$ordby);
	}
	
	function findByFilter($filter_rules, $jumlah = 0, $mulai = 0){
		return $this->find($filter_rules, $jumlah, $mulai);
    }
	
	function find($filters = NULL,$filters_tingkat = '' ,$jumlah = -1, $mulai = NULL,$ordby = Null)
	{
		$results = array();
		$this->_set_where($filters);
		$this->_set_where($filters_tingkat);
		$this->db->from($this->table);
		//echo $this->db->last_query();
		$this->record_count = $this->db->count_all_results();

		$this->_set_where($filters);
		$this->_set_where($filters_tingkat);
		
		if ($jumlah) {
		   if ($mulai) {
			  $this->db->limit( $jumlah,$mulai);
		   }
		   else {
			  $this->db->limit($jumlah);
		   }
		}
		if($ordby)
		$this->db->order_by($ordby." asc");
		$query = $this->db->get( $this->table );
	
		if ($query->num_rows() > 0)
			{
				return $query->result_array();
				
			}
			else 
			{
				return FALSE;
		}
    }
	
	function get_unit($filter)
	{
		if ($filter=='2'){
		$this->db->where('unit_kerja.id_group','2');}
	    $this->db->select("kode_unit,nama_unit,
		(SELECT unit_induk.nama_unit FROM unit_kerja unit_induk WHERE  unit_induk.id_group='2' and unit_induk.kode_unit = unit_kerja.kode_unit_general) AS nama_induk");
		$q=$this->db->get($this->table_name1);
		foreach ($q->result_array() as $value)
		{
            $res[strtoupper($value['nama_induk'])][$value['kode_unit']]=$value['nama_unit'];
        }
		return $res;
    }
	
	function _set_where($filters=NULL)
    {
        if ($filters) 
		{
            if ( is_string($filters) ) 
            {
                $this->db->where($filters);
            }
			elseif ( is_array($filters) ) 
            {
                if ( count($filters) > 0 ) 
                {
                    foreach ($filters as $field => $value) 
                        $this->db->where($field, $value);               
                }
			}
		}
    }
    
    function _set_order($order=NULL)
    {
        if ($order) 
		{
            if ( is_string($order) ) 
			{
                $this->db->order_by($order);
			}
            elseif ( is_array($order) ) 
            {
                if ( count($order) > 0 ) 
                {
					foreach ($order as $field => $value) 
                        $this->db->order_by($field, $value);               
                }
			}
		}
    }
	
	/*function findAll()
	{
		$this->db->order_by($this->pkey);
		$query = $this->db->get($this->table);
		return $query->result_array();
	}*/
	
	
	function get_id()
	{
		$query=$this->db->query("SHOW TABLE STATUS LIKE '".$this->table."'");
		$res = $query->row_array();
		return $res['Auto_increment'];
	}
	
	function retrieve_by_pkey($id)
	{
		$results = array();
		$this->db->where($this->pkey, $id);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);

	    if ($query->num_rows() > 0)
	    {
	       $row = $query->row_array();
	       $results		 = $row;
	    }
	    else
	    {
	       $results = false;
	    }

	    return $results;
	}
	
	function add($data)
	{
		$this->db->insert($this->table,$data);
	}
	
	function delete($id)
	{
		$this->db->delete($this->table, array($this->pkey => $id));
	}
	
	function update($id, $data)
	{
		$this->db->where($this->pkey, $id);
		$this->db->update($this->table, $data);
	}
	
	function get_unit_assoc()
	{
		$this->load->helper('adapter');
		$this->db->where('unit_kerja.GD','G');
		$this->db->order_by($this->pkey." asc");
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),"kode_unit","nama_unit");
	}
	
	function get_unit_assoc_by_key($field)
	{
		$this->load->helper('adapter');
		$this->db->where('unit_kerja.GD','G');
		$this->db->where('unit_kerja.id_group',$field);
		$this->db->order_by($this->pkey." asc");
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),"kode_unit","nama_unit");
	}
	
	function get_assoc($field)
	{
		$this->load->helper('adapter');
		$this->db->order_by($this->pkey);
		$q = $this->db->get($this->table);
		return records_to_assoc2($q->result(),$this->pkey,$field);
	}
	
	
	function get_assoc2()
	{
		$this->load->helper('adapter');
		$this->db->order_by($this->pkey);
		$data = $this->db->get($this->table);
		return records_to_assoc2($data->result(),"kode_unit","nama_unit");
	}
	function get_not_tingkat2()
	{
		$this->load->helper('adapter');
		$this->db->where('unit_kerja.tingkat not like 2');
		$this->db->order_by($this->pkey);
		$data = $this->db->get($this->table);
		return records_to_assoc2($data->result(),"kode_unit","nama_unit");
	}
	
	function get_fakultas()
	{
		$this->load->helper('adapter');
		$this->db->where('unit_kerja.id_group like 2 and (unit_kerja.tingkat like 1 or unit_kerja.tingkat like 2)');
		//$this->db->where('unit_kerja.tingkat like 1');
		//$this->db->or_where('unit_kerja.tingkat like 2');
		$this->db->order_by($this->pkey);
		$data = $this->db->get($this->table);
		return records_to_assoc2($data->result(),"kode_unit","nama_unit");
	}
	
	function get_unit_awal($idField,$idFieldGroup)
	{
	    $this->db->select("kode_unit,nama_unit,kode_unit");
		$this->db->where( $this->pkey, $idField); 
		$this->db->where('unit_kerja.GD','G');
		$q=$this->db->get($this->table);
		
		foreach ($q->result_array() as $value)
		{
            $res[strtoupper($value['nama_unit'])][$value['kode_unit']]=$value['nama_unit'];
        }
			$this->db->select("kode_unit,nama_unit,kode_unit");
			$this->db->where('unit_kerja.id_group',$idFieldGroup);
			$this->db->where('unit_kerja.GD','G');
			$q=$this->db->get($this->table);
			foreach ($q->result_array() as $value)
		{
            $res[strtoupper($value['nama_unit'])][$value['kode_unit']]=$value['nama_unit'];
        }
		return $res;
	}
	
	function retrieve_by_filters($filter){
		$results = array();
		$this->_set_where($filter);
		$this->db->limit( 1 );
		$query = $this->db->get($this->table);
	  
		if ($query->num_rows() > 0){
		   $row = $query->row_array();
		   $results		 = $row;
		}
		else{
		   $results = false;
		}	  
		return $results;
	}
	
	function Hitung($kode_unit = NULL,$tingkat = '',$JK='' ,$jumlah = 0, $mulai = NULL,$ordby = Null){
		if(!$by) $by='pegawai.kd_pegawai';
		$results = array();
		
		$this->db->where('pegawai.kode_unit = unit_kerja.kode_unit');
		$this->db->where('pegawai.kode_unit',$kode_unit);
		$this->db->where('unit_kerja.tingkat',$tingkat);
		$this->db->where('pegawai.jns_kelamin',$JK);
		$this->db->from($this->table_name);
		$this->record_count = $this->db->count_all_results();
		
		//result

		if ($jumlah) {
			if ($mulai)
				$this->db->limit( $jumlah,$mulai);
			else 
				$this->db->limit($jumlah);
		}
		$this->db->where('pegawai.kode_unit = unit_kerja.kode_unit');
		$this->db->where('pegawai.kode_unit',$kode_unit);
		$this->db->where('unit_kerja.tingkat',$tingkat);
		$this->db->where('pegawai.jns_kelamin',$JK);
		
		$query = $this->db->get( $this->table_name );
		if ($query->num_rows() > 0){
			return $query->result_array();
		}
		else 
			return FALSE;
    }

    function get_nama_kode_unit_kerja(){
    	return $this->db->query("select kode_unit, nama_unit from unit_kerja order by nama_unit asc")->result_array();
    }
	
}
