<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 SIMPEGA (Sistem Informasi Kepegawaian)
 */
    class MY_Loader extends CI_Loader
    {
        function MY_Loader()
        {
            parent::CI_Loader();
        }
        function model($model, $name = '', $db_conn = FALSE)
	{
		if (is_array($model))
		{
			foreach($model as $babe)
			{
				$this->model($babe);
			}
			return;
		}

		if ($model == '')
		{
			return;
		}

		if (strpos($model, '/') === FALSE)
		{
			$path = '';
		}
		else
		{
			$x = explode('/', $model);
			$model = end($x);
			unset($x[count($x)-1]);
			$path = implode('/', $x).'/';
		}

		if ($name == '')
		{
			$name = $model;
		}

		if (in_array($name, $this->_ci_models, TRUE))
		{
			return;
		}

		$CI =& get_instance();
		if (isset($CI->$name))
		{
			show_error('The model name you are loading is the name of a resource that is already being used: '.$name);
		}

		$model = strtolower($model);
                $_model_path = APPPATH.'models/'.$path.$model.EXT;
               if ( ! file_exists($_model_path))
		{
                       // echo $CI->uri->segment(0);
                        $RTR =& load_class('Router');
                        if ($RTR->is_module)
                        {
                            $_model_path = MODULEBASE.$RTR->module_name.'/models/'.$path.$model.EXT;

                        }
                        // echo  $_model_path;

                        if ( ! file_exists($_model_path))
                            show_error('Unable to locate the model you have specified: '.$model);
		}

		if ($db_conn !== FALSE AND ! class_exists('CI_DB'))
		{
			if ($db_conn === TRUE)
				$db_conn = '';

			$CI->load->database($db_conn, FALSE, TRUE);
		}

		if ( ! class_exists('Model'))
		{
			load_class('Model', FALSE);
		}

                //cek keberadaan modul

		require_once( $_model_path);

		$model = ucfirst($model);

		$CI->$name = new $model();
		$CI->$name->_assign_libraries();

		$this->_ci_models[] = $name;
	}

        function helper($helpers = array())
	{
		if ( ! is_array($helpers))
		{
			$helpers = array($helpers);
		}
                $RTR = &load_class('Router');
		foreach ($helpers as $helper)
		{
			$helper = strtolower(str_replace(EXT, '', str_replace('_helper', '', $helper)).'_helper');

			if (isset($this->_ci_helpers[$helper]))
			{
				continue;
			}

			$ext_helper = APPPATH.'helpers/'.config_item('subclass_prefix').$helper.EXT;

			// Is this a helper extension request?
			if (file_exists($ext_helper))
			{
				$base_helper = BASEPATH.'helpers/'.$helper.EXT;

				if ( ! file_exists($base_helper))
				{
					show_error('Unable to load the requested file: helpers/'.$helper.EXT);
				}

				include_once($ext_helper);
				include_once($base_helper);
			}
			elseif (file_exists(APPPATH.'helpers/'.$helper.EXT))
			{
				include_once(APPPATH.'helpers/'.$helper.EXT);
			}
                        elseif (file_exists(MODULEBASE.$RTR->module_name.'/helpers/'.$helper.EXT))
			{
				include_once(MODULEBASE.$RTR->module_name.'/helpers/'.$helper.EXT);
			}
			else
			{
				if (file_exists(BASEPATH.'helpers/'.$helper.EXT))
				{
					include_once(BASEPATH.'helpers/'.$helper.EXT);
				}
				else
				{
					show_error('Unable to load the requested file: helpers/'.$helper.EXT);
				}
			}

			$this->_ci_helpers[$helper] = TRUE;
			log_message('debug', 'Helper loaded: '.$helper);
		}
	}


        function _ci_load($_ci_data)
	{
		// Set the default data variables
                foreach (array('_ci_view', '_ci_vars', '_ci_path', '_ci_return') as $_ci_val)
		{
			$$_ci_val = ( ! isset($_ci_data[$_ci_val])) ? FALSE : $_ci_data[$_ci_val];
		}

		// Set the path to the requested file
		if ($_ci_path == '')
		{
			$_ci_ext = pathinfo($_ci_view, PATHINFO_EXTENSION);
                        $_ci_file = ($_ci_ext == '') ? $_ci_view.EXT : $_ci_view;
			$_ci_path = $this->_ci_view_path.$_ci_file;

                        $RTR =& load_class('Router');
                        if ( ! file_exists($_ci_path))
        		{
                                if ($RTR->is_module)
                                {
                                    $_ci_path= MODULEBASE.$RTR->module_name.'/views/'.$_ci_file;
                                    //echo $_ci_path;
                                    if ( ! file_exists($_ci_path))
                                    {
                                        show_error('Unable to load the requested file: '.$_ci_file.' On Module:'.$RTR->module_name);
                                    }
                                }
                                else
                                {
                                     show_error('Unable to load the requested file: '.$_ci_file);
                                }

        		}

		}
		else
		{
			$_ci_x = explode('/', $_ci_path);
			$_ci_file = end($_ci_x);
                        if ( ! file_exists($_ci_path))
        		{
        			show_error('Unable to load the requested file: '.$_ci_file);
        		}

		}

		if ($this->_ci_is_instance())
		{
			$_ci_CI =& get_instance();
			foreach (get_object_vars($_ci_CI) as $_ci_key => $_ci_var)
			{
				if ( ! isset($this->$_ci_key))
				{
					$this->$_ci_key =& $_ci_CI->$_ci_key;
				}
			}
		}

		if (is_array($_ci_vars))
		{
			$this->_ci_cached_vars = array_merge($this->_ci_cached_vars, $_ci_vars);
		}
		extract($this->_ci_cached_vars);

		ob_start();

		if ((bool) @ini_get('short_open_tag') === FALSE AND config_item('rewrite_short_tags') == TRUE)
		{
			/*echo eval('?>'.preg_replace("/;*\s*\?>/", "; ?>", str_replace('<?=', '<?php echo ', file_get_contents($_ci_path))));*/
		}
		else
		{
			include($_ci_path); // include() vs include_once() allows for multiple views with the same name
		}

		log_message('debug', 'File loaded: '.$_ci_path);

		// Return the file data if requested
		if ($_ci_return === TRUE)
		{
			$buffer = ob_get_contents();
			@ob_end_clean();
			return $buffer;
		}

		if (ob_get_level() > $this->_ci_ob_level + 1)
		{
			ob_end_flush();
		}
		else
		{
			// PHP 4 requires that we use a global
			global $OUT;
			$OUT->append_output(ob_get_contents());
			@ob_end_clean();
		}
	}

        function _ci_load_class($class, $params = NULL, $object_name = NULL)
	{
		$class = str_replace(EXT, '', trim($class, '/'));

		$subdir = '';
		if (strpos($class, '/') !== FALSE)
		{
			$x = explode('/', $class);
			$class = end($x);
			unset($x[count($x)-1]);
			$subdir = implode($x, '/').'/';
		}

		foreach (array(ucfirst($class), strtolower($class)) as $class)
		{
			$subclass = APPPATH.'libraries/'.$subdir.config_item('subclass_prefix').$class.EXT;

			// Is this a class extension request?
			if (file_exists($subclass))
			{
				$baseclass = BASEPATH.'libraries/'.ucfirst($class).EXT;

				if ( ! file_exists($baseclass))
				{
					log_message('error', "Unable to load the requested class: ".$class);
					show_error("Unable to load the requested class: ".$class);
				}

				// Safety:  Was the class already loaded by a previous call?
				if (in_array($subclass, $this->_ci_loaded_files))
				{
					// Before we deem this to be a duplicate request, let's see
					// if a custom object name is being supplied.  If so, we'll
					// return a new instance of the object
					if ( ! is_null($object_name))
					{
						$CI =& get_instance();
						if ( ! isset($CI->$object_name))
						{
							return $this->_ci_init_class($class, config_item('subclass_prefix'), $params, $object_name);
						}
					}

					$is_duplicate = TRUE;
					log_message('debug', $class." class already loaded. Second attempt ignored.");
					return;
				}

				include_once($baseclass);
				include_once($subclass);
				$this->_ci_loaded_files[] = $subclass;

				return $this->_ci_init_class($class, config_item('subclass_prefix'), $params, $object_name);
			}

			// Lets search for the requested library file and load it.
			$is_duplicate = FALSE;
                        $RTR =& load_class('Router');
			for ($i = 1; $i <= 3; $i++)
			{
				//$path = ($i % 2) ? APPPATH : BASEPATH;
                                if ( $i == 1 )
                                    $path = APPPATH;
                                elseif ( $i == 2 )
                                    $path = BASEPATH;
                                else
                                    $path = MODULEBASE.$RTR->module_name;
				$filepath = $path.'/libraries/'.$subdir.$class.EXT;
				// Does the file exist?  No?  Bummer...
				if ( ! file_exists($filepath))
				{
					continue;
				}

				// Safety:  Was the class already loaded by a previous call?
				if (in_array($filepath, $this->_ci_loaded_files))
				{
					// Before we deem this to be a duplicate request, let's see
					// if a custom object name is being supplied.  If so, we'll
					// return a new instance of the object
					if ( ! is_null($object_name))
					{
						$CI =& get_instance();
						if ( ! isset($CI->$object_name))
						{
							return $this->_ci_init_class($class, '', $params, $object_name);
						}
					}

					$is_duplicate = TRUE;
					log_message('debug', $class." class already loaded. Second attempt ignored.");
					return;
				}

				include_once($filepath);
				$this->_ci_loaded_files[] = $filepath;
				return $this->_ci_init_class($class, '', $params, $object_name);
			}
		} // END FOREACH

		// One last attempt.  Maybe the library is in a subdirectory, but it wasn't specified?
		if ($subdir == '')
		{
			$path = strtolower($class).'/'.$class;
			return $this->_ci_load_class($path, $params);
		}

		// If we got this far we were unable to find the requested class.
		// We do not issue errors if the load call failed due to a duplicate request
		if ($is_duplicate == FALSE)
		{
			log_message('error', "Unable to load the requested class: ".$class);
			show_error("Unable to load the requested class: ".$class);
		}
	}

        function _ci_init_class($class, $prefix = '', $config = FALSE, $object_name = NULL)
	{
		// Is there an associated config file for this class?
                $RTR = &load_class('Router');
		if ($config === NULL)
		{
			// We test for both uppercase and lowercase, for servers that
			// are case-sensitive with regard to file names
			if (file_exists(APPPATH.'config/'.strtolower($class).EXT))
			{
				include_once(APPPATH.'config/'.strtolower($class).EXT);
			}
                        elseif (file_exists(MODULEBASE.$RTR->module_name.'config/'.strtolower($class).EXT))
                        {
                         	include_once(MODULEBASE.$RTR->module_name.'config/'.strtolower($class).EXT);
                        }
			else
			{
				if (file_exists(APPPATH.'config/'.ucfirst(strtolower($class)).EXT))
				{
					include_once(APPPATH.'config/'.ucfirst(strtolower($class)).EXT);
				}
                                elseif (file_exists(MODULEBASE.$RTR->module_name.'config/'.ucfirst(strtolower($class)).EXT))
                                {
                                        include_once(MODULEBASE.$RTR->module_name.'config/'.ucfirst(strtolower($class)).EXT);
                                }
			}
		}

		if ($prefix == '')
		{
			if (class_exists('CI_'.$class))
			{
				$name = 'CI_'.$class;
			}
			elseif (class_exists(config_item('subclass_prefix').$class))
			{
				$name = config_item('subclass_prefix').$class;
			}
			else
			{
				$name = $class;
			}
		}
		else
		{
			$name = $prefix.$class;
		}

		// Is the class name valid?
		if ( ! class_exists($name))
		{
			log_message('error', "Non-existent class: ".$name);
			show_error("Non-existent class: ".$class);
		}

		// Set the variable name we will assign the class to
		// Was a custom class name supplied?  If so we'll use it
		$class = strtolower($class);

		if (is_null($object_name))
		{
			$classvar = ( ! isset($this->_ci_varmap[$class])) ? $class : $this->_ci_varmap[$class];
		}
		else
		{
			$classvar = $object_name;
		}

		// Save the class name and object name
		$this->_ci_classes[$class] = $classvar;

		// Instantiate the class
		$CI =& get_instance();
		if ($config !== NULL)
		{
			$CI->$classvar = new $name($config);
		}
		else
		{
			$CI->$classvar = new $name;
		}
	}
    }
?>
