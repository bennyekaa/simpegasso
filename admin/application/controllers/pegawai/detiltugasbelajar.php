<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class detiltugasbelajar extends Member_Controller
{
	function detiltugasbelajar()
	{
		parent::Member_Controller();
		$this->load->model('detil_tugas_belajar_model', 'detil_tugas_belajar');
		$this->load->model("jabatan_model");
		$this->load->model("lookup_model");
		$this->load->model("golongan_model");
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data Detil Tugas Belajar');
		//$this->browse($id_riwayat_tb);
	}
	
		
	function browse($id_riwayat_tb) {
		//$id_riwayat_tb=$this->uri->segment(4);
		/*$data['option_jabatan'] = $this->jabatan_model->get_assoc();
		if(!$id_jabatan)
			redirect('setup/jabatan_detil');
		
		$paging_uri=6;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$limit_per_page = 15;
		*/
					
		//$ordby = $this->uri->segment(5);
		//$data['list_detil_tb'] = $this->detil_tugas_belajar->findByFilter(array("id_riwayat_tb"=>$id_riwayat_tb),$limit_per_page,$start,$ordby);
		$search_param = array();
		$search_param[] = "(id_riwayat_tb = '$id_riwayat_tb')";
		//$search_param[] = "(kd_pegawai = '$id_riwayat_tb'";
		$search_param = implode(" AND ", $search_param);
		//$kerja_list = $this->lap_pegawai->findByFilter($search_param,$ordby,$limit_per_page,$start);
		
		$data['list_detil_tb'] = $this->detil_tugas_belajar->findByFilter(NULL, $search_param, NULL, NULL,NULL,NULL);
		$data['id_riwayat_tb'] = $id_riwayat_tb;
		$data['id_detil_riwayat_tb'] = $id_detil_riwayat_tb;
		//$data['golongan_pangkat_assoc'] = $this->golongan_model->get_assoc();
		//$data['id_jenis_jabatan_assoc'] = $this->jabatan_detil->get_assoc_id_jenis_jabatan();
		
		$data['judul'] 		= "Data Detil Tugas Belajar";
			
		$this->template->display('pegawai/detiltugasbelajar/list_detil_tugas_belajar', $data);
	}
	
/*	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->jabatan_detil->add($data);
			set_success('Data Detil Jabatan berhasil disimpan.');
			redirect('/setup/jabatan_detil');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Detil Jabatan :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Jabatan';
			$data['id_jabatan_detil']=$this->jabatan_detil->get_id();
			$this->template->display('/setup/jabatan_detil/detail_jabatan_detil', $data);
		}
	}
	*/
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$id_riwayat_tb = $this->input->post('id_riwayat_tb');
			$this->detil_tugas_belajar->add($data);
			set_success('Data Detil Tugas Belajar berhasil disimpan.');
			redirect('/pegawai/detiltugasbelajar/browse/'.$id_riwayat_tb);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Detil Tugas Belajar :: Tambah');
			$data = $this->_clear_form();
			$data['id_riwayat_tb'] = $id_riwayat_tb;
			//$data['nama_jabatan'] = $this->jabatan_model->get_assoc2();
			$data['action']='add/'.$id_riwayat_tb;
			$data['action_list'] = 'browse/'.$id_riwayat_tb;
			//$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
			$data['judul']='Tambah Detil Tugas Belajar';
			$data['id_detil_riwayat_tb']=$this->detil_tugas_belajar->get_id();
			$this->template->display('/pegawai/detiltugasbelajar/detail_tugas_belajar', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_detil_riwayat_tb']=$id;
			$this->detil_tugas_belajar->update($id, $data);
			set_success('Perubahan Data Detil Tugas Belajar berhasil disimpan');
			redirect('/pegawai/detiltugasbelajar/browse/'.$data['id_riwayat_tb']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Detil Tugas Belajar :: Ubah');
			$data = $this->detil_tugas_belajar->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action_list'] = 'browse/'.$data['id_riwayat_tb'];
				$data['id_riwayat_tb'] = $data['id_riwayat_tb'];
				//$data['nama_jabatan'] = $this->jabatan_model->get_assoc2();
				$data['action'] = 'edit/'.$id;
				//$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
				$data['judul']='Edit Detil Tugas Belajar';
				$this->template->display('/pegawai/detiltugasbelajar/detail_tugas_belajar', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/detiltugasbelajar/browse/'.$data['id_riwayat_tb']);
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->detil_tugas_belajar->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Detil Tugas Belajar :: Hapus');
		confirm("Yakin menghapus data ?");
		$res = $this->detil_tugas_belajar->delete($idField);
		set_success('Data Detil Tugas Belajar berhasil dihapus');
		redirect('/pegawai/detiltugasbelajar/browse/'.$data['id_riwayat_tb']);
	}

	function _clear_form()
	{
		$data['id_detil_riwayat_tb']	= '';
		$data['id_riwayat_tb']	= '';
		$data['no_SK_penunjang']	= '';
		$data['tgl_SK_penunjang']	= '';
		$data['keterangan']		= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_detil_riwayat_tb']	= $this->detil_tugas_belajar->get_id();
		$data['id_riwayat_tb']			= $this->input->post('id_riwayat_tb', TRUE);
	   	$data['no_SK_penunjang']		= $this->input->post('no_SK_penunjang', TRUE);
		$data['tgl_SK_penunjang']		= $this->input->post('tgl_SK_penunjang', TRUE);
		$data['keterangan']				= $this->input->post('keterangan', TRUE);
		
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('no_SK_penunjang', 'no_SK_penunjang', 'required');
		//$this->form_validation->set_rules('id_jabatan', 'id_jabatan', 'required');
		return $this->form_validation->run();
	}
}
