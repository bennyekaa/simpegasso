<?php /**
* 
*/
class Jabatan_remunerasi extends Member_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("jabatan_remunerasi_model", "jabatan_remunerasi");
		$this->load->model("pegawai_model", "pegawai");
		$this->load->model("jenis_jabatan_uj2016_model", "jenis_jabatan_uj2016");
		$this->load->model("golongan_model", "golongan");
		$this->load->model("unit_kerja_model", "unit_kerja");
		$this->load->model("riwayat_jabatanstruktural_model", "riwayat_jabatanstruktural");
	}

	function index($kd_pegawai){
		$data_jabatan_remunerasi=$this->jabatan_remunerasi->get_jabatan_remunerasi($kd_pegawai);

		$data=array('judul'=>"Data Jabatan Remunerasi dari",'data_jabatan_remunerasi'=>$data_jabatan_remunerasi);
		$data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
        $data['judul'] 		= "Data Remunerasi dari: " . $data['pegawai']['nama_pegawai'];
		$this->template->display("pegawai/jabatan_remunerasi/list_jabatan_remunerasi", $data);
	}

	function add(){
		if ($this->_validate())
		{
            $data=$this->_get_form_values();
            if ($this->jabatan_remunerasi->add($data)) {
            	// update id_jabatan_uj di tabel pegawai
            	$this->pegawai->update($data['kd_pegawai'], array("id_jabatan_uj"=>$data['id_jabatan']));
            	set_success("Data riwayat jabatan berhasil disimpan");
            } else {
            	set_error("Gagal menyimpan disimpan");
            }

            redirect("/pegawai/jabatan_remunerasi/index/".$data['kd_pegawai']);
            
		}else{
			$kd_pegawai = $this->input->post('kd_pegawai');
            $NIP = $this->input->post('NIP');
            $data = $this->_get_form_values();
			
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Jabatan Struktural Pegawai :: Tambah');
			// $data = $this->_clear_form();
			// $data = $this->jabatan_remunerasi->getDetailJabatanRemunerasi($kd_pegawai);
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Jabatan Struktural dari: ' . $data['pegawai']['nama_pegawai'];
			
			$data['id_jabatan_uj']=$this->getJabatan();
			$data['id_golongan']=$this->golongan->get_gol_assoc();
			$data['unit_kerja']=$this->unit_kerja->get_assoc2();
			
			//$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
            // $data['status_assoc'] = $this->lookup->status_assoc();
			// $data['option_unit']=$this->unit_kerja->get_unit('1');
			// echo "<pre>";var_dump($data["pegawai"]);exit();
			$this->template->display('/pegawai/jabatan_remunerasi/detail_jabatan_remunerasi', $data);
		}

	}

	function getJabatan()
	{
		$data=$this->jenis_jabatan_uj2016->getJabatan();

		$result=array();
		foreach ($data as $jabatan) {
			$result[$jabatan["id_jabatan_uj"]]=$jabatan["nama_jabatan_uj"];
		}

		return $result;
	}

	function save(){}

	function delete(){
		$id_riwayat_jabatan = $this->uri->segment(4);
		$data = $this->jabatan_remunerasi->getDetailJabatanRemunerasi(null,$id_riwayat_jabatan);

		$this->template->metas('title', 'SIMPEGA | Riwayat Jabatan Remunerasi :: Hapus');
		confirm("Yakin menghapus data riwayat jabatan remunerasi pegawai?");

		if($this->jabatan_remunerasi->delete($id_riwayat_jabatan)){
			set_success('Data riwayat jabatan remunerasi berhasil dihapus');
		}else{
			set_error('Gagal menghapus data riwayat jabatan remunerasi');
		}
		
		 redirect("/pegawai/jabatan_remunerasi/index/".$data['kd_pegawai']);
	}

	function edit($id){
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			if($this->jabatan_remunerasi->update($id, $data)){
				// update id_jabatan_uj di tabel pegawai
				$this->pegawai->update($data['kd_pegawai'], array("id_jabatan_uj"=>$data['id_jabatan']));
				set_success('Perubahan data riwayat jabatan remunerasi berhasil disimpan');
			}else{
				set_error("Gagal mengubah data riwayat jabatan remunerasi");
			}

			redirect('/pegawai/jabatan_remunerasi/index/'. $data['kd_pegawai']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Riwayat Jabatan Remunerasi :: Ubah');
			$data = $this->jabatan_remunerasi->getDetailJabatanRemunerasi(null,$id);
			// var_dump($data);exit();
			//$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
			
			//$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($data[0]["kd_pegawai"]);
			$data['judul']='Tambah Data Jabatan Struktural dari: ' . $data['pegawai']['nama_pegawai'];
			
			$data['id_jabatan_uj']=$this->getJabatan();
			$data['id_golongan']=$this->golongan->get_gol_assoc();
			$data['unit_kerja']=$this->unit_kerja->get_assoc2();
			
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				$data['judul']='Edit Data Mutasi dari: '. $data['pegawai']['nama_pegawai'];
				$data['option_unit']=$this->unit_kerja->get_unit('1');
				$this->template->display('/pegawai/jabatan_remunerasi/detail_jabatan_remunerasi', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatmutasi', 'location');
			}
		}
	}

	function _clear_form()
	{
		$data['id_mutasi']		= '';
		$data['jabatan_asal']	= '';
		$data['jns_mutasi']= '';
		$data['no_SK']			= '';
		$data['tgl_SK']			= '';
		$data['tmt_SK']			= '';
		$data['tgl_mutasi']		= '';
		$data['jabatan_asal']	= '';
		$data['jabatan_baru']	= '';
		$data['unit_kerja_asal']= '';
		$data['unit_kerja_baru']= '';
		$data['ket_mutasi']		= '';
		return $data;
	}

	function _validate()
	{
		$this->form_validation->set_rules('id_jabatan_uj', 'id_jabatan_uj', 'required');
		$this->form_validation->set_rules('id_golpangkat', 'id_golpangkat', 'required');
		$this->form_validation->set_rules('tmt_golongan', 'tmt_golongan', 'required');
		$this->form_validation->set_rules('job_class', 'job_class', 'required|decimal');
		$this->form_validation->set_rules('job_value', 'job_value', 'required|decimal');
		$this->form_validation->set_rules('sk_jabatan', 'sk_jabatan', 'required');
		$this->form_validation->set_rules('tgl_sk_jabatan', 'tgl_sk_jabatan', 'required');
		$this->form_validation->set_rules('tmt_jabatan', 'tmt_jabatan', 'required');
		$this->form_validation->set_rules('unit_kerja', 'unit_kerja', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		return $this->form_validation->run();
	}

	function _get_form_values()
	{
	   	$data['kd_pegawai']		= $this->input->post('kd_pegawai', TRUE);
	   	$data['id_jabatan']	= $this->input->post('id_jabatan_uj', TRUE);
		$data['job_class']		= $this->input->post('job_class', TRUE);
		$data['job_value']		= $this->input->post('job_value', TRUE);
		$data['id_golpangkat']	= $this->input->post('id_golpangkat', TRUE);
		$data['tmt_pangkat']	= $this->input->post('tmt_golongan', TRUE);
		$data['no_sk_jabatan']		= $this->input->post('sk_jabatan', TRUE);
		$data['tgl_sk_jabatan']	= $this->input->post('tgl_sk_jabatan', TRUE);
		$data['tmt_jabatan']	= $this->input->post('tmt_jabatan', TRUE);
		$data['kode_unit']		= $this->input->post('unit_kerja', TRUE);
		$data['keterangan']		= $this->input->post('keterangan', TRUE);
		$data['id_jabatan']		= $this->input->post('id_jabatan_uj', TRUE);
		return $data;
	}


	public function getJVRendah($id_jabatan_uj)
	{
		$result=$this->jenis_jabatan_uj2016->getJVRendah($id_jabatan_uj);

		echo $result["jv_rendah"];
	}

	public function getJVTinggi($id_jabatan_uj)
	{
		$result=$this->jenis_jabatan_uj2016->getJVTinggi($id_jabatan_uj);

		echo $result["jv_tinggi"];
	}

}