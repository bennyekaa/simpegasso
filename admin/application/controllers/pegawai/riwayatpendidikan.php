<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatpendidikan extends Member_Controller
{
	function Riwayatpendidikan()
	{
		parent::Member_Controller();
		$this->load->model('pendidikan_model', 'pendidikan');
		$this->load->model('riwayat_pendidikan_model','riwayat_pendidikan');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model','lookup');
		include "application/libraries/lib/nusoap.php";
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Pendidikan Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		
       
		$ordby='id_pendidikan, th_lulus';
		$data['list_pendidikan'] = $this->riwayat_pendidikan->find(NULL, array('kd_pegawai' => $kd_pegawai), $ordby, $start, null);
		
        $data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Pendidikan dari: " . $data['pegawai']['nama_pegawai'];
		$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
		
		if (isset($this->user->user_group)){
		$this->template->display('pegawai/riwayatpendidikan/list_riwayatpendidikan', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}

	}
	
	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
			$NIP = $this->input->post('NIP');
            $data = $this->_get_form_values();
			//$data['kd_pegawai'] = $this->pegawai->get_namapegawai_by_idpegawai($this->pegawai->kd_pegawai);
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_pendidikan_terakhir'] = $data['id_pendidikan'];
				$data_pegawai['ket_pendidikan'] = $data['keterangan'];

//akses webservice	
				$param=array('id_app' =>'3','key'=>'123','kd_pegawai'=>$NIP,'id_pendidikan_terakhir'=>$data['id_pendidikan']);
				$wsdl="http://192.168.1.209/webserviceum/server/?wsdl";
				$client=new nusoap_client($wsdl,true);
				$cek=$client->call("checkConnection",array("id_app"=>"3"));
				if($cek[item][0]=='200'):
					$message=$client->call("updatePendidikanPegawai",$param);
//begin update
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				$data_status['aktif'] = '0';
				$this->riwayat_pendidikan->update_status($kd_pegawai,$data_status);
//end					
/*					print_r($message);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;*/  
				else:
					print_r($cek);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;  
				endif; 
//end					
			}
			$this->riwayat_pendidikan->add($data);
			set_success('Data pendidikan pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatpendidikan/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Pendidikan Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Pendidikan dari: ' . $data['pegawai']['nama_pegawai'];
			$data['id_riwayat_pendidikan']=$this->riwayat_pendidikan->get_id();
			$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
			$data['pendidikan_assoc'] = array(0=>"-- Pilih Pendidikan --")+$this->pendidikan->get_assoc("nama_pendidikan");
			$data['status_assoc'] = $this->lookup->status_assoc();
			$this->template->display('/pegawai/riwayatpendidikan/detail_riwayatpendidikan', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$kd_pegawai = $this->input->post('kd_pegawai');
			//$data['id_riwayat_pendidikan'] = $id;
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_pendidikan_terakhir'] = $data['id_pendidikan'];
				$data_pegawai['ket_pendidikan'] = $data['keterangan'];
				
//akses webservice	
				$param=array('id_app' =>'3','key'=>'123','kd_pegawai'=>$NIP,'id_pendidikan_terakhir'=>$data['id_pendidikan']);
				$wsdl="http://192.168.1.209/webserviceum/server/?wsdl";
				$client=new nusoap_client($wsdl,true);
				$cek=$client->call("checkConnection",array("id_app"=>"3"));
				if($cek[item][0]=='200'):
					$message=$client->call("updatePendidikanPegawai",$param);
//begin update
		$this->pegawai->modify($kd_pegawai, $data_pegawai);
		//$data_status['aktif'] = '0';
		//$this->riwayat_pendidikan->update_status($kd_pegawai,$data_status);
//end					
/*					print_r($message);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;*/  
				else:
					print_r($cek);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;  
				endif; 
//end						
			}
			$this->riwayat_pendidikan->update($id, $data);
			set_success('Perubahan data pendidikan pegawai berhasil disimpan');
			redirect('/pegawai/riwayatpendidikan/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Pendidikan Pegawai :: Ubah');
			$data = $this->riwayat_pendidikan->retrieve_by_pkey($id);
			$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
			$data['pendidikan_assoc'] = array(0=>"-- Pilih Pendidikan --")+$this->pendidikan->get_assoc("nama_pendidikan");
			$data['status_assoc'] = $this->lookup->status_assoc();
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				$data['judul']='Edit Pendidikan dari: '. $data['pegawai']['nama_pegawai'];
				$this->template->display('/pegawai/riwayatpendidikan/detail_riwayatpendidikan', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatpendidikan', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_pendidikan->retrieve_by_pkey($idField);
		$this->template->metas('title', 'SIMPEGA | Pendidikan Pegawai :: Hapus');
		confirm("Yakin menghapus data pendidikan pegawai?");
		$res = $this->riwayat_pendidikan->delete($idField);
		set_success('Data pendidikan pegawai berhasil dihapus');
		redirect('/pegawai/riwayatpendidikan/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_pendidikan']	= '';
		$data['kd_pegawai']	= '';
		$data['id_pendidikan']	= '';
		//$data['nama_pendidikan']	= '';
		$data['prodi']	= '';
		$data['aktif']	= '0';
		$data['bidang_ilmu']	= '';
		$data['universitas']	= '';
		$data['alamat']	= '';
		$data['kota']	= '';
		$data['negara']	= '';
		$data['nomor_ijazah']	= '';
		$data['tanggal_ijazah']	= '';
		$data['tanggal_mulai']	= '';
		$data['tanggal_selesai']	= '';
		$data['th_lulus']	= '';
		$data['keterangan']	= '-';
		$data['tempat_studi']	='';
		$data['th_mulai'] = '';
		$data['gelar'] = '';
		$data['sumber_dana'] = '';
		return $data;
	}	
	
	function _get_form_values()
	{
	  
		$data['id_riwayat_pendidikan']	= $this->riwayat_pendidikan->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['id_pendidikan']	= $this->input->post('id_pendidikan', TRUE);
		//$data['nama_pendidikan']	= $this->input->post('nama_pendidikan', TRUE);
		$data['prodi']	= $this->input->post('prodi', TRUE);
		$data['bidang_ilmu']	= $this->input->post('bidang_ilmu', TRUE);
		$data['universitas']	= $this->input->post('universitas', TRUE);
		$data['alamat']	= $this->input->post('alamat', TRUE);
		$data['kota']	= $this->input->post('kota', TRUE);
		$data['negara']	= $this->input->post('negara', TRUE);
		$data['nomor_ijazah']	= $this->input->post('nomor_ijazah', TRUE);
		$data['tanggal_ijazah']	= $this->input->post('tanggal_ijazah', TRUE);
		$data['tanggal_mulai']	= $this->input->post('tanggal_mulai', TRUE);
		$data['tanggal_selesai']	= $this->input->post('tanggal_selesai', TRUE);
		$data['th_lulus']	= $this->input->post('th_lulus', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['aktif']	= $this->input->post('aktif', TRUE);
		$data['tempat_studi']	= $this->input->post('tempat_studi', TRUE);
		$data['th_mulai'] = $this->input->post('th_mulai', TRUE);
		$data['gelar'] = $this->input->post('gelar', TRUE);
		$data['sumber_dana'] = $this->input->post('sumber_dana', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('id_pendidikan', 'id_pendidikan', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}