<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pasanganpegawai extends Member_Controller
{
	function Pasanganpegawai()
	{
		parent::Member_Controller();
		$this->load->model('status_keluarga_model', 'status_keluarga');
		$this->load->model('pasangan_pegawai_model', 'pasangan_pegawai');
		$this->load->model('pegawai_model', 'pegawai');
        $this->load->model('lookup_model','lookup');
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Suami/Istri Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
        $ordby = 'id_pasangan_pegawai';
		
		$data['list_pasangan']  = $this->pasangan_pegawai->retrieve_by_idpeg($kd_pegawai);
		$config['base_url']     = site_url('pegawai/pasanganpegawai/browse/');
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Suami/Istri dari: " . $data['pegawai']['nama_pegawai'];
		$this->template->display('pegawai/pasanganpegawai/list_pasanganpegawai', $data);
	}

	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
			$data = $this->_get_form_values();
			
			//$data['kd_pegawai'] = $this->pegawai->get_namapegawai_by_idpegawai($this->pegawai->kd_pegawai);
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_pasangan_pegawai'] = $data['id_pasangan_pegawai'];			
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				$data_status['aktif'] = '0';
				$this->pasangan_pegawai->update_status($kd_pegawai,$data_status);
			}
			
			//menampilkan hanya status istri/suami berdasarkan jenis kelamin pegawai
			$query = mysql_query("select jns_kelamin from pegawai where kd_pegawai='".$kd_pegawai."'");			
			if ($query) {
				$data_kel=mysql_fetch_array($query); 
				$jk = $data_kel['jns_kelamin'];
				if ($jk==1) {	
						$query_status = mysql_query("select kd_status_keluarga from status_keluarga  
						where (kd_status_keluarga like (select id_var from pos_variabel where keterangan ='id_istri'))");					
						if ($query_status) {
							$data_status=mysql_fetch_array($query_status);  
							$data['kd_status_keluarga'] = $data_status['kd_status_keluarga'];}	
					}
				else {
						$query_status = mysql_query("select kd_status_keluarga from status_keluarga  
						where (kd_status_keluarga like (select id_var from pos_variabel where keterangan ='id_suami'))");					
						if ($query_status) {
							$data_status=mysql_fetch_array($query_status);  
							$data['kd_status_keluarga'] = $data_status['kd_status_keluarga'];}	
					}
			}
			
			$this->pasangan_pegawai->add($data);
			//lakukan pengecekan, jika ada id_pasangan_pegawai maka terjadi update pada pasangan dan insert pasangan pula jika ditemukan
			//rumus bisa dibalik dari rumusan diatas 
			
			if ($data['NIP_pasangan'] != '') {
				
				$query = mysql_query("select * from pasangan_pegawai where kd_pegawai='".$this->input->post('kd_pegawai_pasangan', TRUE)."' and NIP_Pasangan=(select NIP from pegawai where kd_pegawai='".$kd_pegawai."')");	
				$datapeg=mysql_fetch_array($query); 
				if ($datapeg==FALSE){
						$id_pasangan_pegawai_balik=$this->pasangan_pegawai->get_id();
						if ($data['aktif']=='1')
							{
								$data_pegawai['id_pasangan_pegawai'] = $id_pasangan_pegawai_balik;			
								$this->pegawai->modify($this->input->post('kd_pegawai_pasangan', TRUE), $data_pegawai);
								$data_status['aktif'] = '0';
								$this->pasangan_pegawai->update_status($this->input->post('kd_pegawai_pasangan', TRUE),$data_status);
							}
						//$this->pasangan_pegawai->add($data_pasangan);
						
								$rs = mysql_query("SELECT *,(select jenis_pegawai from jenis_pegawai where id_jns_pegawai = (pegawai.id_jns_pegawai)) as jenis_pegawai, 
								(select nama_unit from unit_kerja where kode_unit=(pegawai.kode_unit)) as unit_kerja  FROM pegawai WHERE kd_pegawai = '".$kd_pegawai."'");
								if($r = mysql_fetch_array($rs)){
									$nip_pasangan = $r['NIP'];
									$nama_pasangan = $r['nama_pegawai'];
									$tempat_lahir = $r['tempat_lahir'];
									$kerja = $r['jenis_pegawai'];
									$unit = $r['unit_kerja'];
									$tanggal_lahir = $r['tgl_lahir'];
									$pekerjaan = $kerja . ', ' . $unit;
									$jk = $r['jns_kelamin'];
									//dibalik
									if ($jk==1) {	
											$query_status = mysql_query("select kd_status_keluarga from status_keluarga  
											where (kd_status_keluarga like (select id_var from pos_variabel where keterangan ='id_istri'))");					
											if ($query_status) {
												$data_status=mysql_fetch_array($query_status);  
												$kd_status_keluarga = $data_status['kd_status_keluarga'];}	
										}
									else {
											$query_status = mysql_query("select kd_status_keluarga from status_keluarga  
											where (kd_status_keluarga like (select id_var from pos_variabel where keterangan ='id_suami'))");					
											if ($query_status) {
												$data_status=mysql_fetch_array($query_status);  
												$kd_status_keluarga = $data_status['kd_status_keluarga'];}	
										}
									
								}
								$query_add_balik = mysql_query(
									"INSERT INTO pasangan_pegawai
									(id_pasangan_pegawai, kd_pegawai, 
									NIP_pasangan, no_urut, nama_pasangan, pekerjaan,
									tempat_lahir, tanggal_lahir, tanggal_nikah, 
									no_karis_karsu, tgl_karis_karsu, keterangan, aktif, kd_status_keluarga) 
									VALUES ('".$this->pasangan_pegawai->get_id()."', '".$this->input->post('kd_pegawai_pasangan', TRUE)."', 
									'".$nip_pasangan."', '1', '".$nama_pasangan."', '".$kerja."', 
									'".$tempat_lahir."', '".$tanggal_lahir."', '".$this->input->post('tanggal_nikah', TRUE)."', 
									'', '', '', '".$this->input->post('aktif', TRUE)."', '".$kd_status_keluarga."')");	
									
					}	
			}
			
			
			
			/*  
				$nama_pegawai = $this->input->post('nama_pegawai'.$indek, TRUE);
				$query = mysql_query("select * from absensi where kd_pegawai='".$kd_peg."' and kode_unit='".$kd_unit."' and periode='".$periode."' ");				
				//echo "select * from absensi where kd_pegawai='".$kd_peg."' and kode_unit='".$kd_unit."' and periode='".$periode."' ";
				$datapeg=mysql_fetch_array($query); 
				if ($datapeg!=FALSE){
						$query = mysql_query("UPDATE absensi 
						SET Ijin = '".$Ijin."', `TK`= '".$TK."', `TB`='".$TB."', `TD`='".$TD."', `Sakit`='".$Sakit."',
						`SD`='".$SD."', `CT`='".$CT."', `MPP`='".$MPP."', `total_absen`='".$total_absen."', `user_id` ='".$this->user->user_id."'
						WHERE kd_pegawai='".$kd_peg."' and kode_unit='".$kd_unit."' and periode='".$periode."' ");			
						//echo "ubah".$periode;						
						}
				else {
					if ($total_absen!=0){
						$id_absensi_awal = $id_absensi+$counter;
						$query = mysql_query("INSERT INTO `absensi` (`id_absensi`, `periode`, `kode_unit`, `kd_pegawai`, `Ijin`, 
						`TK`, `TB`, `TD`, `Sakit`, `SD`, `CT`, `MPP`, `total_absen`, `user_id`) VALUES
						('".$id_absensi_awal."', '".$periode."', '".$kd_unit."', '".$kd_peg."', '".$Ijin."', '".$TK."', '".$TB."', '".$TD."', '".$Sakit."', '".$SD."', 
						'".$CT."', '".$MPP."', '".$total_absen."', '".$this->user->user_id."')");
						$counter++;
						//echo "tambah".$periode;
					}
				}
			//end pengecekan pasangan */
			set_success('Data pasangan pegawai berhasil disimpan.');
			redirect('/pegawai/pasanganpegawai/index/' . $data['kd_pegawai']);
		}
		else
		{
            $kd_pegawai = $this->uri->segment(4);
			$this->template->metas('title', 'SIMPEGA | Suami/Istri Pegawai :: Tambah');
			$data = $this->_clear_form();
            $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['action']='add';
			$data['judul']='Tambah Suami/Istri dari: '. $data['pegawai']['nama_pegawai'];
            
			$data['id_pasangan_pegawai']=$this->pasangan_pegawai->get_id();
			//menampilkan hanya status istri/suami berdasarkan jenis kelamin pegawai
			$query = mysql_query("select jns_kelamin from pegawai where kd_pegawai='".$kd_pegawai."'");			
			if ($query) {
				$data_kel=mysql_fetch_array($query); 
				$jk = $data_kel['jns_kelamin'];
				if ($jk==1) {	
					$data['status_keluarga_assoc'] = $this->status_keluarga->get_pasangan_assoc("status_keluarga",'id_istri');
					}
				else {
					$data['status_keluarga_assoc'] = $this->status_keluarga->get_pasangan_assoc("status_keluarga",'id_suami');
					}
			}
			else
			{
				$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc("status_keluarga");
			}
			
			$data['status_assoc'] = $this->lookup->status_assoc();
            $this->template->display('/pegawai/pasanganpegawai/detail_pasanganpegawai', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_pasangan_pegawai'] = $id;
			$kd_pegawai = $this->input->post('kd_pegawai');
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_pasangan_pegawai'] = $data['id_pasangan_pegawai'];			
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				
				$data_status['aktif'] = '0';
				$this->pasangan_pegawai->update_status($kd_pegawai,$data_status);
			}
			$this->pasangan_pegawai->update($id, $data);
			set_success('Perubahan data pasangan pegawai berhasil disimpan');
			redirect('/pegawai/pasanganpegawai/index/' . $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Suami/Istri Pegawai :: Ubah');
			$data = $this->pasangan_pegawai->retrieve_by_pkey($id);
			//$data['id_pasangan_pegawai']=$this->pasangan_pegawai->get_id();
			//menampilkan hanya status istri/sumi berdasarkan jenis kelamin pegawai
			$query = mysql_query("select jns_kelamin from pegawai where kd_pegawai='".$id."'");			
			if ($query) {
				$data_kel=mysql_fetch_array($query); 
				$jk = $data_kel['jns_kelamin'];
				if ($jk==1) {	
					$data['status_keluarga_assoc'] = $this->status_keluarga->get_pasangan_assoc("status_keluarga",'id_istri');
					}
				else {
					$data['status_keluarga_assoc'] = $this->status_keluarga->get_pasangan_assoc("status_keluarga",'id_suami');
					}
			}
			else
			{
				$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc("status_keluarga");
			}
			
			$data['status_assoc'] = $this->lookup->status_assoc();
            if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				$data['judul']='Edit Suami/Istri dari: '. $data['pegawai']['nama_pegawai'];
				$this->template->display('/pegawai/pasanganpegawai/detail_pasanganpegawai', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/pasanganpegawai', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->pasangan_pegawai->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Suami/Istri Pegawai :: Hapus');
		confirm("Yakin menghapus data pasangan pegawai <b>".$data['nama_keluarga']."</b> ?");
		$res = $this->pasangan_pegawai->delete($idField);
		set_success('Data pasangan pegawai berhasil dihapus');
		redirect('/pegawai/pasanganpegawai/index/'. $data['kd_pegawai'], 'location');
	}


	function _clear_form_search()
	{
		$data['is_new'] = '';
		$data['search']	= '';
		
		return $data;
	}	
	
	function _get_form_values_search()
	{
	   	$data['search']		= $this->input->post('search', TRUE);
	   
		return $data;
	}
	
	function _validate_search()
	{
		$this->form_validation->set_rules('search', 'search', 'required');
		
		return $this->form_validation->run();
	}

	function _clear_form()
	{
		$data['id_pasangan_pegawai']	= '';
		$data['kd_pegawai']	= '';
		$data['NIP_pasangan'] = '';
		$data['no_urut']	= '';
		$data['nama_pasangan']	= '';
		$data['pekerjaan']	= '';
		$data['tempat_lahir']	= '';
		$data['tanggal_lahir']	= '';
		$data['tanggal_nikah']	= '';
		$data['no_karis_karsu']	= '';
		$data['tgl_karis_karsu']	= '';
		$data['keterangan']	= '';
		$data['aktif']	= '';
		$data['kd_status_keluarga']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
		$data['id_pasangan_pegawai']	= $this->pasangan_pegawai->get_id(); 
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['NIP_pasangan']	= $this->input->post('NIP_pasangan', TRUE);
		$data['no_urut']	= $this->input->post('no_urut', TRUE);
		$data['nama_pasangan']	= $this->input->post('nama_pasangan', TRUE);
		$data['pekerjaan']	= $this->input->post('pekerjaan', TRUE);
		$data['tempat_lahir']	= $this->input->post('tempat_lahir', TRUE);
		$data['tanggal_lahir']	= $this->input->post('tanggal_lahir', TRUE);
		$data['tanggal_nikah']	= $this->input->post('tanggal_nikah', TRUE);
		$data['no_karis_karsu']	= $this->input->post('no_karis_karsu', TRUE);
		$data['tgl_karis_karsu']	= $this->input->post('tgl_karis_karsu', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['aktif']	= $this->input->post('aktif', TRUE);
		$data['kd_status_keluarga']	= $this->input->post('kd_status_keluarga', TRUE);
		return $data;
	}

	function _validate()
	{
		$this->form_validation->set_rules('nama_pasangan', 'nama_pasangan', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}