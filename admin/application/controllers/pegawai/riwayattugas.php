<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayattugas extends Member_Controller
{
	function Riwayattugas()
	{
		parent::MY_Controller();
		$this->load->model('golongan_model', 'golongan');
		$this->load->model('pendidikan_model', 'pendidikan');
		$this->load->model('riwayat_tugas_model','riwayat_tugas');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model','lookup');
		$this->load->model('lap_gaji_model','lap_gaji');
		$this->load->model('unit_kerja_model','unit_kerja');
		
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Data Uraian Tugas');
		$this->browse(id_riwayat_tugas,$kd_pegawai);
	}
	
	function browse($id_riwayat_tugas,$kd_pegawai)
	{
		$ordby = 'id_riwayat_tugas';
		$data['list_tugas'] = $this->riwayat_tugas->find(NULL, array('kd_pegawai' => $kd_pegawai), null, $limit_per_page,$start,$ordby);
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Tugas dari: " . $data['pegawai']['nama_pegawai'];
		
		$data['unit_kerja_assoc']=$this->unit_kerja->get_assoc('nama_unit');
		$data['option_unit']=$this->unit_kerja->get_unit('1');
		$this->template->display('pegawai/riwayattugas/list_riwayat_tugas', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			$this->riwayat_tugas->add($data);
            
			set_success('Data tugas pegawai berhasil disimpan.');
			redirect('/pegawai/riwayattugas/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Data Uraian Tugas :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Tugas dari: ' . $data['pegawai']['nama_pegawai'];
			$data['id_riwayat_tugas']=$this->riwayat_tugas->get_id();
			$data['unit_kerja_assoc']=$this->unit_kerja->get_assoc('nama_unit');
			$data['option_unit']=$this->unit_kerja->get_unit('1');
			$data['status_assoc'] = $this->lookup->status_assoc();
			$this->template->display('/pegawai/riwayattugas/detail_riwayat_tugas', $data);
		}
	}
	
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  		'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_tugas'] = $id;
			$this->riwayat_tugas->update($id, $data);
			set_success('Perubahan data tugas pegawai berhasil disimpan');
			redirect('/pegawai/riwayattugas/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Tugas Dosen :: Ubah');
			$data = $this->riwayat_tugas->retrieve_by_pkey($id);
			$data['unit_kerja_assoc']=$this->unit_kerja->get_assoc('nama_unit');
			$data['option_unit']=$this->unit_kerja->get_unit('1');
			$data['status_assoc'] = $this->lookup->status_assoc();
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				
				$data['judul']='Edit Tugas dari: '. $data['pegawai']['nama_pegawai'];
				$this->template->display('/pegawai/riwayattugas/detail_riwayat_tugas', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayattugas', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_tugas->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Tugas :: Hapus');
		confirm("Yakin menghapus data tugas?");
		$res = $this->riwayat_tugas->delete($idField);
		set_success('Data Tugas pegawai berhasil dihapus');
		redirect('/pegawai/riwayattugas/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_tugas']	= '';
		$data['kd_pegawai']	= '';
		$data['kode_unit']	= '';
		$data['nama_tugas']	='';
		$data['keterangan']	= '';
		$data['tgl_mulai_tugas']	= '';
		$data['tgl_selesai_tugas']	= '';
		$data['aktif']	= '';
	
		return $data;
	}	
	
	function _get_form_values()
	{
		
		$data['id_riwayat_tugas']	= $this->riwayat_tugas->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['kode_unit']	= $this->input->post('kode_unit', TRUE);
		$data['nama_tugas']	= $this->input->post('nama_tugas', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['tgl_mulai_tugas']	= $this->input->post('tgl_mulai_tugas', TRUE);
		$data['tgl_selesai_tugas']	= $this->input->post('tgl_selesai_tugas', TRUE);
		$data['aktif']	= $this->input->post('aktif', TRUE);
	
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_tugas', 'nama_tugas', 'required');
		return $this->form_validation->run();
	}
}
