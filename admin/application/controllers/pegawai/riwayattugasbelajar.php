<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayattugasbelajar extends Member_Controller
{
	function Riwayattugasbelajar()
	{
		parent::Member_Controller();
		$this->load->model('golongan_model', 'golongan');
		$this->load->model('pendidikan_model', 'pendidikan');
		$this->load->model('riwayat_tugas_belajar_model','riwayat_tugas_belajar');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model','lookup');
		$this->load->model('lap_gaji_model','lap_gaji');
		
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Tugas Belajar Pegawai');
		$this->browse($kd_pegawai);
	}
	
	
	function browse($kd_pegawai)
	{
		$ordby = 'tmt_tb';
		//v3 tambahan tb_ib
		$data['tb_ib'] = 'tb';
		$data['list_tb'] = $this->riwayat_tugas_belajar->find(NULL, array('kd_pegawai' => $kd_pegawai,'tb_ib'=>'tb'), $ordby, $start,$limit_per_page);
		//$data['list_tb'] = $this->riwayat_tugas_belajar->find(NULL, array('kd_pegawai' => $kd_pegawai), $ordby, $start,$limit_per_page);
		//end tambahan
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Tugas Belajar dari: " . $data['pegawai']['nama_pegawai'];
		$data['golongan_assoc'] = $this->golongan->get_assoc();
		$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
		$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
		
		/*foreach ($list_tb) {
		list($y, $m, $d) = explode('-', $list_tb['tmt_tb']);
        $str_tmt_tb = $d . "-" . $m . "-" . $y;
        $list_tb['tmt_tb']=$str_tmt_tb;
		
		}*/
		
		if (isset($this->user->user_group)){
		$this->template->display('pegawai/riwayattugasbelajar/list_riwayat_tb', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}

	}
	
	function add()
	{
		
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			//v3 tambahan tb_ib
			$data['tb_ib'] = 'tb';
			//end tambahan
			$this->riwayat_tugas_belajar->add($data);
            
			set_success('Data Tugas belajar pegawai berhasil disimpan.');
			redirect('/pegawai/riwayattugasbelajar/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Tugas Belajar :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			//v3 tambahan tb_ib
			$data['tb_ib'] = 'tb';
			//end tambahan
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Tugas Belajar dari: ' . $data['pegawai']['nama_pegawai'];
			$data['id_riwayat_tb']=$this->riwayat_tugas_belajar->get_id();
			$data['status_assoc'] = $this->lookup->status_assoc();
			$data['perpanjangan_assoc'] = $this->lookup->perpanjangan_assoc();
			$data['tunjangan_assoc'] = $this->lookup->tunjangan_assoc();
			$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			$this->template->display('/pegawai/riwayattugasbelajar/detail_riwayat_tb', $data);
		}
	}
	
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  		'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_tb'] = $id;
			//v3 tambahan tb_ib
			$data['tb_ib'] = 'tb';
			//end tambahan
			$this->riwayat_tugas_belajar->update($id, $data);
			set_success('Perubahan data Tugas belajar pegawai berhasil disimpan');
			redirect('/pegawai/riwayattugasbelajar/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Tugas Belajar :: Ubah');
			$data = $this->riwayat_tugas_belajar->retrieve_by_pkey($id);
			//v3 tambahan tb_ib
			$data['tb_ib'] = 'tb';
			//end tambahan
			$data['golongan_assoc'] = $this->golongan->get_assoc();
            $data['status_assoc'] = $this->lookup->status_assoc();
			$data['perpanjangan_assoc'] = $this->lookup->perpanjangan_assoc();
			$data['tunjangan_assoc'] = $this->lookup->tunjangan_assoc();
			$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
			$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				
				$data['judul']='Edit Tugas Belajar dari: '. $data['pegawai']['nama_pegawai'];
				$this->template->display('/pegawai/riwayattugasbelajar/detail_riwayat_tb', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayattugasbelajar', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_tugas_belajar->retrieve_by_pkey($idField);
		//v3 tambahan tb_ib
			$data['tb_ib'] = 'tb';
		//end tambahan
		$this->template->metas('title', 'SIMPEGA | Tugas Belajar :: Hapus');
		confirm("Yakin menghapus data tugas belajar dosen?");
		$res = $this->riwayat_tugas_belajar->delete($idField);
		set_success('Data Tugas Belajar pegawai berhasil dihapus');
		redirect('/pegawai/riwayattugasbelajar/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_tb']	= '';
		$data['kd_pegawai']	= '';
		$data['tmt_tb']	= '';
		$data['batas_akhir_tb']	= '';
		$data['id_pendidikan_tb']	= '';
		$data['bidang_studi_tb']	= '';
		$data['tempat_studi_tb']	= '';
		$data['biaya_tb']	= '';
		$data['aktif']	= '';
		$data['tmt_pk']	= '';
		$data['keterangan']	= '';
		
		$data['no_dasar_penerimaan']	= '';
		$data['tgl_dasar_penerimaan']	= '';
		
		$data['no_SK_tb']	= '';
		$data['tgl_SK_tb']	= '';
		
		$data['no_surat_bebas']	= '';
		$data['tgl_surat_bebas']	= '';
		
		$data['no_tunjangan_tb']	= '';
		$data['tgl_tunjangan_tb']	= '';
		$data['tunjangan_tb']	= '';
		$data['status_tunjangan']	= '';
	
		$data['no_perpanjangan_tb']	= '';
		$data['tgl_perpanjangan_tb']	= '';
		$data['status_perpanjangan']	= '';
		$data['batas_akhir_tb_perpanjangan']	= '';
		$data['tb_ib']	= 'tb';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
	   	$data['id_riwayat_tb']	= $this->riwayat_tugas_belajar->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['tmt_tb']	= $this->input->post('tmt_tb', TRUE);
		$data['batas_akhir_tb']	= $this->input->post('batas_akhir_tb', TRUE);
		$data['id_pendidikan_tb']	= $this->input->post('id_pendidikan_tb', TRUE);
		$data['bidang_studi_tb']	= $this->input->post('bidang_studi_tb', TRUE);
		$data['tempat_studi_tb']	= $this->input->post('tempat_studi_tb', TRUE);
		$data['biaya_tb']	= $this->input->post('biaya_tb', TRUE);
		$data['aktif']	= $this->input->post('aktif', TRUE);
		$data['tmt_pk']	= $this->input->post('tmt_pk', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		
		$data['no_dasar_penerimaan']	= $this->input->post('no_dasar_penerimaan', TRUE);
		$data['tgl_dasar_penerimaan']	= $this->input->post('tgl_dasar_penerimaan', TRUE);
		
		$data['no_SK_tb']	= $this->input->post('no_SK_tb', TRUE);
		$data['tgl_SK_tb']	= $this->input->post('tgl_SK_tb', TRUE);
		
		$data['no_surat_bebas']	= $this->input->post('no_surat_bebas', TRUE);
		$data['tgl_surat_bebas']	= $this->input->post('tgl_surat_bebas', TRUE);
		
		$data['no_tunjangan_tb']	= $this->input->post('no_tunjangan_tb', TRUE);
		$data['tgl_tunjangan_tb']	= $this->input->post('tgl_tunjangan_tb', TRUE);
		$data['tunjangan_tb']	= $this->input->post('tunjangan_tb', TRUE);
		$data['status_tunjangan']	= $this->input->post('status_tunjangan', TRUE);
	
		$data['no_perpanjangan_tb']	= $this->input->post('no_perpanjangan_tb', TRUE);
		$data['tgl_perpanjangan_tb']	= $this->input->post('tgl_perpanjangan_tb', TRUE);
		$data['status_perpanjangan']	= $this->input->post('status_perpanjangan', TRUE);
		$data['batas_akhir_tb_perpanjangan']	= $this->input->post('batas_akhir_tb_perpanjangan', TRUE);
		$data['tb_ib']	= $this->input->post('tb_ib', TRUE);
		
		
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('biaya_tb', 'biaya_tb', 'required');
		return $this->form_validation->run();
	}
}