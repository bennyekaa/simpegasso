<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class riwayatkeluarga extends Member_Controller
{
	function riwayatkeluarga() 
	{
		parent::Member_Controller();
		$this->load->model('status_keluarga_model', 'status_keluarga');
		$this->load->model('riwayat_keluarga_model', 'riwayat_keluarga');
		$this->load->model('pegawai_model', 'pegawai');
        $this->load->model('lookup_model', 'lookup');
		$this->load->model('pendidikan_model', 'pendidikan');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Keluarga Pegawai');
        $kd_pegawai = $this->uri->segment(4);
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'tgl_lahir';
		
		$data['list_keluarga'] = $this->riwayat_keluarga->retrieve_by_idpeg($kd_pegawai);
		
		$config['base_url']     = site_url('pegawai/riwayatkeluarga/browse/');
		$config['total_rows']   = $this->riwayat_keluarga->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();		    
		$data['pegawai'] 		= $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
		$data['kd_pegawai'] = $kd_pegawai;
		$data['judul'] 		= "Data Keluarga dari: " . $data['pegawai']['nama_pegawai'];
        $data['gender_assoc'] = array(0=>"-- Pilih Jenis Kelamin --") +$this->lookup->gender_assoc();
		if (isset($this->user->user_group)){
		$this->template->display('pegawai/riwayatkeluarga/list_riwayatkeluarga', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}
		
	}
	/*function browse($group=0)
	{
		$pegawai_id = $this->pegawai->d_pegawai);
		$data['list_pasangan'] = $this->pasangan_pegawai->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= "Pasangan Pegawai";
		$this->template->display('pegawai/pasangan/list_riwayatkeluarga', $data);
	}*/
	
	function add()
	{
		if ($this->_validate())
		{
            $kd_pegawai = $this->input->post('kd_pegawai'); 
			$data = $this->_get_form_values();
			//$data['kd_pegawai'] = $this->pegawai->get_namapegawai_by_idpegawai($this->pegawai->kd_pegawai);
			$this->riwayat_keluarga->add($data);
			set_success('Data keluarga pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatkeluarga/index/' . $kd_pegawai);
		}
		else
		{
            $kd_pegawai = $this->uri->segment(4, '');
			$this->template->metas('title', 'SIMPEGA | Keluarga Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Keluarga dari: ' . $data['pegawai']['nama_pegawai'];
			$data['kd_keluarga']=$this->riwayat_keluarga->get_id();
			//$data['status_keluarga_assoc'] = array(0=>"-- Pilih Status --")+$this->status_keluarga->get_assoc("status_keluarga");
			$data['status_keluarga_assoc'] = $this->status_keluarga->get_keluarga_assoc("status_keluarga");
		    $data['gender_assoc'] = $this->lookup->gender_assoc();
			$data['pendidikan_assoc'] = array(0=>"-- Pilih Pendidikan --")+$this->pendidikan->get_assoc("nama_pendidikan");
            $data['status_assoc'] = $this->lookup->status_assoc();
            $this->template->display('/pegawai/riwayatkeluarga/detail_riwayatkeluarga', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['kd_keluarga'] = $id;
			$this->riwayat_keluarga->update($id, $data);
			set_success('Perubahan data keluarga pegawai berhasil disimpan');
			redirect('/pegawai/riwayatkeluarga/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Keluarga Pegawai :: Ubah');
			$data = $this->riwayat_keluarga->retrieve_by_pkey($id);
			//$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc("status_keluarga");
			$data['status_keluarga_assoc'] = $this->status_keluarga->get_keluarga_assoc("status_keluarga");
			$data['gender_assoc'] = $this->lookup->gender_assoc();
			$data['pendidikan_assoc'] = array(0=>"-- Pilih Pendidikan --")+$this->pendidikan->get_assoc("nama_pendidikan");
            $data['status_assoc'] = $this->lookup->status_assoc();
            if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				$data['judul']='Edit Keluarga dari: '. $data['pegawai']['nama_pegawai'];
				$this->template->display('/pegawai/riwayatkeluarga/detail_riwayatkeluarga', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatkeluarga', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_keluarga->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Keluarga Pegawai :: Hapus');
		confirm("Yakin menghapus data keluarga pegawai <b>".$data['nama_keluarga']."</b> ?");
		$res = $this->riwayat_keluarga->delete($idField);
		set_success('Data keluarga pegawai berhasil dihapus');
		redirect('/pegawai/riwayatkeluarga/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['kd_keluarga']	= '';
		$data['kd_pegawai']	= '';
		$data['nama_keluarga']	= '';
		$data['tempat_lahir']	= '';
		$data['tgl_lahir']	= '';
		$data['jns_kelamin']	= '';
		$data['id_pendidikan_keluarga']	= '';
		$data['keterangan']	= '';
		$data['pekerjaan']	= '-';
		$data['kd_status_keluarga']	= '';
		$data['aktif']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
	   	$data['kd_keluarga']	= $this->riwayat_keluarga->get_id();
		
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['nama_keluarga']	= $this->input->post('nama_keluarga', TRUE);
		$data['tempat_lahir']	= $this->input->post('tempat_lahir', TRUE);
		$data['tgl_lahir']	= $this->input->post('tgl_lahir', TRUE);
		$data['jns_kelamin']	= $this->input->post('jns_kelamin', TRUE);
		$data['id_pendidikan_keluarga']	= $this->input->post('id_pendidikan_keluarga', TRUE);
		$data['pekerjaan']	= $this->input->post('pekerjaan', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['kd_status_keluarga']	= $this->input->post('kd_status_keluarga', TRUE);
		$data['aktif']	= $this->input->post('aktif', TRUE);
		return $data;
		
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_keluarga', 'nama_keluarga', 'required');
		$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}