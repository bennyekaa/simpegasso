<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatpengajaran extends Member_Controller
{
	function Riwayatpengajaran()
	{
		parent::Member_Controller();
		
		$this->load->model('riwayat_pengajaran_model','riwayat_pengajaran');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model', 'lookup');
		$this->load->model('pendidikan_model', 'pendidikan');
		
	}
	
	function index()
	{
		$kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | pengajaran Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		/*$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'id_riwayat_pengajaran';
		*/
		$data['list_pengajaran'] = $this->riwayat_pengajaran->find(NULL, array('kd_pegawai' => $kd_pegawai), null, $limit_per_page,$start,$ordby);
		$data['jenis_pengajaran_assoc'] = $this->lookup->get_datafield('pengajaran','kd_pengajaran','nama_pengajaran');
		$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
		/*$config['base_url']     = site_url('pegawai/riwayatpengajaran/browse/');
		$config['total_rows']   = $this->riwayat_pengajaran->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
        */
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data pengajaran dari: " . $data['pegawai']['nama_pegawai'];		    
	
		$this->template->display('pegawai/riwayatpengajaran/list_riwayatpengajaran', $data);
	}
	/*function browse($group=0)
	{
		$pegawai_id = $this->pegawai->d_pegawai);
		$data['list_pasangan'] = $this->pasangan_pegawai->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= "Pasangan Pegawai";
		$this->template->display('pegawai/pasangan/list', $data);
	}*/
	
	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			//$data['kd_pegawai'] = $this->pegawai->get_namapegawai_by_idpegawai($this->pegawai->kd_pegawai);
			$this->riwayat_pengajaran->add($data);
			set_success('Data pengajaran pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatpengajaran/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | pengajaran Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
             $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data pengajaran dari: ' . $data['pegawai']['nama_pegawai'];
            
			$data['id_riwayat_pengajaran']=$this->riwayat_pengajaran->get_id();
			
			$data['semester_assoc'] = $this->lookup->semester_assoc();
			$data['capaian_assoc'] = $this->lookup->capaian_assoc();
			$data['jenis_pengajaran_assoc'] = $this->lookup->get_datafield('pengajaran','kd_pengajaran','nama_pengajaran');
			$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			
			//$data['golongan_pangkat_assoc'] = array(0=>"-- Pilih Golongan/Pangkat --")+$this->golongan->get_assoc("id_golpangkat");
			$this->template->display('/pegawai/riwayatpengajaran/detail_riwayatpengajaran', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_pengajaran'] = $id;
			$this->riwayat_pengajaran->update($id, $data);
			set_success('Perubahan data pasangan pegawai berhasil disimpan');
			redirect('/pegawai/riwayatpengajaran/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data pengajaran Pegawai :: Ubah');
			$data = $this->riwayat_pengajaran->retrieve_by_pkey($id);
			//$data['golongan_pangkat_assoc'] = array(0=>"-- Pilih Golongan/Pangkat --")+$this->golongan->get_assoc("id_golpangkat");
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                 $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				$data['semester_assoc'] = $this->lookup->semester_assoc();
				$data['capaian_assoc'] = $this->lookup->capaian_assoc();
				$data['jenis_pengajaran_assoc'] = $this->lookup->get_datafield('pengajaran','kd_pengajaran','nama_pengajaran');
				$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
				$data['judul']='Edit pengajaran dari: '. $data['pegawai']['nama_pegawai'];			
				$this->template->display('/pegawai/riwayatpengajaran/detail_riwayatpengajaran', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatpengajaran', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_pengajaran->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | pengajaran Pegawai :: Hapus');
		confirm("Yakin menghapus data pengajaran pegawai?");
		$res = $this->riwayat_pengajaran->delete($idField);
		set_success('Data pengajaran pegawai berhasil dihapus');
		redirect('/pegawai/riwayatpengajaran/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_pengajaran']	= '';
		$data['kd_pegawai']	= '';
		$data['nama']	= '';
		$data['jenis_pengajaran'] = '';
		$data['tahun_ajaran'] = '';
		$data['tempat_pengajaran']	= '';
		$data['keterangan']	= '';
		$data['semester' ]	= '';
		$data['tahun_ajaran'] = '';
		$data['bukti_penugasan' ] = '';
		$data['capaian'] = '';
		$data['jml_mhs'] = '';
		$data['sks'] = '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
	   
		
		$data['id_riwayat_pengajaran']	= $this->riwayat_pengajaran->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['nama']	= $this->input->post('nama', TRUE);
		$data['jenis_pengajaran']	= $this->input->post('jenis_pengajaran', TRUE);
		$data['tempat_pengajaran']	= $this->input->post('tempat_pengajaran', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['semester' ]	= $this->input->post('semester', TRUE);
		$data['tahun_ajaran'] = $this->input->post('tahun_ajaran', TRUE);
		$data['bukti_penugasan' ] = $this->input->post('bukti_penugasan', TRUE);
		$data['capaian'] = $this->input->post('capaian', TRUE);
		$data['jml_mhs'] = $this->input->post('jml_mhs', TRUE);
		$data['sks'] = $this->input->post('sks', TRUE);		

		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama', 'nama', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}