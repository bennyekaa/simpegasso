<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class riwayatjabatanstruktural extends Member_Controller
{
	function riwayatjabatanstruktural()
	{
		parent::Member_Controller();
		$this->load->model('jabatan_struktural_model', 'jabatan_struktural');
		$this->load->model('riwayat_jabatanstruktural_model','riwayat_jabatanstruktural');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('unit_kerja_model', 'unit_kerja');
        $this->load->model('lookup_model','lookup');
		include "application/libraries/lib/nusoap.php";
		
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
        $this->template->metas('title', 'SIMPEGA | Jabatan Struktural Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		
		$ordby = 'tahun_mulai, tahun_selesai';
		
		$data['list_jabatan_struktural'] = $this->riwayat_jabatanstruktural->find(NULL, array('kd_pegawai' => $kd_pegawai), $ordby, $start, $limit_per_page);
		$data['jabatan_struktural_assoc']=array('-' => "-- Lainnya --") +$this->jabatan_struktural->get_jabatanstruktural_assoc('nama_jabatan_s');
		$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
		//$data['list_pangkat'] = $this->riwayat_gol_kepangkatan->find(NULL, array('kd_pegawai' => $kd_pegawai), null, $limit_per_page,$start,$ordby);
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
        
        $data['judul'] 		= "Data Jabatan dari: " . $data['pegawai']['nama_pegawai'];
		
		$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
		$data['option_unit']=$this->unit_kerja->get_unit('1');
		
		if (isset($this->user->user_group)){
		$this->template->display('pegawai/riwayatjabatanstruktural/list_riwayatjabatanstruktural', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}
	}
	/*function browse($group=0)
	{
		$pegawai_id = $this->pegawai->d_pegawai);
		$data['list_pasangan'] = $this->pasangan_pegawai->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= "Pasangan Pegawai";
		$this->template->display('pegawai/pasangan/list', $data);
	}*/
	
	function add()
	{
		if ($this->_validate())
		{
            $kd_pegawai = $this->input->post('kd_pegawai');
            $NIP = $this->input->post('NIP');
            $data = $this->_get_form_values();
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_jabatan_struktural'] = $data['id_jabatan_s'];
				

//akses webservice	
				$param=array('id_app' =>'3','key'=>'123','kd_pegawai'=>$NIP,'id_jabatan_struktural'=>$data['id_jabatan_s']);
				$wsdl="http://192.168.1.209/webserviceum/server/?wsdl";
				$client=new nusoap_client($wsdl,true);
				$cek=$client->call("checkConnection",array("id_app"=>"3"));
				if($cek[item][0]=='200'):
					$message=$client->call("updateJabatanStrukturalPegawai",$param);
//begin update					
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				$data_status['aktif'] = '0';
				$this->riwayat_jabatanstruktural->update_status($kd_pegawai,$data_status);					
//end 
/*					print_r($message);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;*/  
				else:
					print_r($cek);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;  
				endif; 
//end	
			}
			$this->riwayat_jabatanstruktural->add($data);
			set_success('Data jabatan pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatjabatanstruktural/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Jabatan Struktural Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Jabatan Struktural dari: ' . $data['pegawai']['nama_pegawai'];
			
			$data['id_riwayat_jabatan_s']=$this->riwayat_jabatanstruktural->get_id();
			//$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
			$data['jabatan_struktural_assoc']=array('-' => "-- Lainnya --") +$this->jabatan_struktural->get_jabatanstruktural_assoc('nama_jabatan_s');
            $data['status_assoc'] = $this->lookup->status_assoc();
			$data['option_unit']=$this->unit_kerja->get_unit('1');
			// echo "<pre>";var_dump($data["pegawai"]);exit();
			$this->template->display('/pegawai/riwayatjabatanstruktural/detail_riwayatjabatanstruktural', $data);
		}
	}
	
	function edit($id)
	{
		
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_jabatan_s'] = $id;
			$kd_pegawai = $this->input->post('kd_pegawai');
			$NIP = $this->input->post('NIP');
			if ($data['aktif']==0){
				$data_pegawai['id_jabatan_struktural'] = '0';
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				}
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_jabatan_struktural'] = $data['id_jabatan_s'];
				$data_pegawai['tmt_jabatan_s_terakhir'] = $data['tmt_jabatan'];

//akses webservice	
				$param=array('id_app' =>'3','key'=>'123','kd_pegawai'=>$NIP,'id_jabatan_struktural'=>$data['id_jabatan_s']);
				$wsdl="http://192.168.1.209/webserviceum/server/?wsdl";
				$client=new nusoap_client($wsdl,true);
				$cek=$client->call("checkConnection",array("id_app"=>"3"));
				if($cek[item][0]=='200'):
					$message=$client->call("updateJabatanStrukturalPegawai",$param);
//begin update				
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				$data_status['aktif'] = '0';
				$this->riwayat_jabatanstruktural->update_status($kd_pegawai,$data_status);
//end					
/*					print_r($message);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;*/  
				else:
					print_r($cek);
					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					exit;  
				endif; 
//end					
			}

			

			$this->riwayat_jabatanstruktural->update($id, $data);
			//jika tidak ada jabatan yg aktif maka di tabel pegawai di non aktifkan
			$query_in_arsip = mysql_query("select * from riwayat_jabatan_struktural where kd_pegawai ='".$id."' and aktif='1'");
			$lakukan=0;
			while ($data_in_arsip=mysql_fetch_array($query_in_arsip))
			{
				$lakukan=1;
			}
			if ($lakukan==0) {

			$query = mysql_query("update pegawai 
					set id_jabatan_struktural='0'
					where kd_pegawai =$id");	

			}
			set_success('Perubahan data jabatan struktural pegawai berhasil disimpan');
			redirect('/pegawai/riwayatjabatanstruktural/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jabatan Struktural Pegawai :: Ubah');
			$data = $this->riwayat_jabatanstruktural->retrieve_by_pkey($id);
			//$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
			$data['jabatan_struktural_assoc']=array('-' => "-- Lainnya --") +$this->jabatan_struktural->get_jabatanstruktural_assoc('nama_jabatan_s');
            $data['status_assoc'] = $this->lookup->status_assoc();
			
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				
				$data['judul']='Edit Jabatan Struktural dari: '. $data['pegawai']['nama_pegawai'];
				$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
				$data['option_unit']=$this->unit_kerja->get_unit('1');
				$this->template->display('/pegawai/riwayatjabatanstruktural/detail_riwayatjabatanstruktural', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatjabatan', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_jabatanstruktural->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Jabatan Struktural Pegawai :: Hapus');
		confirm("Yakin menghapus data jabatan struktural pegawai?");
		$res = $this->riwayat_jabatanstruktural->delete($idField);
		set_success('Data jabatan struktural pegawai berhasil dihapus');
		redirect('/pegawai/riwayatjabatanstruktural/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_jabatan_s']	= '';
		$data['kd_pegawai']	= '';
		$data['id_jabatan_s']	= '';
		$data['no_sk_jabatan']	= '';
		$data['tgl_sk_jabatan']	= '';
		$data['tahun_mulai']	= '';
		$data['tahun_selesai']	= '';
		$data['kode_unit']	= '';
		
		$data['keterangan']	= '';
		$data['aktif']	= '';
		
		
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
        $data['id_riwayat_jabatan_s']	= $this->riwayat_jabatanstruktural->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['id_jabatan_s']	= $this->input->post('id_jabatan_s', TRUE);
		$data['no_sk_jabatan']	= $this->input->post('no_sk_jabatan', TRUE);
		$data['tgl_sk_jabatan']	= $this->input->post('tgl_sk_jabatan', TRUE);
		$data['tmt_jabatan']	= $this->input->post('tmt_jabatan', TRUE);
		$data['tahun_mulai']	= $this->input->post('tahun_mulai', TRUE);
		$data['tahun_selesai']	= $this->input->post('tahun_selesai', TRUE);
		$data['kode_unit']	= $this->input->post('kode_unit', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['aktif']	= $this->input->post('aktif', TRUE);
		
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}