<?php
function right($value, $count){
    return substr($value, ($count*-1));
}

function left($string, $count){
    return substr($string, 0, $count);
}
 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pegawai extends My_Controller {
    var $pegawai_id;
    var $groupuser;

    function Pegawai()  {
        parent::My_Controller();
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('agama_model', 'agama');
        $this->load->model('lookup_model', 'lookup');
        $this->load->model('jenis_pegawai_model', 'jenis_pegawai');
        $this->load->model('pendidikan_model', 'pendidikan');
        $this->load->model('kelurahan_model', 'kelurahan');
		$this->load->model('kabupaten_model', 'kabupaten');
        $this->load->model('unit_kerja_model', 'unit_kerja');
        $this->load->model('golongan_model', 'golongan');
        $this->load->model('status_perkawinan_model', 'status_perkawinan');
        $this->load->model('jabatan_model', 'jabatan');
		$this->load->model('jabatan_struktural_model', 'jabatan_struktural');
        $this->load->model('riwayat_gol_kepangkatan_model', 'riwayat_gol_kepangkatan');
        $this->load->model('riwayat_jabatan_model', 'riwayat_jabatan');
		$this->load->model('riwayat_jabatanstruktural_model','riwayat_jabatanstruktural');;
        $this->load->model('riwayat_pendidikan_model', 'riwayat_pendidikan');
		$this->load->model('riwayat_pelatihan_model', 'riwayat_pelatihan');
		$this->load->model('riwayat_penghargaan_model', 'riwayat_penghargaan');
		$this->load->model('riwayat_mutasi_model','riwayat_mutasi');
		$this->load->model('riwayat_tugas_model','riwayat_tugas');
		$this->load->model('jenis_penghargaan_model','jenis_penghargaan');
        $this->load->model('lap_pegawai_model', 'lap_pegawai');
		$this->load->model('anak_pegawai_model','anak_pegawai');
		$this->load->model('pasangan_pegawai_model','pasangan_pegawai');
		$this->load->model('status_keluarga_model', 'status_keluarga');
		$this->load->model('riwayat_keluarga_model', 'riwayat_keluarga');
        /* $this->load->model('gambar_model', 'gambar');  */
		$this->load->model('users_model','users');
        $this->load->model('user_pegawai_model', 'user_pegawai');
		include "application/libraries/lib/nusoap.php";
    }

    function index() {
        $this->template->metas('title', 'SIMPEGA | Data Induk Pegawai');
        $this->browse('A');
    }
    
	function index1() {
        $this->template->metas('title', 'SIMPEGA | Data Induk Pegawai');
        $this->browse('AC');
    }
	
	function index_honorer() {
        $this->template->metas('title', 'SIMPEGA | Data Induk Pegawai');
        $this->browse('AH');
    }
	
	function index_dosen() {
        $this->template->metas('title', 'SIMPEGA | Data Induk Pegawai');
        $this->browse('D');
    }
	function index_dosen1() {
        $this->template->metas('title', 'SIMPEGA | Data Induk Pegawai');
        $this->browse('DC');
    }
	
	function index_dosen2() {
        $this->template->metas('title', 'SIMPEGA | Data Induk Pegawai');
        $this->browse('DCH');
    }
	
    function browse($jenis='A') {
		if ($this->input->post('jenis'))
		{
            $jenis = $this->input->post('jenis');
		}
        else if ($this->uri->segment(4))
            $jenis = $this->uri->segment(4);
		else {
			$jenis = $jenis;
		}
		
        if ($this->input->post('group'))
		{
            $group = $this->input->post('group');
		}
		else 
			$group=0;
			
       /* else if ($this->uri->segment(4))
            $group = $this->uri->segment(4);*/

		//inisialisasi user
		$this->access->restrict();	
		if (!$this->user)
		$this->user = $this->access->get_user();
		if($this->user->user_group == 'Admin Khusus'){
		$query = mysql_query("select p.kode_unit from user_pegawai u,pegawai 
			p where p.kd_pegawai = u.kd_pegawai and u.user_id='".$this->user->user_id."'");					
			if ($query) {
					$datauser=mysql_fetch_array($query);  
					$unit_user = $datauser['kode_unit']; 
			}	
			$unit_kerja	=left($unit_user,2).'00';
		}
		else
		{
			$unit_kerja = '0';
			if ($this->input->post('unit_kerja'))
			{    
				$unit_kerja = $this->input->post('unit_kerja');
				
			}
			 else if ($this->uri->segment(5)) {
					$unit_kerja = $this->uri->segment(5);
			}
		}
        
		if($this->input->post('search'))
        {    
			$search = $this->input->post('search');
		}
		else
		{
			$search = 0;
		}
		
        $paging_uri = 6;
        if ($this->uri->segment($paging_uri))
            $start = $this->uri->segment($paging_uri);
        else
            $start=0;
        
		$limit_per_page = 100;
        $ordby = 'eselon asc,id_golpangkat_terakhir desc, nip';
        //$ordby = $this->uri->segment(4); // pengaturan agar order by sesuai klik kolom
		
		
		$query = mysql_query("select replace(kode_unit,'0','') as tingkat from unit_kerja where kode_unit='".$unit_kerja."'");					
		if ($query) {
			$data=mysql_fetch_array($query);  
			$tingkat = strlen($data['tingkat']);}		
			$pembanding=left($unit_kerja,$tingkat);
		
        $search_param = array();
        if ($unit_kerja)
            //$search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
			$search_param[] = "(left(pegawai.kode_unit,LENGTH('".$pembanding."'))= '".$pembanding."' OR pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
        if ($group)
            $search_param[] = "(id_jns_pegawai = '$group' and status_pegawai < 3 )";
        if($search)
            $search_param[] = "(nama_pegawai like '%$search%')";
			
		$tgl_skr = date('Y-m-d');

		//pengecekan dosen/adm
		if ($jenis=='A') {
			$search_param[] = "(status_pegawai=2 and id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
			$data['judul'] = "Data Induk PNS - Tenaga Administrasi";
			$data['JN'] = "A";
			$kelompok = $jenis;
			$data['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('1');
			}
		else if ($jenis=='AC') {
			$search_param[] = "(status_pegawai=1 and id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
			$data['judul'] = "Data Induk CPNS - Tenaga Administrasi";
			$data['JN'] = "AC";
			$kelompok = $jenis;
			$data['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('1');
			}
		else if ($jenis=='AH') {
			$search_param[] = "(status_pegawai=3 or status_pegawai=4 )";
			$data['judul'] = "Data Induk HONORER - Tenaga Administrasi";
			$data['JN'] = "AH";
			$kelompok = $jenis;
			$data['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('1');
			}
		else if ($jenis=='D') {
			$search_param[] = "(status_pegawai=2 and id_jns_pegawai like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'))";
			$data['judul'] = "Data Induk PNS - Tenaga Akademik";
			$data['JN'] = "D";
			$kelompok = $jenis;
			$data['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('2');
			}
		else if ($jenis=='DC') {
			$search_param[] = "(status_pegawai=1 and id_jns_pegawai like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'))";
			$data['judul'] = "Data Induk CPNS - Tenaga Akademik";
			$data['JN'] = "DC";
			$kelompok = $jenis;
			$data['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('2');
			}
		else if ($jenis=='DCH') {
			$search_param[] = "(status_pegawai>2 and id_jns_pegawai like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'))";
			$data['judul'] = "Data Induk Dosen Tidak Tetap- Tenaga Akademik";
			$data['JN'] = "DCH"; 
			$kelompok = $jenis;
			$data['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('2');
			}
		else 
		{
			$search_param[] = "(status_pegawai=2 and id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 				'id_tenaga_dosen') and status_pegawai < 3)";
			$data['judul'] = "Data Induk PNS - Tenaga Administrasi";
			$data['JN'] = "A";
			$kelompok = $jenis;
			$data['option_unit']=array(0 => "-- Semua Unit Kerja --") +$this->unit_kerja->get_unit('1');
		}


        $search_param = implode(" AND ", $search_param);
        
		$data['kerja_list'] = $this->lap_pegawai->findByFilter($search_param, $ordby, $limit_per_page, $start);

		// $data['kerja_list'][tmt_pangkat]=$this->lap_pegawai->tmt_pangkat();
		
		
        $data['group'] = $group;
        $data['start'] = $start;
		$data['unit_kerja'] = $unit_kerja;
        $data['unitkerja_assoc'] = $this->unit_kerja->get_assoc2();
		$data['kabupaten_assoc'] = $this->kabupaten->get_assoc();
		
        
		$data['jenis_pegawai_assoc'] = array(0 => "-- Semua Jenis Pegawai --") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
		$data['nama_jenis'] = $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
		
		$display_unit =$this->unit_kerja->get_unit('$unit_kerja');
        
		$config['base_url'] = site_url('pegawai/pegawai/browse/' . $jenis . '/' . $unit_kerja);
        $config['total_rows'] = $this->lap_pegawai->record_count;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = $paging_uri;
        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();
        
		$data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
        $data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
        $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
		$data['jabatan_struktural_assoc']=array('-' => "-- Lainnya --") +$this->jabatan_struktural->get_jabatanstruktural_assoc('nama_jabatan_s');
        //$this->template->display('/pegawai/pegawai/list_perunit_pegawai', $data);
		if (isset($this->user->user_group)){
			// var_dump($data["kerja_list"]);exit();
			$this->template->display('/pegawai/pegawai/list_perunit_pegawai', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}
    }


    function view() {
        $data['judul'] = "Detail Data dari: ";
        $id = $this->uri->segment(4);
        $this->edit($id);
    }
	

    function add() {
        if ($this->_validate()) {
			
            $data = $this->_get_form_values_pegawai();
            // echo "<pre>";var_dump($data);exit();
            $id_peg = $this->pegawai->get_id();
            $data['kd_pegawai'] = $id_peg;
			$kode_unit = $this->unit_kerja->retrieve_by_pkey($data['kode_unit']);
            $data['kode_unit_induk'] = $kode_unit['kode_unit_general'];
        	$batas=$data['batas'];
                list($y, $m, $d) = explode('-', $data['tgl_lahir']);
                $tahun = $y+ $batas;
                if ($m>=2)
                {
                    $bulan = $m+1;    
                    if ($bulan==13)
                    {
                        $bulan = 1;
                        $tahun = $y+$batas+1;
                    }  
                }
                else
                {
                    $bulan = $m;
                }
                 
    			$data['tgl_resign'] =$tahun.'-'.$bulan.'-01';
				
           
			
//fitria : data ke server ptik=================================================================================

	$tgl_lahir = date('Ymd', strtotime( $data['tgl_lahir']));
	$tmt_cpns = date('Ymd', strtotime( $data['tmt_cpns']));
	$tmt_pns = date('Ymd', strtotime( $data['tmt_pns']));
	$tgl_sk_pensiun = date('Ymd', strtotime( $data['tgl_sk_pensiun']));
	$tgl_resign = date('Ymd', strtotime( $data['tgl_resign']));
	
	$param=array('id_app'=>'3','key'=>'123','kd_pegawai'=>$id_peg,'nip'=>$data['NIP'],'nama_pegawai'=>$data['nama_pegawai'],
	'gelar_depan'=>$data['gelar_depan'],'gelar_belakang'=>$data['gelar_belakang'],'tempat_lahir'=>$data['tempat_lahir'],
	'tgl_lahir'=>$tgl_lahir,'jns_kelamin'=>$data['jns_kelamin'],'kd_agama'=>$data['kd_agama'],'id_jns_pegawai'=>$data['id_jns_pegawai'],
	'status_pegawai'=>$data['status_pegawai'],'status_kawin'=>$data['status_kawin'],'alamat'=>$data['alamat'],'RT'=>$data['RT'],'RW'=>$data['RW'],
	'kd_kelurahan'=>$data['kd_kelurahan'],'kode_pos'=>$data['kode_pos'],'no_telp'=>$data['no_telp'],'hp'=>$data['hp'],'kode_unit'=>$data['kode_unit'],
	'kode_unit_induk'=>$data['kode_unit_induk'],'no_sk_cpns'=>$data['no_sk_cpns'],'tmt_cpns'=>$tmt_cpns,'no_sk_pns'=>$data['no_sk_pns'],
	'tmt_pns'=>$tmt_pns,'npwp'=>$data['npwp'],'tgl_npwp'=>$tgl_lahir,'nip_lama'=>$data['nip_lama'],'eselon'=>$data['eselon'],
	'nidn'=>$data['NIDN'],'no_KTP'=>'','sandi_dosen'=>$data['sandi_dosen'],'sertifikat_pendidik'=>$data['sertifikat_pendidik'],
	'nira'=>$data['nira'],'nia'=>$data['nia'],'email'=>$data['email'], 'app_nip'=>$data['app_nip'], 'pp_nip'=>$data['pp_nip']);
	
	$wsdl="http://192.168.1.209/webserviceum/server/?wsdl";
	$client=new nusoap_client($wsdl,true);
	$cek=$client->call("checkConnection",array("id_app"=>"3"));
	if($cek[item][0]=='200'):
		$message=$client->call("insertDataPegawai",$param);
//jalankan add pegawai database simpega
			 $this->pegawai->add($data);
             set_success('Data Induk Pegawai berhasil disimpan.');		
		/*print_r($message);
		echo '<h2>Request</h2>';
		echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
		echo '<h2>Response</h2>';
		echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
		echo '<h2>Query</h2>';
		echo '<pre>' . $tgl_lahir. '</pre>';
		exit;  */
	else:
		print_r($cek);
		echo '<h2>Request</h2>';
		echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
		echo '<h2>Response</h2>';
		echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
		echo '<h2>Query</h2>';
		echo '<pre>' . $tgl_lahir . '</pre>';
		exit;  
	endif;  				
					
//end action ==================================================================================================
			
			if ($data['id_jns_pegawai']==5){
				if ($data['status_pegawai']==1){
					redirect('pegawai/pegawai/index_dosen1');
				}
				else {
					redirect('pegawai/pegawai/index_dosen');						
					}
			}
			else {
				if ($data['status_pegawai']!=1){
					redirect('pegawai/pegawai/index');
					}
					else {
					redirect('/pegawai/pegawai/index1');
					} 
			} 
			
        } else {
            $data = $this->_clear_form_pegawai();
            $data['action'] = 'add';
            $data['judul'] = 'Tambah Pegawai';
            $data['kd_pegawai'] = $this->pegawai->get_id();
            $data['agama_assoc'] = array(0 => "-- Pilih Agama --") + $this->agama->get_assoc('agama');
            $data['gender_assoc'] = $this->lookup->gender_assoc();
            $data['status_asal_assoc'] = array(0 => "-- Umum/Honorer --") + $this->lookup->status_asal_assoc();
            $data['jenis_pegawai_assoc'] = $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
            $data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
            $data['gol_darah_assoc'] = array(0 => "-") + $this->lookup->gol_darah_assoc();
            $data['status_perkawinan_assoc'] = $this->status_perkawinan->get_assoc('status_perkawinan');
            $data['kelurahan_assoc'] = $this->kelurahan->get_assoc('nama_kelurahan');
            $data['unit_kerja_assoc'] = array(0 => "-- Pilih Unit Kerja --") + $this->unit_kerja->get_assoc('nama_unit');
			$data['kabupaten_assoc'] = $this->kabupaten->get_assoc();
            $data['eselon_assoc'] = array(0 => "-- Pilih Eselon --") + $this->lookup->eselon_assoc();
            $data['golongan_assoc'] = $this->golongan->get_assoc();
            $data['pendidikan_assoc'] = $this->pendidikan->get_pendidikan_assoc();
            $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
			$data['option_kecamatan']=$this->kelurahan->get_kelurahan();
			$data['option_kabupaten']=$this->kabupaten->get_kabupaten();
			$data['option_unit']=$this->unit_kerja->get_unit('1');
			$data['tahun_assoc'] = array(0 => "-- Pilih --") + $this->lookup->tahun_assoc(date('Y')-10, date('Y')+50);
            $this->template->display('/pegawai/pegawai/detail_pegawai', $data);
        }
    }

    function edit($id) {
        if ($this->_validate()) {
            $data = $this->_get_form_values_pegawai();
            $data['kd_pegawai'] = $id;
            $kode_unit = $this->unit_kerja->retrieve_by_pkey($data['kode_unit']);
            $data['kode_unit_induk'] = $kode_unit['kode_unit_general'];
			
			
//fitria : data ke server ptik=================================================================================			
$tgl_lahir = date('Ymd', strtotime( $data['tgl_lahir']));
$tmt_cpns = date('Ymd', strtotime( $data['tmt_cpns']));
$tmt_pns = date('Ymd', strtotime( $data['tmt_pns']));
$tgl_sk_pensiun = date('Ymd', strtotime( $data['tgl_sk_pensiun']));
$tgl_resign = date('Ymd', strtotime( $data['tgl_resign']));

	
	$param=array('id_app'=>'3','key'=>'123','kd_pegawai'=>$id,'nip'=>$data['NIP'],'nama_pegawai'=>$data['nama_pegawai'],
	'gelar_depan'=>$data['gelar_depan'],'gelar_belakang'=>$data['gelar_belakang'],'tempat_lahir'=>$data['tempat_lahir'],
	'tgl_lahir'=>$tgl_lahir,'jns_kelamin'=>$data['jns_kelamin'],'kd_agama'=>$data['kd_agama'],'id_jns_pegawai'=>$data['id_jns_pegawai'],
	'status_pegawai'=>$data['status_pegawai'],'status_kawin'=>$data['status_kawin'],'alamat'=>$data['alamat'],'RT'=>$data['RT'],'RW'=>$data['RW'],
	'kd_kelurahan'=>$data['kd_kelurahan'],'kode_pos'=>$data['kode_pos'],'no_telp'=>$data['no_telp'],'hp'=>$data['hp'],'kode_unit'=>$data['kode_unit'],
	'kode_unit_induk'=>$data['kode_unit_induk'],'no_sk_cpns'=>$data['no_sk_cpns'],'tmt_cpns'=>$tmt_cpns,'no_sk_pns'=>$data['no_sk_pns'],
	'tmt_pns'=>$tmt_pns,'npwp'=>$data['npwp'],'tgl_npwp'=>$tgl_lahir,'nip_lama'=>$data['nip_lama'],'eselon'=>$data['eselon'],
	'nidn'=>$data['NIDN'],'no_KTP'=>'','sandi_dosen'=>$data['sandi_dosen'],'sertifikat_pendidik'=>$data['sertifikat_pendidik'],
	'nira'=>$data['nira'],'nia'=>$data['nia'],'email'=>$data['email'], 'app_nip'=>$data['app_nip'], "pp_nip"=>$data['pp_nip']);
	
$wsdl="http://192.168.1.209/webserviceum/server/?wsdl";
$client=new nusoap_client($wsdl,true);
$cek=$client->call("checkConnection",array("id_app"=>"3"));
if($cek[item][0]=='200'):
		$message=$client->call("updateDataPegawai",$param);
//update pegawai
			$this->pegawai->modify($id, $data);
            set_success('Perubahan data pegawai berhasil disimpan');
//end update		
		/*print_r($message);
		echo '<h2>Request</h2>';
		echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
		echo '<h2>Response</h2>';
		echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
		exit;*/  
else:
		print_r($cek);
		echo '<h2>Request</h2>';
		echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
		echo '<h2>Response</h2>';
		echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
		exit;  
endif;  

//end action=================================================================================
			
			
						if ($data['id_jns_pegawai']==5){
				if ($data['status_pegawai']==1){
					redirect('pegawai/pegawai/index_dosen1');
				}
				else {
					redirect('pegawai/pegawai/index_dosen');						
					}
			}
			else {
				if ($data['status_pegawai']!=1){
					redirect('pegawai/pegawai/index');
					}
					else {
					redirect('/pegawai/pegawai/index1');
					} 
			} 
            //redirect('/pegawai/pegawai/', 'location');
        } else {
            $data['action'] = 'edit/' . $id;
            $data['judul'] = 'Edit Data Induk dari:';
            $data['pegawai'] = $this->pegawai->retrieve_by_pkey($id);
            $data['kd_pegawai'] = $id;
            if ($data) {
                $data['agama_assoc'] = $this->agama->get_assoc('agama');
                $data['gender_assoc'] = array(0 => "-- Pilih Jenis Kelamin --") + $this->lookup->gender_assoc();
                $data['status_asal_assoc'] = array(0 => "-- Umum/Honorer --") + $this->lookup->status_asal_assoc();
                $data['jenis_pegawai_assoc'] = $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
                $data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
                $data['gol_darah_assoc'] = array(0 => "-") + $this->lookup->gol_darah_assoc();
                $data['status_perkawinan_assoc'] = $this->status_perkawinan->get_assoc('status_perkawinan');
                $data['kelurahan_assoc'] = $this->kelurahan->get_assoc('nama_kelurahan');
				$data['kabupaten_assoc'] = $this->kabupaten->get_assoc();
                $data['eselon_assoc'] = array(0 => "-- Pilih Eselon --") + $this->lookup->eselon_assoc();
                $data['golongan_assoc'] = $this->golongan->get_assoc();
                $data['pendidikan_assoc'] = $this->pendidikan->get_pendidikan_assoc();
                $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
				$data['option_kecamatan']=$this->kelurahan->get_kelurahan();
				$data['option_kabupaten']=$this->kabupaten->get_kabupaten();
				$data['kd_pegawai'] = $this->pegawai->get_id();
				$data['tahun_assoc'] = array(0 => "-- Pilih --") + $this->lookup->tahun_assoc(date('Y')-10, date('Y')+50);
				$data['jabatan_struktural_assoc']=$this->jabatan_struktural->get_jabatanstruktural_assoc('nama_jabatan_s');
				//$data['status_pegawai']=1;
				$query = mysql_query("select pegawai.kode_unit, u.nama_unit from pegawai, unit_kerja u where 
				pegawai.kode_unit=u.kode_unit and pegawai.kd_pegawai='".$id."'");					
					if ($query) {
						$data_unit=mysql_fetch_array($query);  //$tingkat=($data['tingkat']== 0)?"1":$data['tingkat'];}
						$kd_unit = $data_unit['kode_unit'];	//	echo $tingkat;	
						$unit = $data_unit['nama_unit'];}	//	echo $tingkat;	
				
						$data['option_unit']=array($kd_unit=>$unit)+$this->unit_kerja->get_unit('1');
						$expected_file = PUBLICPATH . 'photo/photo_' . $id . '.jpg';
						if (file_exists($expected_file)){
							$data['photo'] = base_url() . '/public/photo/photo_' . $id . '.jpg';
						}
						else{
							$expected_file = PUBLICPATH . 'photo/photo_' . $id . '.gif';
							if (file_exists($expected_file)){
								$data['photo'] = base_url() . '/public/photo/photo_' . $id . '.gif';
								}
							else{
								$expected_file = PUBLICPATH . 'photo/photo_' . $id . '.bmp';
								if (file_exists($expected_file)){
										$data['photo'] = base_url() . '/public/photo/photo_' . $id . '.bmp';
									}
									else{	
										$data['photo'] = base_url() . '/public/images/image/nophotos.jpg';
								}
							}
						}
				
                //$data['photo'] = file_exists($expected_file) ? base_url() . '/public/photo/photo_' . $id . '.jpg' : base_url() . '/public/images/image/nophotos.jpg';

                $data['id'] = $id;
                $data['action'] = 'edit/' . $id;
				if (isset($this->user->user_group)){
                $this->template->display('/pegawai/pegawai/detail_pegawai', $data);
				} else {
				redirect('/pegawai/pegawai/', 'location');
				}
				
            } else {
                set_error('Data tidak ditemukan');
                redirect('/pegawai/pegawai/', 'location');
            }
        }
    }

	function detail_kp4($id) {
        if ($this->_validate()) {
            $data = $this->_get_form_values_pegawai();
            $data['kd_pegawai'] = $id;
            $kode_unit = $this->unit_kerja->retrieve_by_pkey($data['kode_unit']);
            $data['kode_unit_induk'] = $kode_unit['kode_unit_general'];
            
            set_success('Perubahan data pegawai berhasil disimpan');
            redirect('/pegawai/pegawai/', 'location');
        } else {
            $data['action'] = 'detail_kp4/' . $id;
            $data['judul'] = 'Edit KP4 dari:';
            $data['pegawai'] = $this->pegawai->retrieve_by_pkey($id);
            $data['kd_pegawai'] = $id;
            $data['NIP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'NIP');
			$data['nama_peg'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'nama_pegawai');
			$data['gelar_depan'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gelar_depan');
			$data['gelar_belakang'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gelar_belakang');
			$data['tempat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tempat_lahir');
			$data['tanggal'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tgl_lahir');
			$data['alamat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'alamat');
			$data['no_telp'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'no_telp');
			$data['hp'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hp');
			$data['kode_pos'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_pos');
			$data['kelurahan_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_kelurahan');
			$data['email'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'email');
			$data['jns'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'jns_kelamin');
			if ($data['jns']==1)
				$data['jenis'] = 'Laki-Laki';
			else
				$data['jenis'] = 'Perempuan';
			$data['agama_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_agama');
			$data['agama'] = $this->lookup->get_cari_field('agama','kd_agama',$data['agama_id'],'agama');
			$data['status_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_pegawai');
			if ($data['status_pegawai']==0)
				$data['status_pegawai'] = 'Honorer';
			elseif ($data['status_pegawai']==1){
				$data['status_pegawai_s'] = 'CPNS';
				$data['status_pegawai_p'] = 'Calon Pegawai Negeri Sipil';}
			else{
				$data['status_pegawai_s'] = 'PNS';
				$data['status_pegawai_p'] = 'Pegawai Negeri Sipil';}
			$data['pangkat_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_golpangkat_terakhir');
			$data['pangkat']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'pangkat');
			$data['golongan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'golongan');
			$data['tmt_pangkat']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_golpangkat_terakhir');
			$data['jenis_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jns_pegawai');
			$data['jabatan_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jabatan_terakhir');
        	$data['jabatan_fungsional']=$this->lookup->get_cari_field('jabatan','id_jabatan',$data['jabatan_id'],'nama_jabatan');
			$data['jabatan_struktural_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jabatan_struktural');
        	$data['jabatan_struktural']=$this->lookup->get_cari_field('jabatan_struktural','id_jabatan_s',$data['jabatan_struktural_id'],'nama_jabatan_s');
						
			$data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
			
			$data['kp4_11a']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kp4_11a');
			$data['kp4_11b']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kp4_11b');
			$data['kp4_11c']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kp4_11c');
			
			$data['tingkat'] = $this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'tingkat');
			$data['unit_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_unit');
			$data['unit_kerja']=$this->lookup->get_cari_field('unit_kerja','kode_unit',$data['unit_id'],'nama_unit');
			$data['eselon']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'eselon');
			if ($data['jenis_pegawai'] =='1') 
			{
				$data['jabatan'] = $data['jabatan_struktural'].' '.$data['unit_kerja'];
			}
			else if (($data['jenis_pegawai']=='5') and ($data['eselon']!='non-eselon'))
			{
				$data['jabatan'] = $data['jabatan_struktural'].' '.$data['unit_kerja'];
			}
			else 
			{
				$data['jabatan'] = $data['jabatan_fungsional'].' '.$data['unit_kerja'];
			}
			
			
			$tgl = date('Y-m-d');
			$data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
			$masa_kerja = $this->datediff($data['tmt_cpns'],$tgl);
			$data['mk_tambahan_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_th');
			$data['mk_tambahan_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_bl');
			$data['mk_selisih_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_th');
			$data['mk_selisih_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_bl');	
			$data['masa_kerja_seluruhnya_th']=$masa_kerja['years'] + $data['mk_tambahan_th'];
			$data['masa_kerja_seluruhnya_bl']=$masa_kerja['months'] + $data['mk_tambahan_bl'];

			if($data['masa_kerja_seluruhnya_bl'] >= 12){
				$data['masa_kerja_seluruhnya_th'] = $data['masa_kerja_seluruhnya_th']+1;
				$data['masa_kerja_seluruhnya_bl'] = $data['masa_kerja_seluruhnya_bl'] - 12;
			}

			$data['masa_kerja_seluruhnya'] = $data['masa_kerja_seluruhnya_th']." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
			$tingkatawal=0;
			$tingkatakhir=0;
			$query_mk = mysql_query("SELECT golongan_pangkat.tingkat
FROM riwayat_gol_kepangkatan, `golongan_pangkat`
WHERE riwayat_gol_kepangkatan.id_golpangkat = golongan_pangkat.id_golpangkat AND 
riwayat_gol_kepangkatan.kd_pegawai = '$id' order by riwayat_gol_kepangkatan.id_golpangkat LIMIT 0,1");					
			if ($query_mk)	{
				$datamk=mysql_fetch_array($query_mk);  
				$tingkatawal= $datamk[tingkat];
			}
			$query_mk = mysql_query("SELECT golongan_pangkat.tingkat
FROM pegawai, `golongan_pangkat`
WHERE pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat AND 
pegawai.kd_pegawai = '$id'");					
			if ($query_mk)	{
				$datamk=mysql_fetch_array($query_mk);  
				$tingkatakhir= $datamk[tingkat];
			}
			$data['data_tahun'] = $data['masa_kerja_seluruhnya_th'];
			
			if (($tingkatawal==$tingkatakhir) or ($tingkatawal==3)){
				$data['masa_kerja_golongan'] = $data['masa_kerja_seluruhnya'];
			}
			else if ($tingkatakhir==2){
				$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-6)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
				$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-6;
			}
			else {
				if ($tingkatawal==1){
					$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-11)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
					$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-11;
				}
				else
				{
					$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-5)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
					$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-5;
				}
			}
				
			//$masa_kerja_golongan = $this->datediff($data['tmt_kp'],$tgl);
			
			
			//$data['masa_kerja_golongan_th'] = $masa_kerja_golongan['years']; 
			//$data['masa_kerja_golongan_bl'] = $masa_kerja_golongan['months'];
			//$data['masa_kerja_golongan'] = $data['masa_kerja_golongan_th']." tahun ".$data['masa_kerja_golongan_bl']." bulan";
			if (($data['mk_tambahan_th']>0) and ($data['mk_tambahan_bl']>0))
			{
				$data['masa_kerja_tambahan'] = $data['mk_tambahan_th']." tahun ".$data['mk_tambahan_bl']." bulan";	
			}
			else
			{
				$data['masa_kerja_tambahan'] = '-';
			}
			$data['gaji_pokok_asli']=$this->lookup->get_cari_field2('acuan_gaji_pokok','id_golpangkat','masa_kerja',$data['pangkat_id'],$data['masa_kerja_golongan_th'],'gaji_pokok');
		

			$data['list_pasangan']  = $this->pasangan_pegawai->retrieve_by_idpeg_aktif($id);
			$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc('status_keluarga');
			
			$data['list_anak'] = $this->anak_pegawai->retrieve_by_idpeg($id);
			$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			$data['jumlah_anak_masuk_daftar'] = $this->anak_pegawai->jumlah_anak_masuk_daftar($id);
			$data['jumlah_anak'] = $this->anak_pegawai->jumlah_anak($id);
			
                $data['action'] = 'detail_kp4/' . $id;
				if (isset($this->user->user_group)){
					$this->template->display('/pegawai/pegawai/detail_kp4', $data);
				} else {
					redirect('/pegawai/pegawai/', 'location');
				}
                
            } 
			
			
			
        
    }
	
	function cetak_kp4($id)
		{
		//$properties=array();

		$properties['papersize']="Legal";
		$properties['paperlayout']='P';	      
		$data['pegawai'] = $this->pegawai->retrieve_by_pkey($id);
            $data['kd_pegawai'] = $id;
            $data['NIP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'NIP');
			$data['nama_peg'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'nama_pegawai');
			$data['gelar_depan'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gelar_depan');
			$data['gelar_belakang'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gelar_belakang');
			$data['tempat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tempat_lahir');
			$data['tanggal'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tgl_lahir');
			$data['alamat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'alamat');
			$data['no_telp'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'no_telp');
			$data['hp'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hp');
			$data['kode_pos'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_pos');
			$data['kelurahan_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_kelurahan');
			$data['email'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'email');
			$data['jns'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'jns_kelamin');
			if ($data['jns']==1)
				$data['jenis'] = 'Laki-Laki';
			else
				$data['jenis'] = 'Perempuan';
			$data['agama_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_agama');
			$data['agama'] = $this->lookup->get_cari_field('agama','kd_agama',$data['agama_id'],'agama');
			$data['status_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_pegawai');
			if ($data['status_pegawai']==0)
				$data['status_pegawai'] = 'Honorer';
			elseif ($data['status_pegawai']==1){
				$data['status_pegawai_s'] = 'CPNS';
				$data['status_pegawai_p'] = 'Calon Pegawai Negeri Sipil';}
			else{
				$data['status_pegawai_s'] = 'PNS';
				$data['status_pegawai_p'] = 'Pegawai Negeri Sipil';}
				
			$data['pangkat_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_golpangkat_terakhir');
			$data['pangkat']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'pangkat');
			$data['golongan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'golongan');
			$data['tmt_pangkat']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_golpangkat_terakhir');
			$data['jenis_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jns_pegawai');
			$data['jabatan_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jabatan_terakhir');
        	$data['jabatan_fungsional']=$this->lookup->get_cari_field('jabatan','id_jabatan',$data['jabatan_id'],'nama_jabatan');
			$data['jabatan_struktural_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jabatan_struktural');
        	$data['jabatan_struktural']=$this->lookup->get_cari_field('jabatan_struktural','id_jabatan_s',$data['jabatan_struktural_id'],'nama_jabatan_s');
			$data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
			
			$data['kp4_11a']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kp4_11a');
			$data['kp4_11b']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kp4_11b');
			$data['kp4_11c']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kp4_11c');
			
			$data['tingkat'] = $this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'tingkat');
			$data['unit_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_unit');
			$data['unit_kerja']=$this->lookup->get_cari_field('unit_kerja','kode_unit',$data['unit_id'],'nama_unit');
			$data['eselon']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'eselon');
			if ($data['jenis_pegawai'] =='1') 
			{
				$data['jabatan'] = $data['jabatan_struktural'].' '.$data['unit_kerja'];
			}
			else if (($data['jenis_pegawai']=='5') and ($data['eselon']!='non-eselon'))
			{
				$data['jabatan'] = $data['jabatan_struktural'].' '.$data['unit_kerja'];
			}
			else 
			{
				$data['jabatan'] = $data['jabatan_fungsional'].' '.$data['unit_kerja'];
			}
						
			$tgl = date('Y-m-d');
			$bln = $this->lookup->longonthname(date("m"));
			//$data['tglsurat'] = date('d').' '.$bln.' '.date('Y');
			$bulan = array(
				"01" => "Januari",
				"02" => "Februari",
				"03" => "Maret",
				"04" => "April",
				"05" => "Mei",
				"06" => "Juni",
				"07" => "Juli",
				"08" => "Agustus",
				"09" => "September",
				"10" => "Oktober",
				"11" => "November",
				"12" => "Desemer",
			);
			$data['tglsurat'] = explode(" ", date("d m Y"));
			$data['tglsurat'][1] = $bulan[$data['tglsurat'][1]];

			$data['tglsurat'] = implode(" ", $data['tglsurat']);

			$query = mysql_query("SELECT * FROM pos_variabel WHERE id_var = 'KP4'");					
			if ($query)	{
				$dataku=mysql_fetch_array($query);  
				$data['mengetahui']= $dataku['keterangan'];
				$data['mengetahui1']= $dataku['keterangan2'];
				$data['mengetahui2']= $dataku['keterangan3'];
				}
		
			$data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
			$masa_kerja = $this->datediff($data['tmt_cpns'],$tgl);
			
			/*$query_mk = mysql_query("SELECT * FROM riwayat_gol_kepangkatan WHERE kd_pegawai = '$id' AND aktif LIKE '1'");					
			if ($query_mk)	{
				$datamk=mysql_fetch_array($query_mk);  
				if ( $datamk[tmt_pangkat]['year'] < date("y"))
				{
					$data['masa_kerja_golongan_th']= (date("y") - $datamk[tmt_pangkat]['year']) ;
				}
				//$data['masa_kerja_golongan_th']= $datamk['mk_tahun'];
				if ( $datamk[tmt_pangkat]['mon'] < date("m"))
				{
					$data['masa_kerja_golongan_bl']= date("m") - $datamk[tmt_pangkat]['mon'] ;
				}
				else if( $datamk[tmt_pangkat]['mon'] > date("m"))
				{
					$data['masa_kerja_golongan_bl']= $datamk[tmt_pangkat]['mon'] - date("m");
				}
				$data['tmt_pangkat']=$datamk[tmt_pangkat]['mon'];
				
				
				}
			$data['masa_kerja_golongan'] = $data['masa_kerja_golongan_th']." tahun ".$data['masa_kerja_golongan_bl']." ".$data['tmt_pangkat']." bulan";	
			
			*/
			
			$data['mk_tambahan_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_th');
			$data['mk_tambahan_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_bl');
			$data['mk_selisih_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_th');
			$data['mk_selisih_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_bl');	
			$data['masa_kerja_seluruhnya_th']=$masa_kerja['years'] + $data['mk_tambahan_th'];
			$data['masa_kerja_seluruhnya_bl']=$masa_kerja['months'] + $data['mk_tambahan_bl'];

			if($data['masa_kerja_seluruhnya_bl'] >= 12){
				$data['masa_kerja_seluruhnya_th'] = $data['masa_kerja_seluruhnya_th']+1;
				$data['masa_kerja_seluruhnya_bl'] = $data['masa_kerja_seluruhnya_bl'] - 12;
			}

			$data['masa_kerja_seluruhnya'] = $data['masa_kerja_seluruhnya_th']." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";

			$tingkatawal=0;
			$tingkatakhir=0;
			$query_mk = mysql_query("SELECT golongan_pangkat.tingkat
FROM riwayat_gol_kepangkatan, `golongan_pangkat`
WHERE riwayat_gol_kepangkatan.id_golpangkat = golongan_pangkat.id_golpangkat AND 
riwayat_gol_kepangkatan.kd_pegawai = '$id' order by riwayat_gol_kepangkatan.id_golpangkat LIMIT 0,1");					
			if ($query_mk)	{
				$datamk=mysql_fetch_array($query_mk);  
				$tingkatawal= $datamk[tingkat];
			}
			$query_mk = mysql_query("SELECT golongan_pangkat.tingkat
FROM pegawai, `golongan_pangkat`
WHERE pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat AND 
pegawai.kd_pegawai = '$id'");					
			if ($query_mk)	{
				$datamk=mysql_fetch_array($query_mk);  
				$tingkatakhir= $datamk[tingkat];
			}
			$data['data_tahun'] = $data['masa_kerja_seluruhnya_th'];
			
			if (($tingkatawal==$tingkatakhir) or ($tingkatawal==3)){
				$data['masa_kerja_golongan'] = $data['masa_kerja_seluruhnya'];
			}
			else if ($tingkatakhir==2){
				$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-6)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
				$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-6;
			}
			else {
				if ($tingkatawal==1){
					$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-11)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
					$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-11;
				}
				else
				{
					$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-5)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
					$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-5;
				}
			}
				
			//$masa_kerja_golongan = $this->datediff($data['tmt_kp'],$tgl);
			
			
			//$data['masa_kerja_golongan_th'] = $masa_kerja_golongan['years']; 
			//$data['masa_kerja_golongan_bl'] = $masa_kerja_golongan['months'];
			//$data['masa_kerja_golongan'] = $data['masa_kerja_golongan_th']." tahun ".$data['masa_kerja_golongan_bl']." bulan";
			if (($data['mk_tambahan_th']>0) and ($data['mk_tambahan_bl']>0))
			{
				$data['masa_kerja_tambahan'] = $data['mk_tambahan_th']." tahun ".$data['mk_tambahan_bl']." bulan";	
			}
			else
			{
				$data['masa_kerja_tambahan'] = '-';
			}
			
		
			$data['gaji_pokok_asli']=$this->lookup->get_cari_field2('acuan_gaji_pokok','id_golpangkat','masa_kerja',$data['pangkat_id'],$data['masa_kerja_golongan_th'],'gaji_pokok');
			$data['status_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_pegawai');
			if (($data['status_pegawai']=='CPNS')or($data['status_pegawai']==1))
			{
				//$data['gaji_pokok'] = $data['gaji_pokok_asli'] +' x 80% = '+($data['gaji_pokok_asli']*0.8);
				$data['gaji_pokok'] = ($data['gaji_pokok_asli']*0.8);
			}
			else
			{
				$data['gaji_pokok'] = $data['gaji_pokok_asli'];
			}
			
			$data['jabatan_lain'] = $this->input->post('jabatan_lain', TRUE);
			$data['penghasilan_lain'] = $this->input->post('penghasilan_lain', TRUE);
			$data['pensiun_janda'] = $this->input->post('pensiun_janda', TRUE);
			$data['list_pasangan']  = $this->pasangan_pegawai->retrieve_by_idpeg_aktif($id);
			$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc('status_keluarga');
			
			$data['list_anak'] = $this->anak_pegawai->retrieve_by_idpeg($id);
			$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			$data['jumlah_anak_masuk_daftar'] = $this->anak_pegawai->jumlah_anak_masuk_daftar($id);
			$data['jumlah_anak'] = $this->anak_pegawai->jumlah_anak($id);
			
			if (isset($this->user->user_group)){
					$html=$this->load->view('pegawai/pegawai/cetak_kp4', $data,true);
					$this->printtopdf->htmltopdf($properties,$html);
			} else {
				redirect('/pegawai/pegawai/', 'location');
			}
			
	}

	
	function cetak_drh($id, $singkat=null)
		{
		//$properties=array();
		
		
		$properties['papersize']="Folio";
		$properties['paperlayout']='P';		      
		$data['pegawai'] = $this->pegawai->retrieve_by_pkey($id);
            $data['kd_pegawai'] = $id;
            $data['NIP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'NIP');
			$data['NPWP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'npwp');
			$data['nama_peg'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'nama_pegawai');
			$data['gelar_depan'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gelar_depan');
			$data['gelar_belakang'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gelar_belakang');
			$data['tempat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tempat_lahir');
			$data['tanggal'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tgl_lahir');
			$data['alamat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'alamat');
			$data['rt'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'rt');
			$data['rw'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'rw');
			$data['no_telp'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'no_telp');
			
			$data['gol_darah'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gol_darah');
			$data['drh_tinggi'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_tinggi');
			$data['drh_bb'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_bb');
			$data['drh_rambut'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_rambut');
			$data['drh_bentuk_muka'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_bentuk_muka');
			$data['drh_warna_kulit'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_warna_kulit');
			$data['drh_ciri_ciri'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_ciri_ciri');
			$data['drh_cacat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_cacat');
			$data['hobi_kesenian'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hobi_kesenian');
			$data['hobi_olahraga'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hobi_olahraga');
			$data['hobi_lain'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hobi_lain');
			$data['drh_nama_kecil'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_nama_kecil');
			
			$data['status_kawin'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_kawin');
			
			$data['kelurahan_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_kelurahan');
			$data['kelurahan_assoc'] = $this->kelurahan->get_assoc('nama_kelurahan');
			$data['status_perkawinan_assoc'] = $this->status_perkawinan->get_assoc('status_perkawinan');
			//$data['kecamatan'] = $this->lookup->get_cari_field('kelurahan','kd_kelurahan',$data['agama_id'],'agama');
			
			$data['jns'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'jns_kelamin');
			if ($data['jns']==1)
				$data['jenis'] = 'Laki-Laki';
			else
				$data['jenis'] = 'Perempuan';
			$data['agama_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_agama');
			$data['agama'] = $this->lookup->get_cari_field('agama','kd_agama',$data['agama_id'],'agama');
			$data['status_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_pegawai');
			if ($data['status_pegawai']==0)
				$data['status_pegawai'] = 'Honorer';
			elseif ($data['status_pegawai']==1){
				$data['status_pegawai_s'] = 'CPNS';
				$data['status_pegawai_p'] = 'Calon Pegawai Negeri Sipil';}
			else{
				$data['status_pegawai_s'] = 'PNS';
				$data['status_pegawai_p'] = 'Pegawai Negeri Sipil';}
				
			$data['id_jns_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jns_pegawai');	
			$data['pangkat_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_golpangkat_terakhir');
			$data['pangkat']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'pangkat');
			$data['golongan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'golongan');
			$data['tmt_pangkat']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_golpangkat_terakhir');
			$data['jabatan_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jabatan_struktural');
			$data['jabatan_f']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jabatan_terakhir');
			$data['id_jns_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jns_pegawai');
			
			
			 
        	$data['jabatan']=$this->lookup->get_cari_field('jabatan_struktural','id_jabatan_s',$data['jabatan_id'],'nama_jabatan_s');
			
			
			$data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
			
			$data['tingkat'] = $this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'tingkat');
			$data['unit_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_unit');
			$data['unit_kerja']=$this->lookup->get_cari_field('unit_kerja','kode_unit',$data['unit_id'],'nama_unit');
			$data['eselon']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'eselon');
			
			if (($data['eselon'] =='non-eselon') and ($data['tingkat']=='1'))
			{
				$data['non_jabatan'] = 'Pembantu Pelaksana'.' Subag '.$data['unit_kerja'];
			}
			elseif (($data['eselon'] =='non-eselon') and ($data['tingkat']=='2'))
			{
				$data['non_jabatan'] = 'Pelaksana'.' Subag '.$data['unit_kerja'];
			}
			elseif (($data['eselon'] =='non-eselon') and ($data['tingkat']=='3'))
			{
				$data['non_jabatan'] = 'Pembantu Pimpinan'.' Subag '.$data['unit_kerja'];
			}
			else
			{
				$data['non_jabatan'] = $data['jabatan'].' '.$data['unit_kerja'];
			}
			$ordby1='id_pendidikan, th_lulus';
			$data['list_pendidikan'] = $this->riwayat_pendidikan->find(NULL, array('kd_pegawai' => $id), $ordby1, $start, null);
        	$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			$data['list_pelatihan'] = $this->riwayat_pelatihan->find(NULL, array('kd_pegawai' => $id), $ordby, $limit_per_page,$start,NULL);
			$ordby2 = 'riwayat_gol_kepangkatan.aktif ,riwayat_gol_kepangkatan.status_pegawai, riwayat_gol_kepangkatan.id_golpangkat, riwayat_gol_kepangkatan.tmt_pangkat';
			$search_param = array();
            $search_param[] = "kd_pegawai = $id";
			$search_param[] = "(keterangan like 'SK KP' OR keterangan like 'SK CPNS' OR keterangan like 'SK PNS')";
			//$search_param[] = "(keterangan like 'KGB' AND aktif='1')";
			$search_param = implode(" AND ", $search_param);
			$data['list_pangkat'] = $this->riwayat_gol_kepangkatan->find( NULL,$search_param, $ordby2, $limit_per_page,$start,NULL);
			$data['golongan_assoc'] = $this->golongan->get_gol_assoc();
			$data['pangkat_assoc'] = $this->golongan->get_pangkat_assoc();
			
			//$ordbyjabatan = 'riwayat_jabatan.id_jabatan,riwayat_jabatan.id_riwayat_jabatan';
			//$data['list_jabatan'] = $this->riwayat_jabatan->find(NULL, array('kd_pegawai' => $id), $ordbyjabatan, $limit_per_page,$start,NULL);
			$ordbyjabatan = 'b.id_jabatan,riwayat_jabatan.tmt_jabatan asc';
			$data['list_jabatan'] = $this->riwayat_jabatan->find(NULL, array('kd_pegawai' => $id), $ordbyjabatan,$start,$limit_per_page);
			$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
			
			$ordby3 = 'tahun_mulai, tahun_selesai';
			$data['list_jabatan_struktural'] = $this->riwayat_jabatanstruktural->find(NULL, array('kd_pegawai' => $id), $ordby3, $limit_per_page,$start,NULL);
			$data['jabatan_struktural_assoc']=$this->jabatan_struktural->get_jabatanstruktural_assoc('nama_jabatan_s');
			
			$ordby4 = 'tmt_SK';
			$data['list_mutasi'] = $this->riwayat_mutasi->find(NULL, array('mutasi.kd_pegawai' => $id), $ordby4, $start, $limit_per_page);
			$data['list_tugas'] = $this->riwayat_tugas->find(NULL, array('kd_pegawai' => $id), null, $limit_per_page,$start,$ordby);
			
			$data['list_penghargaan'] = $this->riwayat_penghargaan->find(NULL, array('kd_pegawai' => $id), null, $limit_per_page,$start,$ordby);
			$data['jenis_penghargaan_assoc'] = $this->jenis_penghargaan->get_assoc('nama_penghargaan');
			
			$data['list_pasangan']  = $this->pasangan_pegawai->retrieve_by_idpeg_aktif($id);
			
			$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc('status_keluarga');
			$data['list_ortukandung'] = $this->riwayat_keluarga->get_ortu_kandung_assoc($id);
			$data['list_mertua'] = $this->riwayat_keluarga->get_mertua_assoc($id);
			$data['list_saudara'] = $this->riwayat_keluarga->get_saudara_assoc($id);
			
			$data['list_anak'] = $this->anak_pegawai->retrieve_by_idpeg($id);
			
			$data['unit_kerja_assoc']=$this->unit_kerja->get_assoc('nama_unit');
			$data['gender_assoc']=$this->lookup->gender_assoc();
			
			$tgl = date('Y-m-d');
			$bln = $this->lookup->longonthname(date("m"));
			$data['tglsurat'] = date('d').' '.$bln.' '.date('Y');
			
			$query = mysql_query("SELECT * FROM pos_variabel WHERE id_var = 'KP4'");					
			if ($query)	{
				$dataku=mysql_fetch_array($query);  
				$data['mengetahui']= $dataku['keterangan'];
				$data['mengetahui1']= $dataku['keterangan2'];
				$data['mengetahui2']= $dataku['keterangan3'];
				}
		
			$data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
			$data['no_KTP']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'no_KTP');
			$masa_kerja = $this->datediff($data['tmt_cpns'],$tgl);
			$data['mk_tambahan_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_th');
			$data['mk_tambahan_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_bl');
			$data['mk_selisih_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_th');
			$data['mk_selisih_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_bl');	
			$data['masa_kerja_seluruhnya_th']=$masa_kerja['years'] + $data['mk_tambahan_th'];
			$data['masa_kerja_seluruhnya_bl']=$masa_kerja['months'] + $data['mk_tambahan_bl'];

			if($data['masa_kerja_seluruhnya_bl'] >= 12){
				$data['masa_kerja_seluruhnya_th'] = $data['masa_kerja_seluruhnya_th']+1;
				$data['masa_kerja_seluruhnya_bl'] = $data['masa_kerja_seluruhnya_bl'] - 12;
			}

			$data['masa_kerja_seluruhnya'] = $data['masa_kerja_seluruhnya_th']." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
			$tingkatawal=0;
			$tingkatakhir=0;
			$query_mk = mysql_query("SELECT golongan_pangkat.tingkat
FROM riwayat_gol_kepangkatan, `golongan_pangkat`
WHERE riwayat_gol_kepangkatan.id_golpangkat = golongan_pangkat.id_golpangkat AND 
riwayat_gol_kepangkatan.kd_pegawai = '$id' order by riwayat_gol_kepangkatan.id_golpangkat LIMIT 0,1");					
			if ($query_mk)	{
				$datamk=mysql_fetch_array($query_mk);  
				$tingkatawal= $datamk[tingkat];
			}
			$query_mk = mysql_query("SELECT golongan_pangkat.tingkat
FROM pegawai, `golongan_pangkat`
WHERE pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat AND 
pegawai.kd_pegawai = '$id'");					
			if ($query_mk)	{
				$datamk=mysql_fetch_array($query_mk);  
				$tingkatakhir= $datamk[tingkat];
			}
			$data['data_tahun'] = $data['masa_kerja_seluruhnya_th'];
			
			if (($tingkatawal==$tingkatakhir) or ($tingkatawal==3)){
				$data['masa_kerja_golongan'] = $data['masa_kerja_seluruhnya'];
			}
			else if ($tingkatakhir==2){
				$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-6)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
				$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-6;
			}
			else {
				if ($tingkatawal==1){
					$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-11)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
					$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-11;
				}
				else
				{
					$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-5)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
					$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-5;
				}
			}
				
			//$masa_kerja_golongan = $this->datediff($data['tmt_kp'],$tgl);
			
			
			//$data['masa_kerja_golongan_th'] = $masa_kerja_golongan['years']; 
			//$data['masa_kerja_golongan_bl'] = $masa_kerja_golongan['months'];
			//$data['masa_kerja_golongan'] = $data['masa_kerja_golongan_th']." tahun ".$data['masa_kerja_golongan_bl']." bulan";
			if (($data['mk_tambahan_th']>0) and ($data['mk_tambahan_bl']>0))
			{
				$data['masa_kerja_tambahan'] = $data['mk_tambahan_th']." tahun ".$data['mk_tambahan_bl']." bulan";	
			}
			else
			{
				$data['masa_kerja_tambahan'] = '-';
			}
			$data['gaji_pokok_asli']=$this->lookup->get_cari_field2('acuan_gaji_pokok','id_golpangkat','masa_kerja',$data['pangkat_id'],$data['masa_kerja_golongan_th'],'gaji_pokok');
			$data['status_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_pegawai');
			if ($data['status_pegawai']=='CPNS')
			{
				//$data['gaji_pokok'] = $data['gaji_pokok_asli'] +' x 80% = '+($data['gaji_pokok_asli']*0.8);
				$data['gaji_pokok'] = ($data['gaji_pokok_asli']*0.8);
			}
			else
			{
				$data['gaji_pokok'] = $data['gaji_pokok_asli'];
			}
			
			$data['jabatan_lain'] = $this->input->post('jabatan_lain', TRUE);
			$data['penghasilan_lain'] = $this->input->post('penghasilan_lain', TRUE);
			$data['pensiun_janda'] = $this->input->post('pensiun_janda', TRUE);
			$data['list_pasangan']  = $this->pasangan_pegawai->retrieve_by_idpeg_aktif($id);
			$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc('status_keluarga');
			
			$data['list_anak'] = $this->anak_pegawai->retrieve_by_idpeg($id);
			$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			$data['jumlah_anak_masuk_daftar'] = $this->anak_pegawai->jumlah_anak_masuk_daftar($id);
			$data['jumlah_anak'] = $this->anak_pegawai->jumlah_anak($id);
		
		
			if (isset($this->user->user_group)){
				if($singkat == "singkat"){
					$data["kasubag_tendik"] = $this->lookup->get_cari_field('pegawai','id_jabatan_uj', '22','*');
					// var_dump($this->db->last_query());exit();
					$data["wr2"] = $this->lookup->get_cari_field('pegawai','id_jabatan_struktural', '58', '*');
					// var_dump($data["wr2"]);exit();
					if($data["pegawai"]["id_jns_pegawai"] == 5){
						$data['list_jabatan'] = $this->riwayat_jabatan->find(NULL, array('kd_pegawai' => $id), $limit_per_page,$start,$ordby);
						$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
					}
					$html=$this->load->view("pegawai/pegawai/cetak_singkat_drh", $data, true);
				}elseif(is_null($singkat)){
					$html=$this->load->view('pegawai/pegawai/cetak_drh', $data,true);
				}
					$this->printtopdf->htmltopdf($properties,$html);
			} else {
				redirect('/pegawai/pegawai/', 'location');
			}
		
		//$data['action'] = 'cetak_drh/' . $id;
        //$this->template->display('/pegawai/pegawai/cetak_drh', $data);

	}


function cetak_drh_excel($id)
		{
		//$properties=array();
		
		
				      
		$data['pegawai'] = $this->pegawai->retrieve_by_pkey($id);
            $data['kd_pegawai'] = $id;
            $data['NIP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'NIP');
			$data['NPWP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'npwp');
			$data['nama_peg'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'nama_pegawai');
			$data['gelar_depan'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gelar_depan');
			$data['gelar_belakang'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gelar_belakang');
			$data['tempat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tempat_lahir');
			$data['tanggal'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tgl_lahir');
			$data['alamat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'alamat');
			$data['rt'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'rt');
			$data['rw'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'rw');
			$data['no_telp'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'no_telp');
			
			$data['gol_darah'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gol_darah');
			$data['drh_tinggi'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_tinggi');
			$data['drh_bb'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_bb');
			$data['drh_rambut'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_rambut');
			$data['drh_bentuk_muka'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_bentuk_muka');
			$data['drh_warna_kulit'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_warna_kulit');
			$data['drh_ciri_ciri'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_ciri_ciri');
			$data['drh_cacat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_cacat');
			$data['hobi_kesenian'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hobi_kesenian');
			$data['hobi_olahraga'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hobi_olahraga');
			$data['hobi_lain'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hobi_lain');
			$data['drh_nama_kecil'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_nama_kecil');
			
			$data['status_kawin'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_kawin');
			
			$data['kelurahan_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_kelurahan');
			$data['kelurahan_assoc'] = $this->kelurahan->get_assoc('nama_kelurahan');
			$data['status_perkawinan_assoc'] = $this->status_perkawinan->get_assoc('status_perkawinan');
			//$data['kecamatan'] = $this->lookup->get_cari_field('kelurahan','kd_kelurahan',$data['agama_id'],'agama');
			
			$data['jns'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'jns_kelamin');
			if ($data['jns']==1)
				$data['jenis'] = 'Laki-Laki';
			else
				$data['jenis'] = 'Perempuan';
			$data['agama_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_agama');
			$data['agama'] = $this->lookup->get_cari_field('agama','kd_agama',$data['agama_id'],'agama');
			$data['status_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_pegawai');
			if ($data['status_pegawai']==0)
				$data['status_pegawai'] = 'Honorer';
			elseif ($data['status_pegawai']==1){
				$data['status_pegawai_s'] = 'CPNS';
				$data['status_pegawai_p'] = 'Calon Pegawai Negeri Sipil';}
			else{
				$data['status_pegawai_s'] = 'PNS';
				$data['status_pegawai_p'] = 'Pegawai Negeri Sipil';}
				
			$data['id_jns_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jns_pegawai');	
			$data['pangkat_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_golpangkat_terakhir');
			$data['pangkat']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'pangkat');
			$data['golongan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'golongan');
			$data['tmt_pangkat']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_golpangkat_terakhir');
			$data['jabatan_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jabatan_struktural');
			$data['jabatan_f']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jabatan_terakhir');
			$data['id_jns_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jns_pegawai');
			
			
			 
        	$data['jabatan']=$this->lookup->get_cari_field('jabatan_struktural','id_jabatan_s',$data['jabatan_id'],'nama_jabatan_s');
			
			
			$data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
			
			$data['tingkat'] = $this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'tingkat');
			$data['unit_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_unit');
			$data['unit_kerja']=$this->lookup->get_cari_field('unit_kerja','kode_unit',$data['unit_id'],'nama_unit');
			$data['eselon']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'eselon');
			
			if (($data['eselon'] =='non-eselon') and ($data['tingkat']=='1'))
			{
				$data['non_jabatan'] = 'Pembantu Pelaksana'.' Subag '.$data['unit_kerja'];
			}
			elseif (($data['eselon'] =='non-eselon') and ($data['tingkat']=='2'))
			{
				$data['non_jabatan'] = 'Pelaksana'.' Subag '.$data['unit_kerja'];
			}
			elseif (($data['eselon'] =='non-eselon') and ($data['tingkat']=='3'))
			{
				$data['non_jabatan'] = 'Pembantu Pimpinan'.' Subag '.$data['unit_kerja'];
			}
			else
			{
				$data['non_jabatan'] = $data['jabatan'].' '.$data['unit_kerja'];
			}
			$ordby1='id_pendidikan, th_lulus';
			$data['list_pendidikan'] = $this->riwayat_pendidikan->find(NULL, array('kd_pegawai' => $id), $ordby1, $start, null);
        	$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			$data['list_pelatihan'] = $this->riwayat_pelatihan->find(NULL, array('kd_pegawai' => $id), $ordby, $limit_per_page,$start,NULL);
			$ordby2 = 'riwayat_gol_kepangkatan.aktif ,riwayat_gol_kepangkatan.status_pegawai, riwayat_gol_kepangkatan.id_golpangkat, riwayat_gol_kepangkatan.tmt_pangkat';
			$search_param = array();
            $search_param[] = "kd_pegawai = $id";
			$search_param[] = "(keterangan like 'SK KP' OR keterangan like 'SK CPNS' OR keterangan like 'SK PNS')";
			//$search_param[] = "(keterangan like 'KGB' AND aktif='1')";
			$search_param = implode(" AND ", $search_param);
			$data['list_pangkat'] = $this->riwayat_gol_kepangkatan->find( NULL,$search_param, $ordby2, $limit_per_page,$start,NULL);
			$data['golongan_assoc'] = $this->golongan->get_gol_assoc();
			$data['pangkat_assoc'] = $this->golongan->get_pangkat_assoc();
			
			//$ordbyjabatan = 'riwayat_jabatan.id_jabatan,riwayat_jabatan.id_riwayat_jabatan';
			//$data['list_jabatan'] = $this->riwayat_jabatan->find(NULL, array('kd_pegawai' => $id), $ordbyjabatan, $limit_per_page,$start,NULL);
			$ordbyjabatan = 'b.id_jabatan,riwayat_jabatan.tmt_jabatan asc';
			$data['list_jabatan'] = $this->riwayat_jabatan->find(NULL, array('kd_pegawai' => $id), $ordbyjabatan,$start,$limit_per_page);
			$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
			
			$ordby3 = 'tahun_mulai, tahun_selesai';
			$data['list_jabatan_struktural'] = $this->riwayat_jabatanstruktural->find(NULL, array('kd_pegawai' => $id), $ordby3, $limit_per_page,$start,NULL);
			$data['jabatan_struktural_assoc']=$this->jabatan_struktural->get_jabatanstruktural_assoc('nama_jabatan_s');
			
			$ordby4 = 'tmt_SK';
			$data['list_mutasi'] = $this->riwayat_mutasi->find(NULL, array('mutasi.kd_pegawai' => $id), $ordby4, $start, $limit_per_page);
			$data['list_tugas'] = $this->riwayat_tugas->find(NULL, array('kd_pegawai' => $id), null, $limit_per_page,$start,$ordby);
			
			$data['list_penghargaan'] = $this->riwayat_penghargaan->find(NULL, array('kd_pegawai' => $id), null, $limit_per_page,$start,$ordby);
			$data['jenis_penghargaan_assoc'] = $this->jenis_penghargaan->get_assoc('nama_penghargaan');
			
			$data['list_pasangan']  = $this->pasangan_pegawai->retrieve_by_idpeg_aktif($id);
			
			$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc('status_keluarga');
			$data['list_ortukandung'] = $this->riwayat_keluarga->get_ortu_kandung_assoc($id);
			$data['list_mertua'] = $this->riwayat_keluarga->get_mertua_assoc($id);
			$data['list_saudara'] = $this->riwayat_keluarga->get_saudara_assoc($id);
			
			$data['list_anak'] = $this->anak_pegawai->retrieve_by_idpeg($id);
			
			$data['unit_kerja_assoc']=$this->unit_kerja->get_assoc('nama_unit');
			$data['gender_assoc']=$this->lookup->gender_assoc();
			
			$tgl = date('Y-m-d');
			$bln = $this->lookup->longonthname(date("m"));
			$data['tglsurat'] = date('d').' '.$bln.' '.date('Y');
			
			$query = mysql_query("SELECT * FROM pos_variabel WHERE id_var = 'KP4'");					
			if ($query)	{
				$dataku=mysql_fetch_array($query);  
				$data['mengetahui']= $dataku['keterangan'];
				$data['mengetahui1']= $dataku['keterangan2'];
				$data['mengetahui2']= $dataku['keterangan3'];
				}
		
			$data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
			$data['no_KTP']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'no_KTP');
			$masa_kerja = $this->datediff($data['tmt_cpns'],$tgl);
			$data['mk_tambahan_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_th');
			$data['mk_tambahan_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_bl');
			$data['mk_selisih_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_th');
			$data['mk_selisih_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_bl');	
			$data['masa_kerja_seluruhnya_th']=$masa_kerja['years'] + $data['mk_tambahan_th'];
			$data['masa_kerja_seluruhnya_bl']=$masa_kerja['months'] + $data['mk_tambahan_bl'];

			if($data['masa_kerja_seluruhnya_bl'] >= 12){
				$data['masa_kerja_seluruhnya_th'] = $data['masa_kerja_seluruhnya_th']+1;
				$data['masa_kerja_seluruhnya_bl'] = $data['masa_kerja_seluruhnya_bl'] - 12;
			}

			$data['masa_kerja_seluruhnya'] = $data['masa_kerja_seluruhnya_th']." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
			$tingkatawal=0;
			$tingkatakhir=0;
			$query_mk = mysql_query("SELECT golongan_pangkat.tingkat
FROM riwayat_gol_kepangkatan, `golongan_pangkat`
WHERE riwayat_gol_kepangkatan.id_golpangkat = golongan_pangkat.id_golpangkat AND 
riwayat_gol_kepangkatan.kd_pegawai = '$id' order by riwayat_gol_kepangkatan.id_golpangkat LIMIT 0,1");					
			if ($query_mk)	{
				$datamk=mysql_fetch_array($query_mk);  
				$tingkatawal= $datamk[tingkat];
			}
			$query_mk = mysql_query("SELECT golongan_pangkat.tingkat
FROM pegawai, `golongan_pangkat`
WHERE pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat AND 
pegawai.kd_pegawai = '$id'");					
			if ($query_mk)	{
				$datamk=mysql_fetch_array($query_mk);  
				$tingkatakhir= $datamk[tingkat];
			}
			$data['data_tahun'] = $data['masa_kerja_seluruhnya_th'];
			
			if (($tingkatawal==$tingkatakhir) or ($tingkatawal==3)){
				$data['masa_kerja_golongan'] = $data['masa_kerja_seluruhnya'];
			}
			else if ($tingkatakhir==2){
				$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-6)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
				$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-6;
			}
			else {
				if ($tingkatawal==1){
					$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-11)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
					$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-11;
				}
				else
				{
					$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-5)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
					$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-5;
				}
			}
				
			//$masa_kerja_golongan = $this->datediff($data['tmt_kp'],$tgl);
			
			
			//$data['masa_kerja_golongan_th'] = $masa_kerja_golongan['years']; 
			//$data['masa_kerja_golongan_bl'] = $masa_kerja_golongan['months'];
			//$data['masa_kerja_golongan'] = $data['masa_kerja_golongan_th']." tahun ".$data['masa_kerja_golongan_bl']." bulan";
			if (($data['mk_tambahan_th']>0) and ($data['mk_tambahan_bl']>0))
			{
				$data['masa_kerja_tambahan'] = $data['mk_tambahan_th']." tahun ".$data['mk_tambahan_bl']." bulan";	
			}
			else
			{
				$data['masa_kerja_tambahan'] = '-';
			}
			$data['gaji_pokok_asli']=$this->lookup->get_cari_field2('acuan_gaji_pokok','id_golpangkat','masa_kerja',$data['pangkat_id'],$data['masa_kerja_golongan_th'],'gaji_pokok');
			$data['status_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_pegawai');
			if ($data['status_pegawai']=='CPNS')
			{
				//$data['gaji_pokok'] = $data['gaji_pokok_asli'] +' x 80% = '+($data['gaji_pokok_asli']*0.8);
				$data['gaji_pokok'] = ($data['gaji_pokok_asli']*0.8);
			}
			else
			{
				$data['gaji_pokok'] = $data['gaji_pokok_asli'];
			}
			
			$data['jabatan_lain'] = $this->input->post('jabatan_lain', TRUE);
			$data['penghasilan_lain'] = $this->input->post('penghasilan_lain', TRUE);
			$data['pensiun_janda'] = $this->input->post('pensiun_janda', TRUE);
			$data['list_pasangan']  = $this->pasangan_pegawai->retrieve_by_idpeg_aktif($id);
			$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc('status_keluarga');
			
			$data['list_anak'] = $this->anak_pegawai->retrieve_by_idpeg($id);
			$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			$data['jumlah_anak_masuk_daftar'] = $this->anak_pegawai->jumlah_anak_masuk_daftar($id);
			$data['jumlah_anak'] = $this->anak_pegawai->jumlah_anak($id);
		
		
			if (isset($this->user->user_group)){
					$html=$this->load->view('pegawai/pegawai/cetak_drh_excel', $data,true);
					$this->load->view('xls.php',$html);
			} else {
				redirect('/pegawai/pegawai/', 'location');
			}
		
		//$data['action'] = 'cetak_drh/' . $id;
        //$this->template->display('/pegawai/pegawai/cetak_drh', $data);

	}
	
    function delete() {
        $idField = $this->uri->segment(4);
        $data = $this->pegawai->retrieve_by_pkey($idField);

        confirm("Yakin menghapus data pegawai <b>" . $data['nama_pegawai'] . "</b> ?");
		$param=array('id_app' =>'3','key'=>'123','kd_pegawai'=>$idField);
		$wsdl="http://192.168.1.209/webserviceum/server/?wsdl";
		$client=new nusoap_client($wsdl,true);
		$cek=$client->call("checkConnection",array("id_app"=>"2"));
		if($cek[item][0]=='200'):
			$message=$client->call("deleteDataPegawai",$param);
//fungsi hapus disertakan
			$res = $this->pegawai->delete_by_pkey($idField);
//end fungsi hapus
			/*print_r($message);
			echo '<h2>Request</h2>';
			echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
			echo '<h2>Response</h2>';
			echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
			exit;*/  
		else:
			print_r($cek);
			echo '<h2>Request</h2>';
			echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
			echo '<h2>Response</h2>';
			echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
			exit;  
		endif; 
		set_success('Data pegawai berhasil dihapus');
        redirect('/pegawai/pegawai/', 'location');
		
		
		
    }
	
	function arsipkan() {
	 $idField = $this->uri->segment(4);
        $data = $this->pegawai->retrieve_by_pkey($idField);

        confirm("Yakin mengarsipkan data pegawai <b>" . $data['nama_pegawai'] . "</b> ?");
		$query = mysql_query("select * from pegawai where kd_pegawai='".$idField."'");					
		if ($query) {
			$datapeg=mysql_fetch_array($query); 
			/*$query = mysql_query("INSERT INTO pegawai_pensiun (kd_pegawai, NIP, nama_pegawai, gelar_depan, gelar_belakang, tempat_lahir, tgl_lahir, jns_kelamin, 
			kd_agama, status_asal, 	id_jns_pegawai, status_pegawai, status_kawin, alamat, RT, RW, kd_kelurahan, kode_pos, no_telp, gol_darah, 
			tmt_kp, tmt_kgb, kode_unit, tmt_unit_terakhir, kode_unit_induk, id_golpangkat_terakhir, tmt_golpangkat_terakhir, id_jabatan_terakhir,
			tmt_jabatan_terakhir, id_pendidikan_terakhir, ket_pendidikan, id_pasangan_pegawai, no_sk_cpns, tmt_cpns, no_sk_pns, tmt_pns, 
			no_sk_pensiun, tgl_sk_pensiun, npwp, tgl_npwp, karpeg, no_ASKES,taspen, foto, tgl_resign, nip_lama, loker, eselon, angka_kredit, 
			mk_tambahan_th, mk_tambahan_bl, mk_selisih_th, mk_selisih_bl, ket_pensiun) 
			VALUES('".$datapeg['kd_pegawai']."','".$datapeg['NIP']."','".$datapeg['nama_pegawai']."','".$datapeg['gelar_depan']."','".$datapeg['gelar_belakang']."',
			'".$datapeg['tempat_lahir']."','".$datapeg['tgl_lahir']."','".$datapeg['jns_kelamin']."','".$datapeg['kd_agama']."','".$datapeg['status_asal']."',
			'".$datapeg['id_jns_pegawai']."','".$datapeg['status_pegawai']."','".$datapeg['status_kawin']."','".$datapeg['alamat']."','".$datapeg['RT']."',
			'".$datapeg['RW']."','".$datapeg['kd_kelurahan']."','".$datapeg['kode_pos']."','".$datapeg['no_telp']."','".$datapeg['gol_darah']."','".$datapeg['tmt_golpangkat_terakhir']."',
			'".$datapeg['tmt_kgb']."','".$datapeg['kode_unit']."',	'".$datapeg['tmt_unit_terakhir']."','".$datapeg['kode_unit_induk']."',
			'".$datapeg['id_golpangkat_terakhir']."','".$datapeg['tmt_golpangkat_terakhir']."','".$datapeg['id_jabatan_terakhir']."',
			'".$datapeg['tmt_jabatan_terakhir']."','".$datapeg['id_pendidikan_terakhir']."',
			'".$datapeg['ket_pendidikan']."','".$datapeg['id_pasangan_pegawai']."','".$datapeg['no_sk_cpns']."','".$datapeg['tmt_cpns']."',
			'".$datapeg['no_sk_pns']."','".$datapeg['tmt_pns']."','".$datapeg['no_sk_pensiun']."',	'".$datapeg['tgl_sk_pensiun']."','".$datapeg['npwp']."',
			'".$datapeg['tgl_npwp']."','".$datapeg['karpeg']."','".$datapeg['no_ASKES']."','".$datapeg['taspen']."','".$datapeg['foto']."',	
			'".$datapeg['tgl_resign']."','".$datapeg['nip_lama']."','".$datapeg['loker']."','".$datapeg['eselon']."','".$datapeg['angka_kredit']."',
			'".$datapeg['mk_tambahan_th']."','".$datapeg['mk_tambahan_bl']."','".$datapeg['mk_selisih_th']."',
			'".$datapeg['mk_selisih_bl']."','".$datapeg['ket_pensiun']."')");*/
			$query = mysql_query("INSERT INTO pegawai_pensiun (kd_pegawai, NIP, nama_pegawai, gelar_depan, gelar_belakang, tempat_lahir, tgl_lahir, jns_kelamin, 
			kd_agama, status_asal, 	id_jns_pegawai, status_pegawai, status_kawin, alamat, RT, RW, kd_kelurahan, kode_pos, no_telp, gol_darah, 
			tmt_kp, tmt_kgb, kode_unit, tmt_unit_terakhir, kode_unit_induk, id_golpangkat_terakhir, tmt_golpangkat_terakhir, id_jabatan_terakhir,
			tmt_jabatan_terakhir, id_pendidikan_terakhir, ket_pendidikan, id_pasangan_pegawai, no_sk_cpns, tmt_cpns, no_sk_pns, tmt_pns, 
			no_sk_pensiun, tgl_sk_pensiun, npwp, tgl_npwp, karpeg, no_ASKES,taspen, foto, tgl_resign, nip_lama, loker, eselon, angka_kredit, 
			mk_tambahan_th, mk_tambahan_bl, mk_selisih_th, mk_selisih_bl, ket_pensiun,
			hp,email,sandi_dosen,sertifikat_pendidik,nira,nia) 
			VALUES('".$datapeg['kd_pegawai']."','".$datapeg['NIP']."','".$datapeg['nama_pegawai']."','".$datapeg['gelar_depan']."','".$datapeg['gelar_belakang']."',
			'".$datapeg['tempat_lahir']."','".$datapeg['tgl_lahir']."','".$datapeg['jns_kelamin']."','".$datapeg['kd_agama']."','".$datapeg['status_asal']."',
			'".$datapeg['id_jns_pegawai']."','".$datapeg['status_pegawai']."','".$datapeg['status_kawin']."','".$datapeg['alamat']."','".$datapeg['RT']."',
			'".$datapeg['RW']."','".$datapeg['kd_kelurahan']."','".$datapeg['kode_pos']."','".$datapeg['no_telp']."','".$datapeg['gol_darah']."','".$datapeg['tmt_golpangkat_terakhir']."',
			'".$datapeg['tmt_kgb']."','".$datapeg['kode_unit']."',	'".$datapeg['tmt_unit_terakhir']."','".$datapeg['kode_unit_induk']."',
			'".$datapeg['id_golpangkat_terakhir']."','".$datapeg['tmt_golpangkat_terakhir']."','".$datapeg['id_jabatan_terakhir']."',
			'".$datapeg['tmt_jabatan_terakhir']."','".$datapeg['id_pendidikan_terakhir']."',
			'".$datapeg['ket_pendidikan']."','".$datapeg['id_pasangan_pegawai']."','".$datapeg['no_sk_cpns']."','".$datapeg['tmt_cpns']."',
			'".$datapeg['no_sk_pns']."','".$datapeg['tmt_pns']."','".$datapeg['no_sk_pensiun']."',	'".$datapeg['tgl_sk_pensiun']."','".$datapeg['npwp']."',
			'".$datapeg['tgl_npwp']."','".$datapeg['karpeg']."','".$datapeg['no_ASKES']."','".$datapeg['taspen']."','".$datapeg['foto']."',	
			'".$datapeg['tgl_resign']."','".$datapeg['nip_lama']."','".$datapeg['loker']."','".$datapeg['eselon']."','".$datapeg['angka_kredit']."',
			'".$datapeg['mk_tambahan_th']."','".$datapeg['mk_tambahan_bl']."','".$datapeg['mk_selisih_th']."',
			'".$datapeg['mk_selisih_bl']."','".$datapeg['ket_pensiun']."',
			'".$datapeg['hp']."','".$datapeg['email']."','".$datapeg['sandi_dosen']."','".$datapeg['sertifikat_pendidik']."',
			'".$datapeg['nira']."','".$datapeg['nia']."')");
			if ($query) {
				$query = mysql_query("DELETE FROM pegawai where kd_pegawai='".$idField."'");			
				set_success('Data pegawai ' . $datapeg['nama_pegawai'].' berhasil diarsipkan');
				}
			else
			{
			set_success('Data pegawai ' . $datapeg['nama_pegawai'].' gagal diarsipkan');
			}
		}
        //$res = $this->pegawai->delete_by_pkey($idField);
        redirect('/pegawai/pegawai/', 'location');
	
			
    }


    function cari() {
        if ($this->_validate_search()) {
            $val = $this->_get_form_values_search();
            if ($val['category'] == 1) {
                $cari = array('NIP' => $val['search']);
            } elseif ($val['category'] == 2) {
                $cari = array('nama_pegawai' => $val['search']);
            } elseif ($val['category'] == 3) {
                $cari = array('e.nama_unit' => $val['search']);
            }
            $data['pegawai_list'] = $this->pegawai->get_all_by($cari);
            if ($data['pegawai_list'] != false) {
                $data['is_new'] = false;
            } else {
                $data['is_new'] = true;
            }
            //show_error(var_dump($data));
            $data['judul'] = 'Pencarian Pegawai';
            $this->template->display('/pegawai/pegawai/list_pegawai', $data);
        } else {
            $this->template->metas('title', 'SIMPEGA | Pencarian Pegawai');
            $data['judul'] = 'Pencarian Pegawai';
            $data['action'] = 'cari';
            $data['category_assoc'] = array(1 => 'NIP', 2 => 'Nama', 3 => 'Unit Kerja');
            $this->template->display('/pegawai/pegawai/search_pegawai', $data);
        }
    }

    function photo() {
        $kd_pegawai = $this->uri->segment(4);
		//testing fitria
		$extensionList = array("bmp", "jpg", "gif");
		$fileName = $_FILES['gambar']['name'];
		$pisah = explode(".", $fileName);
		$ekstensi = $pisah[1];
		 if (in_array($ekstensi, $extensionList))
			{
				 $config['allowed_types'] = $ekstensi;
			}
		else
			{
				 $config['allowed_types'] = 'jpg';
			}
		//end testing 

		$config['upload_path'] = './public/photo/';
        $config['max_size'] = '100';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $config['overwrite'] = true;
        $config['file_name'] = 'photo_' . $kd_pegawai;
        
        $this->load->library('upload', $config);

        $data['error'] = '';


        $data['kd_pegawai'] = $kd_pegawai;
        if (!$this->upload->do_upload('gambar')) {

            $data['error'] = $this->upload->display_errors();
            $expected_file = PUBLICPATH . 'photo/photo_' . $kd_pegawai . '.jpg';
			if (file_exists($expected_file)){
				$data['photo'] = base_url() . '/public/photo/photo_' . $kd_pegawai . '.jpg';
			}
			else{
				$expected_file = PUBLICPATH . 'photo/photo_' . $kd_pegawai . '.gif';
				if (file_exists($expected_file)){
					$data['photo'] = base_url() . '/public/photo/photo_' . $kd_pegawai . '.gif';
					}
				else{
					$expected_file = PUBLICPATH . 'photo/photo_' . $kd_pegawai . '.bmp';
					if (file_exists($expected_file)){
							$data['photo'] = base_url() . '/public/photo/photo_' . $kd_pegawai . '.bmp';
						}
						else{	
							$data['photo'] = base_url() . '/public/images/image/nophotos.jpg';
						}
					}
				}
			}
            //$default = file_exists($expected_file) ? base_url() . '/public/photo/photo_' . $kd_pegawai . '.jpg' : base_url() . '/public/images/image/nophotos.jpg';
            //$data['photo'] = $default;
			
        else {
            $imagedata = $this->upload->data();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $imagedata['full_path'];
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = FALSE;
            $config['width'] = 120;
            $config['height'] = 150;

            $this->load->library('image_lib', $config);

            $this->image_lib->resize();
            $data['photo'] = base_url() . '/public/photo/' . $imagedata['file_name'];
        }
        //$this->template->display('/pegawai/pegawai/upload_pegawai', $data);

	//testing fitria
		if (isset($this->user->user_group)){
			$this->template->display('/pegawai/pegawai/upload_pegawai', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}
    }

    function cetak($id) {
        $id = $this->uri->segment(4);
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($id);
        $data['kerja'] = $this->kerja->retrieve_by_pkey($id);
        $data['tgl_lahir'] = substr($data['pegawai']['tgl_lahir'], 0, 10);
        $data['tmt_jabatan'] = substr($data['kerja']['tmt_jabatan'], 0, 10);
        $data['tmt_golongan'] = substr($data['kerja']['tmt_golongan'], 0, 10);
        $data['tgl_awal_kerja'] = substr($data['kerja']['tgl_awal_kerja'], 0, 10);
        $data['tgl_awal_kerja_sk'] = substr($data['kerja']['tgl_awal_kerja_sk'], 0, 10);
        $data['tgl_akhir_kerja'] = substr($data['kerja']['tgl_akhir_kerja'], 0, 10);

        $properties = array();
        $properties['title'] = 'Data Induk Pegawai';
        $properties['subject'] = 'Data Induk Pegawai';
        $properties['keywords'] = 'Data Induk Pegawai';
        $properties['papersize'] = "A4";
        $properties['paperlayout'] = 'P';
        $properties['filename'] = 'data_pegawai';

        $the_results['judul'] = "Data Induk Pegawai";
        $html = $this->load->view('pegawai/pegawai/print_pegawai', $data, true);
        $this->printtopdf->htmltopdf($properties, $html);
    }

    function upload($idfield) {
        if ($this->_validate_upload()) {
            $data['FK_pegawai'] = $this->input->post('FK_pegawai', TRUE);
            $cuk = $this->input->post('gambar', TRUE);
            //show_error(var_dump($cuk));
            if ($cuk != '') {
                $config['upload_path'] = './public/images/image/';
                $config['allowed_types'] = 'png|jpg|jpeg';
                $config['max_size'] = '100';
                $config['max_width'] = '400';
                $config['max_height'] = '500';
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('gambar')) {
                    set_error($this->upload->display_errors()); //'Upload error, File Gambar tidak valid!!');
                    redirect('pegawai/pegawai/edit/' . $idfield);
                } else {
                    $gambar = array('upload_data' => $this->upload->data());

                    $gambar = $this->input->xss_clean($gambar);
                    $tags = $_POST['tags'];
                    unset($_POST['tags']);
                    $photo_id = $gambar['upload_data']['file_name'];
                    list($width, $height, $type, $attr) = getimagesize($config['upload_path'] . $photo_id);

                    $this->load->library('image_lib');
                    if ($width > 100 or $height > 50) {
                        //resize image
                        $config['image_library'] = 'GD2';
                        $config['source_image'] = $config['upload_path'] . $photo_id;
                        $config['new_image'] = $config['upload_path'] . $photo_id;
                        $config['create_thumb'] = FALSE;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = 100;
                        $config['height'] = 50;
                        $config['master_dim'] = (abs($width - $config['width']) >= abs($height - $config['height'])) ? 'width' : 'height';
                        $this->image_lib->initialize($config);
                        $this->load->library('image_lib', $config);
                        if (!$this->image_lib->resize())
                            echo $this->image_lib->display_errors();
                    }
                    $data['image'] = $photo_id;
                }
                $this->gambar->delete($idfield);
                $this->gambar->add($data);

                set_success('Foto berhasil di upload');
                redirect('/pegawai/pegawai/edit/' . $idfield);
            }
            else {
                set_error('Upload gagal, tidak ada file yang dipilih!!');
                redirect('pegawai/pegawai/edit/' . $idfield);
            }
        } else {
            $data['FK_pegawai'] = $this->uri->segment(4);
            $data['action'] = 'upload/' . $data['FK_pegawai'];
            //$this->template->display('pegawai/pegawai/upload_pegawai', $data);
		// v3 2015

		if (isset($this->user->user_group)){
			$this->template->display('pegawai/pegawai/upload_pegawai', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}
        }
    }

	function view_kp4($kd_pegawai)
	{
		$data['judul'] = "Detail Data dari: ";
        //$id = $this->uri->segment(4);
        //$this->detail_kp4($kd_pegawai);
	//tes v3 2015
		if (isset($this->user->user_group)){
			$this->detail_kp4($kd_pegawai);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}
	}
	
	function datediff($d1, $d2){
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
    function _clear_form_pegawai() {
        $data['kd_pegawai'] = '';
        $data['NIP'] = '';
        $data['nip_lama'] = '';
        $data['nama_pegawai'] = '';
        $data['gelar_depan'] = '';
        $data['gelar_belakang'] = '';
        $data['tempat_lahir'] = '';
        $data['tgl_lahir'] = '';
        $data['jns_kelamin'] = '';
        $data['kd_agama'] = '';
        $data['status_kawin'] = '';
        $data['alamat'] = '';
        $data['RT'] = '';
        $data['RW'] = '';
        $data['kd_kelurahan'] = '';
        $data['kode_pos'] = '';
        $data['no_telp'] = '';
		$data['hp'] = '';
        $data['gol_darah'] = '';

        $data['status_asal'] = '';
        $data['id_jns_pegawai'] = '';
        $data['status_pegawai'] = '';
		
        //$data['kewarganegaraan']= $this->input->post('kewarganegaraan', TRUE);
        
		$data['kode_unit'] = '';
        $data['tmt_unit_terakhir'] = '';
        $data['tmt_golpangkat_terakhir'] = '';
        $data['tmt_kgb'] = '';
        $data['no_sk_cpns'] = '';
        //$data['tgl_sk_cpns']	= '';
        $data['no_sk_pns'] = '';
        //$data['tgl_sk_pns']		= '';
        $data['no_sk_pensiun'] = '';
        $data['tgl_sk_pensiun'] = '';
        $data['loker'] = '';
        $data['npwp'] = '';
        $data['tgl_npwp'] = '';
        $data['eselon'] = 'non-eselon';
        $data{'angka_kredit'} = '';
        $data['karpeg'] = '';
        $data['no_ASKES'] = '';
        $data['taspen'] = '';
        $data['foto'] = '';
        $data['tgl_resign'] = '';
		$data['ket_pensiun'] = '';
		$data['drh_tinggi'] = '';
		$data['drh_bb'] = '';
		$data['drh_rambut'] = '';
		$data['drh_bentuk_muka'] = '';
		$data['drh_warna_kulit'] = '';
		$data['drh_ciri_ciri'] = '';
		$data['drh_cacat'] = '';
		$data['hobi_kesenian'] = '';
		$data['hobi_olahraga'] = '';
		$data['hobi_lain'] = '';
		$data['drh_nama_kecil'] = '';
		$data['validasi_data'] = '0';
		$data['sertifikasi_laboran'] = '0';
		$data['alamat_asal']='';
		$data['no_KTP'] = '';
		$data['tahun_tunj_profesi'] = '0';
	    $data['tahun_tunj_kehormatan'] = '0';
		//tambahan skp online
		$data['pp_nip'] = '';
		$data['app_nip'] = '';
        return $data;
    }
    function _get_form_values_pegawai() {
        $data['kd_pegawai'] = $this->input->post('kd_pegawai', TRUE);
        $data['NIP'] = $this->input->post('NIP', TRUE);
        $data['nip_lama'] = $this->input->post('nip_lama', TRUE);
        $data['nama_pegawai'] = $this->input->post('nama_pegawai', TRUE);
        $data['gelar_depan'] = $this->input->post('gelar_depan', TRUE);
        $data['gelar_belakang'] = $this->input->post('gelar_belakang', TRUE);
        $data['tempat_lahir'] = $this->input->post('tempat_lahir', TRUE);
        $data['tgl_lahir'] = $this->input->post('tgl_lahir', TRUE);
		  if ($data['tgl_lahir']==''){
		  	  $data['tgl_lahir']='0000-00-00';
		  }
        $data['jns_kelamin'] = $this->input->post('jns_kelamin', TRUE);
        $data['kd_agama'] = $this->input->post('kd_agama', TRUE);
        $data['status_kawin'] = $this->input->post('status_kawin', TRUE);
        $data['alamat'] = $this->input->post('alamat', TRUE);
        $data['RT'] = $this->input->post('rt', TRUE);
        $data['RW'] = $this->input->post('rw', TRUE);
        $data['kd_kelurahan'] = $this->input->post('kd_kelurahan', TRUE);
        $data['kode_pos'] = $this->input->post('kode_pos', TRUE);
        $data['no_telp'] = $this->input->post('no_telp', TRUE);
        $data['gol_darah'] = $this->input->post('gol_darah', TRUE);
        $data['status_asal'] = $this->input->post('status_asal', TRUE);
        $data['id_jns_pegawai'] = $this->input->post('id_jns_pegawai', TRUE);
        $data['status_pegawai'] = $this->input->post('status_pegawai', TRUE);
        //$data['kewarganegaraan']= $this->input->post('kewarganegaraan', TRUE);
        $data['kode_unit'] = $this->input->post('kode_unit', TRUE);
        $data['tmt_golpangkat_terakhir'] = $this->input->post('tmt_golpangkat_terakhir', TRUE);
			if ($data['tmt_golpangkat_terakhir']==''){
		  	  $data['tmt_golpangkat_terakhir']='0000-00-00';
		  	}		
        $data['tmt_kgb'] = $this->input->post('tmt_kgb', TRUE);
			if ($data['tmt_kgb']==''){
		  	  $data['tmt_kgb']='0000-00-00';
		  	}	
        $data['no_sk_cpns'] = $this->input->post('no_sk_cpns', TRUE);
        //$data['tgl_sk_cpns']	= $this->input->post('tgl_sk_cpns', TRUE);
        $data['tmt_cpns'] = $this->input->post('tmt_cpns', TRUE);
			if ($data['tmt_cpns']==''){
		  	  $data['tmt_cpns']='0000-00-00';
		  	}			
		$data['tmt_unit_terakhir'] = $this->input->post('tmt_unit_terakhir', TRUE);
        $data['no_sk_pns'] = $this->input->post('no_sk_pns', TRUE);
        //$data['tgl_sk_pns']		= $this->input->post('tgl_sk_pns', TRUE);
        $data['tmt_pns'] = $this->input->post('tmt_pns', TRUE);
			if ($data['tmt_pns']==''){
		  	  $data['tmt_pns']='0000-00-00';
		  	}		
        $data['no_sk_pensiun'] = $this->input->post('no_sk_pensiun', TRUE);
        $data['tgl_sk_pensiun'] = $this->input->post('tgl_sk_pensiun', TRUE);
		  if ($data['tgl_sk_pensiun']==''){
		  	  $data['tgl_sk_pensiun']='0000-00-00';
		  }		
        $data['loker'] = $this->input->post('loker', TRUE);
        $data['npwp'] = $this->input->post('npwp', TRUE);
        $data['tgl_npwp'] = $this->input->post('tgl_npwp', TRUE);
		  if ($data['tgl_npwp']==''){
		  	  $data['tgl_npwp']='0000-00-00';
		  }		
        $data['eselon'] = $this->input->post('eselon', TRUE);
        $data{'angka_kredit'} = $this->input->post('angka_kredit', TRUE);
        $data['karpeg'] = $this->input->post('karpeg', TRUE);
        $data['no_ASKES'] = $this->input->post('no_ASKES', TRUE);
        $data['taspen'] = $this->input->post('taspen', TRUE);
        $data['foto'] = $this->input->post('foto', TRUE);
        $data['tgl_resign'] = $this->input->post('tgl_resign', TRUE);
		  if ($data['tgl_resign']==''){
		  	  $data['tgl_resign']='0000-00-00';
		  }			
 		$data['ket_pensiun'] = $this->input->post('ket_pensiun', TRUE);
		
		//fitria: tambahan 20120524 u. webservice
		$data['id_jabatan_struktural'] = $this->input->post('id_jabatan_struktural', TRUE);
		$data['id_jabatan_terakhir'] = $this->input->post('id_jabatan_terakhir', TRUE);
		$data['NIDN'] = $this->input->post('NIDN', TRUE);
		$data['sandi_dosen'] = $this->input->post('sandi_dosen', TRUE);
		$data['sertifikat_pendidik'] = $this->input->post('sertifikat_pendidik', TRUE);
		$data['nira'] = $this->input->post('nira', TRUE);
		$data['nia'] = $this->input->post('nia', TRUE);
		$data['hp'] = $this->input->post('hp', TRUE);
		$data['email'] = $this->input->post('email', TRUE);
		//end tambahan
		
		//fitria: tambahan 20121031 u/drh
				$data['drh_tinggi'] = $this->input->post('drh_tinggi', TRUE);
				$data['drh_bb'] = $this->input->post('drh_bb', TRUE);
				$data['drh_rambut'] = $this->input->post('drh_rambut', TRUE);
				$data['drh_bentuk_muka'] = $this->input->post('drh_bentuk_muka', TRUE);
				$data['drh_warna_kulit'] = $this->input->post('drh_warna_kulit', TRUE);
				$data['drh_ciri_ciri'] = $this->input->post('drh_ciri_ciri', TRUE);
				$data['drh_cacat'] = $this->input->post('drh_cacat', TRUE);
				$data['hobi_kesenian'] = $this->input->post('hobi_kesenian', TRUE);
				$data['hobi_olahraga'] = $this->input->post('hobi_olahraga', TRUE);
				$data['hobi_lain'] = $this->input->post('hobi_lain', TRUE);
				$data['drh_nama_kecil'] = $this->input->post('drh_nama_kecil', TRUE);
				$data['alamat_asal'] = $this->input->post('alamat_asal', TRUE);
				$data['no_KTP'] = $this->input->post('no_KTP', TRUE);
				$data['tahun_tunj_profesi'] = $this->input->post('tahun_tunj_profesi', TRUE);
				$data['tahun_tunj_kehormatan'] = $this->input->post('tahun_tunj_kehormatan', TRUE);
		//end tambahan
		
		//tambahan validasi_data
		$data['validasi_data'] = $this->input->post('validasi_data', TRUE);
		//tambahan sertifikasi_laboran
		$data['sertifikasi_laboran'] = $this->input->post('sertifikasi_laboran', TRUE);
		//tambahan skp online fitria
		$data['pp_nip'] = $this->input->post('pp_nip', TRUE);
		$data['app_nip'] = $this->input->post('app_nip', TRUE);
        return $data;
    }


    function _validate() {
      
        $this->form_validation->set_rules('NIP', 'NIP', 'required');
        $this->form_validation->set_rules('nama_pegawai', 'nama_pegawai', 'required');
        
        return $this->form_validation->run();
    }

    function _clear_form_search() {
        $data['is_new'] = '';
        $data['search'] = '';
        $data['category'] = '';
        return $data;
    }

    function _get_form_values_search() {
        $data['search'] = $this->input->post('search', TRUE);
        $data['category'] = $this->input->post('category', TRUE);
        return $data;
    } 

    function _validate_search() {
        $this->form_validation->set_rules('search', 'search', 'required');
        $this->form_validation->set_rules('category', 'category', 'required');
        return $this->form_validation->run();
    }

    function _validate_upload() {
        $this->form_validation->set_rules('FK_pegawai', 'FK_pegawai', 'required');
        return $this->form_validation->run();
    }
	
	
	
	function form_verifikasi($id)
		{
		//$properties=array();
		
		
		$properties['papersize']="Folio";
		$properties['paperlayout']='P';		      
		$data['pegawai'] = $this->pegawai->retrieve_by_pkey($id);
            $data['kd_pegawai'] = $id;
            $data['NIP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'NIP');
			$data['NPWP'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'npwp');
			$data['nama_peg'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'nama_pegawai');
			$data['gelar_depan'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gelar_depan');
			$data['gelar_belakang'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gelar_belakang');
			$data['tempat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tempat_lahir');
			$data['tanggal'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tgl_lahir');
			$data['alamat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'alamat');
			$data['alamat_asal'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'alamat_asal');
			$data['rt'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'rt');
			$data['rw'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'rw');
			$data['hp'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hp');
			$data['no_telp'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'no_telp');
			$data['id_prodi'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_prodi');
			
			$data['gol_darah'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'gol_darah');
			$data['drh_tinggi'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_tinggi');
			$data['drh_bb'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_bb');
			$data['drh_rambut'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_rambut');
			$data['drh_bentuk_muka'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_bentuk_muka');
			$data['drh_warna_kulit'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_warna_kulit');
			$data['drh_ciri_ciri'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_ciri_ciri');
			$data['drh_cacat'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_cacat');
			$data['hobi_kesenian'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hobi_kesenian');
			$data['hobi_olahraga'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hobi_olahraga');
			$data['hobi_lain'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'hobi_lain');
			$data['drh_nama_kecil'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'drh_nama_kecil');
			
			$data['status_kawin'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_kawin');
			
			$data['kelurahan_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_kelurahan');
			$data['kelurahan_assoc'] = $this->kelurahan->get_assoc('nama_kelurahan');
			$data['status_perkawinan_assoc'] = $this->status_perkawinan->get_assoc('status_perkawinan');
			//$data['kecamatan'] = $this->lookup->get_cari_field('kelurahan','kd_kelurahan',$data['agama_id'],'agama');
			
			$data['jns'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'jns_kelamin');
			if ($data['jns']==1)
				$data['jenis'] = 'Laki-Laki';
			else
				$data['jenis'] = 'Perempuan';
			$data['agama_id'] = $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kd_agama');
			$data['agama'] = $this->lookup->get_cari_field('agama','kd_agama',$data['agama_id'],'agama');
			$data['status_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_pegawai');
			if ($data['status_pegawai']==0)
				$data['status_pegawai'] = 'Honorer';
			elseif ($data['status_pegawai']==1){
				$data['status_pegawai_s'] = 'CPNS';
				$data['status_pegawai_p'] = 'Calon Pegawai Negeri Sipil';}
			else{
				$data['status_pegawai_s'] = 'PNS';
				$data['status_pegawai_p'] = 'Pegawai Negeri Sipil';}
				
			$data['id_jns_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jns_pegawai');	
			$data['pangkat_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_golpangkat_terakhir');
			$data['pangkat']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'pangkat');
			$data['golongan']=$this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'golongan');
			$data['tmt_pangkat']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_golpangkat_terakhir');
			$data['jabatan_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'id_jabatan_struktural');
        	$data['jabatan']=$this->lookup->get_cari_field('jabatan_struktural','id_jabatan_s',$data['jabatan_id'],'nama_jabatan_s');
			$data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
			$data['email']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'email');
			
			$data['tingkat'] = $this->lookup->get_cari_field('golongan_pangkat','id_golpangkat',$data['pangkat_id'],'tingkat');
			$data['unit_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_unit');
			$data['unit_induk_id']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'kode_unit_induk');
			$data['unit_kerja']=$this->lookup->get_cari_field('unit_kerja','kode_unit',$data['unit_id'],'nama_unit');
			$data['eselon']= $this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'eselon');
			if (($data['eselon'] =='non-eselon') and ($data['tingkat']=='1'))
			{
				$data['non_jabatan'] = 'Pembantu Pelaksana'.' Subag '.$data['unit_kerja'];
			}
			elseif (($data['eselon'] =='non-eselon') and ($data['tingkat']=='2'))
			{
				$data['non_jabatan'] = 'Pelaksana'.' Subag '.$data['unit_kerja'];
			}
			elseif (($data['eselon'] =='non-eselon') and ($data['tingkat']=='3'))
			{
				$data['non_jabatan'] = 'Pembantu Pimpinan'.' Subag '.$data['unit_kerja'];
			}
			else
			{
				$data['non_jabatan'] = $data['jabatan'].' '.$data['unit_kerja'];
			}
			$ordby1='id_pendidikan, th_lulus';
			$search_param = array();
            $search_param[] = "kd_pegawai = $id";
			$search_param[] = "(id_pendidikan > 8 )";
			$search_param = implode(" AND ", $search_param);
			//$data['list_pendidikan'] = $this->riwayat_pendidikan->find(NULL, array('kd_pegawai' => $id,'id_pendidikan'=>8), $ordby1, $start, null);
			$data['list_pendidikan'] = $this->riwayat_pendidikan->find(NULL, $search_param, $ordby1, $start, null);
        	$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			$data['list_pelatihan'] = $this->riwayat_pelatihan->find(NULL, array('kd_pegawai' => $id), $ordby, $limit_per_page,$start,NULL);
			
			$ordby2 = 'riwayat_gol_kepangkatan.status_pegawai, riwayat_gol_kepangkatan.tmt_pangkat, riwayat_gol_kepangkatan.id_riwayat_gol';
			$search_param = array();
            $search_param[] = "kd_pegawai = $id";
			$search_param[] = "(keterangan like 'SK KP' OR keterangan like 'SK CPNS' OR keterangan like 'SK PNS')";
			//$search_param[] = "(keterangan like 'KGB' AND aktif='1')";
			$search_param = implode(" AND ", $search_param);
			$data['list_pangkat'] = $this->riwayat_gol_kepangkatan->find( NULL,$search_param, $ordby2, $limit_per_page,$start,NULL);
			$data['golongan_assoc'] = $this->golongan->get_gol_assoc();
			$data['pangkat_assoc'] = $this->golongan->get_pangkat_assoc();
			
			$ordby3 = 'riwayat_jabatan.tmt_jabatan';
$data['list_jabatan'] = $this->riwayat_jabatan->find(NULL, array('kd_pegawai' => $id), $ordby3, $limit_per_page,$start,NULL);
			$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
			
			$ordby3 = 'tahun_mulai, tahun_selesai';
			$data['list_jabatan_struktural'] = $this->riwayat_jabatanstruktural->find(NULL, array('kd_pegawai' => $id), $ordby3, $limit_per_page,$start,NULL);
			$data['jabatan_struktural_assoc']=$this->jabatan_struktural->get_jabatanstruktural_assoc('nama_jabatan_s');
			
			$ordby4 = 'tmt_SK';
			$data['list_mutasi'] = $this->riwayat_mutasi->find(NULL, array('mutasi.kd_pegawai' => $id), $ordby4, $start, $limit_per_page);
			$data['list_tugas'] = $this->riwayat_tugas->find(NULL, array('kd_pegawai' => $id), null, $limit_per_page,$start,$ordby);
			
			$data['list_penghargaan'] = $this->riwayat_penghargaan->find(NULL, array('kd_pegawai' => $id), null, $limit_per_page,$start,$ordby);
			$data['jenis_penghargaan_assoc'] = $this->jenis_penghargaan->get_assoc('nama_penghargaan');
			
			$data['list_pasangan']  = $this->pasangan_pegawai->retrieve_by_idpeg_aktif($id);
			
			$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc('status_keluarga');
			$data['list_ortukandung'] = $this->riwayat_keluarga->get_ortu_kandung_assoc($id);
			$data['list_mertua'] = $this->riwayat_keluarga->get_mertua_assoc($id);
			$data['list_saudara'] = $this->riwayat_keluarga->get_saudara_assoc($id);
			
			$data['list_anak'] = $this->anak_pegawai->retrieve_by_idpeg($id);
			
			$data['unit_kerja_assoc']=$this->unit_kerja->get_assoc('nama_unit');
			$data['gender_assoc']=$this->lookup->gender_assoc();
			
			$tgl = date('Y-m-d');
			$bln = $this->lookup->longonthname(date("m"));
			$data['tglsurat'] = date('d').' '.$bln.' '.date('Y');
			$data['jamsurat'] = gmdate("H:i:s", time()+60*60*7); # WIB 
			
			$query = mysql_query("SELECT * FROM pos_variabel WHERE id_var = 'KP4'");					
			if ($query)	{
				$dataku=mysql_fetch_array($query);  
				$data['mengetahui']= $dataku['keterangan'];
				$data['mengetahui1']= $dataku['keterangan2'];
				$data['mengetahui2']= $dataku['keterangan3'];
				}
		
			$data['tmt_cpns']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'tmt_cpns');
			$data['no_KTP']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'no_KTP');
			$masa_kerja = $this->datediff($data['tmt_cpns'],$tgl);
			$data['mk_tambahan_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_th');
			$data['mk_tambahan_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_tambahan_bl');
			$data['mk_selisih_th']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_th');
			$data['mk_selisih_bl']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'mk_selisih_bl');	
			$data['masa_kerja_seluruhnya_th']=$masa_kerja['years'] + $data['mk_tambahan_th'];
			$data['masa_kerja_seluruhnya_bl']=$masa_kerja['months'] + $data['mk_tambahan_bl'];
			$data['masa_kerja_seluruhnya'] = $data['masa_kerja_seluruhnya_th']." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
			$tingkatawal=0;
			$tingkatakhir=0;
			$query_mk = mysql_query("SELECT golongan_pangkat.tingkat
FROM riwayat_gol_kepangkatan, `golongan_pangkat`
WHERE riwayat_gol_kepangkatan.id_golpangkat = golongan_pangkat.id_golpangkat AND 
riwayat_gol_kepangkatan.kd_pegawai = '$id' order by riwayat_gol_kepangkatan.id_golpangkat LIMIT 0,1");					
			if ($query_mk)	{
				$datamk=mysql_fetch_array($query_mk);  
				$tingkatawal= $datamk[tingkat];
			}
			$query_mk = mysql_query("SELECT golongan_pangkat.tingkat
FROM pegawai, `golongan_pangkat`
WHERE pegawai.id_golpangkat_terakhir = golongan_pangkat.id_golpangkat AND 
pegawai.kd_pegawai = '$id'");					
			if ($query_mk)	{
				$datamk=mysql_fetch_array($query_mk);  
				$tingkatakhir= $datamk[tingkat];
			}
			$data['data_tahun'] = $data['masa_kerja_seluruhnya_th'];
			
			if (($tingkatawal==$tingkatakhir) or ($tingkatawal==3)){
				$data['masa_kerja_golongan'] = $data['masa_kerja_seluruhnya'];
			}
			else if ($tingkatakhir==2){
				$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-6)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
				$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-6;
			}
			else {
				if ($tingkatawal==1){
					$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-11)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
					$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-11;
				}
				else
				{
					$data['masa_kerja_golongan'] =($data['masa_kerja_seluruhnya_th']-5)." tahun ".$data['masa_kerja_seluruhnya_bl']." bulan";
					$data['data_tahun'] = $data['masa_kerja_seluruhnya_th']-5;
				}
			}
				
			//$masa_kerja_golongan = $this->datediff($data['tmt_kp'],$tgl);
			
			
			//$data['masa_kerja_golongan_th'] = $masa_kerja_golongan['years']; 
			//$data['masa_kerja_golongan_bl'] = $masa_kerja_golongan['months'];
			//$data['masa_kerja_golongan'] = $data['masa_kerja_golongan_th']." tahun ".$data['masa_kerja_golongan_bl']." bulan";
			if (($data['mk_tambahan_th']>0) and ($data['mk_tambahan_bl']>0))
			{
				$data['masa_kerja_tambahan'] = $data['mk_tambahan_th']." tahun ".$data['mk_tambahan_bl']." bulan";	
			}
			else
			{
				$data['masa_kerja_tambahan'] = '-';
			}
			$data['gaji_pokok_asli']=$this->lookup->get_cari_field2('acuan_gaji_pokok','id_golpangkat','masa_kerja',$data['pangkat_id'],$data['masa_kerja_golongan_th'],'gaji_pokok');
			$data['status_pegawai']=$this->lookup->get_cari_field('pegawai','kd_pegawai',$data['kd_pegawai'],'status_pegawai');
			if ($data['status_pegawai']=='CPNS')
			{
				//$data['gaji_pokok'] = $data['gaji_pokok_asli'] +' x 80% = '+($data['gaji_pokok_asli']*0.8);
				$data['gaji_pokok'] = ($data['gaji_pokok_asli']*0.8);
			}
			else
			{
				$data['gaji_pokok'] = $data['gaji_pokok_asli'];
			}
			
			$data['jabatan_lain'] = $this->input->post('jabatan_lain', TRUE);
			$data['penghasilan_lain'] = $this->input->post('penghasilan_lain', TRUE);
			$data['pensiun_janda'] = $this->input->post('pensiun_janda', TRUE);
			$data['list_pasangan']  = $this->pasangan_pegawai->retrieve_by_idpeg_aktif($id);
			$data['status_keluarga_assoc'] = $this->status_keluarga->get_assoc('status_keluarga');
			
			$data['list_anak'] = $this->anak_pegawai->retrieve_by_idpeg($id);
			$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			$data['jumlah_anak_masuk_daftar'] = $this->anak_pegawai->jumlah_anak_masuk_daftar($id);
			$data['jumlah_anak'] = $this->anak_pegawai->jumlah_anak($id);
		$html=$this->load->view('pegawai/pegawai/form_verifikasi', $data,true);
		$this->printtopdf->htmltopdf($properties,$html);
		$this->SetAutoPageBreak(false,0); 
		
		//$data['action'] = 'cetak_drh/' . $id;
        //$this->template->display('/pegawai/pegawai/form_verifikasi', $data);

	}



	
	

}
