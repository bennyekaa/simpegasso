<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatpenggajian extends Member_Controller
{
	function Riwayatpenggajian()
	{
		parent::Member_Controller();
		$this->load->model('golongan_model', 'golongan');
		$this->load->model('riwayat_penggajian_model','riwayat_penggajian');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model','lookup');
		$this->load->model('lap_gaji_model','lap_gaji');
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Penggajian Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		$ordby = 'id_rincian_kgb';
		$data['list_gaji'] = $this->riwayat_penggajian->find(NULL, array('kd_pegawai' => $kd_pegawai), null, $limit_per_page,$start,$ordby);
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Gaji dari: " . $data['pegawai']['nama_pegawai'];
		//$data['golongan_assoc'] = $this->golongan->get_assoc();
		$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
		$this->template->display('pegawai/riwayatpenggajian/list_riwayatpenggajian', $data);
	}
	/*function browse($group=0)
	{
		$pegawai_id = $this->pegawai->d_pegawai);
		$data['list_pasangan'] = $this->pasangan_pegawai->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= "Pasangan Pegawai";
		$this->template->display('pegawai/pasangan/list', $data);
	}*/
	
	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			//$data['kd_pegawai'] = $this->pegawai->get_namapegawai_by_idpegawai($this->pegawai->kd_pegawai);
			if ($data['aktif']=='1')
            {
				//$data_pegawai['id_golpangkat_terakhir'] = $data['id_golpangkat'];
				//$data_pegawai['status_pegawai'] = $data['status_pegawai'];
				$data_pegawai['tmt_kgb']= $data['tmt_kgb'];
				
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				
				$data_status['aktif'] = '0';
				$this->riwayat_penggajian->update_status($kd_pegawai,$data_status);
			}
			$this->riwayat_penggajian->add($data);
            
			set_success('Data gaji pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatpenggajian/add/' . $kd_pegawai);
			//redirect('/pegawai/riwayatpenggajian/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Gaji Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Gaji dari: ' . $data['pegawai']['nama_pegawai'];
			$data['id_rincian_kgb']=$this->riwayat_penggajian->get_id();
			$idgol_peg = $gol_peg[$kd_pegawai];
			//id_golpeg di defaultkan + 1
			$idgol_peg_now = $idgol_peg + 1;
			$gol = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','golongan');
			$pkt = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','pangkat');
			$golongan = $gol[$idgol_peg_now];
			$pangkat = $pkt[$idgol_peg_now];
			if ($golongan=='') {
						$golongan = $gol[$idgol_peg];
						$pangkat = $pkt[$idgol_peg];					
						$data['golongan_assoc'] =  array( $idgol_peg => $pangkat.', '.$golongan) + $this->golongan->get_assoc();
						$idgol_peg_now = $idgol_peg;
				}
			else{
				$data['golongan_assoc'] =  array( $idgol_peg_now => $pangkat.', '.$golongan) + $this->golongan->get_assoc();
			}
			$data['status_assoc'] = $this->lookup->status_assoc();
			$this->template->display('/pegawai/riwayatpenggajian/detail_riwayatpenggajian', $data);
		}
	}
	
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  		'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_rincian_kgb'] = $id;
			$kd_pegawai = $this->input->post('kd_pegawai');
			if ($data['aktif']=='1')
            {
				
				$data_pegawai['tmt_kgb']= $data['tmt_kgb'];
				
				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				$data_status['aktif'] = '0';
				
				$this->riwayat_penggajian->update_status($kd_pegawai,$data_status);
				
			}
				$this->riwayat_penggajian->update($id, $data);
			set_success('Perubahan data gaji pegawai berhasil disimpan');
			redirect('/pegawai/riwayatpenggajian/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Gaji Pegawai :: Ubah');
			$data = $this->riwayat_penggajian->retrieve_by_pkey($id);
			
			
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				$data['golongan_assoc'] = $this->golongan->get_assoc();
				$data['status_assoc'] = $this->lookup->status_assoc();
				$data['judul']='Edit Gaji dari: '. $data['pegawai']['nama_pegawai'];
				$this->template->display('/pegawai/riwayatpenggajian/detail_riwayatpenggajian', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatpenggajian', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_penggajian->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Gaji Pegawai :: Hapus');
		confirm("Yakin menghapus data gaji pegawai?");
		$res = $this->riwayat_penggajian->delete($idField);
		set_success('Data Gaji pegawai berhasil dihapus');
		redirect('/pegawai/riwayatpenggajian/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_rincian_kgb']	= '';
		$data['kd_pegawai']	= '';
		$data['tgl_surat_kgb']	= '';
		$data['nomor_kgb']	= '';
		$data['tmt_kgb']	= '';
		$data['gaji_pokok']	= '';
		$data['aktif']	= '';
		$data['mk_tahun']	= '';
		$data['mk_bulan']	= '';
		$data['atas_nama']	= '';
		$data['nama_pejabat']	= '';
		$data['nip_pejabat']	= '';
		$data['id_golpangkat']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
	   
		$data['id_rincian_kgb']	= $this->riwayat_penggajian->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['tgl_surat_kgb']	= $this->input->post('tgl_surat_kgb', TRUE);
		$data['nomor_kgb']	= $this->input->post('nomor_kgb', TRUE);
		$data['tmt_kgb']	= $this->input->post('tmt_kgb', TRUE);
		$data['gaji_pokok']	= $this->input->post('gaji_pokok', TRUE);
		$data['mk_tahun']	= $this->input->post('mk_tahun', TRUE);
		$data['mk_bulan']	= $this->input->post('mk_bulan', TRUE);
		$data['atas_nama']	= $this->input->post('atas_nama', TRUE);
		$data['nama_pejabat']	= $this->input->post('nama_pejabat', TRUE);
		$data['nip_pejabat']	= $this->input->post('nip_pejabat', TRUE);
		$data['aktif']	= $this->input->post('aktif', TRUE);
		$data['id_golpangkat']	= $this->input->post('id_golpangkat', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('kd_pegawai', 'kd_pegawai', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}