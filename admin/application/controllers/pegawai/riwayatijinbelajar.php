<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatijinbelajar extends Member_Controller
{
	function Riwayatijinbelajar()
	{
		parent::Member_Controller();
		$this->load->model('golongan_model', 'golongan');
		$this->load->model('pendidikan_model', 'pendidikan');
		$this->load->model('riwayat_tugas_belajar_model','riwayat_tugas_belajar');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('lookup_model','lookup');
		$this->load->model('lap_gaji_model','lap_gaji');
		
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
		$this->template->metas('title', 'SIMPEGA | Ijin Belajar Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		$ordby = 'tmt_tb';
		//v3 tambahan tb_ib
		$data['tb_ib'] = 'ib';
		$data['list_tb'] = $this->riwayat_tugas_belajar->find(NULL, array('kd_pegawai' => $kd_pegawai,'tb_ib'=>'ib'), $ordby, $start,$limit_per_page);
		//$data['list_tb'] = $this->riwayat_tugas_belajar->find(NULL, array('kd_pegawai' => $kd_pegawai), $ordby, $start,$limit_per_page);
		//end tambahan
		
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
		$data['judul'] 		= "Data Ijin Belajar dari: " . $data['pegawai']['nama_pegawai'];
		$data['golongan_assoc'] = $this->golongan->get_assoc();
		$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
		$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
		
		if (isset($this->user->user_group)){
		$this->template->display('pegawai/riwayattugasbelajar/list_riwayat_tb', $data);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}

	}
	
	function add()
	{
		if ($this->_validate())
		{
			$kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			//v3 tambahan tb_ib
			$data['tb_ib'] = 'ib';
			//end tambahan
			$this->riwayat_tugas_belajar->add($data);
            
			set_success('Data ijin belajar pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatijinbelajar/index/' . $kd_pegawai.'/ib');
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Ijin Belajar :: Tambah');
			$data = $this->_clear_form();
			//v3 tambahan tb_ib
			$data['tb_ib'] = 'ib';
			//end tambahan
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Ijin Belajar dari: ' . $data['pegawai']['nama_pegawai'];
			$data['id_riwayat_tb']=$this->riwayat_tugas_belajar->get_id();
			$data['status_assoc'] = $this->lookup->status_assoc();
			$data['perpanjangan_assoc'] = $this->lookup->perpanjangan_assoc();
			$data['tunjangan_assoc'] = $this->lookup->tunjangan_assoc();
			$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			$this->template->display('/pegawai/riwayattugasbelajar/detail_riwayat_tb', $data);
		}
	}
	
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  		'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_tb'] = $id;
			//v3 tambahan tb_ib
			$data['tb_ib'] = 'ib';
			//end tambahan
			$this->riwayat_tugas_belajar->update($id, $data);
			set_success('Perubahan data ijin belajar pegawai berhasil disimpan');
			redirect('/pegawai/riwayatijinbelajar/index/'. $data['kd_pegawai'].'/ib', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Ijin Belajar :: Ubah');
			$data = $this->riwayat_tugas_belajar->retrieve_by_pkey($id);
			//v3 tambahan tb_ib
			$data['tb_ib'] = 'ib';
			//end tambahan
			$data['golongan_assoc'] = $this->golongan->get_assoc();
            $data['status_assoc'] = $this->lookup->status_assoc();
			$data['perpanjangan_assoc'] = $this->lookup->perpanjangan_assoc();
			$data['tunjangan_assoc'] = $this->lookup->tunjangan_assoc();
			$data['status_pegawai_assoc'] = $this->lookup->status_pegawai_assoc();
			$data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				
				$data['judul']='Edit Ijin Belajar dari: '. $data['pegawai']['nama_pegawai'];
				$this->template->display('/pegawai/riwayattugasbelajar/detail_riwayat_tb', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayattugasbelajar', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_tugas_belajar->retrieve_by_pkey($idField);
		//v3 tambahan tb_ib
		$data['tb_ib'] = 'ib';
		//end tambahan
		$this->template->metas('title', 'SIMPEGA | Ijin Belajar :: Hapus');
		confirm("Yakin menghapus data tugas belajar dosen?");
		$res = $this->riwayat_tugas_belajar->delete($idField);
		set_success('Data Ijin Belajar pegawai berhasil dihapus');
		redirect('/pegawai/riwayatijinbelajar/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_tb']	= '';
		$data['kd_pegawai']	= '';
		$data['tmt_tb']	= '';
		$data['batas_akhir_tb']	= '';
		$data['id_pendidikan_tb']	= '';
		$data['bidang_studi_tb']	= '';
		$data['tempat_studi_tb']	= '';
		$data['biaya_tb']	= '';
		$data['aktif']	= '';
		$data['tmt_pk']	= '';
		$data['keterangan']	= '';
				
		$data['no_SK_tb']	= '';
		$data['tgl_SK_tb']	= '';
		
		$data['no_surat_bebas']	= '';
		$data['tgl_surat_bebas']	= '';

		$data['tb_ib']	= 'ib';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
	   	$data['id_riwayat_tb']	= $this->riwayat_tugas_belajar->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['tmt_tb']	= $this->input->post('tmt_tb', TRUE);
		$data['batas_akhir_tb']	= $this->input->post('batas_akhir_tb', TRUE);
		$data['id_pendidikan_tb']	= $this->input->post('id_pendidikan_tb', TRUE);
		$data['bidang_studi_tb']	= $this->input->post('bidang_studi_tb', TRUE);
		$data['tempat_studi_tb']	= $this->input->post('tempat_studi_tb', TRUE);
		$data['biaya_tb']	= $this->input->post('biaya_tb', TRUE);
		$data['aktif']	= $this->input->post('aktif', TRUE);
		$data['tmt_pk']	= $this->input->post('tmt_pk', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		
		$data['no_SK_tb']	= $this->input->post('no_SK_tb', TRUE);
		$data['tgl_SK_tb']	= $this->input->post('tgl_SK_tb', TRUE);
		$data['tb_ib']	= $this->input->post('tb_ib', TRUE);
		
		
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('tempat_studi_tb', 'tempat_studi_tb', 'required');
		return $this->form_validation->run();
	}
}