<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class riwayatmutasi extends Member_Controller
{
	function riwayatmutasi()
	{
		parent::Member_Controller();
		$this->load->model('jabatan_model', 'jabatan');
		$this->load->model('jenis_mutasi_model', 'jenis_mutasi');
		$this->load->model('riwayat_mutasi_model','riwayat_mutasi');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('unit_kerja_model', 'unit_kerja');
        $this->load->model('lookup_model','lookup');
		$this->load->model('golongan_model', 'golongan');
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
        $this->template->metas('title', 'SIMPEGA | Mutasi Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		$ordby = 'tgl_mutasi asc';
		$data['list_mutasi'] = $this->riwayat_mutasi->find(NULL, array('mutasi.kd_pegawai' => $kd_pegawai), $ordby, $start, $limit_per_page);		
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
        $data['judul'] 		= "Data Mutasi dari: " . $data['pegawai']['nama_pegawai'];
		$data['jabatan_assoc']=$this->jabatan->get_jabatan_assocs('nama_jabatan_s');
		$data['unit_kerja_assoc']=$this->unit_kerja->get_assoc('nama_unit');
		$data['gol_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
		$data['option_unit']=$this->unit_kerja->get_unit('1');
		$data['golongan_assoc'] = $this->golongan->get_assoc();
		$this->template->display('pegawai/riwayatmutasi/list_mutasi', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
            $kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_jabatan_terakhir'] = $data['id_jabatan'];
				$data_pegawai['tmt_SK_terakhir'] = $data['tmt_SK'];

				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				
				$data_status['aktif'] = '0';
				$this->riwayat_mutasi->update_status($kd_pegawai,$data_status);
			}
			$this->riwayat_mutasi->add($data);
			set_success('Data mutasi pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatmutasi/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Mutasi Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Mutasi dari: ' . $data['pegawai']['nama_pegawai'];
			
			$data['id_mutasi']=$this->riwayat_mutasi->get_id();
			//$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
			//$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
			$data['jabatan_assoc']=$this->jabatan->get_jabatan_assocs('nama_jabatan_s');
            $data['status_assoc'] = $this->lookup->status_assoc();
			$data['jenis_mutasi_assoc'] = array(0=>"-- Pilih Jenis Mutasi --")+$this->jenis_mutasi->get_assoc("jenis_mutasi");
			$data['option_unit']=array(0=>"-")+$this->unit_kerja->get_unit('1');
			$data['golongan_assoc'] = $this->golongan->get_assoc();
			$this->template->display('/pegawai/riwayatmutasi/detail_mutasi', $data);
		}
	}
	
	function edit($id)
	{
		
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_mutasi'] = $id;
			$this->riwayat_mutasi->update($id, $data);
			set_success('Perubahan data mutasi pegawai berhasil disimpan');
			redirect('/pegawai/riwayatmutasi/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Mutasi Pegawai :: Ubah');
			$data = $this->riwayat_mutasi->retrieve_by_pkey($id);
			//$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
			
			//$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
			$data['jabatan_assoc']=$this->jabatan->get_jabatan_assocs('nama_jabatan_s');
            $data['status_assoc'] = $this->lookup->status_assoc();
			$data['golongan_assoc'] = $this->golongan->get_assoc();
			$data['jenis_mutasi_assoc'] = array(0=>"-- Pilih Jenis Mutasi --")+$this->jenis_mutasi->get_assoc("jenis_mutasi");
			
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				$data['judul']='Edit Data Mutasi dari: '. $data['pegawai']['nama_pegawai'];
				$data['option_unit']=$this->unit_kerja->get_unit('1');
				$this->template->display('/pegawai/riwayatmutasi/detail_mutasi', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatmutasi', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_mutasi->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Mutasi Pegawai :: Hapus');
		confirm("Yakin menghapus data mutasi pegawai?");
		$res = $this->riwayat_mutasi->delete($idField);
		set_success('Data mutasi pegawai berhasil dihapus');
		redirect('/pegawai/riwayatmutasi/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_mutasi']	= '';
		$data['kd_pegawai']	= '';
		$data['id_jabatan_asal']	= '';
		$data['id_jabatan_baru']	= '';
		$data['no_SK']	= '';
		$data['tgl_SK']	= '';
		$data['tgl_mutasi']	= '';
		$data['kode_unit_kerja_asal']	= '';
		$data['kode_unit_kerja_baru']	= '';
		$data['ket_mutasi']	= '';
		$data['jns_mutasi']	= '';
		
		
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
        $data['id_mutasi']	= $this->riwayat_mutasi->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['id_jabatan_asal']	= $this->input->post('id_jabatan_asal', TRUE);
		$data['id_jabatan_baru']	= $this->input->post('id_jabatan_baru', TRUE);
		$data['no_SK']	= $this->input->post('no_SK', TRUE);
		$data['tgl_SK']	= $this->input->post('tgl_SK', TRUE);
		$data['tmt_SK']	= $this->input->post('tmt_SK', TRUE);
		$data['tgl_mutasi']	= $this->input->post('tgl_mutasi', TRUE);
		$data['kode_unit_kerja_asal']	= $this->input->post('kode_unit_kerja_asal', TRUE);
		$data['kode_unit_kerja_baru']	= $this->input->post('kode_unit_kerja_baru', TRUE);
		$data['ket_mutasi']	= $this->input->post('ket_mutasi', TRUE);
		$data['jns_mutasi']	= $this->input->post('jns_mutasi', TRUE);
		$data['id_golpangkat']	= $this->input->post('id_golpangkat', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('kd_pegawai', 'kd_pegawai', 'required');
		$this->form_validation->set_rules('id_golpangkat', 'id_golpangkat', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		//return $this->form_validation->run();
		$this->form_validation->set_rules('id_jabatan_asal', 'id_jabatan_asal', 'required');
		$this->form_validation->set_rules('id_jabatan_baru', 'id_jabatan_baru', 'required');
		$this->form_validation->set_rules('kode_unit_kerja_asal', 'kode_unit_kerja_asal', 'required');
		$this->form_validation->set_rules('kode_unit_kerja_baru', 'kode_unit_kerja_baru', 'required');
		$this->form_validation->set_rules('no_SK', 'no_SK', 'required');
		
		$this->form_validation->set_rules('jns_mutasi', 'jns_mutasi', 'required');
		return $this->form_validation->run();
	}
}