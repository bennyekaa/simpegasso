<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatkaryailmiah extends Member_Controller
{
	function Riwayatkaryailmiah()
	{
		parent::Member_Controller();
		$this->load->model('riwayat_karyailmiah_model','riwayat_karyailmiah');
		$this->load->model('jenis_karya_model','jenis_karya');
		$this->load->model('pegawai_model','pegawai');
		
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
        $this->template->metas('title', 'SIMPEGA | Karya Ilmiah');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		
		$ordby = 'id_riwayat_karya';
		
		$data['list_karya_ilmiah'] = $this->riwayat_karyailmiah->find(NULL, array('kd_pegawai' => $kd_pegawai), null, $limit_per_page,$start,$ordby);
		$data['jenis_karya_assoc']=$this->jenis_karya->get_assoc('nama_karya');
		
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
        
        $data['judul'] 		= "Data Karya Ilmiah dari: " . $data['pegawai']['nama_pegawai'];
		
		$this->template->display('pegawai/riwayatkaryailmiah/list_riwayatkaryailmiah', $data);
	}
	/*function browse($group=0)
	{
		$pegawai_id = $this->pegawai->d_pegawai);
		$data['list_pasangan'] = $this->pasangan_pegawai->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= "Pasangan Pegawai";
		$this->template->display('pegawai/pasangan/list', $data);
	}*/
	
	function add()
	{
		if ($this->_validate())
		{
            $kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			
			$this->riwayat_karyailmiah->add($data);
			set_success('Data Karya Ilmiah berhasil disimpan.');
			redirect('/pegawai/riwayatkaryailmiah/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Karya Ilmiah :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Karya Ilmiah dari: ' . $data['pegawai']['nama_pegawai'];
			
			$data['id_riwayat_karya']=$this->riwayat_karyailmiah->get_id();
			$data['jenis_karya_assoc']=$this->jenis_karya->get_assoc('nama_karya');
			
			$this->template->display('/pegawai/riwayatkaryailmiah/detail_riwayatkaryailmiah', $data);
		}
	}
	
	function edit($id)
	{
		
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_karya'] = $id;
			
			$this->riwayat_karyailmiah->update($id, $data);
			set_success('Perubahan data karya ilmiah berhasil disimpan');
			redirect('/pegawai/riwayatkaryailmiah/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Karya Ilmiah :: Ubah');
			$data = $this->riwayat_karyailmiah->retrieve_by_pkey($id);
			//$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
			$data['jenis_karya_assoc']=$this->jenis_karya->get_assoc('nama_karya');
			
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				
				$data['judul']='Edit Karya Ilmiah dari: '. $data['pegawai']['nama_pegawai'];
				$data['jenis_karya_assoc']=$this->jenis_karya->get_assoc('nama_karya');
				$this->template->display('/pegawai/riwayatkaryailmiah/detail_riwayatkaryailmiah', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatkaryailmiah', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_karyailmiah->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Karya Ilmiah :: Hapus');
		confirm("Yakin menghapus data karya ilmiah?");
		$res = $this->riwayat_karyailmiah->delete($idField);
		set_success('Data Karya Ilmiah berhasil dihapus');
		redirect('/pegawai/riwayatkaryailmiah/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_karya']	= '';
		$data['kd_pegawai']	= '';
		$data['jenis_karya']	= '';
		$data['judul_karya']	= '';
		$data['bidang_ilmu']	= '';
		$data['dimuat']	= '';
		$data['tahun']	= '';
		$data['akreditasi']	= '';
		
		
		return $data;
	}	
	
	function _get_form_values()
	{
	   
        $data['id_riwayat_karya']	= $this->riwayat_karyailmiah->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['jenis_karya']	= $this->input->post('jenis_karya', TRUE);
		$data['judul_karya']	= $this->input->post('judul_karya', TRUE);
		$data['bidang_ilmu']	= $this->input->post('bidang_ilmu', TRUE);
		$data['dimuat']	= $this->input->post('dimuat', TRUE);
		$data['tahun']	= $this->input->post('tahun', TRUE);
		$data['akreditasi']	= $this->input->post('akreditasi', TRUE);
		
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('judul_karya', 'judul_karya', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}