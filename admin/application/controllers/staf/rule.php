<?php  if (!defined('BASEPATH')) exit('No direct script rule allowed');

class Rule extends Member_Controller
{
	function __construct() 	{
		parent::Member_Controller();
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->model('rule_model');
		$this->load->model('rule_group_model');
		$this->load->model('usergroup_model','usergroup');
	}

	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Hak Akses');
		$this->browse();
	}
	
	function browse()
	{
		$the_results['group_list'] = $this->usergroup->get_group_user();
		$the_results['judul'] = "Daftar Tingkat User";
		$this->template->display('setup/user/manage-rule', $the_results);
	}

	function assigned_user($group_id='')
	{
		$this->session->set_flashdata('xreferrer', 'assigned_user');
		$this->template->load_js_var(
			'
			jQuery.fn.fadeToggle = function(speed, easing, callback) {
			return this.animate({opacity: "toggle"}, speed, easing, callback);
			};
			$(function(){
				$("#search_button").click(function(){
					$("#search_form").fadeToggle();
				});
			});
		');
		$paging_uri=5;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;

		$limit_per_page = 30;
        $the_results['group_list'] = $this->rule_group_model->findByFilter(array('rule_group.group_id'=>$group_id), $limit_per_page,$start);
		$the_results['judul'] = "Manage Hak Akses ";
		$the_results['group_id'] = $group_id;

		$config['base_url']     = site_url('staf/rule/assigned_user/'.$group_id);
		$config['total_rows']   = $this->rule_group_model->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] = 'berikutnya &raquo;';
		$config['prev_link'] = '&laquo; sebelumnya ';

		$this->pagination->initialize($config);

		$the_results['page_links'] = $this->pagination->create_links();
		$this->template->metas('title', 'SIMPEGA | Hak Akses :: Pengelolaan');
		$this->template->display('setup/user/detail-manage-rule', $the_results);
	}

    function delete_assign_rule($idField)
	{
		$this->template->metas('title', 'SIMPEGA | Hak Akses :: Hapus');
		confirm("Apakah Anda yakin menghapus Rule ini ?");
		$this->load->model('rule_group_model');
		$dat = $this->rule_group_model->retrieve_by_pkey($idField);
		$the_results = $this->rule_group_model->delete_by_pkey($idField);
		set_success('Rule Berhasil Dihapus dari group');
		redirect('staf/rule/assigned_user/'.$dat['group_id'], 'location');
	}
	
	function assign()
	{
		$this->form_validation->set_rules('group_id', 'group_id', 'required|callback__rule_check');
		$this->form_validation->set_rules('id_rule', 'id_rule', 'required');

		if ( $this->form_validation->run())
		{
			$data['group_id']=$this->input->post('group_id');
			$data['id_rule']=$this->input->post('id_rule');
			$this->db->insert('rule_group',$data);
			set_success('Rule berhasil Ditambah');
			redirect('staf/rule/assigned_user/'.$this->input->post('group_id'));
		}
		else
		{
			set_error( validation_errors());
			redirect('staf/rule/assigned_user/'.$this->input->post('group_id'));
		}
	}

	function _rule_check()
	{
		$this->db->where(array('group_id' => $this->input->post('group_id'),
							     'id_rule' => $this->input->post('id_rule')));
		$q=$this->db->get('rule_group');
		if ($q->row_array())
		{
			$this->form_validation->set_message('_rule_check', 'Rule yang anda masukkan sudah ada!!');
			return FALSE;
		}
		else
			return TRUE;
	}
	
	function _clear_form()
	{
		$data['id_rule']		= '';
		$data['rule_name']		= '';
		$data['controller']			= '';
		$data['action']			= '';
		return $data;
	}

	function _get_form_values(){
	   	$data['id_rule']		= $this->input->post('id_rule', TRUE);
		$data['rule_name']		= $this->input->post('rule_name', TRUE);
		$data['controller']			= $this->input->post('controller', TRUE);
		$data['action']			= $this->input->post('action', TRUE);
		return $data;
	}

	function _validate()
	{
		$this->form_validation->set_rules('rule_name', 'Nama Rule', 'required');
		return $this->form_validation->run();
	}
}
?>
