<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reset extends Member_Controller
{
	function Reset()
	{
		parent::Member_Controller();
		$this->load->model('users_model');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Ubah Password');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
			
		if($this->input->post('search'))
        {    
			$search = $this->input->post('search');
		}
		else
		{
			$search = '';
		}
					
		
		 $search_param = array();
        if($search)
            $search_param[] = "(username like '%$search%')";
			$search_param[] = "(name like '%$search%')";
			$search_param = implode(" OR ", $search_param);
			
		
		$limit_per_page = 10;
		
		$data['list_user'] = $this->users_model->findAll($limit_per_page,$start,$search_param);
		$config['base_url']     = site_url('staf/reset/browse/');
		$config['total_rows']   = $this->users_model->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['judul'] = 'Daftar User';
		$this->template->display('setup/user/reset-password', $data);
	}
	
	function do_reset()
	{
		$id = $this->uri->segment(4);
		if ($this->_validate())
		{	
			$data = $this->_get_form_values();
			$data['user_id'] = $id;
			//show_error(var_dump($data));
			$this->users_model->modify($id,$data);
			set_success('Password berhasil dirubah');
			redirect('staf/reset');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Ubah Password');;
			$data = $this->users_model->retrieve_by_pkey($id);
			$data['action'] = 'do_reset/'.$id;
			$data['judul']='Reset Password';
			$this->template->display('setup/user/detail-reset-password', $data);
		}
	}
	
	function _get_form_values()
	{
	   	$data['user_id']	= $this->input->post('user_id', TRUE);
		$data['password']	= $this->input->post('password', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('password', 'password', 'required');
		return $this->form_validation->run();
	}
}