<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pegawai extends My_Controller {

    var $pegawai_id;
    var $groupuser;

    function Pegawai() {
        parent::My_Controller();
        $this->load->model('pegawai_model', 'pegawai');
        $this->load->model('agama_model', 'agama');
        $this->load->model('lookup_model', 'lookup');
        $this->load->model('jenis_pegawai_model', 'jenis_pegawai');
        $this->load->model('pendidikan_model', 'pendidikan');
        $this->load->model('kelurahan_model', 'kelurahan');
        $this->load->model('unit_kerja_model', 'unit_kerja');
        $this->load->model('golongan_model', 'golongan');
        $this->load->model('status_perkawinan_model', 'status_perkawinan');
        $this->load->model('jabatan_model', 'jabatan');
        $this->load->model('riwayat_gol_kepangkatan_model', 'riwayat_gol_kepangkatan');
        $this->load->model('riwayat_jabatan_model', 'riwayat_jabatan');
        $this->load->model('riwayat_pendidikan_model', 'riwayat_pendidikan');
        $this->load->model('lap_pegawai_model', 'lap_pegawai');
		$this->load->model('users_model','users');
        /*
          $this->load->model('gambar_model', 'gambar');
         */
        $this->load->model('user_pegawai_model', 'user_pegawai');
    }

    function index() {
        $this->template->metas('title', 'SIMPEGA | Data Induk Pegawai');
        $this->browse();
    }

    function browse($group=0) {


        if ($this->input->post('group'))
            $group = $this->input->post('group');
        else if ($this->uri->segment(4))
            $group = $this->uri->segment(4);
			
        $this->access->restrict();	
		if (!$this->user)
		$this->user = $this->access->get_user();
		if($this->user->user_group == 'Admin Khusus'){
		$query = mysql_query("select p.kode_unit from user_pegawai u,pegawai 
			p where p.kd_pegawai = u.kd_pegawai and u.user_id='".$this->user->user_id."'");					
			if ($query) {
					$datauser=mysql_fetch_array($query);  
					$unit_user = $datauser['kode_unit']; 
			}	
			$unit_kerja	=$unit_user;
		}
		else
		{
		//	$unit_kerja = '0';
			if ($this->input->post('unit_kerja'))
				$unit_kerja = $this->input->post('unit_kerja');
			else if ($this->uri->segment(5))
				$unit_kerja = $this->uri->segment(5);
		}			
        if($this->input->post('search'))
            $search = $this->input->post('search');
        $paging_uri = 6;
        if ($this->uri->segment($paging_uri))
            $start = $this->uri->segment($paging_uri);
        else
            $start=0;
        $limit_per_page = 20;

        $ordby = 'eselon asc,id_golpangkat_terakhir desc';
        //$ordby = $this->uri->segment(4); // pengaturan agar order by sesuai klik kolom

        $search_param = array();
        if ($unit_kerja)
            $search_param[] = "(pegawai.kode_unit = '$unit_kerja' OR pegawai.kode_unit_induk = '$unit_kerja')";
        if ($group)
            $search_param[] = "(id_jns_pegawai = '$group'  and status_pegawai < 3 )";
            
        if($search)
            $search_param[] = "(nama_pegawai like '%$search%')";

        $search_param = implode(" AND ", $search_param);

        $data['kerja_list'] = $this->lap_pegawai->findByFilter($search_param, $ordby, $limit_per_page, $start);


        $data['group'] = $group;
        $data['start'] = $start;

        $data['unitkerja_assoc'] = array(0 => "-- Semua Unit Kerja --") + $this->unit_kerja->get_assoc2();
        $data['jenis_pegawai_assoc'] = array(0 => "-- Semua Jenis Pegawai --") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');

        $config['base_url'] = site_url('pegawai/pegawai/browse/' . $group . '/' . $unit_kerja);

        $config['total_rows'] = $this->lap_pegawai->record_count;
        $config['per_page'] = $limit_per_page;
        $config['uri_segment'] = $paging_uri;
        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();
        $data['judul'] = "Data Induk Pegawai";


        $data['golongan_assoc'] = $this->golongan->get_golpangkat_assoc('golongan');
        $data['pendidikan_assoc'] = $this->pendidikan->get_assoc('nama_pendidikan');
        $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');

        $this->template->display('/pegawai/pegawai/list_perunit', $data);
    }

    function view() {
        $data['judul'] = "Detail Data dari: ";
        $id = $this->uri->segment(4);
        $this->edit($id);
    }

    function add() {
        if ($this->_validate()) {
            $data = $this->_get_form_values_pegawai();
            $id_peg = $this->pegawai->get_id();
            $data['kd_pegawai'] = $id_peg;

            $this->pegawai->add($data);
            set_success('Data Induk Pegawai berhasil disimpan.');
            redirect('pegawai/pegawai/');
        } else {
            $data = $this->_clear_form_pegawai();
            $data['action'] = 'add';
            $data['judul'] = 'Tambah Pegawai';
            $data['kd_pegawai'] = $this->pegawai->get_id();
            /* $data['tmt_golpangkat_terakhir']=substr($data['pegawai']['tmt_golpangkat_terakhir'],0,10);
              $data['tmt_kgb']=substr($data['pegawai']['tmt_kgb'],0,10);
              $data['tgl_lahir']=substr($data['pegawai']['tgl_lahir'],0,10);
              $data['tgl_lahir']=substr($data['pegawai']['tgl_lahir'],0,10);
              $data['tgl_sk_cpns']=substr($data['pegawai']['tgl_sk_cpns'],0,10);
              $data['tgl_sk_pns']=substr($data['pegawai']['tgl_sk_pns'],0,10);
              $data['tgl_sk_pensiun']=substr($data['pegawai']['tgl_sk_pensiun'],0,10); */
            $data['agama_assoc'] = array(0 => "-- Pilih Agama --") + $this->agama->get_assoc('agama');
            $data['gender_assoc'] = array(0 => "-- Pilih Jenis Kelamin --") + $this->lookup->gender_assoc();
            $data['status_asal_assoc'] = array(0 => "-- Umum/Honorer --") + $this->lookup->status_asal_assoc();
            $data['jenis_pegawai_assoc'] = array(0 => "-- Pilih Jenis Pegawai --") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
            $data['status_pegawai_assoc'] = array('statuspeg' => "-- Pilih Status Pegawai --") + $this->lookup->status_pegawai_assoc();
            $data['gol_darah_assoc'] = array(0 => "-- Pilih Golongan Darah --") + $this->lookup->gol_darah_assoc();
            $data['status_perkawinan_assoc'] = array(0 => "-- Pilih Status Perkawinan --") + $this->status_perkawinan->get_assoc('status_perkawinan');
            $data['kelurahan_assoc'] = array(0 => "-- Pilih Kelurahan --") + $this->kelurahan->get_assoc('nama_kelurahan');
            $data['unit_kerja_assoc'] = array(0 => "-- Pilih Unit Kerja --") + $this->unit_kerja->get_assoc('nama_unit');
            $data['eselon_assoc'] = array(0 => "-- Pilih Eselon --") + $this->lookup->eselon_assoc();
            $data['golongan_assoc'] = $this->golongan->get_assoc();
            $data['pendidikan_assoc'] = $this->pendidikan->get_pendidikan_assoc();
            $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
            $this->template->display('/pegawai/pegawai/detail', $data);
        }
    }

    function edit($id) {
        if ($this->_validate()) {
            $data = $this->_get_form_values_pegawai();
            $data['kd_pegawai'] = $id;
            $kode_unit = $this->unit_kerja->retrieve_by_pkey($data['kode_unit']);
            $data['kode_unit_induk'] = $kode_unit['kode_unit_general'];
            $this->pegawai->modify($id, $data);
            set_success('Perubahan data pegawai berhasil disimpan');
            redirect('/pegawai/pegawai/', 'location');
        } else {
            $data['action'] = 'edit/' . $id;
            $data['judul'] = 'Edit Data Induk dari:';
            $data['pegawai'] = $this->pegawai->retrieve_by_pkey($id);
            $data['kd_pegawai'] = $id;
            //$data['kerja'] = $this->kerja->retrieve_by_pkey($id);
            $data['tgl_lahir'] = substr($data['pegawai']['tgl_lahir'], 0, 10);
            $data['tgl_sk_cpns'] = substr($data['pegawai']['tgl_sk_cpns'], 0, 10);
            $data['tgl_sk_pns'] = substr($data['pegawai']['tgl_sk_pns'], 0, 10);
            $data['tgl_sk_pensiun'] = substr($data['pegawai']['tgl_sk_pensiun'], 0, 10);
            //$data['tmt_jabatan']=substr($data['kerja']['tmt_jabatan'],0,10);
            //$data['tmt_golongan']=substr($data['kerja']['tmt_golongan'],0,10);
            //$data['tgl_awal_kerja']=substr($data['kerja']['tgl_awal_kerja'],0,10);
            //$data['tgl_awal_kerja_sk']=substr($data['kerja']['tgl_awal_kerja_sk'],0,10);
            //$data['tgl_akhir_kerja']=substr($data['kerja']['tgl_akhir_kerja'],0,10);
            if ($data) {
                $data['agama_assoc'] = array(0 => "-- Pilih Agama --") + $this->agama->get_assoc('agama');
                $data['gender_assoc'] = array(0 => "-- Pilih Jenis Kelamin --") + $this->lookup->gender_assoc();
                $data['status_asal_assoc'] = array(0 => "-- Umum/Honorer --") + $this->lookup->status_asal_assoc();
                $data['jenis_pegawai_assoc'] = array(0 => "-- Pilih Jenis Pegawai --") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
                $data['status_pegawai_assoc'] = array('statuspeg' => "-- Pilih Status Pegawai --") + $this->lookup->status_pegawai_assoc();
                $data['gol_darah_assoc'] = array(0 => "-- Pilih Golongan Darah --") + $this->lookup->gol_darah_assoc();
                $data['status_perkawinan_assoc'] = array(0 => "-- Pilih Status Perkawinan --") + $this->status_perkawinan->get_assoc('status_perkawinan');
                $data['kelurahan_assoc'] = array(0 => "-- Pilih Kelurahan --") + $this->kelurahan->get_assoc('nama_kelurahan');
                $data['unit_kerja_assoc'] = array(0 => "-- Pilih Unit Kerja --") + $this->unit_kerja->get_assoc('nama_unit');
                $data['eselon_assoc'] = array(0 => "-- Pilih Eselon --") + $this->lookup->eselon_assoc();
                $data['golongan_assoc'] = $this->golongan->get_assoc();
                $data['pendidikan_assoc'] = $this->pendidikan->get_pendidikan_assoc();
                $data['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');

                $expected_file = PUBLICPATH . 'photo/photo_' . $id . '.jpg';
                $data['photo'] = file_exists($expected_file) ? base_url() . '/public/photo/photo_' . $id . '.jpg' : base_url() . '/public/images/image/nophotos.jpg';


                $data['id'] = $id;
                $data['action'] = 'edit/' . $id;
                $this->template->display('/pegawai/pegawai/detail', $data);
            } else {
                set_error('Data tidak ditemukan');
                redirect('/pegawai/pegawai/', 'location');
            }
        }
    }

    function delete() {
        $idField = $this->uri->segment(4);
        $data = $this->pegawai->retrieve_by_pkey($idField);

        confirm("Yakin menghapus data pegawai <b>" . $data['nama_pegawai'] . "</b> ?");
        $res = $this->pegawai->delete_by_pkey($idField);
        set_success('Data pegawai berhasil dihapus');
        redirect('/pegawai/pegawai/', 'location');
    }

    function cari() {
        if ($this->_validate_search()) {
            $val = $this->_get_form_values_search();
            if ($val['category'] == 1) {
                $cari = array('NIP' => $val['search']);
            } elseif ($val['category'] == 2) {
                $cari = array('nama_pegawai' => $val['search']);
            } elseif ($val['category'] == 3) {
                $cari = array('e.nama_unit' => $val['search']);
            }
            $data['pegawai_list'] = $this->pegawai->get_all_by($cari);
            if ($data['pegawai_list'] != false) {
                $data['is_new'] = false;
            } else {
                $data['is_new'] = true;
            }
            //show_error(var_dump($data));
            $data['judul'] = 'Pencarian Pegawai';
            $this->template->display('/pegawai/pegawai/list', $data);
        } else {
            $this->template->metas('title', 'SIMPEGA | Pencarian Pegawai');
            $data['judul'] = 'Pencarian Pegawai';
            $data['action'] = 'cari';
            $data['category_assoc'] = array(1 => 'NIP', 2 => 'Nama', 3 => 'Unit Kerja');
            $this->template->display('/pegawai/pegawai/search', $data);
        }
    }

    function photo() {
        $kd_pegawai = $this->uri->segment(4);
        $config['upload_path'] = './public/photo/';
        $config['allowed_types'] = 'jpg';
        $config['max_size'] = '100';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $config['overwrite'] = true;
        $config['file_name'] = 'photo_' . $kd_pegawai;
        
        $this->load->library('upload', $config);

        $data['error'] = '';


        $data['kd_pegawai'] = $kd_pegawai;
        if (!$this->upload->do_upload('gambar')) {

            $data['error'] = $this->upload->display_errors();
            $expected_file = PUBLICPATH . 'photo/photo_' . $kd_pegawai . '.jpg';
			if (file_exists($expected_file)){
				$data['photo'] = base_url() . '/public/photo/photo_' . $kd_pegawai . '.jpg';
			}
			else{
				$expected_file = PUBLICPATH . 'photo/photo_' . $kd_pegawai . '.gif';
				if (file_exists($expected_file)){
					$data['photo'] = base_url() . '/public/photo/photo_' . $kd_pegawai . '.gif';
					}
				else{
					$expected_file = PUBLICPATH . 'photo/photo_' . $kd_pegawai . '.bmp';
					if (file_exists($expected_file)){
							$data['photo'] = base_url() . '/public/photo/photo_' . $kd_pegawai . '.bmp';
						}
						else{	
							$data['photo'] = base_url() . '/public/images/image/nophotos.jpg';
					}
				}
			}
            //$default = file_exists($expected_file) ? base_url() . '/public/photo/photo_' . $kd_pegawai . '.jpg' : base_url() . '/public/images/image/nophotos.jpg';
            //$data['photo'] = $default;
        } else {
            $imagedata = $this->upload->data();

            $config['image_library'] = 'gd2';
            $config['source_image'] = $imagedata['full_path'];
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = FALSE;
            $config['width'] = 120;
            $config['height'] = 150;

            $this->load->library('image_lib', $config);

            $this->image_lib->resize();
            $data['photo'] = base_url() . '/public/photo/' . $imagedata['file_name'];
        }
        $this->template->display('/pegawai/pegawai/upload_pegawai', $data);
    }

    function cetak($id) {
        $id = $this->uri->segment(4);
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($id);
        $data['kerja'] = $this->kerja->retrieve_by_pkey($id);
        $data['tgl_lahir'] = substr($data['pegawai']['tgl_lahir'], 0, 10);
        $data['tmt_jabatan'] = substr($data['kerja']['tmt_jabatan'], 0, 10);
        $data['tmt_golongan'] = substr($data['kerja']['tmt_golongan'], 0, 10);
        $data['tgl_awal_kerja'] = substr($data['kerja']['tgl_awal_kerja'], 0, 10);
        $data['tgl_awal_kerja_sk'] = substr($data['kerja']['tgl_awal_kerja_sk'], 0, 10);
        $data['tgl_akhir_kerja'] = substr($data['kerja']['tgl_akhir_kerja'], 0, 10);

        $properties = array();
        $properties['title'] = 'Data Induk Pegawai';
        $properties['subject'] = 'Data Induk Pegawai';
        $properties['keywords'] = 'Data Induk Pegawai';
        $properties['papersize'] = "A4";
        $properties['paperlayout'] = 'P';
        $properties['filename'] = 'data_pegawai';

        $the_results['judul'] = "Data Induk Pegawai";
        $html = $this->load->view('pegawai/pegawai/print', $data, true);
        $this->printtopdf->htmltopdf($properties, $html);
    }

    function upload($idfield) {
        if ($this->_validate_upload()) {
            $data['FK_pegawai'] = $this->input->post('FK_pegawai', TRUE);
            $cuk = $this->input->post('gambar', TRUE);
            //show_error(var_dump($cuk));
            if ($cuk != '') {
                $config['upload_path'] = './public/images/image/';
                $config['allowed_types'] = 'png|jpg|jpeg';
                $config['max_size'] = '100';
                $config['max_width'] = '400';
                $config['max_height'] = '500';
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('gambar')) {
                    set_error($this->upload->display_errors()); //'Upload error, File Gambar tidak valid!!');
                    redirect('pegawai/pegawai/edit/' . $idfield);
                } else {
                    $gambar = array('upload_data' => $this->upload->data());

                    $gambar = $this->input->xss_clean($gambar);
                    $tags = $_POST['tags'];
                    unset($_POST['tags']);
                    $photo_id = $gambar['upload_data']['file_name'];
                    list($width, $height, $type, $attr) = getimagesize($config['upload_path'] . $photo_id);

                    $this->load->library('image_lib');
                    if ($width > 100 or $height > 50) {
                        //resize image
                        $config['image_library'] = 'GD2';
                        $config['source_image'] = $config['upload_path'] . $photo_id;
                        $config['new_image'] = $config['upload_path'] . $photo_id;
                        $config['create_thumb'] = FALSE;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = 100;
                        $config['height'] = 50;
                        $config['master_dim'] = (abs($width - $config['width']) >= abs($height - $config['height'])) ? 'width' : 'height';
                        $this->image_lib->initialize($config);
                        $this->load->library('image_lib', $config);
                        if (!$this->image_lib->resize())
                            echo $this->image_lib->display_errors();
                    }
                    $data['image'] = $photo_id;
                }
                $this->gambar->delete($idfield);
                $this->gambar->add($data);

                set_success('Foto berhasil di upload');
                redirect('/pegawai/pegawai/edit/' . $idfield);
            }
            else {
                set_error('Upload gagal, tidak ada file yang dipilih!!');
                redirect('pegawai/pegawai/edit/' . $idfield);
            }
        } else {
            $data['FK_pegawai'] = $this->uri->segment(4);
            $data['action'] = 'upload/' . $data['FK_pegawai'];
            $this->template->display('pegawai/pegawai/upload_pegawai', $data);
        }
    }

    function _clear_form_pegawai() {
        $data['kd_pegawai'] = '';
        $data['NIP'] = '';
        $data['NIP_lama'] = '';
        $data['nama_pegawai'] = '';
        $data['gelar_depan'] = '';
        $data['gelar_belakang'] = '';
        $data['tempat_lahir'] = '';
        $data['tgl_lahir'] = '';
        $data['jns_kelamin'] = '';
        $data['kd_agama'] = '';
        $data['status_kawin'] = '';
        $data['alamat'] = '';
        $data['RT'] = '';
        $data['RW'] = '';
        $data['kd_kelurahan'] = '';
        $data['kode_pos'] = '';
        $data['no_telp'] = '';
        $data['gol_darah'] = '';

        $data['status_asal'] = '';
        $data['id_jns_pegawai'] = '';
        $data['status_pegawai'] = '';
        //$data['kewarganegaraan']= $this->input->post('kewarganegaraan', TRUE);
        $data['kode_unit'] = '';
        $data['tmt_unit_terakhir'] = '';
        $data['tmt_golpangkat_terakhir'] = '';
        $data['tmt_kgb'] = '';
        $data['no_sk_cpns'] = '';
        //$data['tgl_sk_cpns']	= '';
        $data['no_sk_pns'] = '';
        //$data['tgl_sk_pns']		= '';
        $data['no_sk_pensiun'] = '';
        $data['tgl_sk_pensiun'] = '';
        $data['loker'] = '';
        $data['npwp'] = '';
        $data['tgl_npwp'] = '';
        $data['eselon'] = '';
        $data{'angka_kredit'} = '';
        $data['karpeg'] = '';
        $data['no_ASKES'] = '';
        $data['taspen'] = '';
        $data['foto'] = '';
        $data['tgl_resign'] = '';
        return $data;
    }

    function _get_form_values_pegawai() {


        $data['kd_pegawai'] = $this->input->post('kd_pegawai', TRUE);
        $data['NIP'] = $this->input->post('NIP', TRUE);
        $data['NIP_lama'] = $this->input->post('NIP_lama', TRUE);
        $data['nama_pegawai'] = $this->input->post('nama_pegawai', TRUE);
        $data['gelar_depan'] = $this->input->post('gelar_depan', TRUE);
        $data['gelar_belakang'] = $this->input->post('gelar_belakang', TRUE);
        $data['tempat_lahir'] = $this->input->post('tempat_lahir', TRUE);
        $data['tgl_lahir'] = $this->input->post('tgl_lahir', TRUE);
        $data['jns_kelamin'] = $this->input->post('jns_kelamin', TRUE);
        $data['kd_agama'] = $this->input->post('kd_agama', TRUE);
        $data['status_kawin'] = $this->input->post('status_kawin', TRUE);
        $data['alamat'] = $this->input->post('alamat', TRUE);
        $data['RT'] = $this->input->post('rt', TRUE);
        $data['RW'] = $this->input->post('rw', TRUE);
        $data['kd_kelurahan'] = $this->input->post('kd_kelurahan', TRUE);
        $data['kode_pos'] = $this->input->post('kode_pos', TRUE);
        $data['no_telp'] = $this->input->post('no_telp', TRUE);
        $data['gol_darah'] = $this->input->post('gol_darah', TRUE);

        $data['status_asal'] = $this->input->post('status_asal', TRUE);
        $data['id_jns_pegawai'] = $this->input->post('id_jns_pegawai', TRUE);
        $data['status_pegawai'] = $this->input->post('status_pegawai', TRUE);
        //$data['kewarganegaraan']= $this->input->post('kewarganegaraan', TRUE);
        $data['kode_unit'] = $this->input->post('kode_unit', TRUE);
        $data['tmt_unit_terakhir'] = $this->input->post('tmt_unit_kerja', TRUE);
        $data['tmt_golpangkat_terakhir'] = $this->input->post('tmt_golpangkat_terakhir', TRUE);
        $data['tmt_kgb'] = $this->input->post('tmt_kgb', TRUE);
        $data['no_sk_cpns'] = $this->input->post('no_sk_cpns', TRUE);
        //$data['tgl_sk_cpns']	= $this->input->post('tgl_sk_cpns', TRUE);
        $data['tmt_cpns'] = $this->input->post('tmt_cpns', TRUE);
        $data['no_sk_pns'] = $this->input->post('no_sk_pns', TRUE);
        //$data['tgl_sk_pns']		= $this->input->post('tgl_sk_pns', TRUE);
        $data['tmt_pns'] = $this->input->post('tmt_pns', TRUE);
        $data['no_sk_pensiun'] = $this->input->post('no_sk_pensiun', TRUE);
        $data['tgl_sk_pensiun'] = $this->input->post('tgl_sk_pensiun', TRUE);
        $data['loker'] = $this->input->post('loker', TRUE);
        $data['npwp'] = $this->input->post('npwp', TRUE);
        $data['tgl_npwp'] = $this->input->post('tgl_npwp', TRUE);
        $data['eselon'] = $this->input->post('eselon', TRUE);
        $data{'angka_kredit'} = $this->input->post('angka_kredit', TRUE);
        $data['karpeg'] = $this->input->post('karpeg', TRUE);
        $data['no_ASKES'] = $this->input->post('no_ASKES', TRUE);
        $data['taspen'] = $this->input->post('taspen', TRUE);
        $data['foto'] = $this->input->post('foto', TRUE);
        $data['tgl_resign'] = $this->input->post('tgl_resign', TRUE);

        return $data;
    }

    /* function _clear_form_kerja()
      {
      $data['FK_pegawai']			='';
      $data['FK_departemen']		='';
      $data['FK_unit_kerja']		='';
      $data['FK_jabatan']			='';
      $data['id_golpangkat']		='';
      $data['FK_status_pegawai']	='';
      $data['FK_jenis_pegawai']	='';
      $data['FK_wilayah']			='';
      $data['tmt_jabatan']		='';
      $data['tmt_golongan']		='';
      $data['tgl_awal_kerja']		='';
      $data['tgl_awal_kerja_sk']	='';
      return $data;
      }

      function _get_form_values_kerja()
      {
      $data['FK_pegawai']			= $this->input->post('FK_pegawai', TRUE);
      $data['FK_departemen']		= $this->input->post('FK_departemen', TRUE);
      $data['FK_unit_kerja']		= $this->input->post('FK_unit_kerja', TRUE);
      $data['FK_jabatan']			= $this->input->post('FK_jabatan', TRUE);
      $data['id_golpangkat']		= $this->input->post('id_golpangkat', TRUE);
      $data['FK_status_pegawai']	= $this->input->post('FK_status_pegawai', TRUE);
      $data['FK_jenis_pegawai']	= $this->input->post('FK_jenis_pegawai', TRUE);
      $data['FK_wilayah']			= $this->input->post('FK_wilayah', TRUE);
      $data['tmt_jabatan']		= $this->input->post('tmt_jabatan', TRUE);
      $data['tmt_golongan']		= $this->input->post('tmt_golongan', TRUE);
      $data['tgl_awal_kerja']		= $this->input->post('tgl_awal_kerja', TRUE);
      $data['tgl_awal_kerja_sk']	= $this->input->post('tgl_awal_kerja_sk', TRUE);
      return $data;
      }
     */

    function _validate() {
        //return true;
        $this->form_validation->set_rules('NIP', 'NIP', 'required');
        $this->form_validation->set_rules('nama_pegawai', 'nama_pegawai', 'required');
        /* $this->form_validation->set_rules('tempat_lahir', 'tempat_lahir', 'required');
          $this->form_validation->set_rules('tgl_lahir', 'tgl_lahir', 'required');
          $this->form_validation->set_rules('JK', 'JK', 'required');
          $this->form_validation->set_rules('FK_agama', 'FK_agama', 'required');
          $this->form_validation->set_rules('FK_status_kawin', 'FK_status_kawin', 'required');
          $this->form_validation->set_rules('nama_sekolah', 'nama_sekolah', 'required');
          $this->form_validation->set_rules('FK_tingkat_pendidikan', 'FK_tingkat_pendidikan', 'required');
          $this->form_validation->set_rules('jur_fak', 'jur_fak', 'required');
          $this->form_validation->set_rules('tahun', 'tahun', 'required');

          $this->form_validation->set_rules('FK_departemen', 'FK_departemen', 'required');
          $this->form_validation->set_rules('FK_unit_kerja', 'FK_unit_kerja', 'required');
          $this->form_validation->set_rules('FK_jabatan', 'FK_jabatan', 'required');
          $this->form_validation->set_rules('id_golpangkat', 'id_golpangkat', 'required');
          $this->form_validation->set_rules('FK_status_pegawai', 'FK_status_pegawai', 'required');
          $this->form_validation->set_rules('FK_jenis_pegawai', 'FK_jenis_pegawai', 'required');
          $this->form_validation->set_rules('FK_wilayah', 'FK_wilayah', 'required');
          $this->form_validation->set_rules('tmt_jabatan', 'tmt_jabatan', 'required');
          $this->form_validation->set_rules('tmt_golongan', 'tmt_golongan', 'required');
          $this->form_validation->set_rules('tgl_awal_kerja', 'tgl_awal_kerja', 'required');
          $this->form_validation->set_rules('tgl_awal_kerja_sk', 'tgl_awal_kerja_sk', 'required'); */

        return $this->form_validation->run();
    }

    function _clear_form_search() {
        $data['is_new'] = '';
        $data['search'] = '';
        $data['category'] = '';
        return $data;
    }

    function _get_form_values_search() {
        $data['search'] = $this->input->post('search', TRUE);
        $data['category'] = $this->input->post('category', TRUE);
        return $data;
    }

    function _validate_search() {
        $this->form_validation->set_rules('search', 'search', 'required');
        $this->form_validation->set_rules('category', 'category', 'required');
        return $this->form_validation->run();
    }

    function _validate_upload() {
        $this->form_validation->set_rules('FK_pegawai', 'FK_pegawai', 'required');
        return $this->form_validation->run();
    }

}