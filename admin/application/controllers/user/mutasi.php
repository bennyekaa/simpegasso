<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mutasi extends Member_Controller
{
	function Mutasi()
	{
		parent::Member_Controller();
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('unit_kerja_model', 'unit_kerja');
		$this->load->model('jabatan_struktur_model', 'jabatan');
		$this->load->model('kerja_model', 'kerja');
		$this->load->model('jenis_mutasi_model', 'jenis_mutasi');
		$this->load->model('mutasi_model', 'mutasi');
		$this->load->model('lookup_model', 'lookup');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Mutasi Pegawai');
		$this->browse();
	}
	
	function browse($group=0)
	{
		$paging_uri=5;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$limit_per_page = 30;
		$ordby = $this->uri->segment(4);
		
		if($this->user->user_group == 'Administrator')
		{
			$the_results['pegawai_list'] = $this->pegawai->findAll($limit_per_page,$start,$ordby);
			$the_results['is_admin'] = true;
		}
		elseif($this->user->user_group == 'Sub Admin')
		{
			$the_results['pegawai_list'] = $this->pegawai->findByFilter($limit_per_page,$start,$ordby);
			$the_results['is_admin'] = false;
		}
		else
			redirect('/index.php');
		
		$the_results['judul'] = "Mutasi Pegawai";
		
		$config['base_url']     = site_url('pegawai/mutasi/browse/'.$ordby.'/');
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$this->template->display('pegawai/mutasi/list', $the_results);
	}
	
	function do_mutasi($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_mutasi_values();
			$data1 = $this->_get_form_kerja_values();
			$data['id_mutasi']= $this->mutasi->get_id();
			$data['FK_pegawai'] = $id;
			$data1['FK_pegawai'] = $id;
			$data['tgl_mutasi'] = date('Y-m-d');
			$this->mutasi->add($data);
			$this->kerja->update_by_idpeg($id,$data1);
			set_success('Mutasi Pegawai berhasil disimpan.');
			redirect('/pegawai/mutasi');
		}
		else
		{
			$data = $this->_clear_form();
			$data['pegawai'] = $this->pegawai->retrieve_by_pkey($id);
			$data['kerja'] = $this->kerja->retrieve_by_pkey($id);
			$data['action']='do_mutasi/'.$id;
			$data['judul']='Mutasi Pegawai';
			$data['jenis_mutasi_assoc'] = array(0=>"-- Pilih Jenis Mutasi --")+$this->jenis_mutasi->get_assoc("jenis_mutasi");
			$data['jabatan_assoc'] = array(0=>"-- Pilih Jabatan --")+$this->jabatan->get_assoc("jabatan");
			$data['unit_kerja_assoc'] = array(0=>"-- Pilih Unit Kerja --")+$this->unit_kerja->get_assoc("unit_kerja");
			$this->template->display('/pegawai/mutasi/detail', $data);
		}
	}
	
	function info($id)
	{
		$this->template->metas('title', 'SIMPEGA | Riwayat Mutasi Pegawai');
		$data['mutasi_list'] = $this->mutasi->retrieve_by_idpeg($id);
		$data['judul']='Riwayat Mutasi Pegawai';
		$this->template->display('/pegawai/mutasi/list-info', $data);
	}

	function _clear_form()
	{
		$data['id_mutasi']		= '';
		$data['jabatan_asal']	= '';
		$data['jns_mutasi']= '';
		$data['no_SK']			= '';
		$data['tgl_SK']			= '';
		$data['tmt_SK']			= '';
		$data['tgl_mutasi']		= '';
		$data['jabatan_asal']	= '';
		$data['jabatan_baru']	= '';
		$data['unit_kerja_asal']= '';
		$data['unit_kerja_baru']= '';
		$data['ket_mutasi']		= '';
		return $data;
	}	
	
	function _get_form_mutasi_values()
	{
		$data['id_mutasi']			= $this->input->post('id_mutasi', TRUE);
		$data['jns_mutasi']	= $this->input->post('jns_mutasi', TRUE);
		$data['no_SK']				= $this->input->post('no_SK', TRUE);
		$data['tgl_SK']				= $this->input->post('tgl_SK', TRUE);
		$data['tmt_SK']				= $this->input->post('tmt_SK', TRUE);
		$data['FK_jabatan_asal']	= $this->input->post('FK_jabatan_asal', TRUE);
		$data['FK_jabatan_baru']	= $this->input->post('FK_jabatan_baru', TRUE);
		$data['FK_unit_kerja_asal']	= $this->input->post('FK_unit_kerja_asal', TRUE);
		$data['FK_unit_kerja_baru']	= $this->input->post('FK_unit_kerja_baru', TRUE);
		$data['ket_mutasi']			= $this->input->post('ket_mutasi', TRUE);
		return $data;
	}
	
	function _get_form_kerja_values()
	{
		$data['tmt_jabatan']	= $this->input->post('tgl_SK', TRUE);
		$data['FK_jabatan']		= $this->input->post('FK_jabatan_baru', TRUE);
		$data['FK_unit_kerja']	= $this->input->post('FK_unit_kerja_baru', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('FK_jabatan_asal', 'FK_jabatan_asal', 'required');
		$this->form_validation->set_rules('FK_jabatan_baru', 'FK_jabatan_baru', 'required');
		$this->form_validation->set_rules('FK_unit_kerja_asal', 'FK_unit_kerja_asal', 'required');
		$this->form_validation->set_rules('FK_unit_kerja_baru', 'FK_unit_kerja_baru', 'required');
		$this->form_validation->set_rules('no_SK', 'no_SK', 'required');
		$this->form_validation->set_rules('jns_mutasi', 'jns_mutasi', 'required');
		return $this->form_validation->run();
	}
}