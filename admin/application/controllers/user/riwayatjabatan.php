<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatjabatan extends Member_Controller
{
	function Riwayatjabatan()
	{
		parent::Member_Controller();
		$this->load->model('jabatan_model', 'jabatan');
		$this->load->model('riwayat_jabatan_model','riwayat_jabatan');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('unit_kerja_model', 'unit_kerja');
        $this->load->model('lookup_model','lookup');
		
		
	}
	
	function index()
	{
        $kd_pegawai = $this->uri->segment(4);
        $this->template->metas('title', 'SIMPEGA | Jabatan Pegawai');
		$this->browse($kd_pegawai);
	}
	
	function browse($kd_pegawai)
	{
		
		$ordby = 'id_riwayat_jabatan';
		
		$data['list_jabatan'] = $this->riwayat_jabatan->find(NULL, array('kd_pegawai' => $kd_pegawai), null, $limit_per_page,$start,$ordby);
		//$data['list_pangkat'] = $this->riwayat_gol_kepangkatan->find(NULL, array('kd_pegawai' => $kd_pegawai), null, $limit_per_page,$start,$ordby);
        $data['pegawai'] = $this->pegawai->retrieve_by_pkey($kd_pegawai);
        
        $data['judul'] 		= "Data Jabatan dari: " . $data['pegawai']['nama_pegawai'];
		$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
		$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
		$this->template->display('pegawai/riwayatjabatan/list', $data);
	}
	/*function browse($group=0)
	{
		$pegawai_id = $this->pegawai->d_pegawai);
		$data['list_pasangan'] = $this->pasangan_pegawai->retrieve_by_idpeg($pegawai_id);
		$data['judul'] 		= "Pasangan Pegawai";
		$this->template->display('pegawai/pasangan/list', $data);
	}*/
	
	function add()
	{
		if ($this->_validate())
		{
            $kd_pegawai = $this->input->post('kd_pegawai');
            $data = $this->_get_form_values();
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_jabatan_terakhir'] = $data['id_jabatan'];
				$data_pegawai['tmt_jabatan_terakhir'] = $data['tmt_jabatan'];

				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				
				$data_status['aktif'] = '0';
				$this->riwayat_jabatan->update_status($kd_pegawai,$data_status);
			}
			$this->riwayat_jabatan->add($data);
			set_success('Data jabatan pegawai berhasil disimpan.');
			redirect('/pegawai/riwayatjabatan/index/' . $kd_pegawai);
		}
		else
		{
			$kd_pegawai = $this->uri->segment(4, '');
            $this->template->metas('title', 'SIMPEGA | Jabatan Pegawai :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
            $data['pegawai']=$this->pegawai->retrieve_by_pkey($kd_pegawai);
			$data['judul']='Tambah Data Jabatan dari: ' . $data['pegawai']['nama_pegawai'];
			
			$data['id_riwayat_jabatan']=$this->riwayat_jabatan->get_id();
			$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
			$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
            $data['status_assoc'] = $this->lookup->status_assoc();
			$this->template->display('/pegawai/riwayatjabatan/detail', $data);
		}
	}
	
	function edit($id)
	{
		
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_riwayat_jabatan'] = $id;
			if ($data['aktif']=='1')
            {
				$data_pegawai['id_jabatan_terakhir'] = $data['id_jabatan'];
				$data_pegawai['tmt_jabatan_terakhir'] = $data['tmt_jabatan'];

				$this->pegawai->modify($kd_pegawai, $data_pegawai);
				
				$data_status['aktif'] = '0';
				$this->riwayat_jabatan->update_status($kd_pegawai,$data_status);
			}
			$this->riwayat_jabatan->update($id, $data);
			set_success('Perubahan data jabatan pegawai berhasil disimpan');
			redirect('/pegawai/riwayatjabatan/index/'. $data['kd_pegawai'], 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jabatan Pegawai :: Ubah');
			$data = $this->riwayat_jabatan->retrieve_by_pkey($id);
			$data['unit_kerja_assoc'] = $this->unit_kerja->get_assoc('nama_unit');
			$data['jabatan_assoc']=$this->jabatan->get_jabatan_assoc('nama_jabatan');
            $data['status_assoc'] = $this->lookup->status_assoc();
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				
                $data['pegawai'] = $this->pegawai->retrieve_by_pkey($data['kd_pegawai']);
				
				$data['judul']='Edit Jabatan dari: '. $data['pegawai']['nama_pegawai'];
				
				$this->template->display('/pegawai/riwayatjabatan/detail', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/pegawai/riwayatjabatan', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->riwayat_jabatan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Jabatan Pegawai :: Hapus');
		confirm("Yakin menghapus data jabatan pegawai?");
		$res = $this->riwayat_jabatan->delete($idField);
		set_success('Data jabatan pegawai berhasil dihapus');
		redirect('/pegawai/riwayatjabatan/index/'. $data['kd_pegawai'], 'location');
	}

	function _clear_form()
	{
		$data['id_riwayat_jabatan']	= '';
		$data['kd_pegawai']	= '';
		$data['id_jabatan']	= '';
		$data['no_sk_jabatan']	= '';
		$data['tgl_sk_jabatan']	= '';
		$data['tgl_selesai']	= '';
		$data['kode_unit']	= '';
		$data['keterangan']	= '';
		$data['aktif']	= '';
		
		
		return $data;
	}	
	
	function _get_form_values()
	{
	   	
	   
		
		
        $data['id_riwayat_jabatan']	= $this->riwayat_jabatan->get_id();
		$data['kd_pegawai']	= $this->input->post('kd_pegawai', TRUE);
		$data['id_jabatan']	= $this->input->post('id_jabatan', TRUE);
		$data['no_sk_jabatan']	= $this->input->post('no_sk_jabatan', TRUE);
		$data['tgl_sk_jabatan']	= $this->input->post('tgl_sk_jabatan', TRUE);
		$data['tmt_jabatan']	= $this->input->post('tmt_jabatan', TRUE);
		$data['tgl_selesai']	= $this->input->post('tgl_selesai', TRUE);
		$data['kode_unit']	= $this->input->post('kode_unit', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		$data['aktif']	= $this->input->post('aktif', TRUE);
		
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('id_jabatan', 'id_jabatan', 'required');
		//$this->form_validation->set_rules('kd_status_keluarga', 'kd_status_keluarga', 'required');
		return $this->form_validation->run();
	}
}