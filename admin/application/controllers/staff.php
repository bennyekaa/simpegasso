<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Staff extends MY_Controller
{
	function Staff()
	{
		parent::MY_Controller();
		$this->template->metas('title', 'SIMPEGA | Sistem Informasi Kepegawaian');
		$this->load->library('validation');
		$this->load->model('users_model','users');
		$this->load->model('lookup_model','lookup');
		$this->load->model('user_pegawai_model','user_pegawai');
		$this->load->model('pegawai_model', 'pegawai');
	}
	
	function index()
	{
		// echo "ini home"; die();
		$this->access->restrict();	
		if (!$this->user)
		$this->user = $this->access->get_user();
		//show_error(var_dump($this->user));
		if ($this->user->user_group=='Staff Data')
			$this->template->display('staff/home');
		else if ($this->user->user_group=='Sub Admin')
			$this->template->display('staff/home');
		else
			$this->template->display('staff/home');
	}

	/*TESTERR PLEASE HAPUS LATERRR*/
	function background_login() {
		// $auth_url = "https://auth.um.ac.id/saml2/idp/SSOService.php";
		// $auth_url = "http://simpega.web.com/admin/staff/login_sso";
		/*$data = [
			"username" => '1993091020181608',
			"password" => 'hiensouhi3um',
			"AuthState" => '_a8899340801dc478877d95f9e57246037a6af4bcc2',
		];*/
		$data = array();
		$http_data = http_build_query($data);
		//
		// A very simple PHP example that sends a HTTP POST to a remote site
		//

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $auth_url);
		// curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $http_data);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);

		// In real life you should use something like:
		// curl_setopt($ch, CURLOPT_POSTFIELDS, 
		//          http_build_query(array('postvar1' => 'value1')));

		// Receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec($ch);
		$redirectedUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
		curl_close ($ch);

		// var_dump($server_output);
		var_dump($redirectedUrl);
		// Further processing ...
		// if ($server_output == "OK") { ... } else { ... }
	}
	function curl_login() {
		// $url = "http://simpega.web.com/admin/staff/login_sso";
		// var_dump($this->get_web_page($url));
	}
	function get_web_page( $url, $cookiesIn = '' ){
		$options = array(
          CURLOPT_RETURNTRANSFER => true,     // return web page
          CURLOPT_HEADER         => true,     //return headers in addition to content
          CURLOPT_FOLLOWLOCATION => true,     // follow redirects
          CURLOPT_ENCODING       => "",       // handle all encodings
          CURLOPT_AUTOREFERER    => true,     // set referer on redirect
          CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
          CURLOPT_TIMEOUT        => 120,      // timeout on response
          CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
          CURLINFO_HEADER_OUT    => true,
          CURLOPT_SSL_VERIFYPEER => true,     // Validate SSL Certificates
          CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
          CURLOPT_COOKIE         => $cookiesIn
        );

		$ch      = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$rough_content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		$redirectedUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
		curl_close( $ch );

		$header_content = substr($rough_content, 0, $header['header_size']);
		$body_content = trim(str_replace($header_content, '', $rough_content));
		$pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m"; 
		preg_match_all($pattern, $header_content, $matches); 
		$cookiesOut = implode("; ", $matches['cookie']);

		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['headers']  = $header_content;
		$header['redirectedUrl'] = $redirectedUrl;
		$header['content'] = $body_content;
		$header['cookies'] = $cookiesOut;
		return $header;
	}
	/* ----------------- HAPUSSSSSSSSSSSSSSS -----------------------*/

	function login_sso() 
	{
		// echo "waw"; die();
		$this->load->library('validation');
		$_POST['token'] = 'asoy';
		$this->validation->set_rules(array('token' => 'check_login_sso'));
		var_dump($this->validation->run()); die();
		if ($this->validation->run() === TRUE) {
			// $uri = $this->session->flashdata('referrer') ? $this->session->flashdata('referrer') : 'staff';
			redirect('');
		}
		else {
			set_error('Anda tidak memiliki akses sebagai admin');
			$this->index();
		}
	}

	function check_auth() {
		echo "SESSION: ";
		print("<pre>".print_r($this->session->all_userdata(), true)."</pre>");

		echo "<hr> COOKIE: ";
		print("<pre>".print_r($_COOKIE, true)."</pre>");
		
	}

	function login()
	{
		$this->access->restrict('logged_out');

		$this->load->library('validation');
		$this->load->helper('form');

		$rules = array(
				'username'	=>	'trim|required|strip_tags',
				'password'	=>	'trim|required',
				'token'			=>	'check_login'
		);
		$this->validation->set_rules($rules);

		$fields = array(
				'username'	=>	'username',
				'password'	=>	'password'
		);
		$this->validation->set_fields($fields);
		
		if ($this->validation->run() == FALSE)
		{
			$this->template->metas('title', 'SIMPEGA | Login');
			$data['token'] = generate_token();

			/*$data['pegawai']=$this->pegawai->get_list_pegawai();
			$data["ptt"]=$this->pegawai->get_list_ptt();*/
		
			set_error('Username/Password yang anda masukkan salah!');	
			$this->session->keep_flashdata('referrer');
			$this->load->view('staff/login', $data);
		}
		else
		{
			$uri = $this->session->flashdata('referrer') ? $this->session->flashdata('referrer') : 'staff';
			redirect($uri);
		}
	}
	

	function detail_pegawai($id_pegawai=null){
		$data=null;
		if($id_pegawai==null){
			return NULL;
		}
		$data['detail'] = $this->pegawai->detail_pegawai($id_pegawai);
		$data['riwayat'] = $this->pegawai->riwayat_pendidikan($id_pegawai);
		$data['detail']['photo'] = file_exists(APPPATH."../public/photo/photo_".$data['detail']['kd_pegawai'].".jpg") ? $data['detail']['kd_pegawai'] : 'none';
		$data['key'] = "d0453b68-071f-4fa3-ab6e-a8ea6cec148b";
		$data['url'] =
		"https://litabmas.um.ac.id/Api/data/get/profile/identitas/byUser/key=" . $data['key'] . ";nip=" . $data['detail']['NIP'] . ";";
		$data['kepakaran'] = "https://pakar.um.ac.id/Data/Peneliti/u=";
		// var_dump($data);
		$options = array(
			"http" => array(
				"header" => "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n" // i.e. An iPad
			)
		);
		$context = stream_context_create($options);
		$result = file_get_contents($data['url'], false, $context);
		$result = json_decode($result);
		// var_dump($result);
		$data['keyuser'] = $result[0]->key;
		$data['nama'] = $result[0]->nama;
		$data['id_scopus'] = $result[0]->id_scopus;
		$data['id_research_gate'] = $result[0]->id_research_gate;
		$data['id_sinta'] = $result[0]->id_sinta;
		$data['id_google_scholar'] = $result[0]->id_google_scholar;
		// var_dump($data);

		$this->load->view('staff/detail_pegawai', $data);
	}

	/*function detail_ptt($id_pegawai=null){
		$data=null;
		if($id_pegawai==null){
			return NULL;
		}
		$data['detail']=$this->pegawai->detail_ptt($id_pegawai);
		$this->load->view('staff/detail_ptt', $data);
	}*/

	function add()
	{
		$this->access->restrict();
		$this->template->metas('title', 'SIMPEGA | Register User :: Tambah');  
		$this->template->load_js('public/js/linkedselect.js');
		$this->template->load_js('public/js/jquery.validate.js');
		$data_peg = $this->lookup->get_datafield('pegawai','NIP','kd_pegawai');
		$kd_pegawai = $data_peg[$this->input->post('NIP')];
		$pwd1 = $this->input->post('password', TRUE);;
		$pwd2 = $this->input->post('password2', TRUE);;
		$nama == '';
		
			if ($this->_validate('add')) {		
				$query = mysql_query("select nama_pegawai from pegawai where kd_pegawai='".$kd_pegawai."' and tgl_lahir= '".$this->input->post('tgl_lahir')."' "); 					
				if ($query) {
					$data_ada=mysql_fetch_array($query);  
					$nama= $data_ada['nama_pegawai'];	}
					if($pwd1!=$pwd2) {		
						set_error('password yang anda masukkan tidak sama');
						redirect('staff/add');
					}
					elseif  ($nama != ''){
						//set_success('query ada');
	
						$data = $this->_get_form_values();
						$cek = $this->user_pegawai->cek_id_peg($kd_pegawai);
							//show_error(var_dump($cek));
							if($cek!=TRUE) {		
								$this->user_pegawai->add(array('user_id' => $this->input->post('user_id'),'kd_pegawai'=>$kd_pegawai));
								//$this->user_pegawai->add(array('user_id' => $this->input->post('user_id'),'kd_pegawai'=>$kd_pegawai));
								$this->users->add($data);
								set_success('Data user berhasil disimpan');
								redirect('staff/login');
							}
							else {
								set_error('Penyimpanan gagal, pegawai sudah terdaftar');
								redirect('staff/add');
							}
						}
					else{
						set_error('Penyimpanan gagal, NIP dan TGL LAHIR tidak terdaftar');
						redirect('staff/add');
					}
				}
			else{
				$data = $this->_clear_form();
				if($this->input->post('nip')){
					$filterRules=array("NIP"=>$this->input->post('nip'));
					$data['pegawai']=$this->pegawai->find($filterRules);
					// var_dump($data['pegawai']);
					// exit();
					// data = false if pegawai did not found
				}
				
				$data['action']='add';
				$data['judul']='Register User Baru';
				$data['user_id']=$this->users->get_id();
				$data['is_new'] = TRUE;
				//$data['pegawai_options']=array(0=>"-- Pilih Pegawai --")+$this->pegawai->get_assoc("nama");
				$this->template->display('staff/detail_register_user', $data);
			}
			

	}

	function change_password()
	{
		$this->access->restrict();

		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_rules('oldpass', 'Password Lama', 'required|check_password[oldpass]|min_length[1]|max_length[12]');
		$this->form_validation->set_rules('password', 'Password Baru', 'required|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Konfirmasi Password ', 'required');
		$data['judul'] = 'Ubah Password User';

		if ($this->form_validation->run() == FALSE)
		{
			$this->template->metas('title', 'SIMPEGA | Ubah Pasword');
			$this->template->display('sistem/ubah-password', $data);
		}
		else
		{
			$this->access->change_password($this->input->post('password',TRUE));
			set_success('Password Sukses diganti');
			redirect('/staff/logout');
		}
	}
	
	
	
	function logout()
	{
		// Logout App
		$this->access->restrict();
		$this->user = $this->access->get_user();
		$this->load->model('users_model');
		$this->users_model->update_last_login($this->user->user_id);
		$this->access->logout();

		// Logout UM SSO
		$this->load->library('simplesamlphp');
		$as = new \SimpleSAML\Auth\Simple('ssoum-sp');
		$as->logout(array('ReturnTo' => base_url('')));
	}
	function _clear_form()
	{
		$data['user_id']	= '';
		$data['username']	= '';
		$data['password']	= '';
		//$data['password2']	= '';
		$data['FK_group_id']	= '';
		$data['tgl_lahir']	= '';
		$data['NIP']	= '';
		$data['email']	= '';
		$data['name']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['user_id']	= $this->input->post('user_id', TRUE);
		$data['username']	= $this->input->post('username', TRUE);
		$data['password']	= $this->input->post('password', TRUE);;
		//$data['password2']	= $this->input->post('password2', TRUE);;
		$data['FK_group_id']= $this->input->post('FK_group_id', TRUE);;
		$data['email']		= $this->input->post('email', TRUE);;
		$data['name']		= $this->input->post('name', TRUE);;

		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('password2', 'password2', 'required');
		$this->form_validation->set_rules('FK_group_id', 'FK_group_id', 'required');
		$this->form_validation->set_rules('name', 'name', 'required');
		return $this->form_validation->run();
	}

	function get_datatable() 
	{
		$requestData= $_REQUEST;
		$columns = array( 
			0   =>  'id', 
			1   =>  'NIP',
			2   =>  'gelar_depan, nama_pegawai, gelar_belakang',
			3   =>  'golongan',
			4   =>  'nama_unit',
			5   =>  ''
		);
		if($requestData['status_pegawai'] == 'cpns') {
			$data_db = $this->pegawai->get_list_cpns();
		} if($requestData['status_pegawai'] == 'pns'){
			$data_db = $this->pegawai->get_list_pegawai();
		} if ($requestData['status_pegawai'] == 'ptt') {
			$data_db = $this->pegawai->get_list_ptt();
		} if ($requestData['status_pegawai'] == 'dosen') {
			$data_db = $this->pegawai->get_list_dosen();
		} if ($requestData['status_pegawai'] == 'tendik') {
			$data_db = $this->pegawai->get_list_tendik();
		}
		$totalData = count($data_db);

		$sql = "SELECT
		   pegawai.kd_pegawai, pegawai.NIP, pegawai.NIDN, pegawai.gelar_depan, pegawai.nama_pegawai, pegawai.gelar_belakang, pegawai.npwp, pegawai.email, agama.agama, jenis_pegawai.jenis_pegawai, jabatan.nama_jabatan, unit_kerja.nama_unit";
		   if($requestData['status_pegawai'] == 'cpns' || $requestData['status_pegawai'] == 'pns' || $requestData['status_pegawai'] == 'dosen' || $requestData['status_pegawai'] == 'tendik') {
		   	$sql.=", golongan_pangkat.golongan" ;
		   }
	   $sql.=" FROM pegawai"
		   ." LEFT JOIN jabatan ON jabatan.id_jabatan = pegawai.id_jabatan_terakhir"
			." LEFT JOIN agama ON agama.kd_agama = pegawai.kd_agama"
		  ." LEFT JOIN jenis_pegawai ON jenis_pegawai.id_jns_pegawai = pegawai.id_jns_pegawai"
		  ." LEFT JOIN unit_kerja ON unit_kerja.kode_unit = pegawai.kode_unit";
		if ($requestData['status_pegawai'] == 'cpns' || $requestData['status_pegawai'] == 'pns' || $requestData['status_pegawai'] == 'dosen' || $requestData['status_pegawai'] == 'tendik') {
		  	$sql.=" LEFT JOIN golongan_pangkat ON golongan_pangkat.id_golpangkat = pegawai.id_golpangkat_terakhir";
		  }
		$sql.=" WHERE NIP NOT LIKE '' AND NIP NOT LIKE '-'";
		if($requestData['status_pegawai'] == 'pns') {
			$sql.=" AND status_pegawai < 3 ";
		} else if($requestData['status_pegawai'] == 'ptt') { 
			$sql.=" AND status_pegawai > 2 ";
		} else if ($requestData['status_pegawai'] == 'cpns') {
			$sql .= " AND status_pegawai = 1 ";
		} else if ($requestData['status_pegawai'] == 'dosen') {
			$sql .= " AND pegawai.id_jns_pegawai = 5 ";
		} else if ($requestData['status_pegawai'] == 'tendik') {
			$sql .= " AND pegawai.id_jns_pegawai <> 5 ";
		}
		if(!empty($requestData['search']['value'])) {
			$sql.=" AND (pegawai.NIP LIKE '%".$requestData['search']['value']."%'";
			$sql.=" OR pegawai.nama_pegawai LIKE '%".$requestData['search']['value']."%'";
			if($requestData['status_pegawai'] == 'pns') {
				$sql .= " OR pegawai.npwp LIKE '%" . $requestData['search']['value'] . "%'";
				$sql.=" OR golongan_pangkat.golongan LIKE '%".$requestData['search']['value']."%'";
				$sql .= " OR golongan_pangkat.pangkat LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR jabatan.nama_jabatan LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR jenis_pegawai.jenis_pegawai LIKE '%" . $requestData['search']['value'] . "%'";
			}
			if ($requestData['status_pegawai'] == 'cpns') {
				$sql .= " OR pegawai.npwp LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR golongan_pangkat.golongan LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR golongan_pangkat.pangkat LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR jabatan.nama_jabatan LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR jenis_pegawai.jenis_pegawai LIKE '%" . $requestData['search']['value'] . "%'";
			}
			if ($requestData['status_pegawai'] == 'ptt') {
				$sql .= " OR pegawai.npwp LIKE '%" . $requestData['search']['value'] . "%'";
			}
			if ($requestData['status_pegawai'] == 'dosen') {
				$sql .= " OR pegawai.npwp LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR golongan_pangkat.golongan LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR golongan_pangkat.pangkat LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR jabatan.nama_jabatan LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR jenis_pegawai.jenis_pegawai LIKE '%" . $requestData['search']['value'] . "%'";
			}
			if ($requestData['status_pegawai'] == 'tendik') {
				$sql .= " OR pegawai.npwp LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR golongan_pangkat.golongan LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR golongan_pangkat.pangkat LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR jabatan.nama_jabatan LIKE '%" . $requestData['search']['value'] . "%'";
				$sql .= " OR jenis_pegawai.jenis_pegawai LIKE '%" . $requestData['search']['value'] . "%'";
			}
			$sql.=" OR unit_kerja.nama_unit LIKE '%".$requestData['search']['value']."%')";
		}
		$query = $this->pegawai->rawQuery($sql);
		//  var_dump($sql); die();
		$totalFiltered = $query->num_rows();

		$sql .= " ORDER BY pegawai.id_jns_pegawai ASC, pegawai.id_golpangkat_terakhir DESC, ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']." "; 
		$query = $this->pegawai->rawQuery($sql);

		$data = array(); $i=0;
		foreach ($query->result_array() as $row) {
			$tableData   = array(); 
	    $tableData[] = ($i + 1);
	    $tableData[] = $row["NIP"];
	    $tableData[] = $row["gelar_depan"].' '.$row["nama_pegawai"].' '.$row["gelar_belakang"];
	    $tableData[] = isset($row["golongan"]) ? $row["golongan"] : '';
	    $tableData[] = $row["nama_unit"];
	    $tableData[] = '<a href="javascript:void(0);" class="btn btn-info btn-sm" onclick="showDetail('.$row["kd_pegawai"].');" title="Detail pegawai">Detail</a>';
	    $data[] = $tableData; $i++;
	  }
	  $totalData = count($data);
	  $json_data = array(
	  	"draw"            => intval( $requestData['draw'] ),
	  	"recordsTotal"    => intval( $totalData ),
	  	"recordsFiltered" => intval( $totalFiltered ),
	  	"data"            => $data
	  );
	  echo json_encode($json_data);
	}
}