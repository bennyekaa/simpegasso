<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


class pelatihan extends Member_Controller {

	function pelatihan()
	{
		parent::Member_Controller();
		$this->template->load_css('public/css/table.css');
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->model('lookup_model','lookup');
		$this->load->model('pelatihanmodel');
	}

	function index()
	{
		$this->browse();
	}

	function browse($group=0) {
		if ($this->input->post('group'))
			$group= $this->input->post('group');
		$paging_uri=5;
		$this->load->model('lookup_model','lookup');
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
		$start=0;
		$limit_per_page = 20;
		
		$this->load->model('pelatihanmodel');
		$pelatihan_list = $this->pelatihanmodel->findAll( $limit_per_page,$start);
		$the_results['judul'] = "Laporan Pelatihan Pegawai";
		$the_results['group'] =$group;

		$this->load->model('pelatihanmodel');
		$NIP = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		$nm_pegawai= $this->lookup->get_datafield('pegawai','kd_pegawai','nama');
		$tgl_penghargaan = $this->lookup->get_datafield('riwayat_pelatihan','id_riwayat_pelatihan','tgl_mulai');
		$nm_penghargaan = $this->lookup->get_datafield('riwayat_pelatihan','id_riwayat_pelatihan','nama_pelatihan');
		
		$this->load->model('pelatihanmodel');
		if($pelatihan_list) foreach($pelatihan_list as $pelatihan){
			$pelatihan['NIPpeg']=$NIP[$pelatihan['kd_pegawai']];
			$pelatihan['nama_peg']=$nm_pegawai[$pelatihan['kd_pegawai']];
			$pelatihan['tglpenghargaan']=$tgl_penghargaan[$pelatihan['id_riwayat_pelatihan']];
			$pelatihan['nama_penghargaan']=$nm_penghargaan[$pelatihan['id_riwayat_pelatihan']];
			$temp[]=$pelatihan;
		}
		$the_results['pelatihan_list']=$temp;
		
		$config['base_url']     = site_url('laporan/pelatihan/browse/'.$ordby.'/');
		$config['total_rows']   = $this->pelatihanmodel->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] = 'berikutnya &raquo;';
		$config['prev_link'] = '&laquo; sebelumnya ';
		$the_results['ordbyid'] = 'laporan/pelatihan/browse/id_riwayat_pelatihan';	
		$this->template->display('laporan/pelatihan/list_pelatihan', $the_results);
	} 	

	function view($idField){
		
		$this->load->model('pelatihanmodel');
		$data = $this->pelatihanmodel->retrieve_by_pkey($idField);
		$data['NIP'] = $this->lookup->get_NIPpegawai($data['kd_pegawai']);
		$data['nama_peg'] = $this->lookup->get_namapegawai($data['kd_pegawai']);
		$data['nama_penghargaan']	= $this->lookup->get_nama_pelatihan($data['id_riwayat_pelatihan']);
		$data['ket_riwayat_penghargaan']	= $this->lookup->get_ket_pelatihan($data['id_riwayat_pelatihan']);
		$data['judul']='Detail pelatihan';
		$this->template->display('/laporan/pelatihan/view', $data);
	}

	function printtopdf()
	{
		$this->load->model('pelatihanmodel');
		$properties=array();
		$properties['title']='Laporan Pelatihan Pegawai';
		$properties['subject']='pegawai';
		$properties['keywords']='riwayat_pelatihan';
		$properties['papersize']="A4";
		$properties['paperlayout']='P';	
		$properties['filename']='Laporan pelatihan';
		$pelatihan_list = $this->pelatihanmodel->findAll( $limit_per_page,$start);

		$this->load->model('pelatihanmodel');
		$NIP = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		$nm_pegawai= $this->lookup->get_datafield('pegawai','kd_pegawai','nama');
		$tgl_penghargaan = $this->lookup->get_datafield('riwayat_pelatihan','id_riwayat_pelatihan','tgl_mulai');
		$nm_penghargaan = $this->lookup->get_datafield('riwayat_pelatihan','id_riwayat_pelatihan','nama_pelatihan');
		$ket_penghargaan = $this->lookup->get_datafield('riwayat_pelatihan','id_riwayat_pelatihan','ket_riwayat_pelatihan');
		
		$this->load->model('pelatihanmodel');
		if($pelatihan_list) foreach($pelatihan_list as $pelatihan){
			$pelatihan['NIPpeg']=$NIP[$pelatihan['kd_pegawai']];
			$pelatihan['nama_peg']=$nm_pegawai[$pelatihan['kd_pegawai']];
			$pelatihan['tglpenghargaan']=$tgl_penghargaan[$pelatihan['id_riwayat_pelatihan']];
			$pelatihan['nama_penghargaan']=$nm_penghargaan[$pelatihan['id_riwayat_pelatihan']];
			$pelatihan['ket']=$ket_penghargaan[$pelatihan['id_riwayat_pelatihan']];
			$temp[$pelatihan['id_riwayat_pelatihan']]=$pelatihan;
		}
		$the_results['pelatihan_list']=$temp;
		
		$the_results['judul'] = "Daftar pelatihan";
		$html=$this->load->view('laporan/pelatihan/list_pelatihanpdf', $the_results,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}
}
?>
