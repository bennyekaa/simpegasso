<?php 


/**
* 
*/
class smart_mutasi extends Member_Controller
{
	
	function __construct()
	{
		parent::Member_Controller();
		$this->load->model('unit_kerja_model', 'unit_kerja');
		$this->load->model('jabatan_model', 'jabatan');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('jenis_mutasi_model', 'jenis_mutasi');
		$this->load->model('mutasi_model', 'mutasi');
		$this->load->model('jenis_jabatan_uj2016_model', 'jenis_jabatan');
		$this->load->model('riwayat_jabatan_uj_model', 'riwayat_jabatan_uj');
	}


	function index(){

		// $data['draft_mutasi_eksternal'] =$this->draft_mutasi_eksternal(); //contain 2 key:internal and eksternal
		// $data['draft_mutasi_internal'] =$this->draft_mutasi_internal(); //contain 2 key:internal and eksternal
		$data['draft_mutasi_internal'] =$this->draft_mutasi(); //contain 2 key:internal and eksternal

		$this->template->metas('title', 'Fitur Smart Mutasi');
		$this->browse($data);


	}

	function browse($data){

		$this->template->display("laporan/smart_mutasi/smart_mutasi", $data);
	}

	function list_daftar_nominatif($kode_unit_kerja=null){
		if ($kode_unit_kerja===null) {
			return null;
		}
		return $this->pegawai->list_daftar_nominatif($kode_unit_kerja);
	}

	function get_jabatan_struktural(){
		$result="";
		$get_jabatan_struktural=$this->jabatan->get_jabatan_struktural();
		if (sizeof($get_jabatan_struktural)!=0) {
			foreach ($get_jabatan_struktural as $jabatan_struktural) {
				// $jabatan_struktural['nama_jabatan_uj']=;
				$jabatan_struktural['nama_jabatan_uj'] = (strlen($jabatan_struktural['nama_jabatan_uj'])>44) ? substr($jabatan_struktural['nama_jabatan_uj'], 0,41)."..." : $jabatan_struktural['nama_jabatan_uj'];
				$result.="<option value='".$jabatan_struktural['id_jabatan_uj']."'>".$jabatan_struktural['nama_jabatan_uj']."</option>";
			}
		}

		echo $result;
	}

	function get_jabatan_fungsional_umum(){
		$result="";
		$get_jabatan_fungsional_umum=$this->jabatan->get_jabatan_fungsional_umum();
		if (sizeof($get_jabatan_fungsional_umum)!=0) {
			foreach ($get_jabatan_fungsional_umum as $jabatan_fungsional_umum) {
				$jabatan_fungsional_umum['nama_jabatan_uj'] = (strlen($jabatan_fungsional_umum['nama_jabatan_uj'])>44) ? substr($jabatan_fungsional_umum['nama_jabatan_uj'], 0,41)."..." : $jabatan_fungsional_umum['nama_jabatan_uj'];
				$result.="<option value='".$jabatan_fungsional_umum['id_jabatan_uj']."'>".$jabatan_fungsional_umum['nama_jabatan_uj']."</option>";
			}
		}

		echo $result;
	}

	function get_jabatan_fungsional_khusus(){
		$result="";
		$get_jabatan_fungsional_khusus=$this->jabatan->get_jabatan_fungsional_khusus();
		if (sizeof($get_jabatan_fungsional_khusus)!=0) {
			foreach ($get_jabatan_fungsional_khusus as $jabatan_fungsional_khusus) {
				 					
				if(strlen($jabatan_fungsional_khusus['nama_jabatan_uj'])>44){
					if (strpos($jabatan_fungsional_khusus['nama_jabatan_uj'], 'Pengelola Pengadaan Barang dan Jasa')!==false) {
						$jabatan_fungsional_khusus['nama_jabatan_uj']=substr($jabatan_fungsional_khusus['nama_jabatan_uj'], 0,27).".. ".substr($jabatan_fungsional_khusus['nama_jabatan_uj'],-20);
					}
					else{
							$jabatan_fungsional_khusus['nama_jabatan_uj'] =substr($jabatan_fungsional_khusus['nama_jabatan_uj'], 0,42)."..";
					}
				}

				$result.="<option value='".$jabatan_fungsional_khusus['id_jabatan_uj']."'>".$jabatan_fungsional_khusus['nama_jabatan_uj']."</option>";
			}
		}

		echo $result;
	}

	function get_unit_kerja_otk($tahun=null){
		$result=null;
		if ($tahun!=null) {
			$tahun="OTK ".$tahun;
			$unit_kerja_otk=$this->jabatan->get_unit_kerja_otk($tahun);

			foreach ($unit_kerja_otk as $unit_kerja) {
				$unit_kerja['nama_unit']= (strlen($unit_kerja['nama_unit'])>40) ? substr($unit_kerja['nama_unit'], 0,38).".." : $unit_kerja['nama_unit'] ;
				$result.="<option value='".$unit_kerja['kode_unit']."'>".$unit_kerja['nama_unit']."</option>";
			}
		}

		echo $result;
	}

	function draft_mutasi(){
		$mutasi_internal=$this->pegawai->draft_mutasi();

		for ($i=0; $i <sizeof($mutasi_internal) ; $i++) { 
			$mutasi_internal[$i]['usia']=$this->hariKeTahunBulan($mutasi_internal[$i]['usia']);
			$mutasi_internal[$i]['selama']=$this->hariKeTahunBulan($mutasi_internal[$i]['selama']);
		}
		
		return $mutasi_internal;
	}

	function draft_mutasi_internal(){
		$mutasi_internal=$this->pegawai->draft_mutasi_internal();

		for ($i=0; $i <sizeof($mutasi_internal) ; $i++) { 
			$mutasi_internal[$i]['usia']=$this->hariKeTahunBulan($mutasi_internal[$i]['usia']);
			$mutasi_internal[$i]['selama']=$this->hariKeTahunBulan($mutasi_internal[$i]['selama']);
		}
		
		return $mutasi_internal;
	}

	function draft_mutasi_eksternal(){
		$mutasi_eksternal=$this->pegawai->draft_mutasi_eksternal();

		for ($i=0; $i <sizeof($mutasi_eksternal) ; $i++) { 
			$mutasi_eksternal[$i]['usia']=$this->hariKeTahunBulan($mutasi_eksternal[$i]['usia']);
			$mutasi_eksternal[$i]['selama']=$this->hariKeTahunBulan($mutasi_eksternal[$i]['selama']);
		}
		
		return $mutasi_eksternal;
	}

	function hariKeTahunBulan($hari){
		$data["tahun"]=intval(floor($hari/365));
		$data["bulan"]=intval(ceil(($hari%365)/30));

		return $data;
	}

	function detail($kd_pegawai){
		$data=null;
		$this->template->display("laporan/smart_mutasi/detail", $data);
	}

	function tambah(){
		$data = array(
						'jenis_mutasi' => $this->get_tipe_mutasi(),
						'data_mutasi'  => $this->get_data_mutasi(),
					);
	
		$this->template->display("laporan/smart_mutasi/tambah", $data);
	}

	function get_tipe_mutasi(){
		$result="<select name='jenis_mutasi' id=''>";
		
		foreach ($this->jenis_mutasi->get_tipe_mutasi() as $jenis_mutasi) {
			$result.="<option value='".$jenis_mutasi->jns_mutasi."'>".$jenis_mutasi->nama_mutasi."</option>";
		}

		$result.="</select>";

		return $result;
			
	}

	function simpan(){

		$data = array(
				'jns_mutasi' => $this->input->post('jenis_mutasi'), 
				'kd_pegawai'=> $this->input->post("kd_pegawai"),
				'no_sk'=> $this->input->post("nomor_sk"),
				'tgl_sk'=> $this->input->post("tgl_sk"),
				'tmt_sk'=> $this->input->post("tgl_berlaku_sk"),
				'tgl_mutasi'=> $this->input->post("tgl_mutasi"),
				'kode_unit_kerja_asal'=> $this->input->post("id_unit_kerja_lama"),
				'kode_unit_kerja_baru'=> $this->input->post("unit_kerja_baru"),
				'id_jabatan_asal'=> $this->input->post("id_jabatan_lama"),
				'id_jabatan_baru'=> $this->input->post("jabatan_baru"),
				'ket_mutasi'=> $this->input->post("keterangan_mutasi"),
				'id_golpangkat'=> $this->input->post("id_golpangkat"),
				'isaktif'=> $this->input->post("status")

			);

		$data_riwayat = array(
			"kd_pegawai" => $data["kd_pegawai"],
			"id_jabatan" => $data["id_jabatan_baru"],
			"job_class" => $this->input->post("job_class"),
			"job_value" => $this->input->post("job_value"),
			"id_golpangkat" => $data["id_golpangkat"],
			"tmt_pangkat" => "",
			"no_sk_jabatan" => $data["no_sk"],
			"tgl_sk_jabatan" => $data["tgl_sk"],
			"tmt_jabatan" => $data["tmt_sk"],
			"kode_unit" => $data["kode_unit_kerja_baru"],
			"keterangan" => $data["ket_mutasi"]
			);

		if($this->mutasi->add($data)!=null && $this->riwayat_jabatan_uj->add($data_riwayat) != null){
			$this->session->set_flashdata('msg', "Berhasil menambahkan data mutasi");
		}
		else{
			$this->session->set_flashdata('msg', "Terjadi kesalahan dalam menambah data mutasi");
		}

		redirect("laporan/smart_mutasi/tambah");
	}
	function get_data_mutasi(){
		$result=null;
		
		$data=$this->mutasi->get_data_mutasi();
		// tambah $data dengan array get_jabatan()

		$i=1;
		foreach ($data as $data_mutasi) {
			// if ($data_mutasi['tahun_sk']<=1983) {
			// 	$otk="OTK 1983";
			// } elseif ($data_mutasi['tahun_sk']>1983&&$data_mutasi['tahun_sk']<=1999) {
			// 	$otk="OTK 1999";
			// }else{
			// 	$otk="OTK 2012";
			// }

			$data_unit_kerja=$this->mutasi->get_unit_kerja($data_mutasi['kd_pegawai']);			

			$result.="<tr>
			<td>".$i++."</td>
			<td>".$data_mutasi['gelar_depan'].$data_mutasi['nama_pegawai'].$data_mutasi['gelar_belakang']."</td>
			<td>".$data_mutasi['NIP']."</td>
			<td>".$data_mutasi['golongan']."/".$data_mutasi['pangkat']."</td>
			<td>".$data_unit_kerja[0]['nama_unit_kerja_asal']."</td>
			<td>".$data_unit_kerja[0]['nama_unit_kerja_baru']."</td>
			</tr>";
		}
		return $result;
	}

	function getJVJC(){
		$jabatan_baru = $_POST['jabatan_baru'];
		$golpangkat = $_POST['golpangkat'];
		if (empty($golpangkat)) {
			header("Content-type: application/json");
			echo null;
			exit();
			return false;
		}else{
			$data = $this->jenis_jabatan->getJvJc($jabatan_baru, $golpangkat);
			$result = array();
			foreach ($data as $jvjc) {
				$result = array("job_value"=>$jvjc['jv'], "job_class"=>$jvjc['job_class']);
			}
			
			header("Content-type: application/json");
			echo json_encode($result);
		}
	}

}