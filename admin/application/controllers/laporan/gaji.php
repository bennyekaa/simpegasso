<?php
function right($value, $count){
    return substr($value, ($count*-1));
}
function left($string, $count){
    return substr($string, 0, $count);
}
?>
<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class gaji extends Member_Controller {

	function gaji()
	{
		parent::Member_Controller();
		//$this->template->load_css('public/css/table.css');
		//$this->load->library('pagination');
		//$this->load->helper('url');
		$this->load->model('lookup_model','lookup');
		$this->load->model('lap_gaji_model','lap_gaji');
		$this->load->model('lap_unit_model','lap_unit');
        $this->load->model('lap_pegawai_model','lap_pegawai');
        $this->load->model('unit_kerja_model','unit_kerja');
		$this->load->model('jenis_pegawai_model', 'jenis_pegawai');
	}

	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Rencana Kenaikan Gaji Berkala');
        $this->browse();
	}

	function browse($group=0) {
		if ($this->input->post('group'))
			$group= $this->input->post('group');
		else if($this->uri->segment(4))
            $group=$this->uri->segment(4);
        $tahun=date('Y');
		if ($this->input->post('tahun'))
			$tahun= $this->input->post('tahun');
		if ($this->input->post('jns_pegawai'))
			$jns_pegawai= $this->input->post('jns_pegawai');
		
		$paging_uri=5;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		
		//$limit_per_page = 20;
        $ordby = 'id_golpangkat_terakhir';
        //list($datestart, $dateend) = $this->get_date_range($group, $tahun); 
        //$dateend= date('Y-m-d');
		
		if($jns_pegawai)
            $where_val[] = "(id_jns_pegawai = '$jns_pegawai' and status_pegawai < 3)";
		elseif ($jns_pegawai=='1')
        	$where_val[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
		
		
			
        list($date1, $date2) = $this->get_date_range($group, $tahun);
        $fields = "pegawai.*, DATE_ADD( tmt_kgb, INTERVAL 2 YEAR ) AS next_year";
		$where_val[] = "DATE_ADD(tmt_kgb, INTERVAL 2 YEAR) BETWEEN '$date1' AND '$date2'";
		$where_val = implode(" AND ", $where_val);
		$kerja_list = $this->lap_gaji->find($fields, $where_val, NULL, $limit_per_page, $start);
		
        //$kerja_list = $this->lap_gaji->findByFilter($search_param,$ordby,$limit_per_page,$start);		
        //show_error(var_dump($kerja_list));
		
        $data['judul'] = "Rencana Kenaikan Gaji Berkala";
		$data['group'] =$group;
		$data['tahun'] =$tahun;
		$data['jns_pegawai']=$jns_pegawai;
        $data['start'] = $start;
		//$data['unit_kerja_assoc'] = array(0=>"-- Semua Unit Kerja --") + $this->unit_kerja->get_assoc('nama_unit');
		$data['jenis_pegawai_assoc'] = array(0=>"-- Semua Jenis Pegawai --") + array(1=>"--Tenaga Administratif--") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
		$jabatan = $this->lookup->get_datafield('jabatan','id_jabatan','nama_jabatan');
		$golongan = $this->lookup->get_datafield2('golongan_pangkat','id_golpangkat',array('golongan','tingkat'));
		
		//$pangkat = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','pangkat');
		$NIP_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		$pendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('mk_tambahan','id_golpangkat_awal'));
		$golpendidikanawal = $this->lookup->get_datafield('pendidikan','id_pendidikan','id_golpangkat_awal');	
		$namapendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('nama_pendidikan','id_golpangkat_awal'));
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');		

		if(trim($group) !== '0'){
				//$tgl = $tahun."-".$group."-".date("d");
				$tgl = $tahun."-".$group."-01";
				$tglkgb = $pegawai['tmt_kgb'];
				if($kerja_list) foreach($kerja_list as $pegawai){
					$tglkgb = $pegawai['tmt_kgb'];
					$pegawai['nama_jabatan']=$jabatan[$pegawai['id_jabatan_terakhir']];
					$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']]['golongan'];
					$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
					$pegawai['NIP']=$NIP_peg[$pegawai['kd_pegawai']];
					$pegawai['nama_pendidikan']=$namapendidikan[$pegawai['id_pendidikan_terakhir']]['nama_pendidikan'];
					
					$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
					$pegawai['golpangkat_awal'] =$pendidikan[$pegawai['id_pendidikan_terakhir']]['id_golpangkat_awal'];
					$mk_tambahan = $pendidikan[$pegawai['id_pendidikan_terakhir']]['mk_tambahan'];
					$tingkat = $golongan[$pegawai['id_golpangkat_terakhir']]['tingkat'];
					$tingkat_awal = $golongan[$golpendidikanawal[$pegawai['id_pendidikan_terakhir']]]['tingkat']; 
					
					//untuk golongan IIIa keatas yang pendidikannya SLTA, D1,D2,D3
					if ($tingkat_awal==3)
					{
						$tingkat_akhir=0;
					}
					else
					{
						$tingkat_akhir= $tingkat - $tingkat_awal;
					}
					if (($pegawai['id_golpangkat_terakhir'] >= 9))
					{
						 $masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
						 $masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
						 
						 $masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
						 $masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
						 
						 if ($tingkat_akhir==1){
							$masa_kerja_golongan = $masa_kerja-5;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-5;
						 }
						 elseif ($tingkat_akhir==2){
							$masa_kerja_golongan = $masa_kerja-11;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;
							}
						 else{
							$masa_kerja_golongan = $masa_kerja;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb;
						 }  
					}
					elseif (($pegawai['id_golpangkat_terakhir'] >=5)){
						$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
						$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
						
						$masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
						$masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
						 
						 if ($tingkat_akhir==1){
							$masa_kerja_golongan = $masa_kerja-6;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-6;
						 }
						 elseif ($tingkat_akhir==2){
							$masa_kerja_golongan = $masa_kerja-11;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;}
						 else{
							$masa_kerja_golongan = $masa_kerja;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb;
						 }
					}
					else{
						$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
						$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
						$masa_kerja_golongan = $masa_kerja['years'];
						$masa_kerja_golongan_kgb = $masa_kerja_kgb['years'];
					}
					
					$golpangkat_terakhir=$pegawai['id_golpangkat_terakhir'];
					
					//masa kerja gol. lama bisa dianggap ambil dari tahun KGB yg lama
					
					$gajilama = $this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan_kgb);
					
					if ($gajilama == 0 ){
						//jika gaji lama 0 maka pangkat - 1
						$pegawai['gajilama']= $this->lap_gaji->getgajipegawai($golpangkat_terakhir-1,$masa_kerja_golongan_kgb);  
					}
					else {
						$pegawai['gajilama']= $gajilama;
					}
					//$pegawai['gajilama']= $gajilama;  
					$pegawai['gajibaru']=$this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan);  
				
				   if ($masa_kerja_awal['months'] == '') {
						$pegawai['masa_kerja_golongan']= $masa_kerja_golongan." th" ; 
						
						$pegawai['masa_kerja']=$masa_kerja." th";
					   }
					   else{
							 $pegawai['masa_kerja_golongan']= $masa_kerja_golongan." th ".$masa_kerja_awal['months']." bln" ; 
							 $pegawai['masa_kerja']=$masa_kerja." th ".$masa_kerja_awal['months']." bln";
					   }		

					$temp[]=$pegawai;
				}		
		}
		
		//jika sepanjang tahun
		else {
			for($month = 1; $month <= 12; $month++)
				{
					$month = str_pad($month, 2, '0', STR_PAD_LEFT);
					//$tgl = $tahun."-".$month."-".date("d");
					$tgl = $tahun."-".$month."-01";
					$tglkgb = $pegawai['tmt_kgb'];
					
					if($kerja_list) foreach($kerja_list as $pegawai){
						if (right(left($pegawai['tmt_kgb'],7),2) == $month){
							$tglkgb = $pegawai['tmt_kgb'];
							$pegawai['nama_jabatan']=$jabatan[$pegawai['id_jabatan_terakhir']];
							$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']]['golongan'];
							$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
							$pegawai['NIP']=$NIP_peg[$pegawai['kd_pegawai']];
							$pegawai['nama_pendidikan']=$namapendidikan[$pegawai['id_pendidikan_terakhir']]['nama_pendidikan'];
							
							$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
							$pegawai['golpangkat_awal'] =$pendidikan[$pegawai['id_pendidikan_terakhir']]['id_golpangkat_awal'];
							$mk_tambahan = $pendidikan[$pegawai['id_pendidikan_terakhir']]['mk_tambahan'];
							$tingkat = $golongan[$pegawai['id_golpangkat_terakhir']]['tingkat'];
							$tingkat_awal = $golongan[$golpendidikanawal[$pegawai['id_pendidikan_terakhir']]]['tingkat']; 
							
							//untuk golongan IIIa keatas yang pendidikannya SLTA, D1,D2,D3
							if ($tingkat_awal==3)
							{
								$tingkat_akhir=0;
							}
							else
							{
								$tingkat_akhir= $tingkat - $tingkat_awal ;
							}
							if (($pegawai['id_golpangkat_terakhir'] >= 9))
							{
								 $masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
								 $masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
								 
								 $masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
								 $masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
								 
								 if ($tingkat_akhir==1){
									$masa_kerja_golongan = $masa_kerja-5;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb-5;
								 }
								 elseif ($tingkat_akhir==2){
									$masa_kerja_golongan = $masa_kerja-11;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;
									}
								 else{
									$masa_kerja_golongan = $masa_kerja;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb;
								 }  
							}
							elseif (($pegawai['id_golpangkat_terakhir'] >=5)){
							
								$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
								$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
								 
								$masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
								$masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
								 
								 if ($tingkat_akhir==1){
									$masa_kerja_golongan = $masa_kerja-6;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb-6;
								 }
								 elseif ($tingkat_akhir==2){
									$masa_kerja_golongan = $masa_kerja-11;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;}
								 else{
									$masa_kerja_golongan = $masa_kerja;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb;
								 }
							}
							else{
								$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
								$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
								
								$masa_kerja_golongan = $masa_kerja['years'];
								$masa_kerja_golongan_kgb = $masa_kerja_kgb['years'];
							}
							
							$golpangkat_terakhir=$pegawai['id_golpangkat_terakhir'];
							
							//masa kerja gol. lama bisa dianggap ambil dari tahun KGB yg lama
							
							$gajilama = $this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan_kgb);
							
							if ($gajilama == 0 ){
								//jika gaji lama 0 maka pangkat - 1
								$pegawai['gajilama']= $this->lap_gaji->getgajipegawai($golpangkat_terakhir-1,$masa_kerja_golongan_kgb);  
							}
							else {
								$pegawai['gajilama']= $gajilama;
							}
							//$pegawai['gajilama']= $gajilama;  
							$pegawai['gajibaru']=$this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan);  
						
						   if ($masa_kerja_awal['months'] == '') {
								$pegawai['masa_kerja_golongan']= $masa_kerja_golongan." th" ; 
								
								$pegawai['masa_kerja']=$masa_kerja." th";
							   }
							   else{
									 $pegawai['masa_kerja_golongan']= $masa_kerja_golongan." th ".$masa_kerja_awal['months']." bln"; $pegawai['masa_kerja']=$masa_kerja." th ".$masa_kerja_awal['months']." bln";
							   }		
						   
							
						
							$temp[]=$pegawai;
						}		
					}
				}
		}
		
		
		
		$data['kerja_list']=$temp;
		$data['group_assoc'] = array('0' => '--sepanjang tahun--') + $this->lookup->month_assoc();
        $data['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y'), date('Y')+100);
		
		
		$config['base_url']     = site_url('laporan/gaji/browse/'.$group.'/'.$tahun.'/'.$jns_pegawai);  
		$config['total_rows']   = $this->lap_gaji->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
        $this->pagination->initialize($config);
        $data['page_links'] 	= $this->pagination->create_links();
		$this->template->display('laporan/gaji/list_gaji', $data);
        
	
		
	}

	function get_date_range($group, $tahun)
	{
		//echo "Processing \$group = $group  :: \$tahun = $tahun";
		if(!$group)
		{
			$tahun2 = $tahun;
			$bulan1 = '01';
			$bulan2 = '12';
			$date1 = $tahun2 . '-' . $bulan1 . '-' . '01';
			$date2 = $tahun . '-' . $bulan2 . '-' . '31';
			
			return array($date1, $date2);
		}
		else
		{
			$tahun2 = $tahun;
			$bulan1 = $group;
			$bulan2 = $group;
			$date1 = $tahun2 . '-' . $bulan1 . '-' . '01';
			$date2 = $tahun . '-' . $bulan2 . '-' . date('t', mktime(0,0,0,$group,1,$tahun));
			
			return array($date1, $date2);
		}
	}


	function printtopdf($group,$tahun,$jns_pegawai)
	{
	
		$properties=array();
		$properties['subject']='kerja';
		$properties['keywords']='acuan_gaji_pokok';
		$properties['papersize']="A4";
		$properties['paperlayout']='L';	
		$properties['filename']='Laporan Penggajian';
		$periode ='';
        $ordby = 'id_golpangkat_terakhir';
        //list($datestart, $dateend) = $this->get_date_range($group, $tahun); 
        //$dateend= date('Y-m-d');
        list($date1, $date2) = $this->get_date_range($group, $tahun);
        $fields = "pegawai.*, DATE_ADD( tmt_kgb, INTERVAL 2 YEAR ) AS next_year";
		$where_val = "DATE_ADD(tmt_kgb, INTERVAL 2 YEAR) BETWEEN '$date1' AND '$date2'";
		$kerja_list = $this->lap_gaji->find($fields, $where_val, NULL, $limit_per_page, $start);
		
        //$kerja_list = $this->lap_gaji->findByFilter($search_param,$ordby,$limit_per_page,$start);		
        //show_error(var_dump($kerja_list));
		
        $data['judul'] = "Rencana Kenaikan Gaji Berkala";
		$data['group'] =$group;
		$data['tahun'] =$tahun;
		$data['jns_pegawai'] =$jns_pegawai;
        $data['start'] = $start;
		//$data['unit_kerja_assoc'] = array(0=>"-- Semua Unit Kerja --") + $this->unit_kerja->get_assoc('nama_unit');
	
		$jabatan = $this->lookup->get_datafield('jabatan','id_jabatan','nama_jabatan');
		$golongan = $this->lookup->get_datafield2('golongan_pangkat','id_golpangkat',array('golongan','tingkat'));
		//$pangkat = $this->lookup->get_datafield('golongan_pangkat','id_golpangkat','pangkat');
		$NIP_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		$pendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('mk_tambahan','id_golpangkat_awal'));
		$golpendidikanawal = $this->lookup->get_datafield('pendidikan','id_pendidikan','id_golpangkat_awal');	
		$namapendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('nama_pendidikan','id_golpangkat_awal'));
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');		

		if(trim($group) !== '0'){
				//$tgl = $tahun."-".$group."-".date("d");
				
				$tgl = $tahun."-".$group."-01";
				$periode = "Periode : ".date('d M Y',strtotime($tgl));
				$tglkgb = $pegawai['tmt_kgb'];
				if($kerja_list) foreach($kerja_list as $pegawai){
					$tglkgb = $pegawai['tmt_kgb'];
					$pegawai['nama_jabatan']=$jabatan[$pegawai['id_jabatan_terakhir']];
					$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']]['golongan'];
					$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
					$pegawai['NIP']=$NIP_peg[$pegawai['kd_pegawai']];
					$pegawai['nama_pendidikan']=$namapendidikan[$pegawai['id_pendidikan_terakhir']]['nama_pendidikan'];
					
					$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
					$pegawai['golpangkat_awal'] =$pendidikan[$pegawai['id_pendidikan_terakhir']]['id_golpangkat_awal'];
					$mk_tambahan = $pendidikan[$pegawai['id_pendidikan_terakhir']]['mk_tambahan'];
					$tingkat = $golongan[$pegawai['id_golpangkat_terakhir']]['tingkat'];
					$tingkat_awal = $golongan[$golpendidikanawal[$pegawai['id_pendidikan_terakhir']]]['tingkat']; 
					
					//untuk golongan IIIa keatas yang pendidikannya SLTA, D1,D2,D3
					if ($tingkat_awal==3)
					{
						$tingkat_akhir=0;
					}
					else
					{
						$tingkat_akhir= $tingkat - $tingkat_awal ;
					}
					if (($pegawai['id_golpangkat_terakhir'] >= 9))
					{
						 $masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
						 $masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
						 
						 $masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
						 $masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
						 
						 if ($tingkat_akhir==1){
							$masa_kerja_golongan = $masa_kerja-5;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-5;
						 }
						 elseif ($tingkat_akhir==2){
							$masa_kerja_golongan = $masa_kerja-11;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;
							}
						 else{
							$masa_kerja_golongan = $masa_kerja;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb;
						 }  
					}
					elseif (($pegawai['id_golpangkat_terakhir'] >=5)){
						$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
						$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
						
						$masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
						$masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
						 
						 if ($tingkat_akhir==1){
							$masa_kerja_golongan = $masa_kerja-6;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-6;
						 }
						 elseif ($tingkat_akhir==2){
							$masa_kerja_golongan = $masa_kerja-11;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;}
						 else{
							$masa_kerja_golongan = $masa_kerja;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb;
						 }
					}
					else{
						$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
						$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
						$masa_kerja_golongan = $masa_kerja['years'];
						$masa_kerja_golongan_kgb = $masa_kerja_kgb['years'];
					}
					
					$golpangkat_terakhir=$pegawai['id_golpangkat_terakhir'];
					
					//masa kerja gol. lama bisa dianggap ambil dari tahun KGB yg lama
					
					$gajilama = $this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan_kgb);
					
					if ($gajilama == 0 ){
						//jika gaji lama 0 maka pangkat - 1
						$pegawai['gajilama']= $this->lap_gaji->getgajipegawai($golpangkat_terakhir-1,$masa_kerja_golongan_kgb);  
					}
					else {
						$pegawai['gajilama']= $gajilama;
					}
					//$pegawai['gajilama']= $gajilama;  
					$pegawai['gajibaru']=$this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan);  
				
				   if ($masa_kerja_awal['months'] == '') {
						$pegawai['masa_kerja_golongan']= $masa_kerja_golongan." th" ; 
						
						$pegawai['masa_kerja']=$masa_kerja." th";
					   }
					   else{
							 $pegawai['masa_kerja_golongan']= $masa_kerja_golongan." th ".$masa_kerja_awal['months']." bln" ; 
							 $pegawai['masa_kerja']=$masa_kerja." th ".$masa_kerja_awal['months']." bln";
					   }		

					$temp[]=$pegawai;
				}		
		}
		
		//jika sepanjang tahun
		else {
			$periode = "Sepanjang tahun " .$tahun;
			for($month = 1; $month <= 12; $month++)
				{
					$month = str_pad($month, 2, '0', STR_PAD_LEFT);
					//$tgl = $tahun."-".$month."-".date("d");
					$tgl = $tahun."-".$month."-01";
					$tglkgb = $pegawai['tmt_kgb'];
					
					if($kerja_list) foreach($kerja_list as $pegawai){
						if (right(left($pegawai['tmt_kgb'],7),2) == $month){
							$tglkgb = $pegawai['tmt_kgb'];
							$pegawai['nama_jabatan']=$jabatan[$pegawai['id_jabatan_terakhir']];
							$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']]['golongan'];
							$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
							$pegawai['NIP']=$NIP_peg[$pegawai['kd_pegawai']];
							$pegawai['nama_pendidikan']=$namapendidikan[$pegawai['id_pendidikan_terakhir']]['nama_pendidikan'];
							
							$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
							$pegawai['golpangkat_awal'] =$pendidikan[$pegawai['id_pendidikan_terakhir']]['id_golpangkat_awal'];
							$mk_tambahan = $pendidikan[$pegawai['id_pendidikan_terakhir']]['mk_tambahan'];
							$tingkat = $golongan[$pegawai['id_golpangkat_terakhir']]['tingkat'];
							$tingkat_awal = $golongan[$golpendidikanawal[$pegawai['id_pendidikan_terakhir']]]['tingkat']; 
							
							//untuk golongan IIIa keatas yang pendidikannya SLTA, D1,D2,D3
							if ($tingkat_awal==3)
							{
								$tingkat_akhir=0;
							}
							else
							{
								$tingkat_akhir= $tingkat - $tingkat_awal ;
							}
							if (($pegawai['id_golpangkat_terakhir'] >= 9))
							{
								 $masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
								 $masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
								 
								 $masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
								 $masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
								 
								 if ($tingkat_akhir==1){
									$masa_kerja_golongan = $masa_kerja-5;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb-5;
								 }
								 elseif ($tingkat_akhir==2){
									$masa_kerja_golongan = $masa_kerja-11;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;
									}
								 else{
									$masa_kerja_golongan = $masa_kerja;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb;
								 }  
							}
							elseif (($pegawai['id_golpangkat_terakhir'] >=5)){
							
								$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
								$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
								 
								$masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
								$masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
								 
								 if ($tingkat_akhir==1){
									$masa_kerja_golongan = $masa_kerja-6;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb-6;
								 }
								 elseif ($tingkat_akhir==2){
									$masa_kerja_golongan = $masa_kerja-11;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;}
								 else{
									$masa_kerja_golongan = $masa_kerja;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb;
								 }
							}
							else{
								$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
								$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
								
								$masa_kerja_golongan = $masa_kerja['years'];
								$masa_kerja_golongan_kgb = $masa_kerja_kgb['years'];
							}
							
							$golpangkat_terakhir=$pegawai['id_golpangkat_terakhir'];
							
							//masa kerja gol. lama bisa dianggap ambil dari tahun KGB yg lama
							
							$gajilama = $this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan_kgb);
							
							if ($gajilama == 0 ){
								//jika gaji lama 0 maka pangkat - 1
								$pegawai['gajilama']= $this->lap_gaji->getgajipegawai($golpangkat_terakhir-1,$masa_kerja_golongan_kgb);  
							}
							else {
								$pegawai['gajilama']= $gajilama;
							}
							//$pegawai['gajilama']= $gajilama;  
							$pegawai['gajibaru']=$this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan);  
						
						   if ($masa_kerja_awal['months'] == '') {
								$pegawai['masa_kerja_golongan']= $masa_kerja_golongan." th" ; 
								
								$pegawai['masa_kerja']=$masa_kerja." th";
							   }
							   else{
									 $pegawai['masa_kerja_golongan']= $masa_kerja_golongan." th ".$masa_kerja_awal['months']." bln"; 
									 $pegawai['masa_kerja']=$masa_kerja." th ".$masa_kerja_awal['months']." bln";
							   }		
						   
							
						
							$temp[]=$pegawai;
						}		
					}
				}
		}
        
		$data['kerja_list']=$temp;  		
		$properties['title']='Laporan Gaji Berkala '.$periode;
		$html=$this->load->view('laporan/gaji/list_gajipdf', $data,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}
	
	
	function printtoxls($group,$tahun,$jns_pegawai){
		$this->load->model('lookup_model','lookup');
		$this->load->model('lap_gaji_model','lap_gaji');
		$this->load->model('lap_unit_model','lap_unit');
        $this->load->model('lap_pegawai_model','lap_pegawai');
        $this->load->model('unit_kerja_model','unit_kerja');
		
		$periode ='';
        $ordby = 'id_golpangkat_terakhir';
        //list($datestart, $dateend) = $this->get_date_range($group, $tahun); 
        //$dateend= date('Y-m-d');
		$data['group'] =$group;
		$data['tahun'] =$tahun;
		$data['jns_pegawai'] =$jns_pegawai;
        list($date1, $date2) = $this->get_date_range($group, $tahun);
        $fields = "pegawai.*, DATE_ADD( tmt_kgb, INTERVAL 2 YEAR ) AS next_year";
		$where_val = "DATE_ADD(tmt_kgb, INTERVAL 2 YEAR) BETWEEN '$date1' AND '$date2'";
		$kerja_list = $this->lap_gaji->find($fields, $where_val, NULL, $limit_per_page, $start);
	
		$jabatan = $this->lookup->get_datafield('jabatan','id_jabatan','nama_jabatan');
		$golongan = $this->lookup->get_datafield2('golongan_pangkat','id_golpangkat',array('golongan','tingkat'));
		$NIP_peg = $this->lookup->get_datafield('pegawai','kd_pegawai','NIP');
		$pendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('mk_tambahan','id_golpangkat_awal'));
		$golpendidikanawal = $this->lookup->get_datafield('pendidikan','id_pendidikan','id_golpangkat_awal');	
		$namapendidikan = $this->lookup->get_datafield2('pendidikan','id_pendidikan',array('nama_pendidikan','id_golpangkat_awal'));
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');		

		if(trim($group) !== '0'){
				//$tgl = $tahun."-".$group."-".date("d");
				
				$tgl = $tahun."-".$group."-01";
				$periode = "Periode : ".date('d M Y',strtotime($tgl));
				$tglkgb = $pegawai['tmt_kgb'];
				if($kerja_list) foreach($kerja_list as $pegawai){
					$tglkgb = $pegawai['tmt_kgb'];
					$pegawai['nama_jabatan']=$jabatan[$pegawai['id_jabatan_terakhir']];
					$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']]['golongan'];
					$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
					$pegawai['NIP']=$NIP_peg[$pegawai['kd_pegawai']];
					$pegawai['nama_pendidikan']=$namapendidikan[$pegawai['id_pendidikan_terakhir']]['nama_pendidikan'];
					
					$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
					$pegawai['golpangkat_awal'] =$pendidikan[$pegawai['id_pendidikan_terakhir']]['id_golpangkat_awal'];
					$mk_tambahan = $pendidikan[$pegawai['id_pendidikan_terakhir']]['mk_tambahan'];
					$tingkat = $golongan[$pegawai['id_golpangkat_terakhir']]['tingkat'];
					$tingkat_awal = $golongan[$golpendidikanawal[$pegawai['id_pendidikan_terakhir']]]['tingkat']; 
					
					//untuk golongan IIIa keatas yang pendidikannya SLTA, D1,D2,D3
					if ($tingkat_awal==3)
					{
						$tingkat_akhir=0;
					}
					else
					{
						$tingkat_akhir= $tingkat - $tingkat_awal ;
					}
					if (($pegawai['id_golpangkat_terakhir'] >= 9))
					{
						 $masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
						 $masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
						 
						 $masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
						 $masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
						 
						 if ($tingkat_akhir==1){
							$masa_kerja_golongan = $masa_kerja-5;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-5;
						 }
						 elseif ($tingkat_akhir==2){
							$masa_kerja_golongan = $masa_kerja-11;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;
							}
						 else{
							$masa_kerja_golongan = $masa_kerja;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb;
						 }  
					}
					elseif (($pegawai['id_golpangkat_terakhir'] >=5)){
						$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
						$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
						
						$masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
						$masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
						 
						 if ($tingkat_akhir==1){
							$masa_kerja_golongan = $masa_kerja-6;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-6;
						 }
						 elseif ($tingkat_akhir==2){
							$masa_kerja_golongan = $masa_kerja-11;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;}
						 else{
							$masa_kerja_golongan = $masa_kerja;
							$masa_kerja_golongan_kgb = $masa_kerja_kgb;
						 }
					}
					else{
						$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
						$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
						$masa_kerja_golongan = $masa_kerja['years'];
						$masa_kerja_golongan_kgb = $masa_kerja_kgb['years'];
					}
					
					$golpangkat_terakhir=$pegawai['id_golpangkat_terakhir'];
					
					//masa kerja gol. lama bisa dianggap ambil dari tahun KGB yg lama
					
					$gajilama = $this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan_kgb);
					
					if ($gajilama == 0 ){
						//jika gaji lama 0 maka pangkat - 1
						$pegawai['gajilama']= $this->lap_gaji->getgajipegawai($golpangkat_terakhir-1,$masa_kerja_golongan_kgb);  
					}
					else {
						$pegawai['gajilama']= $gajilama;
					}
					//$pegawai['gajilama']= $gajilama;  
					$pegawai['gajibaru']=$this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan);  
				
				   if ($masa_kerja_awal['months'] == '') {
						$pegawai['masa_kerja_golongan']= $masa_kerja_golongan." th" ; 
						
						$pegawai['masa_kerja']=$masa_kerja." th";
					   }
					   else{
							 $pegawai['masa_kerja_golongan']= $masa_kerja_golongan." th ".$masa_kerja_awal['months']." bln" ; 
							 $pegawai['masa_kerja']=$masa_kerja." th ".$masa_kerja_awal['months']." bln";
					   }		

					$temp[]=$pegawai;
				}		
		}
		
		//jika sepanjang tahun
		else {
			$periode = "Sepanjang tahun " .$tahun;
			for($month = 1; $month <= 12; $month++)
				{
					$month = str_pad($month, 2, '0', STR_PAD_LEFT);
					//$tgl = $tahun."-".$month."-".date("d");
					$tgl = $tahun."-".$month."-01";
					$tglkgb = $pegawai['tmt_kgb'];
					
					if($kerja_list) foreach($kerja_list as $pegawai){
						if (right(left($pegawai['tmt_kgb'],7),2) == $month){
							$tglkgb = $pegawai['tmt_kgb'];
							$pegawai['nama_jabatan']=$jabatan[$pegawai['id_jabatan_terakhir']];
							$pegawai['nama_golongan']=$golongan[$pegawai['id_golpangkat_terakhir']]['golongan'];
							$pegawai['nama_pangkat']=$pangkat[$pegawai['id_golpangkat_terakhir']];
							$pegawai['NIP']=$NIP_peg[$pegawai['kd_pegawai']];
							$pegawai['nama_pendidikan']=$namapendidikan[$pegawai['id_pendidikan_terakhir']]['nama_pendidikan'];
							
							$pegawai['unit_kerja']=$unit[$pegawai['kode_unit']];
							$pegawai['golpangkat_awal'] =$pendidikan[$pegawai['id_pendidikan_terakhir']]['id_golpangkat_awal'];
							$mk_tambahan = $pendidikan[$pegawai['id_pendidikan_terakhir']]['mk_tambahan'];
							$tingkat = $golongan[$pegawai['id_golpangkat_terakhir']]['tingkat'];
							$tingkat_awal = $golongan[$golpendidikanawal[$pegawai['id_pendidikan_terakhir']]]['tingkat']; 
							
							//untuk golongan IIIa keatas yang pendidikannya SLTA, D1,D2,D3
							if ($tingkat_awal==3)
							{
								$tingkat_akhir=0;
							}
							else
							{
								$tingkat_akhir= $tingkat - $tingkat_awal ;
							}
							if (($pegawai['id_golpangkat_terakhir'] >= 9))
							{
								 $masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
								 $masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
								 
								 $masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
								 $masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
								 
								 if ($tingkat_akhir==1){
									$masa_kerja_golongan = $masa_kerja-5;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb-5;
								 }
								 elseif ($tingkat_akhir==2){
									$masa_kerja_golongan = $masa_kerja-11;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;
									}
								 else{
									$masa_kerja_golongan = $masa_kerja;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb;
								 }  
							}
							elseif (($pegawai['id_golpangkat_terakhir'] >=5)){
							
								$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
								$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
								 
								$masa_kerja_kgb = $this->datediff($pegawai['tmt_cpns'],$tglkgb);
								$masa_kerja_kgb = $masa_kerja_kgb['years'] + $mk_tambahan;
								 
								 if ($tingkat_akhir==1){
									$masa_kerja_golongan = $masa_kerja-6;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb-6;
								 }
								 elseif ($tingkat_akhir==2){
									$masa_kerja_golongan = $masa_kerja-11;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb-11;}
								 else{
									$masa_kerja_golongan = $masa_kerja;
									$masa_kerja_golongan_kgb = $masa_kerja_kgb;
								 }
							}
							else{
								$masa_kerja_awal = $this->datediff($pegawai['tmt_cpns'],$tgl);
								$masa_kerja = $masa_kerja_awal['years'] + $mk_tambahan;
								
								$masa_kerja_golongan = $masa_kerja['years'];
								$masa_kerja_golongan_kgb = $masa_kerja_kgb['years'];
							}
							
							$golpangkat_terakhir=$pegawai['id_golpangkat_terakhir'];
							
							//masa kerja gol. lama bisa dianggap ambil dari tahun KGB yg lama
							
							$gajilama = $this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan_kgb);
							
							if ($gajilama == 0 ){
								//jika gaji lama 0 maka pangkat - 1
								$pegawai['gajilama']= $this->lap_gaji->getgajipegawai($golpangkat_terakhir-1,$masa_kerja_golongan_kgb);  
							}
							else {
								$pegawai['gajilama']= $gajilama;
							}
							//$pegawai['gajilama']= $gajilama;  
							$pegawai['gajibaru']=$this->lap_gaji->getgajipegawai($golpangkat_terakhir,$masa_kerja_golongan);  
						
						   if ($masa_kerja_awal['months'] == '') {
								$pegawai['masa_kerja_golongan']= $masa_kerja_golongan." th" ; 
								
								$pegawai['masa_kerja']=$masa_kerja." th";
							   }
							   else{
									 $pegawai['masa_kerja_golongan']= $masa_kerja_golongan." th ".$masa_kerja_awal['months']." bln"; 
									 $pegawai['masa_kerja']=$masa_kerja." th ".$masa_kerja_awal['months']." bln";
							   }		
						   
							
						
							$temp[]=$pegawai;
						}		
					}
				}
		}
		
		$data['kerja_list']=$temp;  		
		$properties['title']='Laporan Gaji Berkala '.$periode;
		$data['html']=$this->load->view('laporan/gaji/list_gajipdf', $data,true);
		$this->load->view('xls.php',$data);
	}
	
    function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
}
?>
