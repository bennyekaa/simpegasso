<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


class pensiun extends My_Controller {

	function pensiun()
	{
		parent::My_Controller();
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->model('lookup_model','lookup');
		$this->load->model('lap_pensiun_model','lap_pensiun');
		$this->load->model('jenis_pegawai_model', 'jenis_pegawai');
		$this->load->model('jabatan_model', 'jabatan');
		$this->load->model('jabatan_struktural_model', 'jabatan_struktural');
	}

	function index()
	{
		$this->browse($tahun = '2010');
	}

	function browse($tahun = '2010') {
		if ($this->input->post('group'))
			$group= $this->input->post('group');
		else
			$group=0;
		$tahun=date('Y');
		
		$tahun = '2010';
		if ($this->input->post('tahun'))
			$tahun= $this->input->post('tahun');
			
		$jns_pegawai='0';
		if ($this->input->post('jns_pegawai'))
			$jns_pegawai= $this->input->post('jns_pegawai');
		else if($this->uri->segment(4))
            $jns_pegawai=$this->uri->segment(4);
		
		$paging_uri=5;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0;
		
		$ordby = 'tgl_resign';
		$limit_per_page = 20;
		$search_param = array();
		$search_param[] = "(year(tgl_resign) = '$tahun')";
		if ($jns_pegawai=='padm') 
		{
			$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') 
			and status_pegawai < 3 and eselon not like 'non-eselon' )";
			if($group==0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				$search_param[] = "(month(tgl_resign) = '$group')";
				
			}
		}
		elseif ($jns_pegawai=='All') 
		{
			if($group==0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				$search_param[] = "(month(tgl_resign) = '$group')";
				
			}
		}
		elseif ($jns_pegawai=='nAll')
		{
			$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
			if($group==0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				$search_param[] = "(month(tgl_resign) = '$group')";
				
			}
		}
		elseif ($jns_pegawai)
		{
			
            $search_param[] = "(id_jns_pegawai = '$jns_pegawai' and status_pegawai < 3 )";
			if($group==0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				$search_param[] = "(month(tgl_resign) = '$group')";
				
			}
		}
		
           
        	
		
		
		$search_param = implode(" AND ", $search_param);
		$ordby = "tgl_resign asc";
		$pensiun_list = $this->lap_pensiun->find(NULL,$search_param,$ordby,NULL,$start);
		
		//print_r( $pensiun_list );
        $the_results['group'] = $group;
		$the_results['tahun'] = $tahun;
		$the_results['jns_pegawai'] = $jns_pegawai;
		$the_results['jenis_pegawai_assoc'] = array('padm'=>"-- Pejabat Struktural --") + array('All'=>"-- Semua Jenis Pegawai --") + array('nAll'=>"--Tenaga Administratif--") + $this->jenis_pegawai->get_jenispeg_assoc('jenis_pegawai');
		$the_results['jabatan_assoc'] = $this->jabatan->get_jabatan_assoc('nama_jabatan');
		$the_results['jabatan_struktural_assoc']=array('-' => "-- Lainnya --") +$this->jabatan_struktural->get_jabatanstruktural_assoc('nama_jabatan_s');
		$gol_pangkat = $this->lookup->get_datafield2('golongan_pangkat','id_golpangkat', array('pangkat', 'golongan'));
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');
        $jabatan = $this->lookup->get_datafield2('jabatan','id_jabatan', array('batas_maks_pensiun', 'nama_jabatan'));
        $tgl = $tahun."-".$group."-".date("d");
        
		if($pensiun_list) foreach($pensiun_list as $pensiun)
			{
				$pensiun['nama_jabatan']=$jabatan[$pensiun['id_jabatan_terakhir']]['nama_jabatan'];
				$pensiun['nama_pangkat']=$gol_pangkat[$pensiun['id_golpangkat_terakhir']]['pangkat'];
				$pensiun['nama_golongan']=$gol_pangkat[$pensiun['id_golpangkat_terakhir']]['golongan'];
				$pensiun['unit_kerja']=$unit[$pensiun['kode_unit']];
				$pensiun['batas']=$jabatan[$pensiun['id_jabatan_terakhir']]['batas_maks_pensiun'];
                
                $usia = $this->datediff($pensiun['tgl_lahir'],$tgl);
                $pensiun['usia'] = $usia['years']." tahun ".$usia['months']." bulan";
				$temp[]=$pensiun;
				
		}
		
		$the_results['pensiun_list']=$temp;
		//$the_results['pensiun_list']=$pensiun_list;
		
		$the_results['tahun'] =$tahun;
		
		$the_results['group'] =$group; 
		$the_results['judul'] = "Laporan Rencana Pensiun";
        $the_results['group_assoc'] = array('0' => '--sepanjang tahun--') + $this->lookup->month_assoc();
        $the_results['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y')-1, date('Y')+50);
        
		$config['base_url']     = site_url('laporan/pensiun/browse/');
		$config['total_rows']   = $this->lap_pensiun->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		//$this->template->display('laporan/pensiun/list_pensiun', $the_results);
		if (isset($this->user->user_group)){
			$this->template->display('laporan/pensiun/list_pensiun', $the_results);
		} else {
			redirect('/pegawai/pegawai/', 'location');
		}
	} 	

	function printtopdf($group,$tahun,$jns_pegawai)
	{
		$properties=array();
		$properties['subject']='kerja';
		$properties['keywords']='master_pensiun';
		$properties['papersize']="A4";
		$properties['paperlayout']='L';	
		$properties['filename']='Laporan pensiun';
		
		$search_param = array();
		$search_param[] = "(year(tgl_resign) = '$tahun')";
		if ($jns_pegawai=='padm') 
		{
			$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') 
			and status_pegawai < 3 and eselon not like 'non-eselon' )";
			if($group==0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				$search_param[] = "(month(tgl_resign) = '$group')";
			}
		}
		elseif ($jns_pegawai=='All') 
		{
			if($group==0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				$search_param[] = "(month(tgl_resign) = '$group')";
			}
		}
		elseif ($jns_pegawai=='nAll')
		{
			$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
			if($group==0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				$search_param[] = "(month(tgl_resign) = '$group')";
			}
		}
		elseif ($jns_pegawai)
		{
            $search_param[] = "(id_jns_pegawai = '$jns_pegawai' and status_pegawai < 3 )";
			if($group==0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				$search_param[] = "(month(tgl_resign) = '$group')";
			}
		}
		$search_param = implode(" AND ", $search_param);
		$ordby = "tgl_resign asc";
		$pensiun_list = $this->lap_pensiun->find(NULL,$search_param,$ordby,NULL,$start);
		
        $the_results['group'] = $group;
		$the_results['tahun'] = $tahun;
		$the_results['jns_pegawai'] = $jns_pegawai;
		
		$gol_pangkat = $this->lookup->get_datafield2('golongan_pangkat','id_golpangkat', array('pangkat', 'golongan'));
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');
        $jabatan = $this->lookup->get_datafield2('jabatan','id_jabatan', array('batas_maks_pensiun', 'nama_jabatan'));
        $tgl = $tahun."-".$group."-".date("d");
        
		if($pensiun_list) foreach($pensiun_list as $pensiun)
			{
				$pensiun['nama_jabatan']=$jabatan[$pensiun['id_jabatan_terakhir']]['nama_jabatan'];
				$pensiun['nama_pangkat']=$gol_pangkat[$pensiun['id_golpangkat_terakhir']]['pangkat'];
				$pensiun['nama_golongan']=$gol_pangkat[$pensiun['id_golpangkat_terakhir']]['golongan'];
				$pensiun['unit_kerja']=$unit[$pensiun['kode_unit']];
				$pensiun['batas']=$jabatan[$pensiun['id_jabatan_terakhir']]['batas_maks_pensiun'];
                
                $usia = $this->datediff($pensiun['tgl_lahir'],$tgl);
                $pensiun['usia'] = $usia['years']." tahun ".$usia['months']." bulan";
				$temp[]=$pensiun;
				
		}
		
		$the_results['pensiun_list']=$temp;
		$the_results['tahun'] =$tahun;
		$the_results['group'] =$group; 
		$the_results['judul'] = "Daftar pegawai";
		
		$html=$this->load->view('laporan/pensiun/list_pensiunpdf', $the_results,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}	
function printtoxls($group,$tahun,$jns_pegawai)
	{
		$search_param = array();
		$search_param[] = "(year(tgl_resign) = '$tahun')";
		if ($jns_pegawai=='padm') 
		{
			$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') 
			and status_pegawai < 3 and eselon not like 'non-eselon' )";
			if($group==0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				$search_param[] = "(month(tgl_resign) = '$group')";
			}
		}
		elseif ($jns_pegawai=='All') 
		{
			if($group==0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				$search_param[] = "(month(tgl_resign) = '$group')";
			}
		}
		elseif ($jns_pegawai=='nAll')
		{
			$search_param[] = "(id_jns_pegawai not like (SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen') and status_pegawai < 3)";
			if($group==0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				$search_param[] = "(month(tgl_resign) = '$group')";
			}
		}
		elseif ($jns_pegawai)
		{
            $search_param[] = "(id_jns_pegawai = '$jns_pegawai' and status_pegawai < 3 )";
			if($group==0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
			}
			elseif ($group!=0)
			{
				$search_param[] = "(year(tgl_resign) = '$tahun')";
				$search_param[] = "(month(tgl_resign) = '$group')";
			}
		}
		$search_param = implode(" AND ", $search_param);
		$ordby = "tgl_resign asc";
		$pensiun_list = $this->lap_pensiun->find(NULL,$search_param,$ordby,NULL,$start);
		
        $the_results['group'] = $group;
		$the_results['tahun'] = $tahun;
		$the_results['jns_pegawai'] = $jns_pegawai;
		
		$gol_pangkat = $this->lookup->get_datafield2('golongan_pangkat','id_golpangkat', array('pangkat', 'golongan'));
		$unit = $this->lookup->get_datafield('unit_kerja','kode_unit','nama_unit');
        $jabatan = $this->lookup->get_datafield2('jabatan','id_jabatan', array('batas_maks_pensiun', 'nama_jabatan'));
        $tgl = $tahun."-".$group."-".date("d");
        
		if($pensiun_list) foreach($pensiun_list as $pensiun)
			{
				$pensiun['nama_jabatan']=$jabatan[$pensiun['id_jabatan_terakhir']]['nama_jabatan'];
				$pensiun['nama_pangkat']=$gol_pangkat[$pensiun['id_golpangkat_terakhir']]['pangkat'];
				$pensiun['nama_golongan']=$gol_pangkat[$pensiun['id_golpangkat_terakhir']]['golongan'];
				$pensiun['unit_kerja']=$unit[$pensiun['kode_unit']];
				$pensiun['batas']=$jabatan[$pensiun['id_jabatan_terakhir']]['batas_maks_pensiun'];
                
                $usia = $this->datediff($pensiun['tgl_lahir'],$tgl);
                $pensiun['usia'] = $usia['years']." tahun ".$usia['months']." bulan";
				$temp[]=$pensiun;		
			}
		$the_results['pensiun_list']=$temp;
		
		$the_results['tahun'] =$tahun;
		$the_results['group'] =$group; 
		$the_results['judul'] = "Daftar pegawai";

        $the_results['html']=$this->load->view('laporan/pensiun/list_pensiunpdf', $the_results,true);
		$this->load->view('xls.php',$the_results);
	}
	function datediff($d1, $d2)
	{  
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
		$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
		$diff_secs = abs($d1 - $d2);  
		$base_year = min(date("Y", $d1), date("Y", $d2));  
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
		return array( 'years' => date("Y", $diff) - $base_year,  'months_total' => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  'months' => date("n", $diff) - 1,  'days_total' => floor($diff_secs / (3600 * 24)),  'days' => date("j", $diff) - 1);  
	}
}
?>
