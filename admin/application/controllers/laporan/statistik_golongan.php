<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class statistik_golongan extends MY_Controller
{
	function statistik_golongan()
	{
		parent::MY_Controller();
		$this->load->model('group_unit_model', 'group_unit');
		$this->load->model('unit_kerja_model', 'unit_kerja');
		$this->load->model("group_unit_model");
		$this->load->model('lookup_model','lookup');
		$this->load->model("golongan_model");
		$this->load->model("jenis_pegawai_model");
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data sub unit');
		$this->statistik();
	}		
	
	function statistik() {
		$paging_uri=6;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
			
		$limit_per_page = 100;
		//$ordby = $this->uri->segment(5);
		$ordby = 'id_group,kode_unit,tingkat';
		
		$tingkat = $this->input->post('tingkat');

		if ($this->input->post('tingkat')){
			$tingkat = $this->input->post('tingkat');
		}
		else {
			$tingkat = '1';
		}
		
		if ($this->input->post('id_jns_pegawai')){
			$id_jns_pegawai = $this->input->post('id_jns_pegawai');
		}
		else {
			$id_jns_pegawai = 'All';
		}
		
		//penambahan pengecekan arsip
		if ($this->input->post('tahun')){
			$tahun = $this->input->post('tahun');	}
		else {
			$tahun = date('Y');}
			
		if ($this->input->post('group'))
		{
            $group = $this->input->post('group');
		}
		else 
			$group= date('m');
			$tahunskr = date('Y');
			$groupskr = date('m');
			
		$data['periodeskr'] = $tahunskr.$groupskr;
		$data['tahun'] = $tahun;
		$data['group'] = $group;
		$data['periode'] = $tahun.$group;
		//end tambahan
		
		$data['id_jns_pegawai'] = $id_jns_pegawai;
		$data['tingkat'] = $tingkat;
		
		// query untuk memfilter unit kerja 
		$query = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'");					
		if ($query)	{
			$dataku=mysql_fetch_array($query);  
			$id_tenaga_dosen= $dataku['id_var'];
			}
			if ($id_jns_pegawai == $id_tenaga_dosen)
				$param_unit[] = "(unit_kerja.id_group='2')";
			else
				$param_unit[] = "";
				
			$param_unit = implode(" AND ", $param_unit);
		
				
		if ($id_jns_pegawai == 'nAll')
			{
				//menampilkan semua jenis pegawai selain dosen
				$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawai($id_tenaga_dosen);
			}
		else 
			{	
				if ($tingkat=='1')
					$data['list_unit_kerja'] = $this->unit_kerja->find($param_unit,array("tingkat"=>$tingkat), $limit_per_page,$start,$ordby);
				else
					$data['list_unit_kerja'] = $this->unit_kerja->find($param_unit,array("tingkat"=>$tingkat) , $limit_per_page,$start,$ordby);
			}
			
		$data['get_jenis_pegawai_all'] =array('All'=>"--Semua--")+ array('nAll'=>"--Tenaga Administratif--")+ array('nAllUnit'=>"--Tenaga Administratif Unit--") + $this->jenis_pegawai_model->get_jenis_pegawai_assoc();
		$data['golongan_pangkat_assoc'] =$this->golongan_model->findAlldesc();
		$data['group_assoc'] =  array(date('m') => $this->lookup->longonthname(date('m'))) + $this->lookup->month_assoc();
        $data['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y')-10, date('Y')+50);
				
		//$data['unit_kerja_assoc'] = $this->unit_kerja->get_unit_assoc($id_jenis_pegawai);
		$data['GD_assoc'] = $this->lookup->GD_assoc();
		$data['get_tingkat_all'] =$this->lookup->get_tingkat_all();
		$data['judul'] 		= "REKAPITULASI JUMLAH PEGAWAI BERDASARKAN PANGKAT/GOLONGAN DI UNIVERSITAS NEGERI MALANG";
		$config['base_url']     = site_url('laporan/statistik_golongan/statistik/'.$id_group.'/'.$ordby.'/');
		$config['total_rows']   = $this->unit_kerja->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$this->template->display('laporan/statistik/list_statistik', $data);
	}
	
	function statistiktopdf($tingkat,$id_jns_pegawai,$tahun,$group,$periode,$periodeskr){
		$properties=array();
/*		$properties['title']='RAPORT TAHUN AJARAN '.$tahunajaran_assoc[$tahun].
							" SEMESTER ".$semester_assoc[$semester];;*/
		$properties['title']='REKAPITULASI JUMLAH PEGAWAI ADMINISTRASI, PUSTAKAWAN, DAN PRANATA HUMAS';
		$properties['subject']='statistik golongan';
		$properties['keywords']='statistik golongan';
		$properties['filename']='statistik_golongan'; //.$this->sims->tahun
		$properties['paperlayout']='L';
		$properties['papersize']="A3";
		
		$this->load->model("golongan_model");
		$this->load->model("lookup_model");
		$this->load->model("unit_kerja_model");
		
		//load the number_spelling helper class
		//$this->load->helper("number_spelling");
		$ordby = 'id_group,kode_unit,tingkat';
				
		$data['id_jns_pegawai'] = $id_jns_pegawai;
		$data['tingkat'] = $tingkat;
		$data['periodeskr'] = $periodeskr;
		$data['tahun'] = $tahun;
		$data['group'] = $group;
		$data['periode'] = $periode;
		// query untuk memfilter unit kerja 
		$query = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'");					
		if ($query)	{
			$dataku=mysql_fetch_array($query);   
			$id_tenaga_dosen= $dataku['id_var'];
			}
			if ($id_jns_pegawai == $id_tenaga_dosen)
				$param_unit[] = "(unit_kerja.id_group='2')";
			else
				$param_unit[] = "";
				
			$param_unit = implode(" AND ", $param_unit);
			
		if ($id_jns_pegawai == 'nAll')
			{
				$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawai(1);
			}
		else 
			{
				if ($tingkat=='1')
					$data['list_unit_kerja'] = $this->unit_kerja->find($param_unit,array("tingkat"=>$tingkat), $limit_per_page,$start,$ordby);
				else
					$data['list_unit_kerja'] = $this->unit_kerja->find($param_unit,array("tingkat <="=>$tingkat) , $limit_per_page,$start,$ordby);
			}
		$data['get_jenis_pegawai_all'] =array('All'=>"--Semua--")+ array('nAll'=>"--Fungsional--") + $this->jenis_pegawai_model->get_jenis_pegawai_assoc();
		
		$data['id_tenaga_dosen'] =$id_tenaga_dosen;
		//getting golpangkat
		$data['golongan_pangkat_assoc'] =$this->golongan_model->findAll();
		$data['unit_kerja_assoc'] = $this->unit_kerja_model->get_unit_assoc();
		$data['GD_assoc'] = $this->lookup_model->GD_assoc();
		
		$data['judul'] 				= "REKAPITULASI JUMLAH PEGAWAI ADMINISTRASI, PUSTAKAWAN, DAN PRANATA HUMAS<BR>UNIVERSITAS NEGERI MALANG";
		$html=$this->load->view('laporan/statistik/list_statistik_pdf', $data,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}
	
	function printtoxls($tingkat,$id_jns_pegawai,$tahun,$group,$periode,$periodeskr){
		$this->load->model("golongan_model");
		$this->load->model("lookup_model");
		$this->load->model("unit_kerja_model");
		$start=0 ; 
		$limit_per_page = 1000;
		$ordby = 'id_group,kode_unit,tingkat';
				
		$data['id_jns_pegawai'] = $id_jns_pegawai;
		$data['tingkat'] = $tingkat;
		$data['periodeskr'] = $periodeskr;
		$data['tahun'] = $tahun;
		$data['group'] = $group;
		$data['periode'] = $periode;
		// query untuk memfilter unit kerja 
		$query = mysql_query("SELECT id_var FROM pos_variabel WHERE keterangan = 'id_tenaga_dosen'");					
		if ($query)	{
			$dataku=mysql_fetch_array($query);  
			$id_tenaga_dosen= $dataku['id_var'];
			}
			if ($id_jns_pegawai == $id_tenaga_dosen)
				$param_unit[] = "(unit_kerja.id_group='2')";
			else
				$param_unit[] = "";
				
			$param_unit = implode(" AND ", $param_unit);
			
		if ($id_jns_pegawai == 'nAll')
			{
				$data['list_unit_kerja'] = $this->jenis_pegawai_model->getAllJenisPegawai(1);
			}
		else 
			{
				if ($tingkat=='1')
					$data['list_unit_kerja'] = $this->unit_kerja->find($param_unit,array("tingkat"=>$tingkat), $limit_per_page,$start,$ordby);
				else
					$data['list_unit_kerja'] = $this->unit_kerja->find($param_unit,array("tingkat <="=>$tingkat) , $limit_per_page,$start,$ordby);
			}
		//getting golpangkat
		$data['golongan_pangkat_assoc'] =$this->golongan_model->findAll();
		$data['unit_kerja_assoc'] = $this->unit_kerja_model->get_unit_assoc();
		$data['GD_assoc'] = $this->lookup_model->GD_assoc();
		
		$data['html']=$this->load->view('laporan/statistik/list_statistik_pdf', $data,true);
		$this->load->view('xls.php',$data);
	}
}

