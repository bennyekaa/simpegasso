<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class utp_detil extends Member_Controller
{
	function utp_detil()
	{
		parent::Member_Controller();
		$this->load->model('utp_detil_model', 'utp_detil');
		$this->load->model("utp_model");
		$this->load->model("lookup_model");
		$this->load->model("golongan_model");
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data Rincian UTP');
		$this->browse();
	}
	
		
	function browse() {
		$id_utp=$this->uri->segment(4);
		$data['option_utp'] = $this->utp_model->get_assoc();
		if(!$id_utp)
			redirect('setup/utp');
		$paging_uri=6;
		
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$limit_per_page = 15;
		
		$ordby = $this->uri->segment(5);
		$data['id_utp'] = $id_utp;
		$data['nama_utp'] = $this->utp_model->get_assoc2();
		$data['list_utp_detil'] = $this->utp_detil->findByFilter(array("id_utp"=>$id_utp),$limit_per_page,$start,$ordby);
		$data['golongan_pangkat_assoc'] = $this->golongan_model->get_assoc();
		$data['judul'] 		= "Data Rincian UTP";
		$config['base_url']     = site_url('setup/utp_detil/browse/'.$id_utp.'/'.$ordby.'/');
		$config['total_rows']   = $this->utp_detil->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['ordbynama'] 	= 'setup/utp_detil/browse/'.$id_utp.'/nama_detil';
		$data['ordbyno_urut'] 	= 'setup/utp_detil/browse/'.$id_utp.'/id_detil';
		$this->template->display('setup/utp_detil/list_utp_detil', $data);
	}

	
	function add($id_utp)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->utp_detil->add($data);
			set_success('Data Rincian UTP berhasil disimpan.');
			redirect('/setup/utp_detil/browse/'.$data['id_utp']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Rincian UTP :: Tambah');
			$data = $this->_clear_form();
			$data['id_utp'] = $id_utp;
			$data['nama_utp'] = $this->utp_model->get_assoc2();
			$data['action']='add/'.$id_utp;
			$data['action_list'] = 'browse/'.$id_utp;
			$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
			$data['judul']='Tambah Rincian UTP';
			$data['id_detil']=$this->utp_detil->get_id();
			$this->template->display('/setup/utp_detil/detail_utp_detil', $data);
		}
	}
	
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_detil']=$id;
			$this->utp_detil->update($id, $data);
			set_success('Perubahan data Rincian UTP berhasil disimpan');
			redirect('/setup/utp_detil/browse/'.$data['id_utp']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Rincian UTP :: Ubah');
			$data = $this->utp_detil->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action_list'] = 'browse/'.$data['id_utp'];
				$data['id_utp'] = $data['id_utp'];
				$data['nama_utp'] = $this->utp_model->get_assoc2();
				$data['action'] = 'edit/'.$id;
				$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
				$data['judul']='Edit Rincian UTP';
				$this->template->display('/setup/utp_detil/detail_utp_detil', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/utp_detil/browse/'.$data['id_utp']);
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->utp_detil->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Rincian UTP :: Hapus');
		confirm("Yakin menghapus data kelompok <b>".$data['nama_detil']."</b> ?");
		$res = $this->utp_detil->delete($idField);
		set_success('Data Rincian UTP berhasil dihapus');
		redirect('/setup/utp_detil/browse/'.$data['id_utp']);
	}

	function _clear_form()
	{
		$data['id_detil']	= '';
		$data['nama_detil']	= '';
		$data['id_utp']	= '';
//		$data['id_golpangkat_minimal']	= '';
//		$data['batas_maks_pensiun']		= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_detil']	= $this->input->post('id_detil', TRUE);
	   	$data['nama_detil']		= $this->input->post('nama_detil', TRUE);
		$data['id_utp']		= $this->input->post('id_utp', TRUE);
//		$data['id_golpangkat_minimal']		= $this->input->post('id_golpangkat_minimal', TRUE);
//		$data['batas_maks_pensiun']		= $this->input->post('batas_maks_pensiun', TRUE); 
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_detil', 'nama_detil', 'required');
		$this->form_validation->set_rules('id_utp', 'id_utp', 'required');
		return $this->form_validation->run();
	}
}