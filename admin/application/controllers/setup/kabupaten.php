<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class kabupaten extends Member_Controller
{
	function kabupaten()
	{
		parent::Member_Controller();
		$this->load->model('kabupaten_model', 'kabupaten');
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data Kabupaten');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 15;
		$ordby = 'kd_kabupaten';
		
		$data['list_kabupaten'] = $this->kabupaten->findAll($limit_per_page,$start,$ordby);
		$data['kd_kabupaten'] = $data['kd_kabupaten'];
		
		$config['base_url']     = site_url('setup/kabupaten/browse/');
		$config['total_rows']   = $this->kabupaten->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();	    
		$data['judul'] 		= "Data Kabupaten";
		//show_error(var_dump($data));
		$this->template->display('setup/kabupaten/list_kabupaten', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->kabupaten->add($data);
			set_success('Data Kabupaten berhasil disimpan.');
			redirect('/setup/kabupaten');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Kabupaten :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Kabupaten';
			$data['kd_kabupaten']=$this->kabupaten->get_id();
			$this->template->display('/setup/kabupaten/detail_kabupaten', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['kd_kabupaten']=$id;
			$this->kabupaten->update($id, $data);
			set_success('Perubahan data Kabupaten berhasil disimpan');
			redirect('/setup/kabupaten', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Kabupaten :: Ubah');
			$data = $this->kabupaten->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Kabupaten';
				$this->template->display('/setup/kabupaten/detail_kabupaten', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/kabupaten', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->kabupaten->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Kabupaten :: Hapus');
		confirm("Yakin menghapus data kelompok <b>".$data['kabupaten']."</b> ?");
		$res = $this->kabupaten->delete($idField);
		set_success('Data Kabupaten berhasil dihapus');
		redirect('/setup/kabupaten', 'location');
	}

	function _clear_form()
	{
		$data['kd_kabupaten']	= '';
		$data['nama_kabupaten']	= '';
		$data['nama_propinsi']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['kd_kabupaten']	= $this->input->post('kd_kabupaten', TRUE);
	   	$data['nama_kabupaten']		= $this->input->post('nama_kabupaten', TRUE);
		$data['nama_propinsi']		= $this->input->post('nama_propinsi', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_kabupaten', 'nama_kabupaten', 'required');
		$this->form_validation->set_rules('nama_propinsi', 'nama_propinsi', 'required');
		return $this->form_validation->run();
	}
}