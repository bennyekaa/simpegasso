<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gaji extends Member_Controller

{
	function Gaji()
	{
		parent::Member_Controller();
		$this->load->model('gaji_model', 'gaji');
		$this->load->model('golongan_model', 'golongan');
		$this->load->model('lookup_model', 'lookup');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data Gaji Pokok');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
			$limit_per_page = 16;
		
		if ($this->input->post('group'))
			$group= $this->input->post('group');
			$ordby = 'id_gaji';
		
        if ($this->input->post('tahun'))
			$tahun= $this->input->post('tahun');
		else 
			$tahun=date('Y');
			
		
		if($group){
			$start=0 ; 
			$param = "acuan_gaji_pokok.tahun = '".$tahun."' and acuan_gaji_pokok.id_golpangkat = '".$group."'";
			//$data['list_gaji'] = $this->gaji->findByFilter($group);
			}
		else	
			$param = "acuan_gaji_pokok.tahun = '".$tahun."'";
			//$data['list_gaji'] = $this->gaji->findAll($limit_per_page,$start,$ordby);
		
		$data['list_gaji'] = $this->gaji->findByFilter($param);
		
		$data['tahun_assoc'] = $this->lookup->tahun_assoc(date('Y')-5, date('Y'));
			
		$data['group'] = $group;
		$data['tahun'] = $tahun;
		$data['golongan_assoc'] = array('' => '-- Semua Golongan --') + $this->golongan->get_assoc();
		$data['golongan_pangkat_assoc1'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>1),Null,0,'golongan_pangkat.id_golpangkat asc');
		$data['golongan_pangkat_assoc2'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>2),Null,0,'golongan_pangkat.id_golpangkat asc');
		$data['golongan_pangkat_assoc3'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>3),Null,0,'golongan_pangkat.id_golpangkat asc');
		$data['golongan_pangkat_assoc4'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>4),Null,0,'golongan_pangkat.id_golpangkat asc');
		
		//$data['MKG_assoc1'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>1),Null,0,'golongan_pangkat.masa_kerja');
		
		$config['base_url']     = site_url('setup/gaji/browse/'.$group);
		$config['total_rows']   = $this->gaji->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();		    
		$data['judul'] 		= "Data Gaji Pokok";
		if($group){
		$this->template->display('setup/gaji/list_gaji', $data);}
		else{
		$this->template->display('setup/gaji/listAll', $data);}
	}
	
	
	function gajitopdf($tahun){
		$properties=array();
		$properties['title']='PERATURAN PEMERINTAH REPUBLIK INDONESIA NOMOR 25 TAHUN 2010';
		$properties['subject']='acuan_gaji_pokok';
		$properties['keywords']='acuan_gaji_pokok';
		$properties['filename']='acuan_gaji_pokok'; //.$this->sims->tahun
		$properties['paperlayout']='L';
		$properties['papersize']="A4";
		
		$this->load->model('gaji_model', 'gaji');
		$this->load->model('golongan_model', 'golongan');
		
		$start=0 ; 
		$limit_per_page = 1000;
		$ordby = 'id_gaji'; 
		//$data['list_gaji'] = $this->gaji->findAll($limit_per_page,$start,$ordby);
		
		$data['tahun'] = $tahun;
		$data['golongan_assoc'] = array('' => '-- Semua Golongan --') + $this->golongan->get_assoc();
		$data['golongan_pangkat_assoc1'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>1),Null,0,'golongan_pangkat.id_golpangkat asc');
		$data['golongan_pangkat_assoc2'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>2),Null,0,'golongan_pangkat.id_golpangkat asc');
		$data['golongan_pangkat_assoc3'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>3),Null,0,'golongan_pangkat.id_golpangkat asc');
		$data['golongan_pangkat_assoc4'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>4),Null,0,'golongan_pangkat.id_golpangkat asc');

		$data['judul'] 				= "PERATURAN PEMERINTAH REPUBLIK INDONESIA NOMOR 25 TAHUN 2010";
		$html=$this->load->view('setup/gaji/listAll_pdf', $data,true);
		$this->printtopdf->htmltopdf($properties,$html);
	}
	
	function printtoxls($tahun){
		$this->load->model('gaji_model', 'gaji');
		$this->load->model('golongan_model', 'golongan');
		
		$start=0 ; 
		$limit_per_page = 1000;
		$ordby = 'id_gaji'; 
		//$data['list_gaji'] = $this->gaji->findAll($limit_per_page,$start,$ordby);
		
		$data['tahun'] = $tahun;
		$data['golongan_assoc'] = array('' => '-- Semua Golongan --') + $this->golongan->get_assoc();
		$data['golongan_pangkat_assoc1'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>1),Null,0,'golongan_pangkat.id_golpangkat asc');
		$data['golongan_pangkat_assoc2'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>2),Null,0,'golongan_pangkat.id_golpangkat asc');
		$data['golongan_pangkat_assoc3'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>3),Null,0,'golongan_pangkat.id_golpangkat asc');
		$data['golongan_pangkat_assoc4'] =$this->golongan->findByFilters(array('golongan_pangkat.tingkat'=>4),Null,0,'golongan_pangkat.id_golpangkat asc');
		
		$data['judul'] 				= "PERATURAN PEMERINTAH REPUBLIK INDONESIA NOMOR 25 TAHUN 2010";
		$data['html']=$this->load->view('setup/gaji/listAll_pdf', $data,true);
		$this->load->view('xls.php',$data);
	}
	
	function add()
	{
		$this->template->metas('title', 'SIMPEGA | Data Gaji Pokok :: Tambah');
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$cek = $this->gaji->cek_id($data['id_gaji']);
			if($cek==true)
			{
				$this->gaji->add($data);
				set_success('Data gaji berhasil disimpan.');
			}
			else
			{	
				set_error('Data gagal disimpan. Duplikasi data golongan.');
			}
			redirect('/setup/gaji/add');
			
		}
		else
		{
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Gaji';
			$data['id_gaji']=$this->gaji->get_id();
			$data['golongan_assoc']=array(0=>'-- Pilih Golongan --')+$this->golongan->get_assoc("golongan");
			$this->template->display('/setup/gaji/detail_gaji', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_gaji']=$id;
			$this->gaji->update($id, $data);
			set_success('Perubahan data gaji berhasil disimpan');
			redirect('/setup/gaji', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Gaji Pokok :: Ubah');
			$data = $this->gaji->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Gaji';
				$data['golongan_assoc']=array(0=>'-- Pilih Golongan --')+$this->golongan->get_assoc("golongan");
				$this->template->display('/setup/gaji/detail_gaji', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/gaji', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->gaji->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Gaji Pokok :: Hapus');
		confirm("Yakin menghapus data gaji <b>".$data['gaji_pokok']."</b> ?");
		$res = $this->gaji->delete($idField);
		set_success('Data Gaji berhasil dihapus');
		redirect('/setup/gaji', 'location');
	}

	function _clear_form()
	{
		$data['id_gaji']	= '';
		$data['id_golpangkat']='';
		$data['gaji_pokok']	= '';
		$data['masa_kerja']	= '';
		$data['tahun']	=date('Y');
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_gaji']	= $this->input->post('id_gaji', TRUE);
	   	$data['id_golpangkat']= $this->input->post('id_golpangkat', TRUE);
		$data['masa_kerja']		= $this->input->post('masa_kerja', TRUE);
	   	$data['gaji_pokok']		= $this->input->post('gaji_pokok', TRUE);
		$data['tahun']		= $this->input->post('tahun', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('id_golpangkat', 'FK golongan', 'required');
		$this->form_validation->set_rules('gaji_pokok', 'gaji_pokok', 'required');
		$this->form_validation->set_rules('tahun', 'tahun', 'required');
		return $this->form_validation->run();
	}
}
