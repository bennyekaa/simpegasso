<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class jabatan extends Member_Controller
{
	function jabatan()
	{
		parent::Member_Controller();
		$this->load->model('jabatan_model', 'jabatan');
		$this->load->model("jenis_jabatan_model");
		$this->load->model("lookup_model");
		$this->load->model("golongan_model");
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data Jabatan');
		$this->browse();
	}
	
		
	function browse() {
		$id_jenis_jabatan=$this->uri->segment(4);
		$data['option_jenis_jabatan'] = $this->jenis_jabatan_model->get_assoc();
		if(!$id_jenis_jabatan)
			redirect('setup/jenis_jabatan');
		$paging_uri=6;
		
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$limit_per_page = 15;
		
		$ordby = $this->uri->segment(5);
		$data['id_jenis_jabatan'] = $id_jenis_jabatan;
		$data['nama_jenis_jabatan'] = $this->jenis_jabatan_model->get_assoc2();
		$data['list_jabatan'] = $this->jabatan->findByFilter(array("id_jenis_jabatan"=>$id_jenis_jabatan),$limit_per_page,$start,$ordby);
		$data['golongan_pangkat_assoc'] = $this->golongan_model->get_assoc();
		$data['judul'] 		= "Data Jabatan";
		$config['base_url']     = site_url('setup/jabatan/browse/'.$id_jenis_jabatan.'/'.$ordby.'/');
		$config['total_rows']   = $this->jabatan->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['ordbynama'] 	= 'setup/jabatan/browse/'.$id_jenis_jabatan.'/nama_jabatan';
		$data['ordbyno_urut'] 	= 'setup/jabatan/browse/'.$id_jenis_jabatan.'/id_jabatan';
		$this->template->display('setup/jabatan/list_jabatan', $data);
	}

	
	function add($id_jenis_jabatan)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->jabatan->add($data);
			set_success('Data Jabatan berhasil disimpan.');
			redirect('/setup/jabatan/browse/'.$data['id_jenis_jabatan']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jabatan :: Tambah');
			$data = $this->_clear_form();
			$data['id_jenis_jabatan'] = $id_jenis_jabatan;
			$data['nama_jenis_jabatan'] = $this->jenis_jabatan_model->get_assoc2();
			$data['action']='add/'.$id_jenis_jabatan;
			$data['action_list'] = 'browse/'.$id_jenis_jabatan;
			$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
			$data['judul']='Tambah Jabatan';
			$data['id_jabatan']=$this->jabatan->get_id();
			$this->template->display('/setup/jabatan/detail_jabatan', $data);
		}
	}
	
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_jabatan']=$id;
			$this->jabatan->update($id, $data);
			set_success('Perubahan data Jabatan berhasil disimpan');
			redirect('/setup/jabatan/browse/'.$data['id_jenis_jabatan']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jabatan :: Ubah');
			$data = $this->jabatan->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action_list'] = 'browse/'.$data['id_jenis_jabatan'];
				$data['id_jenis_jabatan'] = $data['id_jenis_jabatan'];
				$data['nama_jenis_jabatan'] = $this->jenis_jabatan_model->get_assoc2();
				$data['action'] = 'edit/'.$id;
				$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
				$data['judul']='Edit Jabatan';
				$this->template->display('/setup/jabatan/detail_jabatan', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/jabatan/browse/'.$data['id_jenis_jabatan']);
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->jabatan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Jabatan :: Hapus');
		confirm("Yakin menghapus data kelompok <b>".$data['nama_jabatan']."</b> ?");
		$res = $this->jabatan->delete($idField);
		set_success('Data Jabatan berhasil dihapus');
		redirect('/setup/jabatan/browse/'.$data['id_jenis_jabatan']);
	}

	function _clear_form()
	{
		$data['id_jabatan']	= '';
		$data['nama_jabatan']	= '';
		$data['id_jenis_jabatan']	= '';
		$data['id_golpangkat_minimal']	= '';
		$data['batas_maks_pensiun']		= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_jabatan']	= $this->input->post('id_jabatan', TRUE);
	   	$data['nama_jabatan']		= $this->input->post('nama_jabatan', TRUE);
		$data['id_jenis_jabatan']		= $this->input->post('id_jenis_jabatan', TRUE);
		$data['id_golpangkat_minimal']		= $this->input->post('id_golpangkat_minimal', TRUE);
		$data['batas_maks_pensiun']		= $this->input->post('batas_maks_pensiun', TRUE); 
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_jabatan', 'nama_jabatan', 'required');
		$this->form_validation->set_rules('id_jenis_jabatan', 'id_jenis_jabatan', 'required');
		return $this->form_validation->run();
	}
}