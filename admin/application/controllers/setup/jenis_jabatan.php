<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class jenis_jabatan extends Member_Controller
{
	function jenis_jabatan()
	{
		parent::Member_Controller();
		$this->load->model('jenis_jabatan_model', 'jenis_jabatan');
		$this->load->model("jenis_pegawai_model");
		$this->load->model("lookup_model");
		$this->load->model("golongan_model");
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data Jenis Jabatan');
		$this->browse();
	}
	
		
	function browse() {
		$id_jns_pegawai=$this->uri->segment(4);
		$data['option_jenis_pegawai'] = $this->jenis_pegawai_model->get_assoc();
		if(!$id_jns_pegawai)
			redirect('setup/jenis_pegawai');
		$paging_uri=6;
		
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$limit_per_page = 15;
		
		$ordby = $this->uri->segment(5);
		$data['id_jns_pegawai'] = $id_jns_pegawai;
		$data['nama_jenis_pegawai'] = $this->jenis_pegawai_model->get_assoc2();
		$data['list_jenis_jabatan'] = $this->jenis_jabatan->findByFilter(array("id_jns_pegawai"=>$id_jns_pegawai),$limit_per_page,$start,$ordby);
		$data['golongan_pangkat_assoc'] = $this->golongan_model->get_assoc();
		$data['judul'] 		= "Data Jenis Jabatan";
		$config['base_url']     = site_url('setup/jenis_jabatan/browse/'.$id_jns_pegawai.'/'.$ordby.'/');
		$config['total_rows']   = $this->jenis_jabatan->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['ordbynama'] 	= 'setup/jenis_jabatan/browse/'.$id_jns_pegawai.'/nama_jenis_jabatan';
		$data['ordbyno_urut'] 	= 'setup/jenis_jabatan/browse/'.$id_jns_pegawai.'/id_jenis_jabatan';
		$this->template->display('setup/jenis_jabatan/list_jenis_jabatan', $data);
	}

	
	function add($id_jns_pegawai)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->jenis_jabatan->add($data);
			set_success('Data Jenis Jabatan berhasil disimpan.');
			redirect('/setup/jenis_jabatan/browse/'.$data['id_jns_pegawai']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jenis Jabatan :: Tambah');
			$data = $this->_clear_form();
			$data['id_jns_pegawai'] = $id_jns_pegawai;
			$data['nama_jenis_pegawai'] = $this->jenis_pegawai_model->get_assoc2();
			$data['action']='add/'.$id_jns_pegawai;
			$data['action_list'] = 'browse/'.$id_jns_pegawai;
			$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
			$data['judul']='Tambah Jenis Jabatan';
			$data['id_jenis_jabatan']=$this->jenis_jabatan->get_id();
			$this->template->display('/setup/jenis_jabatan/detail_jenis_jabatan', $data);
		}
	}
	
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_jenis_jabatan']=$id;
			$this->jenis_jabatan->update($id, $data);
			set_success('Perubahan data Jenis Jabatan berhasil disimpan');
			redirect('/setup/jenis_jabatan/browse/'.$data['id_jns_pegawai']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jenis Jabatan :: Ubah');
			$data = $this->jenis_jabatan->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action_list'] = 'browse/'.$data['id_jns_pegawai'];
				$data['id_jns_pegawai'] = $data['id_jns_pegawai'];
				$data['nama_jenis_pegawai'] = $this->jenis_pegawai_model->get_assoc2();
				$data['action'] = 'edit/'.$id;
				$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
				$data['judul']='Edit Jenis Jabatan';
				$this->template->display('/setup/jenis_jabatan/detail_jenis_jabatan', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/jenis_jabatan/browse/'.$data['id_jns_pegawai']);
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->jenis_jabatan->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Jenis Jabatan :: Hapus');
		confirm("Yakin menghapus data kelompok <b>".$data['nama_jenis_jabatan']."</b> ?");
		$res = $this->jenis_jabatan->delete($idField);
		set_success('Data Jenis Jabatan berhasil dihapus');
		redirect('/setup/jenis_jabatan/browse/'.$data['id_jns_pegawai']);
	}

	function _clear_form()
	{
		$data['id_jenis_jabatan']	= '';
		$data['nama_jenis_jabatan']	= '';
		$data['id_jns_pegawai']	= '';
		$data['keterangan']		= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_jenis_jabatan']	= $this->input->post('id_jenis_jabatan', TRUE);
	   	$data['nama_jenis_jabatan']		= $this->input->post('nama_jenis_jabatan', TRUE);
		$data['id_jns_pegawai']		= $this->input->post('id_jns_pegawai', TRUE);
		$data['keterangan']		= $this->input->post('keterangan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_jenis_jabatan', 'nama_jenis_jabatan', 'required');
		$this->form_validation->set_rules('id_jns_pegawai', 'id_jns_pegawai', 'required');
		return $this->form_validation->run();
	}
}