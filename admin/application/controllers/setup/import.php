<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Import extends Member_Controller
{

	function Import()
	{
		parent::Member_Controller();
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->model('pegawai_model', 'pegawai');
		$this->load->model('users_model', 'users');
		$this->load->model('kerja_model', 'kerja');
		$this->load->model('user_pegawai_model', 'user_pegawai');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Import Data Pegawai');
		$data['judul']= 'Import Data Pegawai Dari Excel';
		$this->template->display('setup/import/list', $data);
	}
	
	function do_import()
	{
		$this->load->plugin('Excel');
		$config['upload_path'] = './public/tmp/';
		$config['allowed_types'] = 'xlsx|xls|doc';
		$config['max_size'] = '20000000000000';
		$config['max_width']  = '2000000000000';
		$config['max_height']  = '200000000000';
		
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload())
		{
			set_error('Import error, File Excel tidak valid!!');
			redirect('import/index/');
		}   
		else
		{
			
			//show_error('**');
			$datafile = array('upload_data' => $this->upload->data());
			
			$data = new Spreadsheet_Excel_Reader();
			$data->setOutputEncoding('CP1251');
			
			$config =$this->config->config;

			$file = $config['base_url']."index.php/../public/tmp/".$datafile['upload_data']['file_name'];
			
			$data->read($datafile['upload_data']['full_path']);
			error_reporting(E_ALL ^ E_NOTICE);

			for ($i = 9; $i <= $data->sheets[0]['numRows']; $i++) {
				if ($data->sheets[0]['cells'][$i][2]) 
				{
					//$cek = $this->pegawai->retrieve_by_pkey($data->sheets[0]['cells'][$i][2]);
					
					//if (!$cek) {
						for ($j = 1; $j <= 12; $j++) { //$data->sheets[0]['numCols']; $j++) {
							if ($data->sheets[0]['cells'][5][$j] == '')
								continue;
							$field[$data->sheets[0]['cells'][5][$j]] = $data->sheets[0]['cells'][$i][$j];
						}
						for ($j = 24; $j <= 27; $j++) { //$data->sheets[0]['numCols']; $j++) {
							if ($data->sheets[0]['cells'][5][$j] == '')
								continue;
							$field[$data->sheets[0]['cells'][5][$j]] = $data->sheets[0]['cells'][$i][$j];
						}
						//show_error(var_dump($field));
						$auto=$this->pegawai->add($field);
						
						for ($j = 13; $j <= 23; $j++) {
							if ($data->sheets[0]['cells'][5][$j] == '')
								continue;
							$field1[$data->sheets[0]['cells'][5][$j]] = $data->sheets[0]['cells'][$i][$j];
						}
						$field1['FK_pegawai'] = $auto;
						//show_error(var_dump($field+$field1));
						$this->kerja->add($field1);

						$id_user = $this->users->get_id();
						$userpeg = array('user_id'=>$id_user,'kd_pegawai'=>$auto);
						$this->user_pegawai->add($userpeg);
						
						$userdata = array('user_id' => $id_user,
								'FK_group_id' =>'3',
								'join_date' => date('Y-m-d H:i:s'),
								'name' => $field['nama'],
								'username' => 'user-'.$auto,
								'password' => '123456',
								'email' => ''
								);
						$this->users->add($userdata);

					//}
				}
				else break;
				
			}
			//show_error($datafile['upload_data']['file_name']);
			unlink($datafile['upload_data']['full_path']);
			
			set_success('Impor Data Pegawai Berhasil');
			redirect('/pegawai/pegawai/index', 'location');
		}
	}
}