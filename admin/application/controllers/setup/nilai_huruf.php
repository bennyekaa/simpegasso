<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class nilai_huruf extends Member_Controller
{
	function nilai_huruf()
	{
		parent::Member_Controller();
		$this->load->model('nilai_huruf_model', 'nilai_huruf');
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data Nilai Huruf');
		$this->browse();
	}
	
		
	function browse() {
		
		$paging_uri=6;
		
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$limit_per_page = 15;
		$ordby = $this->uri->segment(5);
		$data['list_nilai_huruf'] = $this->nilai_huruf->findAll($limit_per_page,$start,$ordby);
		$data['judul'] 		= "Data Nilai Huruf";
		$config['base_url']     = site_url('setup/nilai_huruf/'.$nilai_awal.'/'.$ordby.'/');
		$config['total_rows']   = $this->nilai_huruf->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['ordbynama'] 	= 'setup/nilai_huruf/'.$nilai_awal.'/nama_nilai_huruf';
		$data['ordbyno_urut'] 	= 'setup/nilai_huruf/'.$nilai_awal.'/id_nilai_huruf';
		$this->template->display('setup/nilai_huruf/list_nilai_huruf', $data);
	}

	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->nilai_huruf->add($data);
			set_success('Data Nilai Huruf berhasil disimpan.');
			redirect('/setup/nilai_huruf');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Nilai Huruf :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Nilai Huruf';
			$data['id_nilai_huruf']=$this->nilai_huruf->get_id();
			$this->template->display('/setup/nilai_huruf/detail_nilai_huruf', $data);
		}
	}
	
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_nilai_huruf']=$id;
			$this->nilai_huruf->update($id, $data);
			set_success('Perubahan data Nilai Huruf berhasil disimpan');
			redirect('/setup/nilai_huruf');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Nilai Huruf :: Ubah');
			$data = $this->nilai_huruf->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Nilai Huruf';
				$this->template->display('/setup/nilai_huruf/detail_nilai_huruf', $data);
			}
			
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/nilai_huruf', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->nilai_huruf->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Nilai Huruf :: Hapus');
		confirm("Yakin menghapus data kelompok <b>".$data['nama_nilai_huruf']."</b> ?");
		$res = $this->nilai_huruf->delete($idField);
		set_success('Data Nilai Huruf berhasil dihapus');
		redirect('/setup/nilai_huruf', 'location');
	}

	function _clear_form()
	{
		$data['id_nilai_huruf']	= '';
		$data['nilai_awal']		= '';
		$data['nilai_akhir']	= '';
		$data['keterangan']		= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_nilai_huruf']	= $this->input->post('id_nilai_huruf', TRUE);
		$data['nilai_awal']		= $this->input->post('nilai_awal', TRUE);
		$data['nilai_akhir']		= $this->input->post('nilai_akhir', TRUE);
		$data['keterangan']		= $this->input->post('keterangan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nilai_akhir', 'nilai_akhir', 'required');
		$this->form_validation->set_rules('nilai_awal', 'nilai_awal', 'required');
		return $this->form_validation->run();
	}
}