<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Agama extends Member_Controller
{
	function Agama()
	{
		parent::Member_Controller();
		$this->load->model('agama_model', 'agama');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data Agama');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'kd_agama';
		
		$data['list_agama'] = $this->agama->findAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('setup/agama/browse/');
		$config['total_rows']   = $this->agama->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();	
		$data['judul'] 		= "Data Agama";
		$this->template->display('setup/agama/list_agama', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->agama->add($data);
			set_success('Data agama berhasil disimpan.');
			redirect('/setup/agama');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Agama :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Agama';
			$data['kd_agama']=$this->agama->get_id();
			$this->template->display('/setup/agama/detail_agama', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['kd_agama'] = $id;
			$this->agama->update($id, $data);
			set_success('Perubahan data agama berhasil disimpan');
			redirect('/setup/agama', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Agama :: Ubah');
			$data = $this->agama->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Agama';
				$this->template->display('/setup/agama/detail_agama', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/agama', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->agama->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Agama :: Hapus');
		confirm("Yakin menghapus data agama <b>".$data['agama']."</b> ?");
		$res = $this->agama->delete($idField);
		set_success('Data agama berhasil dihapus');
		redirect('/setup/agama', 'location');
	}

	function _clear_form()
	{
		$data['kd_agama']	= '';
		$data['agama']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['kd_agama']	= $this->input->post('kd_agama', TRUE);
	   	$data['agama']		= $this->input->post('agama', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('agama', 'agama', 'required');
		return $this->form_validation->run();
	}
}