<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class jabatan_detil extends Member_Controller
{
	function jabatan_detil()
	{
		parent::Member_Controller();
		$this->load->model('jabatan_detil_model', 'jabatan_detil');
		$this->load->model("jabatan_model");
		$this->load->model("lookup_model");
		$this->load->model("golongan_model");
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data Detil Jabatan');
		$this->browse();
	}
	
		
	function browse() {
		$id_jabatan=$this->uri->segment(4);
		$data['option_jabatan'] = $this->jabatan_model->get_assoc();
		if(!$id_jabatan)
			redirect('setup/jabatan_detil');
		$paging_uri=6;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ;
		$limit_per_page = 15;
		
		$ordby = $this->uri->segment(5);
		$data['list_jabatan_detil'] = $this->jabatan_detil->findByFilter(array("id_jabatan"=>$id_jabatan),$limit_per_page,$start,$ordby);
		$data['id_jabatan'] = $id_jabatan;
		$data['golongan_pangkat_assoc'] = $this->golongan_model->get_assoc();
		$data['id_jenis_jabatan_assoc'] = $this->jabatan_detil->get_assoc_id_jenis_jabatan();
		$data['nama_jabatan'] = $this->jabatan_model->get_assoc2();
		$data['judul'] 		= "Data Detil Jabatan";
		$config['base_url']     = site_url('setup/jabatan_detil/browse/'.$id_jabatan.'/'.$ordby.'/');
		$config['total_rows']   = $this->jabatan_detil->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();
		$data['ordbynama'] 	= 'setup/jabatan_detil/browse/'.$id_jabatan.'/nama_jabatan_detil';
		$data['ordbyno_urut'] 	= 'setup/jabatan_detil/browse/'.$id_jabatan.'/id_jabatan_detil';
		$this->template->display('setup/jabatan_detil/list_jabatan_detil', $data);
	}
	
/*	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->jabatan_detil->add($data);
			set_success('Data Detil Jabatan berhasil disimpan.');
			redirect('/setup/jabatan_detil');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Detil Jabatan :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Jabatan';
			$data['id_jabatan_detil']=$this->jabatan_detil->get_id();
			$this->template->display('/setup/jabatan_detil/detail_jabatan_detil', $data);
		}
	}
	*/
	
	function add($id_jabatan)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->jabatan_detil->add($data);
			set_success('Data Detil Jabatan berhasil disimpan.');
			redirect('/setup/jabatan_detil/browse/'.$data['id_jabatan']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Detil Jabatan :: Tambah');
			$data = $this->_clear_form();
			$data['id_jabatan'] = $id_jabatan;
			$data['nama_jabatan'] = $this->jabatan_model->get_assoc2();
			$data['action']='add/'.$id_jabatan;
			$data['action_list'] = 'browse/'.$id_jabatan;
			$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
			$data['judul']='Tambah Detil Jabatan';
			$data['id_jabatan_detil']=$this->jabatan_detil->get_id();
			$this->template->display('/setup/jabatan_detil/detail_jabatan_detil', $data);
		}
	}
	
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_jabatan_detil']=$id;
			$this->jabatan_detil->update($id, $data);
			set_success('Perubahan Data Detil Jabatan berhasil disimpan');
			redirect('/setup/jabatan_detil/browse/'.$data['id_jabatan']);
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Detil Jabatan :: Ubah');
			$data = $this->jabatan_detil->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action_list'] = 'browse/'.$data['id_jabatan'];
				$data['id_jabatan'] = $data['id_jabatan'];
				$data['nama_jabatan'] = $this->jabatan_model->get_assoc2();
				$data['action'] = 'edit/'.$id;
				$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
				$data['judul']='Edit Detil Jabatan';
				$this->template->display('/setup/jabatan_detil/detail_jabatan_detil', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/jabatan_detil/browse/'.$data['id_jabatan']);
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->jabatan_detil->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Detil Jabatan :: Hapus');
		confirm("Yakin menghapus data <b>".$data['nama_jabatan_detil']."</b> ?");
		$res = $this->jabatan_detil->delete($idField);
		set_success('Data Detil Jabatan berhasil dihapus');
		redirect('/setup/jabatan_detil/browse/'.$data['id_jabatan']);
	}

	function _clear_form()
	{
		$data['id_jabatan_detil']	= '';
		$data['nama_jabatan_detil']	= '';
		$data['id_jabatan']	= '';
		$data['id_golpangkat']	= '';
		$data['ak_minimal']		= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_jabatan_detil']	= $this->input->post('id_jabatan_detil', TRUE);
	   	$data['nama_jabatan_detil']		= $this->input->post('nama_jabatan_detil', TRUE);
		$data['id_jabatan']		= $this->input->post('id_jabatan', TRUE);
		$data['id_golpangkat']		= $this->input->post('id_golpangkat', TRUE);
		$data['ak_minimal']		= $this->input->post('ak_minimal', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_jabatan_detil', 'nama_jabatan_detil', 'required');
		$this->form_validation->set_rules('id_jabatan', 'id_jabatan', 'required');
		return $this->form_validation->run();
	}
}