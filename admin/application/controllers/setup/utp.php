<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class utp extends Member_Controller
{

	function utp()
	{
		parent::Member_Controller();
		$this->load->model('utp_model', 'utp');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data UTP');
		$this->browse();
	}
	
	function browse()
	{
		 $paging_uri = 4;
        if ($this->uri->segment($paging_uri))
            $start = $this->uri->segment($paging_uri);
        else
            $start=0;
			
		$limit_per_page = 15;
		$ordby = 'id_utp';
		$data['start'] = $start;
		$data['list_utp'] 	= $this->utp->findAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('setup/utp/browse/');
		$config['total_rows']   = $this->utp->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();   
		
		$data['judul'] 		= "Data UTP";
		//show_error(var_dump($data));
		$this->template->display('setup/utp/list_utp', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->utp->add($data);
			set_success('Data UTP Berhasil Disimpan.');
			redirect('/setup/utp');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data UTP :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah UTP';
			$data['id_utp']=$this->utp->get_id();
			$this->template->display('/setup/utp/detail_utp', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_utp'] = $id;
			
			$this->utp->update($id, $data);
			set_success('Perubahan data UTP berhasil disimpan');
			redirect('/setup/utp', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data UTP :: Ubah');
			$data = $this->utp->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit UTP';
				$this->template->display('/setup/utp/detail_utp', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/utp', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->utp->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data UTP :: Hapus');
		confirm("Yakin menghapus data UTP <b>".$data['nama_utp']."</b> ?");
		$res = $this->utp->delete($idField);
		set_success('Data UTP berhasil dihapus');
		redirect('/setup/utp', 'location');
	}

	function _clear_form()
	{
		$data['id_utp']	= '';
		$data['nama_utp']	= '';
		$data['keterangan'] 	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_utp']	= $this->input->post('id_utp', TRUE);
	   	$data['nama_utp']	= $this->input->post('nama_utp', TRUE);
		$data['keterangan']	= $this->input->post('keterangan', TRUE);
		
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('id_utp', 'required');
		return $this->form_validation->run();
	}
}