<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jenjang extends Member_Controller
{
	function Jenjang()
	{
		parent::Member_Controller();
		$this->load->model('jenjang_model', 'jenjang');
		$this->load->model("golongan_model");
	}
	
	function index()
	{
		$this->template->metas('title','SIMPEGA | Data Jenjang kenaikan pangkat');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 15;
		$ordby = 'id_jenjang_kenaikan';
		$data['golongan_pangkat_assoc'] = $this->golongan_model->get_assoc();
		$data['list_jenjang'] = $this->jenjang->findAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('setup/jenjang/browse/');
		$config['total_rows']   = $this->jenjang->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();	  
		$data['judul'] 		= "Data Jenjang kenaikan pangkat";
		$this->template->display('setup/jenjang/list_jenjang', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->jenjang->add($data);
			set_success('Data jenjang berhasil disimpan.');
			redirect('/setup/jenjang');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jenjang kenaikan pangkat :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['golongan_pangkat_assoc'] =$this->golongan_model->get_assoc();
			$data['judul']='Tambah Jenjang kenaikan pangkat';
			$data['id_jenjang_kenaikan']=$this->jenjang->get_id();
			$this->template->display('/setup/jenjang/detail_jenjang', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_jenjang_kenaikan']= $id;
			$this->jenjang->update($id, $data);
			set_success('Perubahan data jenjang berhasil disimpan');
			redirect('/setup/jenjang', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jenjang kenaikan pangkat :: Ubah');
			$data = $this->jenjang->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['golongan_pangkat_assoc'] =array(0=>"-- Pilih Golongan --") + $this->golongan_model->get_assoc();
				$data['judul']='Edit Jenjang kenaikan pangkat';
				$this->template->display('/setup/jenjang/detail_jenjang', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/jenjang', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->jenjang->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Jenjang kenaikan pangkat :: Hapus');
		confirm("Yakin menghapus data jenjang <b>".$data['nama_jenjang']."</b> ?");
		$res = $this->jenjang->delete($idField);
		set_success('Data tingkat jenjang berhasil dihapus');
		redirect('/setup/jenjang', 'location');
	}

	function _clear_form()
	{
		$data['id_jenjang_kenaikan']	= '';
		$data['id_golpangkat_awal']	= '';
		$data['id_golpangkat_akhir']	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_jenjang_kenaikan']	= $this->input->post('id_jenjang_kenaikan', TRUE);
		$data['id_golpangkat_awal']		= $this->input->post('id_golpangkat_awal', TRUE);
		$data['id_golpangkat_akhir']		= $this->input->post('id_golpangkat_akhir', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('id_golpangkat_awal', 'id_golpangkat_awal', 'required');
		$this->form_validation->set_rules('id_golpangkat_akhir', 'id_golpangkat_akhir', 'required');
		return $this->form_validation->run();
	}
}