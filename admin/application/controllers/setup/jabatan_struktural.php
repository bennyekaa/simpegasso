<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class jabatan_struktural extends Member_Controller
{

	function jabatan_struktural()
	{
		parent::Member_Controller();
		$this->load->model('jabatan_struktural_model', 'jabatan_struktural');
		$this->load->model('jenis_pegawai_model', 'jenis_pegawai');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data Jenis Jabatan Struktural');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 100;
		$ordby = 'eselon,nama_jabatan_s';
		
		$data['list_jabatan_struktural'] 	= $this->jabatan_struktural->findAll($limit_per_page,$start,$ordby);
		$data['jenis_pegawai_assoc'] = $this->jenis_pegawai->get_assoc2();
		$config['base_url']     = site_url('setup/jabatan_struktural/browse/');
		$config['total_rows']   = $this->jabatan_struktural->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();   
		$data['judul'] 		= "Data Jenis Jabatan Struktural";
		//show_error(var_dump($data));
		$this->template->display('setup/jabatan_struktural/list_jabatan_struktural', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->jabatan_struktural->add($data);
			set_success('Data Jenis Jabatan Struktural Berhasil Disimpan.');
			redirect('/setup/jabatan_struktural');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jenis Jabatan Struktural :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Jenis Jabatan Struktural';
			$data['id_jabatan_s']=$this->jabatan_struktural->get_id();
			$data['jenis_pegawai_assoc'] = array(0 => "-") +$this->jenis_pegawai->get_assoc2();
			$this->template->display('/setup/jabatan_struktural/detail_jabatan_struktural', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_jabatan_s'] = $id;
			
			$this->jabatan_struktural->update($id, $data);
			set_success('Perubahan data jenis jabatan struktural berhasil disimpan');
			redirect('/setup/jabatan_struktural', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jenis Jabatan Struktural :: Ubah');
			$data = $this->jabatan_struktural->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Jenis Jabatan Struktural';
				$data['jenis_pegawai_assoc'] = array(0 => "-") +$this->jenis_pegawai->get_assoc2();
				$this->template->display('/setup/jabatan_struktural/detail_jabatan_struktural', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/jabatan_struktural', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->jabatan_struktural->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Jenis Jabatan Struktural :: Hapus');
		confirm("Yakin menghapus data jabatan_struktural <b>".$data['nama_jabatan_s']."</b> ?");
		$res = $this->jabatan_struktural->delete($idField);
		set_success('Data jenis jabatan struktural berhasil dihapus');
		redirect('/setup/jabatan_struktural', 'location');
	}

	function _clear_form()
	{
		$data['id_jabatan_s']	= '';
		$data['id_jns_pegawai']	= '';
		$data['nama_jabatan_s'] 	= '';
		$data['eselon'] 	= '';
		$data['tunjangan'] 	= '';
		$data['batas_maks_pensiun'] 	= '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_jabatan_s']	= $this->jabatan_struktural->get_id();
	   	$data['id_jns_pegawai']	= $this->input->post('id_jns_pegawai', TRUE);
		$data['nama_jabatan_s']	= $this->input->post('nama_jabatan_s', TRUE);
		$data['eselon']	= $this->input->post('eselon', TRUE);
		$data['tunjangan']	= $this->input->post('tunjangan', TRUE);
		$data['batas_maks_pensiun'] 	= $this->input->post('batas_maks_pensiun', TRUE);;
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_jabatan_s', 'nama_jabatan_s', 'required');
		return $this->form_validation->run();
	}
}