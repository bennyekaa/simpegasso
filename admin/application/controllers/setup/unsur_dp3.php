<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class unsur_dp3 extends Member_Controller
{
	function unsur_dp3()
	{
		parent::Member_Controller();
		$this->load->model('unsur_dp3_model', 'unsur_dp3');
	}
	
	function index()
	{
		$this->template->metas('title', 'SIMPEGA | Data Jenis unsur_dp3');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 10;
		$ordby = 'urutan';
		
		$data['list_unsur_dp3'] 	= $this->unsur_dp3->findAll($limit_per_page,$start,$ordby);
		$config['base_url']     = site_url('setup/unsur_dp3/browse/');
		$config['total_rows']   = $this->unsur_dp3->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();   
		$data['judul'] 		= "Data Unsur DP3";
		//show_error(var_dump($data));
		$this->template->display('setup/unsur_dp3/list_unsur_dp3', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->unsur_dp3->add($data);
			set_success('Data Jenis Unsur DP3 Berhasil Disimpan.');
			redirect('/setup/unsur_dp3');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jenis Unsur DP3 :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Jenis Unsur DP3';
			$data['id_unsur_dp3
']=$this->unsur_dp3->get_id();
			$this->template->display('/setup/unsur_dp3/detail_unsur_dp3', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['id_unsur_dp3
'] = $id;
			$this->unsur_dp3->update($id, $data);
			set_success('Perubahan data jenis unsur DP3 berhasil disimpan');
			redirect('/setup/unsur_dp3', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jenis Unsur DP3 :: Ubah');
			$data = $this->unsur_dp3->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Jenis unsur_dp3';
				$this->template->display('/setup/unsur_dp3/detail_unsur_dp3', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/unsur_dp3', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->unsur_dp3->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Jenis Unsur DP3 :: Hapus');
		confirm("Yakin menghapus data jenis unsur_dp3 <b>".$data['unsur_dp3']."</b> ?");
		$res = $this->unsur_dp3->delete($idField);
		set_success('Data jenis Unsur DP3 berhasil dihapus');
		redirect('/setup/unsur_dp3', 'location');
	}

	function _clear_form()
	{
		$data['id_unsur_dp3']	= '';
		$data['nama_unsur_dp3']	= '';
		$data['urutan'] = '';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['id_unsur_dp3']		= $this->input->post('id_unsur_dp3
', TRUE);
	   	$data['nama_unsur_dp3']		= $this->input->post('nama_unsur_dp3', TRUE);
		$data['urutan']	= $this->input->post('urutan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_unsur_dp3', 'Jenis Unsur DP3', 'required');
		return $this->form_validation->run();
	}
}