<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class jenis_mutasi extends Member_Controller
{
	function jenis_mutasi()
	{
		parent::Member_Controller();
		$this->load->model('jenis_mutasi_model', 'jenis_mutasi');
	}
	
	function index() {
		$this->template->metas('title', 'SIMPEGA | Data Jenis Mutasi');
		$this->browse();
	}
	
	function browse()
	{
		$paging_uri=4;
		if ($this->uri->segment($paging_uri))
			$start=$this->uri->segment($paging_uri);
 		else
			$start=0 ; 
		$limit_per_page = 15;
		$ordby = 'jns_mutasi';
		
		$data['list_jenis_mutasi'] = $this->jenis_mutasi->findAll($limit_per_page,$start,$ordby);
		$data['jns_mutasi'] = $data['jns_mutasi'];
		
		$config['base_url']     = site_url('setup/jenis_mutasi/browse/');
		$config['total_rows']   = $this->jenis_mutasi->record_count;
		$config['per_page']     = $limit_per_page;
		$config['uri_segment'] 	= $paging_uri;
		$config['next_link'] 	= 'berikutnya &raquo;';
		$config['prev_link'] 	= '&laquo; sebelumnya ';
		$this->pagination->initialize($config);
		$data['page_links'] 	= $this->pagination->create_links();	    
		$data['judul'] 		= "Data Jenis Mutasi";
		//show_error(var_dump($data));
		$this->template->display('setup/jenis_mutasi/list_jenis_mutasi', $data);
	}
	
	function add()
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$this->jenis_mutasi->add($data);
			set_success('Data Jenis Mutasi berhasil disimpan.');
			redirect('/setup/jenis_mutasi');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jenis Mutasi :: Tambah');
			$data = $this->_clear_form();
			$data['action']='add';
			$data['judul']='Tambah Jenis Mutasi';
			$data['jns_mutasi']=$this->jenis_mutasi->get_id();
			$this->template->display('/setup/jenis_mutasi/detail_jenis_mutasi', $data);
		}
	}
	
	function edit($id)
	{
		if ($this->_validate())
		{
			$data = $this->_get_form_values();
			$data['jns_mutasi']=$id;
			$this->jenis_mutasi->update($id, $data);
			set_success('Perubahan data Jenis Mutasi berhasil disimpan');
			redirect('/setup/jenis_mutasi', 'location');
		}
		else
		{
			$this->template->metas('title', 'SIMPEGA | Data Jenis Mutasi :: Ubah');
			$data = $this->jenis_mutasi->retrieve_by_pkey($id);
			if ($data)
			{
				$data['action'] = 'edit/'.$id;
				$data['judul']='Edit Jenis Mutasi';
				$this->template->display('/setup/jenis_mutasi/detail_jenis_mutasi', $data);
			}
			else
			{
				set_error('Data tidak ditemukan');
				redirect('/setup/jenis_mutasi', 'location');
			}
		}
	}
		
	function delete()
	{
		$idField = $this->uri->segment(4);
		$data = $this->jenis_mutasi->retrieve_by_pkey($idField);
		
		$this->template->metas('title', 'SIMPEGA | Data Jenis Mutasi :: Hapus');
		confirm("Yakin menghapus data kelompok <b>".$data['jenis_mutasi']."</b> ?");
		$res = $this->jenis_mutasi->delete($idField);
		set_success('Data Jenis Mutasi berhasil dihapus');
		redirect('/setup/jenis_mutasi', 'location');
	}

	function _clear_form()
	{
		$data['jns_mutasi']	= '';
		$data['nama_mutasi']	= '';
		$data['keterangan']	= '-';
		return $data;
	}	
	
	function _get_form_values()
	{
	   	$data['jns_mutasi']	= $this->input->post('jns_mutasi', TRUE);
	   	$data['nama_mutasi']		= $this->input->post('nama_mutasi', TRUE);
		$data['keterangan']		= $this->input->post('keterangan', TRUE);
		return $data;
	}
	
	function _validate()
	{
		$this->form_validation->set_rules('nama_mutasi', 'nama_mutasi', 'required');
		$this->form_validation->set_rules('jns_mutasi', 'jns_mutasi', 'required');
		return $this->form_validation->run();
	}
}