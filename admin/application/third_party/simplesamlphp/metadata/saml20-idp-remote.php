<?php
/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote 
 */
$metadata['https://auth.um.ac.id/saml2/idp/metadata.php'] = array (
    'metadata-set' => 'saml20-idp-remote',
    'entityid' => 'https://auth.um.ac.id/saml2/idp/metadata.php',
    'SingleSignOnService' =>
        array (
            0 =>
                array (
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                    'Location' => 'https://auth.um.ac.id/saml2/idp/SSOService.php',
                ),
        ),
    'SingleLogoutService' =>
        array (
            0 =>
                array (
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                    'Location' => 'https://auth.um.ac.id/saml2/idp/SingleLogoutService.php',
                ),
        ),
    'certData' => 'MIIDxDCCAqygAwIBAgIJANx+r4u3ylNdMA0GCSqGSIb3DQEBBQUAMEoxCzAJBgNVBAYTAklOMQ4wDAYDVQQIEwVKQVRJTTEMMAoGA1UEBxMDTUxHMQ8wDQYDVQQKEwZVTS1ERVYxDDAKBgNVBAsTA3RpazAeFw0xODA0MDgxMzU1MTJaFw0yODA0MDcxMzU1MTJaMEoxCzAJBgNVBAYTAklOMQ4wDAYDVQQIEwVKQVRJTTEMMAoGA1UEBxMDTUxHMQ8wDQYDVQQKEwZVTS1ERVYxDDAKBgNVBAsTA3RpazCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALyGRQNEOHEmNCENKIPW/nxkWL3OaE0BSfTJ4HCLUKGS9TAMnqxs9IeY1S1jKE5nIIOdkmtZySitNJR/kjkMG0w3x8LtdYX8xIW4yk1ZLDIlr9TXrU10WeTitZosZIjwVpfsKPQQj5h9KyaCQL0iheVLaAuGBETriwMNdUNc1Yc0UD+bDHNCxRNxtBoUgFlItz37Q0NwoBGegSU6D/6F+rdEmMjeysmq2ymnTumP9riSJalovTWV9xfV6kiXbY7t0asFpgZtebKXKlYFNltx7VZLiZ4755huT2MOxHZHrVU0LLVY3BVt6Kad7biq9xy8xXZbCwUQ9Pt0YZ+oDqX8ojMCAwEAAaOBrDCBqTAdBgNVHQ4EFgQUW9RuzmNGveaHddck8zQs5+FaTkowegYDVR0jBHMwcYAUW9RuzmNGveaHddck8zQs5+FaTkqhTqRMMEoxCzAJBgNVBAYTAklOMQ4wDAYDVQQIEwVKQVRJTTEMMAoGA1UEBxMDTUxHMQ8wDQYDVQQKEwZVTS1ERVYxDDAKBgNVBAsTA3Rpa4IJANx+r4u3ylNdMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAAYfZ/eE96RF1iRxuvLfFL70tazJ+km8YFJQaKjLfg8dDjGRNmwuTVS1D5BUEqBl5bfFfmJSUeowV1NXWb7/jTU5KMj0vTG2a4fohewWR3eACL/aW0EcEdQPRYxrvokRJWa1d8gcIgH4fx4otNaOkVTjHYf0tUvRygVcBSxqnOLaCwuaIh7MA0ZSHl8xe+16qlQ7kFe1KJoWbUxbZy9wMCTP1tZeB+0qqSGeIBrCmOaGZvb/JLXTqaWQkYwrgGJ8oiXD9/6DXl3qXw0s1cgjikub34HkwQhGbWrHgjeDW/xvQeappHxBEsT+QYq4G9cGLKPRDwfOZojUI3uxs1Glqcw=',
    'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
);