<!-- Skrip untuk meng-upload gambar: -->
<?php
if ($_GET['act'] == 'upload') {
    
include_once('config.php');
$pegawai = $_GET['kodepegawai'];
$file_name = "photo_" . $pegawai . ".jpg";
$tes = basename($_FILES["fgambar"]["name"]);
$ukuran = $_FILES['fgambar']['size'];
$direktori = "admin/public/photo/";
$upload = $direktori . '/' . $tes;
$width_size = 480;
    move_uploaded_file($_FILES['fgambar']['tmp_name'], $upload);
    // menentukan nama image setelah dibuat
    $resize_image = $direktori . "photo_" . $pegawai . ".jpg";
    // mendapatkan ukuran width dan height dari image
    list($width, $height) = getimagesize($upload);
    // mendapatkan nilai pembagi supaya ukuran skala image yang dihasilkan sesuai dengan aslinya
    $k = $width / $width_size;
    // menentukan width yang baru
    $newwidth = $width / $k;
    // menentukan height yang baru
    $newheight = $height / $k;
    // fungsi untuk membuat image yang baru
    $thumb = imagecreatetruecolor($newwidth, $newheight);
    $source = imagecreatefromjpeg($upload);
    // men-resize image yang baru
    imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
    // menyimpan image yang baru
    imagejpeg($thumb, $resize_image);

    imagedestroy($thumb);
    imagedestroy($source);
    header('location:index.php?lp=' . encrypt_url('datadiri'). '&date='.date('ymdHis'));
// echo "Upload: " . $_FILES["fgambar"]["name"] . "<br>";
// echo "Type: " . $_FILES["fgambar"]["type"] . "<br>";
// echo "Size: " . ($_FILES["fgambar"]["size"] / 1024) . " kB<br>";
// echo "Stored in: " . $_FILES["fgambar"]["tmp_name"];


// $namafile = $_FILES['fgambar']['name'];
// $tipe = $_FILES['fgambar']['type'];
// $ukuran = $_FILES['fgambar']['size'];
// $error = $_FILES['fgambar']['error'];     

// //     echo $namafile.' - '.$type. ' - '.$ukuran;
// //     // bila ukuran file lebih besar dari 0 dan tidak ada error
// //     if($ukuran > 0 || $error == 0)
//     // {
//         if ( $tipe == "image/jpg" || $tipe == "image/jpeg" || $tipe == "image/pjpeg" || $tipe == "image/png" || $tipe == "image/gif") 
//         {
//             if ($ukuran <= 5000000) {
//                 $file_name = "photo_" . $pegawai . ".jpg";
//                 $tes = basename($_FILES["fgambar"]["name"]);
//                 $ukuran = $_FILES['fgambar']['size'];
//                 $direktori = "admin/public/photo/";
//                 $upload = $direktori.'/'.$tes;
//                 //Cek gambar
//                 //upload gambar
//                 $move = move_uploaded_file($_FILES['fgambar']['tmp_name'], $upload);
//                 //simpan data gambar
//                 if ($move) {
//                     // pesan bila gambar berhasil di upload	
//                     echo "  <br>File '$namafile' dengan ukuran $ukuran sudah terupload. ke $upload";
//                     // header('location:index.php?lp=' . encrypt_url('datadiri'));
//                 } else {
//                     echo "Terjadi kesalahan sewaktu mengupload file";
//                 }  
//             }else{
//                 echo '<p><b>Upload Gagal</b></p>';
//                 echo '<p>Ukuran file MAX 5MB.</p>';
//                 echo '<p><a href="index.php">ULANGI</p></p>';
//             }
//         }else{
//             echo '<p><b>Upload Gagal</b></p>';
//             echo '<p>Tipe file yang diperbolehkan jpg, jpeg, pjpeg, png atau gif.</p>';
//             echo '<p><a href="index.php">ULANGI</p></p>';
//         }
    
    }
?>